import React from "react";
import styled from "styled-components";
import { Container } from "../Elements/Container";


const StylePreloader = styled(Container)`

`;


const Preloader = () => {

    return(
        <>
            <StylePreloader>
                <div className="cssload-container">
                    <div className="cssload-speeding-wheel"></div>
                </div>
            </StylePreloader>
        </>
    );
};

export default Preloader;