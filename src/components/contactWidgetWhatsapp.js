
import React, { useState, useEffect } from "react";
import styled from "styled-components";


const CircleContact = styled.div`

    background-color: #91c508;
    opacity: ${(props) => !props.transparent ? 0 : 1};
    align-items: center;
    border: 0;
    border-radius: 50%;
    bottom: 10px;
    cursor: pointer;
    display: flex;
    font-size: 2em;
    width: 60px;
    height: 60px;
    justify-content: center;
    outline: none;
    padding: 10px;
    position: fixed;
    right: 14px;
    text-align: center;
    text-decoration: none;
    transition: all .7s;
    z-index: 2;

`;

const CentralIcon = styled.div`

    width: 100px;
    height: 100px;
    background-size: contain;
    background-repeat: no-repeat;
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
`;


export const ContactWidgetWhatsapp = (props) => {


    const [lastScrollPosition, setLastScrollPosition] = useState(0);
    const [transparentWidget, setTransparentWidget] = useState(false);

    const onScroll = () => {
        
        const currentScrollPosition = window.pageYOffset ||
                document.documentElement.scrollTop;

        if(currentScrollPosition < 0){
            return;
        }

        if(currentScrollPosition >= 160){
            setTransparentWidget(true);
        }else{
            setTransparentWidget(false);
        }
    }

    useEffect(() => {
        window.addEventListener("scroll",onScroll);
    });

    return(
        <>
            <CircleContact transparent={transparentWidget}>
                    <CentralIcon>
                        <a href="https://api.whatsapp.com/send?phone=+584242414841"
                            target="_blank"><img src="/img/whatsapp-2.png"></img></a>
                    </CentralIcon>                
                </CircleContact>
        </>
    );
};
