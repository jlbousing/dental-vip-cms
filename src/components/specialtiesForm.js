import React, {useState} from 'react'
import BackgroundImage from "gatsby-background-image";
import styled from "styled-components";
import { rhythm, scale } from "../utils/typography";
import { Container } from "../Elements/Container";
import ReactHtmlParser from "react-html-parser";
import * as yup from "yup";
import { navigate } from "gatsby";
import { useForm } from "react-hook-form";
import ReCAPTCHA from "react-google-recaptcha";
import textSpanish from "../img/espanol.svg";
import textEnglish from "../img/ingles.svg";

import Axios from "axios";


const StyledContent = styled(Container)`
  padding-left: calc(5vw - 15px);
  padding-right: calc(5vw - 15px);
  justify-content: space-around;
  color: white;
  padding-top: ${rhythm(4)};
  padding-bottom: 3rem;

  .warning-text {
    line-height: 20px;
     width: 90%;
     display: flex;
     justify-content: center;
     margin-top: 0.5rem;
     margin-bottom: 1.7rem;
  }

  .checkbox-container {
    margin: 1rem 0;
    display: flex;
    justify-content: center;

    @media(min-width: 1024px){
      justify-content: flex-end;
      margin-right: 1rem;
    }
  }

  .text-checkbox{
    display: block;
    color: #fff;
    font-family: 'Roboto';
    margin-left: 0.5rem;
    font-size: 14px;
    padding: 0 !important;

    a {
      color: #FFF;
      text-decoration: underline;
    }

    a:hover {
      color: #c2c2c2;
    }
  }

  .cbox1 {
    margin-top: 0 !important;
  }

  .cbox1:checked{
    background-color: green;
  }

  .row {
    @media (max-width: 768px){
      margin-top: -40px;
    }
  }

  .footer-warning {
    width: 100%;
    display: flex;
    justify-content: center;
    padding-bottom: 10px;
  }

  .warning-container{
    display: flex;
    justify-content: center;
    flex-direction: row;
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    @media (min-width: 768px){
      width: 50%;
    }
  }
  .warning-icon {
    width: 10%;
    margin-right: 0.8rem;

    @media (min-width: 768px){
      width: 7%;
    }
  }
  .warning-icon img{
    height: 60%;
    width: 70%;
    margin-left: 40px;

    @media (min-width: 768px){
      margin-top: -5px;
    }
  }

  margin: auto;
  .nmb {
    margin-bottom: 0 !important;
  }
  @media screen and (max-width: 768px) {
    flex-direction: column !important;
  }
  h1 {
    @media (max-width: 768px){
      margin-top: -30px;
    }
    padding-left: 15px;
    padding-right: 15px;
    margin-bottom: 2.7rem;
    &.big {
      font-size: ${rhythm(1.8)};
    }
    &.mt-1 {
      margin-top: ${rhythm(1)};
    }
  }

  h1,
  h2,
  h3 {
    text-align: center;
    font-weight: 300;
    font-size: 34px;
  }
  p {
    text-align: center;
    padding-left: 15px;
    padding-right: 15px;
    ${scale(-0.2)}
    margin-bottom: ${rhythm(0.5)};
    &.text-left {
      align-self: flex-start;
    }
  }
  .submit-group{
    width: fit-content;

    display: block;
    margin: 0 0 0 auto;
    @media screen and (max-width: 1024px) {
      display: block;
    margin: 0 auto;
    }
  }
 
  form {
    display: flex;

   
    width: 100%;
    @media screen and (max-width: 1024px) {
      flex-direction: column;
    }
    aside{
      flex-direction: column !important;
    }
    main{
      flex-direction: row !important;

        flex-flow: row wrap;
    }
    main,
    aside {
      display: flex;
      flex-direction: column;
      width: 100%;
      button {
        font-size: 24px;
        font-family: Bebas Neue Bold;
        background: #91c508;
        width: fit-content;
        color: white;
        outline: none !important;
        margin-top: 10px !important;
        margin: 0 auto;
        border: none !important;
        &:hover {
          background: #678c07;
        }
      }
      label{
        padding: 0;
        display: flex;
        color: red;
      }
      span{
        flex-direction:column;
        height: fit-content;
        max-height: fit-content;
        flex-basis: 100%;
        &.half {
      @media screen and (min-width: 1025px) {
        flex-basis: 50%;
      }
    }
      }
      button{
        margin: 17px;
    min-width: calc(100% - 35px);
      }
      span, .captcha {
        padding: 10px 15px !important;
        margin-top: 0;
        display: flex;

        input,
        textarea,
        select {
          display: flex;
          padding-left: 5px;
          background: rgba(51, 51, 51, 1);
          border: 1px solid #777676;
          padding: 10px 15px !important;
          font-size: 16px;
          width: 100%;
          color: white !important;
          font-family: Roboto, sans-serif;
          font-weight: 300 !important;
          &:focus {
            outline: 1px solid #91c508;
          }
          &.half {
            margin-right: 20px;
          }
        }
        select {
          background: rgba(51, 51, 51);
        }
        select {
          color: #999999 !important;
        }
      }

      @media (max-width: 359px){
        span, .captcha {
          padding: 10px 6px !important;
        }
      }
    }
  }
`;

export const SpecialtiesForm = (props) => {
   
  const [check,setCheck] = useState(false);

  console.log("props SpecialtiesForm ",props);

  const messages = {
    isRequired: () => {
      return props.language === "es"
        ? "* Este campo es requerido"
        : "* This field is required";
    },
    notDefault: () => {
      return props.language === "es"
        ? "* El valor de este campo no puede ser el valor por defecto"
        : "* The value of this field cannot be the default one";
    },
    validEmail: () => {
      return props.language === "es" ? "* Email no valido" : "* Invalid Email";
    },
    validPhone: () => {
      return props.language === "es"
        ? "* Telefono no valido, EJ: 00584121234567"
        : "* Invalid Phone, EX: 00584121234567";
    },
  };

  
    const schema = yup.object().shape({
      referredBy: yup
        .string()
        //.notOneOf(["default"], messages.notDefault())
        .oneOf(["default"],messages.notDefault())
        .required(messages.isRequired()),
      specialty: yup
        .string()
        //.notOneOf(["default"], messages.notDefault())
        .oneOf(["default"],messages.notDefault())
        .required(messages.isRequired()),
      subject: yup.string().required(messages.isRequired()),
      name: yup.string().required(messages.isRequired()),
      lastName: yup.string().required(messages.isRequired()),
      email: yup
        .string()
        .email(messages.validEmail())
        .required(messages.isRequired()),
      phone: yup
        .string()
        .notOneOf(["default"], messages.notDefault())
        .matches(
          /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
          messages.validPhone()
        )
        .required(messages.isRequired()),
      city: yup.string().required(messages.isRequired()),
      country: yup.string().required(messages.isRequired()),
      message: yup.string().required(messages.isRequired()),
    });
    const { register, handleSubmit, watch, errors } = useForm({schema});
    const [recaptcha, setRecaptcha] = useState(false);
    const [error, setError] = useState(false);


    const onSubmit = (data) => {

      console.log("probando ");

      if(typeof window)

      if (!recaptcha) {
        setError(true);
        return null;
      }

      let headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'X-Requested-Width': 'XMLHttpRequest'
      };

      console.log("probando 1 2 3 ");

          
      const body = { ...data, time: Date.now() };

      Axios.post("https://dentalvipcaracas.com:8000/contact", body, headers)
        .then((data) => {
          console.log(data);
          
          //navigate(nextPage);
          if(typeof window !== 'undefined' && props.language == 'es'){
          //nextPage = "/gracias-por-contactarnos/";
            window.location.href = "https://dentalvipcaracas.com/gracias-por-contactarnos/"
          }else{
            //nextPage = "/en/thank-you/";
            window.location.href = "https://dentalvipcaracas.com/en/thank-you"
          }
        })
        .catch((error) => {
          return error;
        }); 
    };
  
 
  return (
      
      <>
          <BackgroundImage
              critical={true}
              className="parallax"
              Tag="section"
              fluid={props.img ? props.img.childImageSharp.fluid : null}
          >
              <StyledContent flexDirection="column" color="none">
                  {ReactHtmlParser(props.title)}
                  <div className="footer-warning">
                    <div className="warning-container">
                      <p className="warning-text">
                        <img src={props.language === 'es' ? textSpanish : textEnglish}
                             width="300"
                             height="16.49"  />
                      </p>
                    </div>
                  </div>
                  <br></br>
                  <form
                      name="Specialties Form"
                      onSubmit={handleSubmit(onSubmit)}
                      className="row"
                  >
                      <main>
                          {props.data.fields.map((i, k) => {
                              return (
                                          <>
                                              {i.type === "select" && (
                                                  <span key={k}>
                                                      <select name={i.name} ref={register({required: true})}>
                                                          <option disabled selected value>
                                                              {i.placeholder}
                                                          </option>
                      
                                                          {i.options.items.map((opt, key) => {
                                                              return (
                                                                  <option key={key} value={opt.value}>
                                                                      {opt.value}
                                                                  </option>
                                                              );
                                                          })}
                                                      </select>
                                                      {errors[i.name] && (
                                                          <label>
                                                            {/*errors[i.name]*/}
                                                            {props.data.warning}
                                                          </label>
                                                      )}
                                                  </span>
                                              )}

                                              {i.type !== "textarea" && i.type !== "select" && (
                                                  <span className={i.name !== "subject" && "half"}>
                                                  {" "}
                                                      <input
                                                          type={i.type}
                                                          name={i.name}
                                                          placeholder={i.placeholder}
                                                          ref={register({required: true})}
                                                          required
                                                      />{" "}
                                                          {errors[i.name] && (
                                                              <label>
                                                                {/*errors[i.name].message */}
                                                                {props.data.warning}
                                                              </label>
                                                          )}
                                                  </span>
                                              )}
                                          </>
                              );
                          })}
                      </main>
                      <aside>
                          {props.data.fields.map((i, k) => {
                              return (
                                  i.type === "textarea" && (
                                      <span key={k}>
                                          <textarea
                                              placeholder={i.placeholder}
                                              rows="6"
                                              cols="40"
                                              name={i.name}
                                              ref={register({required: true})}
                                          ></textarea>
                                          
                                          {errors[i.name] && <label>{errors[i.name].message} {props.data.warning}</label>}
                                      </span>
                                  )
                              );
                          })}
                          
                          <div className="checkbox-container">
                            <label className='label-checkbox'>
                              <input type="checkbox" 
                                     class="cbox1" 
                                     value={check}  
                                     ref={register({required: true})} 
                                    onChange={(event) => setCheck(event.target.checked)}
                                    required /> 
                              {props.language == 'es' ? 
                                <span className='text-checkbox'>
                                    He leído y acepto la <a href='https://dentalvipcaracas.com/politica-de-privacidad/'>
                                                Política de Privacidad
                                              </a>
                                </span>   
                            : <span className='text-checkbox'>
                                  I have read and accept the <a href='https://dentalvipcaracas.com/en/privacy-policy/'>
                                                  Privacy Policy
                                               </a>
                          </span>}
                            
                            </label>
                          </div>
                          
                          <div className="submit-group">
                              <ReCAPTCHA
                                  className="captcha"
                                  theme={"Dark"}
                                  Badge="inline"
                                  /*size="compact"*/
                                  /*sitekey="6LfMI6YZAAAAAO1akvBG2ILmPfl8mEmZoHl4KWNS"*/
                                  sitekey="6Lfm1GYaAAAAAI2I-APy8sALURS-nKT89MhHOJDZ"
                                  onChange={() => {
                                  setRecaptcha(true);
                              }}
                              />
                              {error && !recaptcha && (
                                  <label style={{ padding: "0 15px" }}>
                                      {props.language === "es"
                                          ? "Debe confirmar que no es un robot"
                                          : "You need to confirm that you are not a robot"}
                                  </label>
                              )}

                              <button type="submit" onClick={handleSubmit(onSubmit)} >
                                  {props.language === "es" ? "ENVIAR" : "SEND"}
                              </button>
                          </div>
                      </aside>
                  </form>
              </StyledContent>
          </BackgroundImage>
      </>
  );
}
