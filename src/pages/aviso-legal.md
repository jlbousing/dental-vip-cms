---
templateKey: advise-page
language: es
title: Aviso legal
description: >
  una breve descripcion de seo
redirects: /en/legal-notice/
published: true
keywords:
  - default
---

# Aviso legal

## Datos identificativos

Usted está visitando la página web [https://www.dentalvipcaracas.com/](/) titularidad de DENTAL VIP, Especialidades Odontológicas s.c. _(en adelante, EL TITULAR)_, con domicilio social en Multicentro Empresarial del Este, Torre Miranda, Núcleo A, Piso 14, Oficina A-143, Chacao, Caracas, Venezuela. Inscrita ante el Registro Público del Municipio Chacao en fecha 14/09/2012, bajo el número 45, folio 366 y tomo 42 del protocolo del año 2012.

Puede contactar con EL TITULAR por cualquiera de los siguientes medios:  
Teléfonos: +58 _(212)_ 261.3732 / 261.3331 / 261.5251
Correo electrónico: [info@dentalvipcaracas.com](mailto:info@dentalvipcaracas.com)

## Hospedaje web

DigitalOcean, LLC es una startup proveedora de servicios de infraestructura de cloud computing que ofrece una plataforma enfocada en desarrolladores de software.  
Dirección: 101 Avenue of the Americas 10th Floor New York, NY 10013 United States.  
Teléfono: 1-347-903-7918  
Dirección de correo electrónico: [team@info.digitalocean.com](mailto:team@info.digitalocean.com)  
Página web: https://www.digitalocean.com/

## Usuarios

Las presentes condiciones _(en adelante, Aviso Legal)_ tienen por finalidad regular el uso de la página web que EL TITULAR pone a disposición del público general.

El acceso y/o uso de esta página web atribuye la condición de USUARIO, que acepta, desde dicho acceso y/o uso, las condiciones generales aquí reflejadas. Las citadas condiciones serán de aplicación universal y obligado cumplimiento.

## Uso del portal

[https://www.dentalvipcaracas.com/](/) expone multitud de informaciones, servicios, programas y datos _(en adelante, LOS CONTENIDOS)_ en internet, pertenecientes a EL TITULAR o a sus licenciantes; y a los que EL USUARIO puede tener acceso.

EL USUARIO asume la responsabilidad del uso del portal. Dicha responsabilidad se extiende al registro que fuese necesario para acceder a determinados servicios o contenidos. En dicho registro, EL USUARIO será responsable de aportar información veraz y lícita. Como consecuencia de este registro, al USUARIO se le podría proporcionar una contraseña de la que será responsable, comprometiéndose a hacer un uso diligente y confidencial de la misma.

EL USUARIO se compromete a hacer un uso adecuado de los contenidos y servicios que EL TITULAR ofrece a través de su portal, y con carácter enunciativo pero no limitativo, a no emplearlos para:

- Incurrir en actividades ilícitas, ilegales o contrarias a la buena fe y al orden público.
- Difundir contenidos o propaganda racista, xenófoba, pornográfica, de apología del terrorismo o atentatoria contra los derechos humanos.
- Provocar daños en los sistemas físicos y lógicos de DENTAL VIP, Especialidades Odontológicas s.c., de sus proveedores o de terceras personas, introducir o difundir en la red virus informáticos o cualesquiera otros sistemas físicos o lógicos que sean susceptibles de provocar los daños anteriormente mencionados.
- Intentar acceder y utilizar las cuentas de correo electrónico de otros usuarios, modificar o manipular sus mensajes.
- Utilizar el sitio web o las informaciones que él contiene con fines comerciales, políticos, publicitarios o de índole similar, y particularmente, el envío de correos electrónicos no solicitados.

EL TITULAR se reserva el derecho a retirar todos aquellos comentarios y aportaciones que vulneren el respeto a la dignidad de la persona, que sean discriminatorios, xenófobos, racistas, pornográficos, que atenten contra la juventud o la infancia, el orden o la seguridad pública o que, a su juicio; no resultaran adecuados para su publicación. En cualquier caso, EL TITULAR no será responsable de las opiniones vertidas por los usuarios a través de los foros, chats, redes sociales u otras herramientas de participación digital.

## Protección de datos

Todo lo relativo a la política de protección de datos se encuentra recogido en el documento de [Política de Privacidad.](/)

## Contenidos. Propiedad intelectual e industrial

EL TITULAR es propietario de todos los derechos de propiedad intelectual e industrial de su página web, así como de los elementos contenidos en la misma: imágenes, fotografías, sonido, audio, video, software o textos, marcas o logotipos, combinaciones de colores, estructura y diseño, selección de materiales usados, códigos, programas de informática necesarios para su funcionamiento, acceso y uso; entre otros.

Todos los Derechos Reservados Este sitio web está protegido por derechos de autor, marcas registradas, marcas de servicio, patentes y otros derechos de propiedad; además de los derivados de las leyes vigentes. El uso no autorizado de cualquier material con derechos de autor, marcas o cualquier otra propiedad intelectual sin el consentimiento expreso y por escrito del propietario, está terminantemente prohibido.

## Exclusión de garantías y responsabilidad

EL USUARIO reconoce que la utilización de la página web, de sus contenidos y servicios se desarrolla bajo su exclusiva responsabilidad. En concreto, y a título meramente enunciativo, EL TITULAR no asume ninguna responsabilidad en los siguientes ámbitos:

1.  La disponibilidad del funcionamiento de la página web, sus servicios, contenidos y su calidad o interoperabilidad.
2.  La finalidad para la que la página web sirva a los objetivos del USUARIO.
3.  La infracción de la legislación vigente por parte del USUARIO o terceros y, en concreto, de los derechos de propiedad intelectual e industrial que sean titularidad de otras personas o entidades.
4.  La existencia de códigos maliciosos o cualquier otro elemento informático dañino que pudiera dañar el sistema informático del USUARIO o de terceros. Corresponde al USUARIO, en todo caso, disponer de herramientas adecuadas para la detección y desinfección de estos elementos.
5.  El acceso fraudulento a los contenidos o servicios por terceros no autorizados, o, en su caso; la captura, eliminación, alteración, modificación o manipulación de los mensajes y comunicaciones de cualquier clase que dichos terceros pudieran realizar.
6.  La exactitud, veracidad, actualidad y utilidad de los contenidos y servicios ofrecidos; y la utilización posterior que de ellos haga EL USUARIO. EL TITULAR empleará todos los esfuerzos y medios razonables para facilitar una información actualizada y fehaciente.
7.  Los daños producidos a equipos informáticos durante el acceso a la página web y los daños producidos a los USUARIOS cuando tengan su origen en fallos o desconexiones en las redes de telecomunicaciones que interrumpan el servicio.
8.  Los daños o perjuicios que se deriven de circunstancias acaecidas por caso fortuito o de fuerza mayor.

En caso de que existan foros, en el uso de los mismos u otros espacios análogos, ha de tenerse en cuenta que los mensajes reflejarán únicamente la opinión del USUARIO que los remite, que es el único responsable. EL TITULAR no se hace responsable del contenido de los mensajes enviados por EL USUARIO.

## Modificación de este aviso legal y duración

EL TITULAR se reserva el derecho de efectuar, sin previo aviso, las modificaciones que considere oportunas en su portal; pudiendo cambiar, suprimir o añadir tantos contenidos y servicios como desee, así como la forma en la que éstos aparezcan representados o localizados en su portal.

La vigencia de las citadas condiciones irá en función de su exposición y estarán vigentes hasta que sean modificadas por otras debidamente publicadas.

## Enlaces

En el caso de que en [https://www.dentalvipcaracas.com/](/) se incluyesen enlaces o hipervínculos hacia otros sitios de internet, EL TITULAR no ejercerá ningún tipo de control sobre dichos sitios y contenidos. En ningún caso, EL TITULAR asumirá responsabilidad alguna por los contenidos de algún enlace perteneciente a un sitio web ajeno, ni garantizará la disponibilidad técnica, calidad, fiabilidad, exactitud, amplitud, veracidad, validez y constitucionalidad de cualquier materia o información contenida en ninguno de dichos hipervínculos u otros sitios de internet. Igualmente, la inclusión de estas conexiones externas no implicará ningún tipo de asociación, fusión o participación con las entidades conectadas.

## Derechos de exclusión

EL TITULAR se reserva el derecho a denegar o retirar el acceso al portal y/o a los servicios ofrecidos sin necesidad de advertencia previa, a instancia propia o de un tercero, a aquellos USUARIOS que incumplan el contenido de este Aviso Legal.

## Generalidades

EL TITULAR perseguirá el incumplimiento de las presentes condiciones, así como cualquier utilización indebida de su portal, ejerciendo todas las acciones civiles y penales que le puedan corresponder en derecho.

## Legislación aplicable y jurisdicción

La relación entre EL TITULAR y EL USUARIO se regirá por la normativa Venezolana vigente. Todas las disputas y reclamaciones derivadas de este Aviso Legal se resolverán por los juzgados y tribunales Venezolanos.

## Menores de edad

[https://www.dentalvipcaracas.com](/) dirige sus servicios a USUARIOS mayores de 18 años. Los menores de esta edad no están autorizados a utilizar nuestros servicios y no deberán, por tanto, enviarnos sus datos personales. Informamos que si se da tal circunstancia, DENTAL VIP, Especialidades Odontológicas s.c. no se hará responsable de las posibles consecuencias que pudieran derivarse del incumplimiento de la advertencia que en esta misma cláusula se establece.
