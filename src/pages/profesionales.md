---
templateKey: professionals-page
language: es
redirects: /en/professional-staff/
title: Profesionales
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/profesionales-banner.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 14%
  content:
    position: center
    body: >
      <h1 class="dark">Profesionales</h1>

# Parallax
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads//uploads/parallax-form-professionals.jpg

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title">¡Máxima Capacitación y Experiencia!</h1>
    <p>Un reconocido equipo de Odontólogos Especialistas con amplia formación académica de cuarto nivel, larga trayectoria asistencial y sólido liderazgo en la profesión; plenamente identificado con la excelencia y óptima calidad de servicio.</p>
staff:
  title: >
    <h1>Personal de Clínica, Laboratorio y Administración</h1>
  cards:
    - img: /uploads/personal-angelo-sansone-ruggero.jpg
      content: >
        <div class="info"><h5>TPD. Angelo Sansone Ruggero</h5>
        <p>Prótesis Fija y Cerámica Dental</p></div>
    - img: /uploads/personal-denis-diaz-alvarez.jpg
      content: >
        <div class="info">
          <h5>TPD. Denis Díaz Álvarez</h5>
          <p>Acrílicos y Dentaduras Removibles</p>
        </div>
    - img: /uploads/personal-vanesa-hernandez.jpg
      content: >
        <div class="info">
          <h5>TPD. Vanesa Hernández</h5>
          <p>Aparatos Funcionales y Placas Activas</p>
        </div>
    - img: /uploads/personal-aymara-guillen-portillo.jpg
      content: >
        <div class="info">
          <h5>Aymara Guillén Portillo</h5>
          <p>Recepción y Atención al Paciente</p>
        </div>
    - img: /uploads/personal-maria-betancourt-matos.jpg
      content: >
        <div class="info">
          <h5>María Betancourt Matos</h5>
          <p>Higienista Dental</p>
        </div>
    - img: /uploads/personal-gisela-garcia.jpg
      content: >
        <div class="info">
          <h5>Gisela García</h5>
          <p>Higienista Dental</p>
        </div>
    - img: /uploads/personal-paola-rivas.jpg
      content: >
        <div class="info">
          <h5>Paola Rivas</h5>
          <p>Asesoría y Gestión Financiera</p>
        </div>
    - img: /uploads/personal-esteban-garrido.jpg
      content: >
        <div class="info">
          <h5>Lic. Esteban Garrido</h5>
          <p>Administración y Contabilidad</p>
        </div>
    - img: /uploads/personal-maria-jose-tirado.jpg
      content: >
        <div class="info">
          <h5>María José Tirado</h5>
          <p>Coordinación Clínica y Social Media</p>
        </div>
# anex-links
professionals:
  display: true
  items:
    - img: /uploads/professionals-dr-castor-jose-garaban-povea.png
      content: >
        <h2 class="light">Dr. Castor José Garabán Povea</h2>
        <h3 class="bebas boxed">CIRUGÍA BUCAL - IMPLANTES DENTALES</h3>
        <ul>
        <li class="nl"><strong>Odontólogo</strong>&nbsp;<em>(Universidad Central de Venezuela, 1994)</em>.</li>
        <li class="nl"><strong>Título de Especialista en Cirugía Bucal</strong>&nbsp;<em>(Universidad Central de Venezuela, 2006)</em>.</li>
        <li class="nl">Curso de Postgrado en Implantología Oral y Prótesis sobre Implantes.</li>
        <li class="nl">Diplomado en Oseointegración y Regeneración Ósea Periimplantaria.</li>
        <li class="nl">Capacitación permanente en Técnicas Quirúrgicas Conservadoras y Microinvasivas.</li>
        <li class="nl">Experto Clínico en el manejo de múltiples sistemas de Implantología Oral Avanzada.</li>
        <li class="nl">Profesor Asistente de la Facultad de Odontología, U.C.V.</li>
        <li class="nl">Miembro del Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Miembro del Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Miembro de la Sociedad Venezolana de Cirugía Buco-Maxilofacial&nbsp;<em>(S.V.C.B.M.F.)</em>.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-castor-jose-garaban-povea-studies.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-filomena-montemurro-tafuri.png
      content: >
        <h2 class="light">Dra. Filomena Montemurro Tafuri</h2>
        <h3 class="bebas boxed">PRÓTESIS
        - ESTÉTICA DENTAL</h3>
        <ul>
        <li class="nl"><strong>Odontólogo</strong>&nbsp;<em>(Universidad Santa María, 2001)</em>.</li>
        <li class="nl"><strong>Especialista en Prostodoncia</strong>&nbsp;<em>(Collegio dei Docenti di Odontoiatria, Italia, 2003).</em></li>
        <li class="nl">Curso avanzado de Odontología Estética y Restauradora&nbsp;<em>(U.S.M., 2004).</em></li>
        <li class="nl">Master en diseño y confección de Prótesis sobre Implantes.</li>
        <li class="nl">Residencia en Prostodoncia clínica aplicada e Implantología.</li>
        <li class="nl">Múltiples seminarios de capacitación en Tecnologías CAD-CAM y Diseño de Sonrisa.</li>
        <li class="nl">Diplomado en Cerámica Dental, Oclusión y Odontología Operatoria.</li>
        <li class="nl">Práctica privada limitada al área de Estética Dental, Prótesis y Rehabilitación Oral.</li>
        <li class="nl">Miembro del Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Miembro del Colegio de Odontólogos Metropolitano.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-filomena-montemurro-tafuri-studies.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-javier-martinez-tellez.png
      content: >
        <h2 class="light">Dr. Javier Martínez Téllez</h2>
        <h3 class="bebas boxed">ODONTOLOGÍA
        GENERAL - PERIODONCIA</h3>
        <ul>
        <li class="nl"><strong>Odontólogo</strong>&nbsp;<em>(Universidad Central de Venezuela, 2000).</em></li>
        <li class="nl"><strong>Postgrado en Estomatología Integral del Adulto</strong>&nbsp;<em>(Universidad Santa María, 2004).</em></li>
        <li class="nl"><strong>Título de Especialista en Periodoncia</strong>&nbsp;<em>(Universidad Central de Venezuela, 2014).</em></li>
        <li class="nl">Aspirante al grado de DOCTOR EN ODONTOLOGÍA.</li>
        <li class="nl">Diplomado en Técnicas Avanzadas de Regeneración Tisular Guiada.</li>
        <li class="nl">Autor de diversos artículos en revistas nacionales e internacionales.</li>
        <li class="nl">Práctica enfocada a la prevención y tratamiento de la Patología Periodontal.</li>
        <li class="nl">Miembro del Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Miembro del Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Miembro de la Sociedad Venezolana de Periodontología.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-javier-martinez-tellez-studies.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-jose-miguel-gomez-diez.png
      content: >
        <h2 class="light">Dr. José Miguel Gómez Díez</h2>
        <h3 class="bebas boxed">ORTODONCIA
        - ORTOPEDIA DENTOFACIAL</h3>
        <ul>
        <li class="nl"><strong>Odontólogo</strong>&nbsp;<em>(Universidad Central de Venezuela, 1996).</em></li>
        <li class="nl"><strong>Maestría en Ortodoncia</strong>&nbsp;<em>(Universidad Autónoma de Tamaulipas, México, 2003).</em></li>
        <li class="nl">Fellowship Program in Clinical Orthodontics.</li>
        <li class="nl">Especialización en Técnica de Arco Recto&nbsp;<em>(Straight Wire System).</em></li>
        <li class="nl">Entrenamiento avanzado en Oclusión y Disfunción Craneomandibular.</li>
        <li class="nl">Numerosas estancias de formación académica en España, México y USA.</li>
        <li class="nl">Dedicación exclusiva a la Especialidad de Ortodoncia y Ortopedia Dentofacial.</li>
        <li class="nl">Miembro del Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Miembro del Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Miembro del Colegio de Odontólogos del Estado Miranda.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-jose-miguel-gomez-diez-studies.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dra-vianka-xaviera-torres.png
      content: >
        <h2 class="light">Dra. Vianka Xaviera Torres</h2>
        <h3 class="bebas boxed">ODONTOLOGÍA
        GENERAL - ENDODONCIA</h3>
        <ul>
        <li class="nl"><strong>Odontólogo</strong>&nbsp;<em>(Universidad Central de Venezuela, 2000).</em></li>
        <li class="nl"><strong>Postgrado en Endodoncia</strong>&nbsp;<em>(Universidad Autónoma de Tamaulipas, México, 2003)</em>.</li>
        <li class="nl">Diplomado en Farmacoterapia e Inmunofarmacología.</li>
        <li class="nl">Experto Clínico en el manejo de Lesiones Endoperiodontales y Endoprotésicas.</li>
        <li class="nl">Entrenamiento avanzado en el uso práctico de Sistemas Rotatorios de Níquel-Titanio.</li>
        <li class="nl">Adiestramiento certificado en Técnicas de Condensación y Obturación Termoplástica.</li>
        <li class="nl">Asistencia a más de 50 cursos teóricos y prácticos de la Especialidad.</li>
        <li class="nl">Miembro del Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Miembro del Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Miembro del Colegio de Odontólogos del Estado Miranda.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dra-vianka-xaviera-torres-studies.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: <h5>Por Qué elegirnos</h5>
      to: /la-clinica/por-que-elegirnos/
      img: /uploads/procedures-why-choose-us.png
    - title: <h5>Instalaciones</h5>
      to: /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: <h5>Tecnología</h5>
      to: /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
---
