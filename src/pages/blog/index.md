---
templateKey: blog
title: Blog
language: es
featuredimage: /img/hero-home.jpg
redirects: >
  /en/blog/
published: true
description: seo description
keywords:
  - default keyowrd
tags:
  - default tag

minititle1: ¡DISCULPE!
minititle2: NUESTRO NUEVO BLOG ESTÁ

bigtitle1: EN
bigtitle2: PROCESO

button: REGRESAR

background: /uploads/blog-background.jpg
---
