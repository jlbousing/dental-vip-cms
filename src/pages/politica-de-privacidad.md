---
templateKey: advise-page
language: es
description: >
  some seo
title: Política de Privacidad
redirects: /en/privacy-policy/
published: true
keywords:
  - default
---

# Política de Privacidad

## Responsable del tratamiento de los datos

- Su nombre es: DENTAL VIP, Especialidades Odontológicas s.c.

- Su denominación social es: DENTAL VIP, Especialidades Odontológicas s.c.

- Su RIF: J-40271686-9

- Su domicilio social es: Multicentro Empresarial del Este, Torre Miranda, Núcleo A, Piso 14, Oficina A-143, Chacao, Caracas, Venezuela.

- Su actividad social es: Consultas, Clínicas de Odontología y Tratamientos Dentales.

- Correo electrónico: [info@dentalvipcaracas.com](mailto:info@dentalvipcaracas.com)

- Finalidad del portal: Información, Divulgación y Promoción.

- Página web: [https://www.dentalvipcaracas.com/](/)

## Procedencia, finalidad y legitimidad

Cuando un USUARIO se conecta con esta página, por ejemplo, para enviar un formulario, está facilitando información de carácter personal de la que es responsable DENTAL VIP, Especialidades Odontológicas s.c. Esa información puede incluir datos personales como pueden ser la dirección IP, nombre, dirección física, dirección de correo electrónico y número de teléfono; entre otros.

Al facilitar dicha información, Usted como USUARIO da su consentimiento para que sea recopilada, utilizada, gestionada y almacenada; solo como se describe en el [Aviso Legal](/aviso-legal) y en la presente Política de Privacidad.

La base legal para el tratamiento de sus datos es el consentimiento. Para contactar, suscribirse o comprar; se requiere de su consentimiento con esta Política de Privacidad.

Las finalidades con las que se recogen y gestionan datos son:

<u>FORMULARIOS DE CONTACTO O DE SUSCRIPCIÓN A CONTENIDOS</u>

**Web y hosting:** el sitio web de DENTAL VIP, Especialidades Odontológicas s.c. cuenta con un cifrado SSL que permite el envío seguro de sus datos personales a través de formularios de contacto de tipo estándar.

**Datos recabados a través de la web:** los datos personales recogidos serán objeto de tratamiento automatizado e incorporados a los correspondientes ficheros de los que DENTAL VIP, Especialidades Odontológicas s.c. es titular.

Será recabada su dirección IP, que será usada para comprobar el origen del mensaje con objeto de ofrecerle recomendaciones legales adecuadas al mismo y para detectar posibles irregularidades, así como datos relativos a su ISP.

Asimismo, podrá facilitar sus datos a través de correo electrónico particular y otros medios de comunicación digital.

**Otros servicios:** ciertos servicios prestados a través del sitio web _(por ejemplo, la posibilidad de participar en un concurso o sorteo) pueden contener condiciones particulares con previsiones específicas en materia de protección de datos personales. Se hace indispensable su lectura y aceptación con carácter previo a la solicitud del servicio de que se trate._

**Finalidad y legitimación:** la finalidad del tratamiento de estos datos será únicamente la de prestarle la información o servicios que nos solicite.

<u>REDES SOCIALES</u>

**Presencia en redes:** DENTAL VIP, Especialidades Odontológicas s.c. cuenta con perfil en algunas de las principales redes sociales de internet, reconociéndose responsable del tratamiento en relación con los datos publicados, o de los datos que los usuarios envíen de forma privada.

**Finalidad y legitimación:** el tratamiento que DENTAL VIP, Especialidades Odontológicas s.c. llevará a cabo con los datos dentro de cada una de las referidas redes será, como máximo, el que la red social permita a los perfiles corporativos. Así pues, podremos informar, cuándo la ley no lo prohíba, sobre actividades, ponencias y ofertas; así como prestar servicio personalizado de atención al cliente.

**Extracción de datos:** en ningún caso DENTAL VIP, Especialidades Odontológicas s.c. extraerá datos de las redes sociales, a menos que se obtuviera puntual y expresamente el consentimiento del usuario para ello.

**Derechos:** cuando, debido a la propia naturaleza de las redes sociales, el ejercicio efectivo de los derechos de protección de datos del seguidor quede supeditado a la modificación del perfil personal de este, DENTAL VIP, Especialidades Odontológicas s.c. le ayudará y aconsejará a tal fin, siempre en la medida de sus posibilidades.

## Destinatarios de los datos

Para poder prestar servicios estrictamente necesarios, en DENTAL VIP, Especialidades Odontológicas s.c. compartimos datos con los siguientes proveedores informáticos bajo sus correspondientes condiciones de privacidad:

**Google Analytics:** un servicio de analítica web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View _(California)_, CA 94043, Estados Unidos.

Google Analytics utiliza “cookies”, que son archivos de texto ubicados en su dispositivo, para ayudar a www.dentalvipcaracas.com a analizar el uso que hacen los visitantes del portal. La información que genera la cookie acerca de su uso _(incluyendo su dirección IP)_ será directamente transmitida y archivada por Google en sus servidores de Estados Unidos.

**Hosting:** DigitalOcean, LLC empresa de computación en la nube con sede en
Nueva York, Estados Unidos de Norteamérica; trata los datos con la finalidad de prestar sus servicios de hosting a DENTAL VIP, Especialidades Odontológicas s.c.

Se incluyen en este tipo de cookies servicios de terceros como: Google Ads y Facebook Ads. Al visitar esta página, se envían cookies procedentes de esos servicios a su navegador. Si tiene dudas, le recomendamos consultar más información sobre los tipos de cookies que utilizan Google y Facebook.

## Conservación de los datos

**Datos desagregados:** los datos desagregados serán conservados sin plazo de supresión.

## Derechos del usuario sobre sus datos

Aunque en Venezuela no existe una legislación en materia de Protección de Datos, reconocemos una serie de derechos que tiene sobre DENTAL VIP, Especialidades Odontológicas s.c. como USUARIO que ha prestado sus datos, como son:

**Derecho a solicitar el acceso a los datos personales:** Usted podrá preguntar si la empresa está tratando sus datos.

**Derecho a solicitar su supresión:** o rectificación en caso de que sean incorrectos.

**Derecho a solicitar la limitación de su tratamiento:** en cuyo caso únicamente serán conservados por DENTAL VIP, Especialidades Odontológicas s.c. para el ejercicio o la defensa de reclamaciones.

**Derecho a oponerse al tratamiento:** DENTAL VIP, Especialidades Odontológicas s.c. dejará de tratar los datos en la forma que Usted indique, salvo que por motivos legítimos, imperiosos o del ejercicio de la defensa de posibles reclamaciones, se tengan que seguir tratando.

**Derecho a la portabilidad de los datos:** en caso de que desee que sus datos sean tratados por otra firma, DENTAL VIP, Especialidades Odontológicas s.c. le facilitará la portabilidad de sus datos al nuevo responsable.

**Posibilidad de retirar el consentimiento:** en el caso de que se haya otorgado el consentimiento para alguna finalidad específica, Usted tiene derecho a retirarlo en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
