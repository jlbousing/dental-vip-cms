---
templateKey: thank-you-page
language: en
redirects: /gracias-por-contactarnos
title: Thank you
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Follow Us</h1>
    <h2>On our blog and social networks</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/en/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">News, Articles, Topical Advices and Much More …</h2>
# Brand Section
brand:
  logo: /uploads/logo.svg
  title: >
    <h4><b style="color:#333">YOUR MESSAGE HAS BEEN SENT SUCCESSFULLY!.<b></h4>
  main: >
    <p>We will respond as soon as our next work day begins.</p>
    <p>thank you so much for your trust!</p>
  partners:
    - image: /uploads/partners-idd.jpg
      alt: Institute of Digital Dentistry
    - image: /uploads/partners-qdc.jpg
      alt: Quality Dental Center
    - image: /uploads/partners-iti.jpg
      alt: International Team for Implantology
  footer: >
    <h1>Latest Posts</h1>

# Gallery Section
gallery:
  type: singleGallery
  carousel:
    display: true
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/lightbox-smiles-01-en.jpg
      - /uploads/lightbox-smiles-02-en.jpg
      - /uploads/lightbox-smiles-03-en.jpg
      - /uploads/lightbox-smiles-04-en.jpg
      - /uploads/lightbox-smiles-05-en.jpg
      - /uploads/lightbox-smiles-06-en.jpg
      - /uploads/lightbox-smiles-07-en.jpg
      - /uploads/lightbox-smiles-08-en.jpg
      - /uploads/lightbox-smiles-09-en.jpg
      - /uploads/lightbox-smiles-10-en.jpg
      - /uploads/lightbox-smiles-11-en.jpg
      - /uploads/lightbox-smiles-12-en.jpg
      - /uploads/lightbox-smiles-13-en.jpg
      - /uploads/lightbox-smiles-14-en.jpg
      - /uploads/lightbox-smiles-15-en.jpg
      - /uploads/lightbox-smiles-16-en.jpg
      - /uploads/lightbox-smiles-17-en.jpg
      - /uploads/lightbox-smiles-18-en.jpg
      - /uploads/lightbox-smiles-19-en.jpg
      - /uploads/lightbox-smiles-20-en.jpg
      - /uploads/lightbox-smiles-21-en.jpg
      - /uploads/lightbox-smiles-22-en.jpg
      - /uploads/lightbox-smiles-23-en.jpg
      - /uploads/lightbox-smiles-24-en.jpg
  items:
    - link:
        display: true
        to: /en/the-clinic/why-choose-us/
      image: /img/gallery-why.png
      action: false
      placeholder: >
        <span> View more </span>
      body: >
        <h3>WHY CHOOSE US</h3>
        <p class="dv-text-feat">Our trajectory is your best guarantee</p> <p class="dv-text-feat-100">
          Know the 10 reasons that distinguish us from the competition and
          make up our value proposition.
        </p>
    - link:
        display: true
        to: /en/the-clinic/facilities/
      image: /img/gallery-facilities.png
      action: false
      placeholder: >
        <span> View more </span>
      body: >
        <h3>THE CLINIC IN IMAGES</h3>
        <p class="dv-text-feat">
          A relaxed, calm and maximum comfort environment ...
        </p>
    - link:
        display: true
        to: /en/the-clinic/technology/
      image: /img/gallery-technology.png
      action: false
      placeholder: >
        <span> View more </span>
      body: >
        <h3>Dotation and Technology</h3>
        <p class="dv-text-feat">
          On the forefront of equipments and digital processes!
        </p>
    - link:
        display: true
        to: /en/professional-staff/
      image: /img/gallery-professionals.png
      action: false
      placeholder: >
        <span> View more </span>
      body: >
        <h3>Professional Staff</h3>
        <p class="dv-text-feat">Your mouth in the hands of experts!</p>
    - link:
        display: false
        to: /
      image: /img/gallery-smiles.png
      action: true
      placeholder: >
        <span> View gallery </span>
      body: >
        <h3>SMILES GALLERY</h3>
        <p class="d-none d-lg-block dv-text-feat">
          Passion for beauty... Devotion to naturalness!
        </p> 
        <p class="d-none d-lg-block dv-text-feat-100">
          A small sample of what we can do for you: modern, integral and specialized Dentistry.
        </p>
        <p class="d-lg-none  dv-text-feat">
          A small sample of what we can do for you: modern, integral and specialized Dentistry.
        </p>
    - link:
        display: true
        to: /en/foreign-patients/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> View more </span>
      body: >
        <h3>FOREIGN PATIENTS</h3>
        <p class="dv-text-feat">A special attention protocol</p> <p class="dv-text-feat-100">
          We are consequent with all patients who visit us from anywhere in
          Venezuela and the world.
        </p>
---
