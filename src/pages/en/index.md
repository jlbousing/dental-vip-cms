---
templateKey: home-page
language: en
redirects: "/"
title: Homepage
description: seo description
keywords:
- default keyowrd
published: true
tags:
- default tag
ogImage: https://imagenes.dentalvipcaracas.com/open-graph-página-inicio.jpg
hero:
  background:
    scaleOnReveal: true
    img: "/uploads/hero-home.jpg"
    isParallax: false
  anim:
    display: true
    type: left
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: bottom left-aligned
    body: '<h2 class="wrapped">Health, Beauty & Function</h2> <br> <h3 class="no-mob">One
      Specialty for Each Treatment!</h3> <h1>INNOVATION AND PRESTIGE IN DENTISTRY</h1>
      <br> <br>

'
  type: ''
  image: cloudinary://822598879251417:d_bMGvIpc_yUIC8BvHoDccOZkhM@dzmrjudl5/uploads/v1596683930/uploads/hero-home.jpg
  captions: []
brand:
  logo: "/uploads/logo.svg"
  title: '<h4 class="light">CENTER FOR AESTHETICS AND ORAL REHABILITATION</h4>

'
  main: |
    <p>Welcome to our online space!</p>  <p>
      In DENTAL VIP we offer you the experience of the best Dentists
      Specialist team, the most modern and comfortable facilities and the
      latest technology worldwide.
    </p>  <p>
      We are a human group truly committed to what it does, capable of
      providing an integral, personalized and high scientific value health
      service; always focused on the ethics, responsibility and social sense
      of our work.
    </p>
  partners:
  - image: "/uploads/partners.jpg"
  footer: 'CARACAS - VENEZUELA

'
quote:
  body: |
    <p>
      <strong>
        The Quality in Dentistry is not the fruit of chance.
      </strong>
      <br />
      <em>
        It is always the result of the pursuit of professional excellence
        and an intelligent effort for continuous improvement. Permanent
        training in new trends and therapeutic philosophies, honesty,
        efficient use of resources and cordial attention of people
        generate the highest levels of satisfaction in all our patients.
      </em>
    </p>
  footer:
    author: "<strong>María José Tirado</strong>\n"
    details: |
      <strong>
        Office Coordination
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>
    position: ''
    clinic: ''
  title: ''
  author: ''
statistics:
  image: "/uploads/parallax-stadistics.jpg"
  items:
  - number: 20
    title: "<h1>years of<br>experience</h1>\n"
  - number: 580000
    title: "<h1>elective<br>interventions</h1>\n"
  - number: 9500
    title: "<h1>solved<br>cases</h1>\n"
gallery:
  type: singleGallery
  carousel:
    display: true
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
    - "/uploads/lightbox-smiles-01-en.jpg"
    - "/uploads/lightbox-smiles-02-en.jpg"
    - "/uploads/lightbox-smiles-03-en.jpg"
    - "/uploads/lightbox-smiles-04-en.jpg"
    - "/uploads/lightbox-smiles-05-en.jpg"
    - "/uploads/lightbox-smiles-06-en.jpg"
    - "/uploads/lightbox-smiles-07-en.jpg"
    - "/uploads/lightbox-smiles-08-en.jpg"
    - "/uploads/lightbox-smiles-09-en.jpg"
    - "/uploads/lightbox-smiles-10-en.jpg"
    - "/uploads/lightbox-smiles-11-en.jpg"
    - "/uploads/lightbox-smiles-12-en.jpg"
    - "/uploads/lightbox-smiles-13-en.jpg"
    - "/uploads/lightbox-smiles-14-en.jpg"
    - "/uploads/lightbox-smiles-15-en.jpg"
    - "/uploads/lightbox-smiles-16-en.jpg"
    - "/uploads/lightbox-smiles-17-en.jpg"
    - "/uploads/lightbox-smiles-18-en.jpg"
    - "/uploads/lightbox-smiles-19-en.jpg"
    - "/uploads/lightbox-smiles-20-en.jpg"
    - "/uploads/lightbox-smiles-21-en.jpg"
    - "/uploads/lightbox-smiles-22-en.jpg"
    - "/uploads/lightbox-smiles-23-en.jpg"
    - "/uploads/lightbox-smiles-24-en.jpg"
  items:
  - link:
      display: true
      to: "/en/the-clinic/why-choose-us/"
    image: "/img/gallery-why.png"
    action: false
    placeholder: "<span> View more </span>\n"
    body: |
      <h3>WHY CHOOSE US</h3> <p class="dv-text-feat">Our trajectory is your best guarantee</p> <p class="dv-text-feat-100">
        Know the 10 reasons that distinguish us from the competition and
        make up our value proposition.
      </p>
  - link:
      display: true
      to: "/en/the-clinic/facilities/"
    image: "/img/gallery-facilities.png"
    action: false
    placeholder: "<span> View more </span>\n"
    body: |
      <h3>THE CLINIC IN IMAGES</h3> <p class="dv-text-feat">
        A relaxed, calm and maximum comfort environment ...
      </p>
  - link:
      display: true
      to: "/en/the-clinic/technology/"
    image: "/img/gallery-technology.png"
    action: false
    placeholder: "<span> View more </span>\n"
    body: |
      <h3>Dotation and Technology</h3> <p class="dv-text-feat">
        On the forefront of equipments and digital processes!
      </p>
  - link:
      display: true
      to: "/en/professional-staff/"
    image: "/img/gallery-professionals.png"
    action: false
    placeholder: "<span> View more </span>\n"
    body: '<h3>Professional Staff</h3> <p class="dv-text-feat">Your mouth in the hands
      of experts!</p>

'
  - link:
      display: false
      to: "/"
    image: "/img/gallery-smiles.png"
    action: true
    placeholder: "<span> View gallery </span>\n"
    body: |
      <h3>SMILES GALLERY</h3> <p class="d-none d-lg-block dv-text-feat">
        Passion for beauty... Devotion to naturalness!
      </p>  <p class="d-none d-lg-block dv-text-feat-100">
        A small sample of what we can do for you: modern, integral and specialized Dentistry.
      </p> <p class="d-lg-none  dv-text-feat">
        A small sample of what we can do for you: modern, integral and specialized Dentistry.
      </p>
  - link:
      display: true
      to: "/en/foreign-patients/"
    image: "/img/gallery-foreigns.png"
    action: false
    placeholder: "<span> View more </span>\n"
    body: |
      <h3>FOREIGN PATIENTS</h3> <p class="dv-text-feat">A special attention protocol</p> <p class="dv-text-feat-100">
        We are consequent with all patients who visit us from anywhere in
        Venezuela and the world.
      </p>
testimonials:
  title: Testimonials and Ratings
  items:
  - img: "/img/testimonials-mohammad.png"
    testimonial: "“After 11 implants and 11 ceramic crowns in the center, I have always
      had very good experiences. I was honestly very satisfied with the services of
      DENTAL VIP. Implants are a great success, and the only thing I regret is not
      having done it before. Thanks to everyone, and especially Dr. Garabán, for his
      patience, human quality and professional ethics”."
    position: Businessman
    name: Mohammad Byherzade
  - img: "/img/testimonials-viviana.png"
    testimonial: "“Before I was embarrassed to smile because I was not satisfied with
      the appearance of my teeth. Since I visited DENTAL VIP I feel more comfortable
      and sure of myself. They made me a digital smile design, cut my gums, bleached
      my teeth and placed me four truly spectacular porcelain veneers. I would recommend
      them with my eyes closed”."
    position: General Physician
    name: Viviana Hernández
  - img: "/img/testimonials-carlos.png"
    testimonial: "“After two orthodontic treatments and more than 5 years with braces,
      I thought that my case had no solution. Almost without hope, and only because
      of the results I observed in my cousin Marta, I decided to visit Dr. José Miguel
      Gómez. In just 3 months with my new brackets I noticed an impressive improvement,
      and finally, I was able to recover my smile. I learned that, definitely, cheap
      is very expensive”."
    position: Photographer
    name: Carlos Gutiérrez
  - img: "/img/testimonials-brenda.png"
    testimonial: "“I needed to put on dental implants and I didn't know who to trust.
      I requested several opinions and finally decided on DENTAL VIP, and not because
      it was a recognized clinic, but because they offered me the best guarantees
      and gave me a lot of security. Now that I have finished the treatment I can
      say that I was delighted and that the whole team seemed me very close and professional”."
    position: Engineer
    name: Brenda Uzcátegui
  - img: "/img/testimonials-jose.png"
    testimonial: "“I found myself in need of going to the clinic to treat some cavities
      and getting two root canal treatments. I would recommend to DENTAL VIP for the
      advice provided, for the easy access to its location, for the receptivity of
      its staff, for its impeccable facilities, and above all; because the procedures
      were executed without any kind of pain. Very good Dentists”."
    position: Public Accountant
    name: José Luis Dávila
features:
  title: '<h1 class="title"> What Advantages Does Our Exclusive Work Methodology Offer
    You? </h1>

'
  description: |
    <p>
        A newfangled multidisciplinary approach that makes possible the integrative management of knowledge and maximizes the resolution capacity of our healthcare team.
        <br />
        <strong>All Specialties in the Same Place!</strong>
    </p>
  features:
  - to: "/en/specialties/oral-surgery/\n"
    img: "/uploads/specialties-oral-surgery.png"
    title: '<h3 class="title bebas bold">ORAL SURGERY</h3>

'
    description: "<p> Prevention, diagnosis and treatment of all surgical pathology
      itself or associated with teeth, mucous membranes, lips, gums and maxillary
      bones.</p>\n"
  - to: "/en/specialties/dental-implants/\n"
    img: "/uploads/specialties-dental-implants.png"
    title: '<h3 class="title bebas bold">IMPLANTS</h3>

'
    description: "<p> Replacement of lost teeth with biocompatible titanium devices
      that enable fixed oral rehabilitation of partially or totally edentulous patients.</p>\n"
  - to: "/en/specialties/aesthetic-dentistry/\n"
    img: "/uploads/specialties-dental-aesthetics.png"
    title: '<h3 class="title bebas bold">aesthetic dentistry</h3>

'
    description: "<p> Analysis and study of dentogingival features to correct cosmetic
      defects and beautify the general appearance of the smile.</p>\n"
  - to: "/en/specialties/endodontics/\n"
    img: "/uploads/specialties-endodontics.png"
    title: '<h3 class="title bebas bold">ENDODONTICS</h3>

'
    description: "<p> Cleaning, disinfection and conformation of the root canals as
      a previous step to the multiple procedures of prosthesis and dental restoration.</p>\n"
  - to: "/en/specialties/orthodontics/\n"
    img: "/uploads/specialties-orthodontics.png"
    title: '<h3 class="title bebas bold">ORTHODONTICS</h3>

'
    description: "<p> Brackets and functional appliances for the biomechanical correction
      of malocclusions, dental malpositions and dentofacial deformities.</p>\n"
  - to: "/en/specialties/prosthodontics/\n"
    img: "/uploads/specialties-prosthesis.png"
    title: '<h3 class="title bebas bold">PROSTHESIS AND ORAL REHABILITATION</h3>

'
    description: "<p> Design and confection of ceramic crowns and prosthetic structures
      that restore the integrity of dental arches, aesthetics and masticatory function.</p>\n"
procedures:
  title: '<h1 class="title">Featured Procedures</h1>

'
  procedures:
  - title: "<h5> IMPLANT SUPPORTED RESTORATIONS</h5>\n"
    to: "/en/specialties/dental-implants/implant-supported-restorations/"
    img: "/uploads/procedures-dental-implants.jpg"
  - title: "<h5> 3D DIAGNOSIS AND PLANNING</h5>\n"
    to: "/en/specialties/dental-implants/3d-diagnosis-and-planning/"
    img: "/uploads/procedures-diagnostic.jpg"
  - title: "<h5> PORCELAIN VENEERS</h5>\n"
    to: "/en/specialties/aesthetic-dentistry/porcelain-veneers/"
    img: "/uploads/procedures-veneers.jpg"
  - title: "<h5> CAD-CAM TECHNOLOGY</h5>\n"
    to: "/en/specialties/prosthodontics/cad-cam-technology/"
    img: "/uploads/procedures-cad-cam.jpg"
  - title: "<h5> DIGITAL SMILE DESIGN</h5>\n"
    to: "/en/specialties/aesthetic-dentistry/digital-smile-design/"
    img: "/en/uploads/procedures-design.jpg"
  - title: "<h5> AESTHETIC BRACKETS</h5>\n"
    to: "/en/specialties/orthodontics/aesthetic-braces/"
    img: "/uploads/procedures-brackets.jpg"
welcome:
  logo: ''
  heading: ''
  main: ''
  location: ''
  partners: []
testimonial:
  title: ''
  items: []
parallax:
  stadistics: ''
  landscape: ''
  portrait: ''
  desktop: ''
  portraitxl: ''
specialties:
  title: ''
  paragraph: ''
  slogan: ''
  features: []
elements: []
lightbox:
  display: false
  type: ''
  placeholder: ''
  images: []

---
