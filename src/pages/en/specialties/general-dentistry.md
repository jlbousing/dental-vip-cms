---
templateKey: specialties-page
language: en
redirects: /especialidades/odontologia-general/
title: General Dentistry
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-general-dentistry.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 56%
  content:
    position: center
    body: >
      <h1 class="bebas">GENERAL DENTISTRY</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-general-dentistry"></i></h1>
    <p class="thin"><em>The Best Treatment of Any Disease is its Prevention and our main objective
    should be to interpose biological barriers that truly interfere with its development.</em></p>

# Description Section
article:
  content: >
    <p><strong>The General Dentist is trained to prevent, diagnose and treat
    common problems of the general public</strong>. If you require a ROUTINE CHECK
    or Basic Dentistry such as caries removal, cleaning, prophylaxis or oral prosthesis
    without aesthetic, functional or periodontal compromise; a general practice professional
    can provide an excellent service. However, because the different areas of Dentistry
    have become increasingly complex and highly technified, <strong>there are numerous
    cases that require the intervention of professionals with long and intensive training
    called Specialists</strong>. These individuals are particularly trained to execute
    advanced techniques and procedures providing highly predictable and often surprising
    results for patients.</p><p><strong>A good General Dentist is responsible for
    performing the triage and primary diagnosis of the patient</strong>, solving hygiene
    problems and CARIES RESTORATION that may be presented and refer, if necessary,
    to the most indicated Specialist. It is important to clarify that <strong>a true
    Specialist must have completed fourth level or Postgraduate studies</strong> at
    a recognized national or foreign University, with an academic curriculum that
    covers the teaching hours required by the different Scientific Societies of the
    country.</p><p><strong>The Generalist must have solid knowledge of oral pathology,
    radiology, periodontics and gnatology</strong> to be able to detect the presence
    of lesions or diseases in the tongue, gums, soft tissues and temporomandibular
    joints. He must also be an expert in OPERATIVE DENTISTRY, since <strong>he is
    usually the one who does the cavitary preparations necessary for the treatment
    of Dental Caries</strong>, applying conservative designs and trying to preserve
    pulp vitality at all times, to finally; return to the organ its usual anatomy
    and functionality through the dental restoration process. He must therefore know
    and properly manage the different adhesive systems based on the acid etching technique,
    the multiple restorative materials based on dental composites and the most basic
    and universal principles of occlusion and masticatory function.</p>
  img: /uploads/aside-general-dentistry.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Unfortunately, Tooth Decay represents a serious public health problem in
    our environment, because more than 98% of the Venezuelan population has suffered,
    suffers or will suffer it at some time throughout life.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Vianka Xaviera Torres</strong>
    details: >
      <strong>
        General Dentist - Endodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-general-dentistry.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-general-dentistry.jpg
  mobilePosition: '{"mobilePosition320":"-700px","mobilePosition360":"-800px","mobilePosition375":"-1095px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>What are the most common diseases that affect teeth and gums?</h4>
      content: >
        <p>The most frequent oral pathologies are caries and periodontal conditions
        <em>(gingivitis and periodontitis)</em>. Then, to a lesser extent,
        infectious diseases of bacterial origin, physical trauma, congenital lesions
        and mouth cancer. According to the World Health Organization between 60% to
        90% of school-age children and about 99% of adults suffer from tooth decay,
        often accompanied by pain or discomfort.</p>
    - title: <h4>Is it possible to avoid them?</h4>
      content: >
        <p>Although the genetic component is decisive, the incidence of oral
        diseases can be significantly reduced by controlling the known risk factors.
        The control of dental plaque through the constant practice of a correct oral
        hygiene technique, the reduction of sugar intake, the well-balanced diet
        rich in fruits, vegetables and fiber, the reduction of alcohol consumption,
        the cessation of smoking habit, the periodic professional controls and the
        topical applications of fluoride will have a favorable impact on the
        appearance and prevalence of caries, periodontal diseases and oral
        neoplasms.</p>
    - title: <h4>What is bacterial plaque?</h4>
      content: >
        <p>Dental or bacterial plaque is a white-yellowish sticky substance that
        easily adheres to teeth and accumulates many species of microbes
        <em>(aerobic and anaerobic)</em> within an intercellular matrix of organic
        origin. The plaque is formed from food debris that has not been removed and
        that serve as a perfect substrate for colonization and multiplication of
        bacteria. The importance of its control lies in the fact that it is the main
        cause of the two most common infections of the oral cavity: Dental Caries
        and Periodontal Disease.</p>
    - title: <h4>How is it different from calculus or dental tartar?</h4>
      content: >
        <p>Dental calculus, also known as dental stone or dental tartar, is the
        result of solidification of the bacterial plaque by the precipitation and
        progressive accumulation of mineral salts; calcium and phosphorus mainly.
        Because it is a mineralized, rough and firmly adhered to the teeth material,
        it houses and retains enormous amounts of bacterial plaque on its surface,
        and unlike it; it cannot be removed by the toothbrush. Only a professional
        cleaning can eliminate it.</p>
    - title: <h4>Is it normal for the gums to bleed when I brushing my teeth?</h4>
      content: >
        <p>At all! Although such a phenomenon could be due to a systemic condition,
        the usual cause is due to the chronic accumulation of dental plaque that
        generates a reversible inflammatory pathological entity known as Gingivitis,
        but that aggravated, leads to the formation of tartar, pathological sacs,
        resorption of the alveolar bone and teeth fall <em>(Periodontitis)</em>.
        Hemorrhage of gingival origin is the most obvious sign of the so-called
        Periodontal Disease.</p>
    - title: <h4>Why does bad breath occur?</h4>
      content: >
        <p>Halitosis, also known as bad breath, is defined as the set of unpleasant
        odors that are emitted through the mouth. It is a problem that affects one
        in three people and is associated with poor oral hygiene or diseases of the
        oral cavity, although sometimes, it can be a clinical manifestation of some
        other systemic or gastrointestinal disease.</p>
    - title: <h4>Which is the best oral hygiene technique?</h4>
      content: >
        <p>That which is practiced religiously after each meal and that contemplates
        the use of the conventional brush, interproximal brush, dental floss and
        mouthwash. Optimal brushing takes at least three minutes of time and to
        perform it properly we must place the bristles on the dental surface and its
        adjacent gum with an angle of 45 degrees, to then, apply short and smooth
        horizontal movements paying special attention to the tooth-gum interface,
        hard to reach back teeth and areas where there are fillings, crowns,
        orthodontic appliances or dental implants. Never forget to brush your
        tongue.</p>
    - title: <h4>Are electric brushes better than manual ones?</h4>
      content: >
        <p>Both are definitely effective, with both types you can achieve optimal
        and adequate tooth brushing. You can use the one that suits you, as long as
        the technique is appropriate. Electric brushes may work better than manual
        ones in people with limited dexterity, arthritis, degenerative or mental
        diseases that compromising motor skills.</p>
    - title: <h4>How often should I visit the Dentist?</h4>
      content: >
        <p>Normally recommended once a year. Fortunately, the process of caries and
        tartar development is relatively slow if reasonable oral hygiene is
        practiced, so a check every 12 months will be adequate to prevent diseases,
        detect and treat incipient lesions and maintain healthy mouth, teeth and
        gums. However, for some people with pre-existing pathologies, extensive oral
        rehabilitations, dental implants, orthodontic treatments or bad habits; a
        higher frequency may be necessary, which can range between 2 and 4 times a
        year. A typical case is that of those patients who suffer from gingivitis or
        chronic periodontitis, and who usually require permanent periodontal support
        therapy.</p>
    - title: <h4>Why are several X-rays necessary to make a good Oral Diagnosis?</h4>
      content: >
        <p>Because they expose details that are not accessible to the human eye.
        Only through radiological images we will can detect the very frequent
        proximal caries <em>(those that form on the contact surfaces of the
        teeth)</em>, alveolar and interproximal bone alterations and resorptions,
        periodontal diseases, pulp and periapical conditions, root resorptions,
        impacted teeth, odontogenic tumors and cysts and defective or permeable
        restorations; among other anomalies. A Diagnosis without the support of
        radiographic evidence will always leave much to be desired.</p>
    - title: <h4>What is a tartrectomy or prophylaxis?</h4>
      content: >
        <p>It is what is popularly known as dental cleaning. Tartrectomy is a
        clinical procedure that consists of the mechanical removal of all calculus
        and dental plaque accumulated in the teeth, gingival margin and interdental
        spaces, through the use of ultrasonic devices and special dental
        instruments. It should be practiced periodically, every 6 or 12 months,
        since there are areas in the mouth that even a correct brushing cannot
        reach.</p>
    - title: <h4>What is a tooth polishing?</h4>
      content: >
        <p>Both natural and artificial teeth suffer wear, and over time, they
        acquire certain roughness on their outer layers that it is advisable to
        remove. There are several techniques for this that provide not only an
        aesthetic benefit, but also a hygienic and functional ones, since a smooth
        and polished surface retains a smaller amount of dental plaque and is then
        easier to address and clean.</p>
    - title: <h4>What are the benefits of a topical fluoride application?</h4>
      content: >
        <p>This chemical element gives three main benefits: it increases the
        resistance of the enamel, is antibacterial and promotes remineralization.
        Fluorides directly inhibit the formation of bacterial acids and contribute
        to the incorporation of calcium and phosphate ions in the enamel, decreasing
        its susceptibility to tooth decay. Normally they are applied in gel form
        with the use of special trays and directly on the dental surface.</p>
    - title: <h4>How are cavities formed?</h4>
      content: >
        <p>If they are not removed periodically, the bacteria contained in the
        dental plaque generate powerful organic acids that attack and demineralize
        the hard or inorganic tissues of susceptible teeth. Thus, on the surface of
        the enamel, grooves and cracks may be formed that will cause the entry of
        new bacteria and the proliferation of a greater amount of dental plaque.
        This demineralization process can be reversible in the early stages,
        however, the continued attack of the acids will lead to further destruction
        of the tooth enamel and the creation of holes on the tooth surfaces that
        could reach the dentin, and even, to the dental pulp; compromising then its
        vitality.</p>
    - title: <h4>How are they cured or eliminated?</h4>
      content: >
        <p>It depends on the development, extension and depth of them. If it is
        incipient or moderate all infected tissue is removed and the cavity is
        sealed with a special material, usually based on Dental Composite Resins. In
        more advanced states caries usually affects the dental pulp or nerve of the
        tooth and it is then necessary to perform a Root Canal Treatment. In
        addition, after Endodontics, a Ceramic Inlay or Crown is usually indicated
        to protect the weakened remaining dental structure and prevent its
        fracture.</p>
    - title: <h4>Is dental amalgam still used?</h4>
      content: >
        <p>Although the appearance of new adhesive materials for direct filling has
        been very beneficial from the aesthetic point of view, they do not in any
        way affect the relevance and indication of other traditional materials that
        are even yet more resistant, more durable and more economical. This is the
        case of silver amalgam, which undoubtedly remains like the best option in
        situations where restored teeth, due to the type of bite, are exposed to
        considerable forces or overloads; as in many posterior teeth, at the level
        of molars.</p>
    - title: <h4>What are and how are Dental Composites placed?</h4>
      content: >
        <p>They are synthetic materials mixed heterogeneously to form a single
        compound of varied elements. They are used in Dentistry to fill teeth
        because they are very aesthetic, and in addition, because they adhere
        micromechanically to their surface using the acid etching technique.
        Composite Resins consist of a polymeric organic component called matrix and
        an inorganic component that acts as a filler mineral. They are generally
        photosensitive and halogen light is used for their placement.</p>
    - title: <h4>When is it necessary to replace an old filling?</h4>
      content: >
        <p>The answer is very simple: when there is dental sensitivity, when the
        material is damaged, broken or fractured, when marginal filtration and/or
        recurrence caries is observed or when a radiographic study shows a coronal
        radiolucent area that suggests a failure of cohesive type.</p>
    - title: <h4>What is Bruxism?</h4>
      content: >
        <p>Bruxism is the involuntary habit of clenching or grinding the teeth
        without functional purposes. It affects between 10% and 20% of the
        population, being able to generate headache, pain of the masticatory
        muscles, neck and ears. Grinding habit, if not treated on time, can lead to
        wear or fracture of the teeth completely, compromising the aesthetics and
        occlusal function of the person.</p>
    - title: <h4>When is a night splint indicated?</h4>
      content: >
        <p>The dental, discharge, stabilization or deprogramming splint is a
        transparent, hard and custom-made acrylic resin device that is placed on the
        upper or lower arch <em>(depending on the characteristics of the case)</em>
        of the patient, and which is used for palliative treatment of Bruxism and to
        reduce muscle hyperactivity associated with occlusal, functional and
        inflammatory disorders of the temporomandibular joint <em>(TMJ)</em>. Also,
        we indicate it routinely as a protective device in patients with extensive
        fixed or implant-assisted rehabilitations.</p>

cases:
  title: >
    <h1>General Dentistry - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-general-dentistry-en-01.jpg
      - /uploads/clinic-cases-general-dentistry-en-02.jpg
      - /uploads/clinic-cases-general-dentistry-en-03.jpg
      - /uploads/clinic-cases-general-dentistry-en-04.jpg
      - /uploads/clinic-cases-general-dentistry-en-05.jpg
      - /uploads/clinic-cases-general-dentistry-en-06.jpg
      - /uploads/clinic-cases-general-dentistry-en-07.jpg
      - /uploads/clinic-cases-general-dentistry-en-08.jpg
      - /uploads/clinic-cases-general-dentistry-en-09.jpg
      - /uploads/clinic-cases-general-dentistry-en-10.jpg
      - /uploads/clinic-cases-general-dentistry-en-11.jpg
      - /uploads/clinic-cases-general-dentistry-en-12.jpg
      - /uploads/clinic-cases-general-dentistry-en-13.jpg
      - /uploads/clinic-cases-general-dentistry-en-14.jpg
      - /uploads/clinic-cases-general-dentistry-en-15.jpg
      - /uploads/clinic-cases-general-dentistry-en-16.jpg
      - /uploads/clinic-cases-general-dentistry-en-17.jpg
      - /uploads/clinic-cases-general-dentistry-en-18.jpg
      - /uploads/clinic-cases-general-dentistry-en-19.jpg
      - /uploads/clinic-cases-general-dentistry-en-20.jpg
      - /uploads/clinic-cases-general-dentistry-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CERVICAL ABRASION INJURIES </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FAILURE OF DENTAL AMALGAM </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>RESTORING PROXIMAL CARIES LESIONS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SPLINTING BY DENTAL TRAUMA</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTAL CARIES TREATMENT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CLASS III CAVITIES AND PHOTOCURED COMPOSITES</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PROVISIONAL FIXED PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER REMOVABLE PARTIAL DENTURE (RPD) </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FRACTURED INCISAL EDGE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TONGUE LESION TREATMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTO-MUCO-SUPPORTED PROSTHESIS REPLACEMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PORCELAIN INLAY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TARTRECTOMY OR DENTAL CLEANING</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MICRORELLENE COMPOSITE RESTORATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TOTAL CERAMIC CROWN</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTAL IMPLANT AND ZIRCONIA CROWN</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTAL PROPHYLAXIS AND TOOTH POLISHING </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>REMOVABLE DENTURE WITHOUT VISIBLE HOOKS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>THERMO-POLYMERIZABLE ACRYLIC RPD</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FIBER POSTS AND METAL-FREE CROWNS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-general-dentistry-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ZIRCONIA CROWNS ON SUPERIOR INCISORS</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">EXCELLENT PROFESSIONALS, VERY DETAILED AT THE TIME OF DIAGNOSING AND WITH
    A VERY GOOD WILLINGNESS TO EXPOSE TO THE PATIENT THE DETECTED DENTAL PROBLEMS
    AND THE PERTINENT PROCEDURES FOR THEIR IMMEDIATE CORRECTION. THEY FORM A FIRST
    LEVEL TEAM".</p>
  images:
    portrait: /uploads/quotes-general-dentistry-portrait-en.jpg
    landscape: /uploads/quotes-general-dentistry-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
