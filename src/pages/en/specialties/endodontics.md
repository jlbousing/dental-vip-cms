---
templateKey: specialties-page
language: en
redirects: /especialidades/endodoncia/
title: Endodontics
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-endodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 50%
  content:
    position: center
    body: >
      <h1 class="bebas">Endodontics</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-endodontics"></i></h1>
    <p class="thin"><em>If indicated, a Root Canal Treatment performed on time is the only viable alternative to avoid odontectomy or extraction of a compromised tooth.</em></p>

# Description Section
article:
  content: >
    <p>Any stimulus or aggressive entity capable of compromising the structural integrity of a tooth is also capable of causing damage, infection or necrosis of its pulp tissue. <b>The dental pulp or "nerve" of a tooth is a specialized connective tissue that is lodged internally throughout the crown and root</b>. When a trauma, excessive dental wear or indiscriminate advance of an untreated caries reaches, exposes or infects that pulp tissue, and of course we want to preserve the dental organ; there is no alternative but <b>to remove it, disinfect and then mechanically widen the empty root canals so that they are capable of receiving an inert, sterile and biocompatible filling material</b> that completely occupies them, avoids pain and the spread of the infectious process. In this consist a ROOT CANAL TREATMENT. <b>If it is not performed on time, large facial abscesses, phlegmones or extremely painful and aggressive cellulite processes can be formed</b> that can spread rapidly and seriously compromise other vital structures of the orofacial region, and in extreme cases; until the patient's life.</p>
    <p>Given how intricate and variable the root anatomy of the teeth is and the fact that the canals are small cavities inaccessible to the human eye, <b>it is a considerably hostile and difficult field of work</b>, therefore it is necessary for the Dentist to have extensive anatomical knowledge of the RADICULAR MORPHOLOGY and has developed a very special sense of tactile perception that allows him <b>to locate, access, instrument and fill the canals with great precision, with no margin of error possible</b>. It is our opinion that such qualities are only achieved through formal studies of the Specialty and limiting professional practice exclusively to the area of Endodontics. </p>
    <p><b>Our Endodontist has the latest technological advances such as high-speed rotary systems for mechanical canal instrumentation</b>, electronic root apex locator, computerized digital radiology or RADIOVISIOGRAPHY that minimizes the emission of X-rays and modern thermoplastic shutter systems that reduce almost to zero the rates of clinical failure and the need for retreatments, periapical endodontic surgeries, apicectomies and dental extractions.</p>
  img: /uploads/aside-endodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      An adequate and immediate Post-Endodontic Restoration is the key factor to
    prevent reinfections, fractures and dental losses. In many cases a ceramic crown
    will be the first choice treatment.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Vianka Xaviera Torres</strong>
    details: >
      <strong>
        Endodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-endodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-endodontics.jpg
  mobilePosition: '{"mobilePosition320":"-144px","mobilePosition360":"-144px","mobilePosition375":"-144px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: >
        <h4>How is a tooth inside?</h4>
      content: >
        <p>It is conformed in all its extension by a cavity or space that lodges
        a “nerve” or pulp tissue, and that is divided in two portions; the coronary and
        the radicular. The coronary portion is called the pulp chamber and houses the
        coronary pulp. The radicular portion is called the root canal, extends to the
        end or apex of the root and is also occupied by specialized connective tissue
        or dental pulp.</p>
    - title: >
        <h4>How many roots and canals do they have?</h4>
      content: >
        <p>There are monoradicular, biradicular and multiradicular teeth. According
        to the position they occupy in the archway they usually have one, two or three
        roots. Within each root there may be one or two canals.</p>
    - title: >
        <h4>How to know if a Root Canal is needed?</h4>
      content: >
        <p>Generally by the symptomatology. Spontaneous pain, constant hypersensitivity
        to thermal stimuli, discomfort from sugar intake and the presence of dental abscesses
        are usually clear hints of pulpitis and/or pulp necrosis.</p>
    - title: >
        <h4>What is a pulpitis?</h4>
      content: >
        <p>It is the inflammation of the dental pulp. It can be reversible or
        irreversible. It is considered irreversible when, despite being vital, the pulp
        has completely lost its regeneration capacity.</p>
    - title: >
        <h4>What is a pulp necrosis?</h4>
      content: >
        <p>It is the death of the nerve of a tooth as a final consequence of a
        chronic, very acute or traumatic inflammation of its pulp tissue. Generally, the
        process begins at the most coronal fraction of the pulp and then extends to its
        root portion.</p>
    - title: >
        <h4>Can a Root Canal Treatment be performed in the presence of acute pain
        or inflammation?</h4>
      content: >
        <p>Many times it is difficult. It is often necessary to prescribe antibiotics,
        analgesics and anti-inflammatory drugs before accessing the canals. In other cases,
        it becomes pertinent to drain abscesses and treat other apical processes as a
        prerequisite for root therapy.</p>
    - title: >
        <h4>How many sessions are needed to complete a conventional Endodontics?</h4>
      content: >
        <p>It depends on several factors. From the symptomatology, whether it
        is live pulp or dead pulp, whether there is or not a presence of exudate and the
        number of roots and canals to be treated; among others. As a general rule, treatments
        are completed in one, two or three clinical sessions.</p>
    - title: >
        <h4>Is there any other alternative to endodontic therapy?</h4>
      content: >
        <p>Tooth extraction only.</p>
    - title: >
        <h4>Is it necessary to place anesthesia?</h4>
      content: >
        <p>Of course! They are usually infected, inflamed or necrotic tissues,
        highly sensitive to clinical manipulation. Fortunately, a good previous diagnosis
        and an adequate technique of truncal or infiltrative local anesthesia, will be
        sufficient to carry out the procedure without major discomfort for the patient.</p>
    - title: >
        <h4>hy is it always recommended to isolate the tooth to be treated with
        a rubber dam?</h4>
      content: >
        <p> The absolute isolation of the operative field allows to maintain at all times the conditions of asepsis and facilitates the procedures of antisepsis. In addition to preventing the entry of saliva <em>(substance rich in bacteria)</em> into the canals, the rubber dam improves the visibility of the area and prevents the patient from aspirating or swallowing instruments and chemicals substances during the treatment.</p>
    - title: >
        <h4>What is conductometry?</h4>
      content: >
        <p>It consists in determining the exact length between the apical constriction
        of each canal and the incisal edge or occlusal surface of the tooth being treated.
        This measure constitutes the length of work and must be respected at all times
        to avoid complications and therapeutic failures.</p>
    - title: >
        <h4>What are the objectives of instrumentation or biomechanical preparation?</h4>
      content: >
        <p>Remove the pulp, remove residues and necrotic material, widen the canal
        and give it a conical shape for your complete disinfection and adequate filling.</p>
    - title: >
        <h4>What materials are used to fill or seal off the empty root canals?</h4>
      content: >
        <p>Throughout history a wide variety of materials have been used for this
        purpose, however, none has been shown to meet all desirable requirements of the
        ideal material. At present gutta-percha remains the most used, a type of polymer
        substance similar to rubber and that is extracted from a tree originating from
        the islands of the Malay archipelago. Additionally, a cement is placed that guarantees
        apical sealing, usually based on calcium hydroxide.</p>
    - title: >
        <h4>How to know if a Root Canal Treatment was correctly performed?</h4>
      content: >
        <p>Basically due to the absence of symptoms and radiographic evidence.
        A well-performed Root Canal Treatment should reflect a filling that extends until
        the apical constriction of the root, that lacks lights inside, that has sufficient
        body and volume, that is compact in all its extension and that faithfully reproduces
        the conical shape of the canal it occupies.</p>
    - title: >
        <h4>What complications could occur?</h4>
      content: >
        <p>If the Specialist is not well trained and attentive to his work, he
        could widen the root walls excessively and weaken the tooth, he could fracture
        instruments inside the canal, he could create steps and false pathways, coronary,
        cameral or root perforations, he could boost necrotic or organic remains to the
        periapice and generate sub or overobturations.</p>
    - title: >
        <h4>What is an overobturation?</h4>
      content: >
        <p>It consists of extravasation of a small amount of sealant or gutta-percha
        into the periradicular area, outside the root of the tooth. Fortunately, numerous
        scientific articles support the success of the treatment even in cases where there
        has been a small over-sealing.</p>
    - title: >
        <h4>What is an Endodontic Retreatment?</h4>
      content: >
        <p>Root Canal Re-Treatment is the intervention that is carried out to
        eliminate the filling material that is inside the canals of a tooth already treated
        for its new cleaning, shaping and filling. In other words, it is a new Root Canal
        Treatment in a previously treated tooth.</p>
    - title: >
        <h4>When should it be done?</h4>
      content: >
        <p>When for some reason the initial endodontic therapy has failed, or
        when the canals have become contaminated again. Such circumstance usually occurs
        when the definitive restoration is not made as soon as possible, when new caries
        appear, in cases of advanced periodontal disease or when the tooth cracks and
        suffers a small fracture. Any of these situations could lead to a Reinfection.</p>
    - title: >
        <h4>Are teeth that have undergone Root Canal Treatments more fragile?</h4>
      content: >
        <p>Undoubtedly! The loss of tooth coronal structure and pulp mechanoreceptors
        make them less flexible, less resistant and more susceptible to fracture. That
        is why their permanence and longevity, will depend on their adequate and immediate
        reconstruction.</p>
    - title: >
        <h4>How long can I stay without the definitive restoration?</h4>
      content: >
        <p>The shortest possible time. As long as an endodontic treated tooth
        is not restored, it will be prone to fracture and reinfection. In those cases
        of great destruction, which merit the manufacture of artificial posts and cores,
        we recommend starting the root preparation two or three days after the Endodontics
        is finished.</p>

cases:
  title: >
    <h1>Endodontics - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-endodontics-en-01.jpg
      - /uploads/clinic-cases-endodontics-en-02.jpg
      - /uploads/clinic-cases-endodontics-en-03.jpg
      - /uploads/clinic-cases-endodontics-en-04.jpg
      - /uploads/clinic-cases-endodontics-en-05.jpg
      - /uploads/clinic-cases-endodontics-en-06.jpg
      - /uploads/clinic-cases-endodontics-en-07.jpg
      - /uploads/clinic-cases-endodontics-en-08.jpg
      - /uploads/clinic-cases-endodontics-en-09.jpg
      - /uploads/clinic-cases-endodontics-en-10.jpg
      - /uploads/clinic-cases-endodontics-en-11.jpg
      - /uploads/clinic-cases-endodontics-en-12.jpg
      - /uploads/clinic-cases-endodontics-en-13.jpg
      - /uploads/clinic-cases-endodontics-en-14.jpg
      - /uploads/clinic-cases-endodontics-en-15.jpg
      - /uploads/clinic-cases-endodontics-en-16.jpg
      - /uploads/clinic-cases-endodontics-en-17.jpg
      - /uploads/clinic-cases-endodontics-en-18.jpg

  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>RETREATMENT OF A LOWER FIRST MOLAR</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ENDODONTIC RETREATMENT BY FILTRATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>REMOVAL OF FRACTURED INSTRUMENT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MULTI-ROOT CANAL TREATMENT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CONVENTIONAL ENDODONTIC THERAPY BY CARIES</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMMATURE APEX TREATMENT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CRACKED TOOTH SYNDROME</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MULTIRADICULAR CANAL TREATMENT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SEALING WITH THERMOPLASTICIZED GUTTA-PERCHA</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BIOPULPECTOMY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ENDODONTICS IN A CASE OF SEVERE BRUXISM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PULP NECROSIS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>NECROPULPECTOMY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ACUTE APICAL PERIODONTITIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FAILURE DUE TO BAD DIAGNOSIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IRREVERSIBLE PULPITIS BY PROXIMAL CARIES</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MULTIPLE ENDODONTICS FOR PROSTHETIC REASONS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-endodontics-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>RETREATMENT, DEOBTURATION AND POST AND CORE SYSTEM</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">I WENT TO THE CLINIC TO EXTRACT A MOLAR THAT HAD BEEN CRACKED AND THE DOCTORS
    TOLD ME THAT IT WOULD BE A GREAT MISTAKE TO DO IT, THAT IT COULD BE SAVED WITH
    ENDODONTICS. STILL SCEPTIC, I STARTED THE TREATMENT, AND NOW I THANKS; EVEN WITH
    A CERAMIC CROWN I STILL KEEP MY TEETHING IN FULL".</p>
  images:
    portrait: /uploads/quotes-endodontics-portrait-en.jpg
    landscape: /uploads/quotes-endodontics-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
