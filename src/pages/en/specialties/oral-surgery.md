---
templateKey: specialties-page
language: en
redirects: /especialidades/cirugia-bucal/
title: Oral Surgery
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-oral-surgery.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 38%
  content:
    position: center
    body: >
      <h1 class="bebas">Oral Surgery</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-oral-surgery"></i></h1>
    <p class="thin"><em>Avoid pain, complications and bad times. A true Surgeon will be able to
    carry out a clean, safe, fast and accurate intervention with hardly any postoperative
    sequel.</em></p>

# Description Section
article:
  content: >
    <p>Although the mouth represents a small and limited region of the human
    body, <b>a large number of congenital and/or acquired pathological entities that
    require elective surgical treatment</b> can be presented in it. Among the most
    common of those that are treated in our clinic we can mention the extraction of
    the included third molars  <em>(wisdom teeth)</em>, simple, complex and multiple
    dental extractions; placement of DENTAL IMPLANTS, phrenilectomies, apicectomies
    and periapical surgeries, excision and biopsy of minor oral lesions and dentigerous
    cysts; among others.</p><p><b>Usually all these procedures are performed under
    local anesthesia;</b> however, they are occasionally executed in conjunction with
    an Anesthesiologist under CONSCIOUS SEDATION, especially in excessively nervous
    patients or with any physical or emotional commitment. <b>Its objective is to
    get people into a state of tranquility and deep relaxation during the clinical
    act.</b></p><p><b>Although the purpose of the intervention may vary, the techniques
    used are always very similar; </b>  and involve, prior anesthesia, the gum incision,
    its detachment to a greater or lesser extent, the removal of pathological or excessive
    tissues, placement of implants or biocompatible grafts, and finally; the closure
    and suture of the wound. It will always be essential, for the final success of
    the procedure, that the operator knows in detail the specific function and clinical
    application of the numerous surgical instruments of DIERESIS, EXERESIS and SYNTHESIS.</p><p><b>Oral
    Surgery is science, art and skill, </b> and although it is governed by the same
    principles of general surgery; it has its own peculiarities that emanate from
    the anatomical area to be treated. Not in vain, <b>it is the oldest Specialty
    of Dentistry.</b></p>
  img: /uploads/aside-oral-surgery.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      As a general rule, extractions of ectopic, including or partially erupted
    third molars, are easier to perform in patients under 25 years old; before their
    roots have fully developed.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Oral Surgeon
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-oral-surgery.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-oral-surgery.jpg
  mobilePosition: '{"mobilePosition320":"-376px","mobilePosition360":"-376px","mobilePosition375":"-587px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: >
        <h4>Why choose a Specialist in Oral Surgery?</h4>
      content: >
        <p>Because he is a broad connoisseur of oral anatomy and pathology, because
        he exercises great mastery over contemporary medical-surgical principles and because
        he possesses great skill and dexterity on handling the most innovative clinical
        techniques of the Specialty; qualities that confer him the level of expert in
        the matter.</p>
    - title: >
        <h4>Are surgical acts performed in the mouth very painful?</h4>
      content: >
        <p>The evolution that Dentistry has experienced in recent decades allows
        us to affirm that, under normal conditions, the discomfort associated with any
        surgical act is truly small. Although all Oral Surgery procedures are essentially
        invasive, the reality is that the postoperative period is perfectly controllable.
        The application of an effective anesthetic technique, of a controlled surgical
        act and the adoption of an adequate postoperative protocol; will minimize the
        risk and the possibility of pain, infection and inflammation of the affected area.</p>
    - title: >
        <h4>Is it necessary to undergo to blood tests before an intervention?</h4>
      content: >
        <p>Although surgical complications are extremely rare in young and apparently
        healthy patients, it will never hurt to have a preoperative hematological profile
        that evaluates the ability of blood coagulation and tissue scarring. So a blood
        test will be routine as a prerequisite to the intervention.</p>
    - title: >
        <h4>Should the patient go on an empty stomach?</h4>
      content: >
        <p>Conversely! If the procedure is going to be performed under local anesthesia,
        we should always avoid it. Fasting is only necessary in cases of general anesthesia.</p>
    - title: >
        <h4>What others preoperative cares are recommended?</h4>
      content: >
        <p>We generally suggest using fresh and light clothes during the procedure,
        brushing carefully your teeth, gums and mucous membranes before the intervention;
        and having an adult companion who can provide you the necessary support.</p>
    - title: >
        <h4>For an Oral Surgery, could you give me general anesthesia?</h4>
      content: >
        <p>Only in cases of Major Surgery such as Orthognathic and Maxillofacial.
        Due to its great effectiveness, safety and almost absolute absence of side effects;
        local trunk or infiltrative anesthesia is the one of choice for minor Oral Surgery
        interventions in outpatient settings. Conscious sedation with benzodiazepine derivatives
        is the best option for special or excessively apprehensive people; however, it
        requires the participation of an Anesthesiologist.</p>
    - title: >
        <h4>What is an atraumatic extraction and what is its importance?</h4>
      content: >
        <p>Atraumatic surgical methods for tooth extraction aim to preserve intact
        the bone that surrounds the tooth root. This work philosophy is based on a set
        of very well defined principles and an instruments specially designed to preserve
        the alveolus and its bone crest, thus favoring the subsequent placement of dental
        implants. In oral implantology the bone is pure gold, and if there is bone, we
        have to take care of it.</p>
    - title: >
        <h4>When removing a tooth, can an implant be placed immediately?</h4>
      content: >
        <p>Sometimes yes, sometimes no. The immediate implants are inserted in
        the same surgical act in which the extractions of the teeth to be replaced are
        performed. Although they are very desirable and offer great advantages, they cannot
        be placed in the presence of extensive apical lesions and/or active periodontal
        pathology.</p>
    - title: >
        <h4>Is it always necessary to extract the third molars or wisdom teeth?</h4>
      content: >
        <p>Definitely not! They should be removed when they are unable to erupt
        due to lack of space and remain included, when generate pain or chronic discomfort,
        when erupt in a bad position, when interfere with occlusion or bite or when they
        suffer carious lesions of considerable extent.</p>
    - title: >
        <h4>What are included teeth?</h4>
      content: >
        <p> Dental inclusion is a developmental disorder in which a given tooth,
        come the normal time of its eruption, remains inside the tissues of the oral cavity
        <em>(alveolar bone and oral mucosa)</em>. The teeth that most frequently suffer
        from this situation are the third molars <em>(wisdom teeth)</em>; mainly those
        of the lower jaw. The diagnosis of the inclusion of a tooth can only be made using
        radiological examinations, usually a panoramic dental X-ray.</p>
    - title: >
        <h4>Should they always be extracted?</h4>
      content: >
        <p>Yes in the vast majority of cases; however, there is no general rule
        for making the decision. The different criteria must be evaluated in each particular
        clinical situation, weighing thoroughly the risks and benefits of the intervention.</p>
    - title: >
        <h4>What is a phrenilectomy or frenectomy?</h4>
      content: >
        <p>It consists of a small operation in which the lingual or labial frenulum
        is repositioned so that it does not interfere with the correct position of the
        teeth and/or tongue mobility. Very low or very fibrous labial insertions generate
        pressure and unsightly spaces between the central incisors <em>(diastemas)</em>.
        Abnormal lingual insertions cause serious phonetic and diction alterations.</p>
    - title: >
        <h4>Are cysts and tumors of the oral cavity very common?</h4>
      content: >
        <p>Fortunately, they are pathologies of very low prevalence. However,
        once its existence is detected, it is imperative to study the case and immediately
        determine the therapeutic behavior to be implemented. </p>
    - title: >
        <h4>What is a biopsy and what is it done for?</h4>
      content: >
        <p>It is a microscopic study of a piece of tissue or a portion of organic
        liquid that is extracted from a living being. Some biopsies involve the removal
        of a small amount of tissue with a needle, while others include surgical removal
        of the entire lesion. Usually, a biopsy is performed for the screening of oral
        cancer or for the diagnosis of ulcers, erosions and blisters that does not show
        evidence of spontaneous healing within 5 to 10 days, rapidly growing nodules,
        black lesions, white lesions, red lesions with suspected erythroplasia, lesions
        without a definite diagnosis, and in some cases; for the study of infectious,
        fungal and bacterial diseases. </p>
    - title: >
        <h4>What is an apicectomy?</h4>
      content: >
        <p>Apicectomy is a surgical procedure whose objective is to eliminate
        the apex <em>(end)</em> of the root of a tooth to solve a localized infectious
        process of endodontic origin. This type of surgery is performed only if other
        more conservative treatments such as conventional endodontics have failed, if
        it is impossible to access the apex of the root, if there are false canals in
        the affected tooth, or when inside it; has fractured some instrument of intraradicular
        use. </p>
    - title: >
        <h4>When are bone grafts indicated?</h4>
      content: >
        <p>Usually in cases of advanced periodontal disease and in cases where
        the quantity and quality of the recipient bone are not favorable for the placement
        of dental implants. Although there are several types of grafts, a mixed technique
        is usually executed. Autologous bone taken from the same patient <em>(autograft)</em>
        and lyophilized bone of animal origin <em>(xenograft)</em> are usually placed
        at the same time.</p>
    - title: >
        <h4>Is inflammation normal after an Oral Surgery procedure?</h4>
      content: >
        <p>It is natural to experience some swelling after any surgery. A surgical
        cut in the skin or mucosa, also called incision, is a form of aggression to the
        organism. The body's natural response to an injury of this type is the inflammatory
        process, which as we know; occurs with increased volume. However, if the inflammation
        becomes exaggerated, contact your Specialist immediately.</p>
    - title: >
        <h4>Is it necessary to take antibiotics, analgesics and anti-inflammatories?</h4>
      content: >
        <p>Depends on the situation. Simple procedures rarely require postoperative
        medication; however, more complex interventions do. We generally prescribe a broad-spectrum
        antibiotic and a non-steroidal anti-inflammatory analgesic <em>(NSAID)</em>.</p>
    - title: >
        <h4>In case of bleeding, what is the behavior to follow?</h4>
      content: >
        <p>It is normal to suffer slight bleeding during the first 24 hours. If
        this bleeding is more intense <em>(hemorrhage)</em>, then one or more gauze should
        be folded, placed on the wound and compressed by biting gently with the opposing
        teeth until the blood stops. If it is necessary to place more gauzes, never remove
        the first ones; apply new ones on these. Place ice and avoid to lying down. If
        the problem persists, contact your Specialist immediately.</p>
    - title: >
        <h4>How long should the sutures remain in the mouth?</h4>
      content: >
        <p>It all depends on the case and extent of the procedure. As a general
        rule, a period of between 7 and 10 days is sufficient; however, it could be extended
        for a longer time. At present it is very common to use resorbable sutures that,
        for being biodegradable; do not require a second clinical intervention for their
        removal.</p>

cases:
  title: >
    <h1>Oral Surgery - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-oral-surgery-en-01.jpg
      - /uploads/clinic-cases-oral-surgery-en-02.jpg
      - /uploads/clinic-cases-oral-surgery-en-03.jpg
      - /uploads/clinic-cases-oral-surgery-en-04.jpg
      - /uploads/clinic-cases-oral-surgery-en-05.jpg
      - /uploads/clinic-cases-oral-surgery-en-06.jpg
      - /uploads/clinic-cases-oral-surgery-en-07.jpg
      - /uploads/clinic-cases-oral-surgery-en-08.jpg
      - /uploads/clinic-cases-oral-surgery-en-09.jpg
      - /uploads/clinic-cases-oral-surgery-en-10.jpg
      - /uploads/clinic-cases-oral-surgery-en-11.jpg
      - /uploads/clinic-cases-oral-surgery-en-12.jpg
      - /uploads/clinic-cases-oral-surgery-en-13.jpg
      - /uploads/clinic-cases-oral-surgery-en-14.jpg
      - /uploads/clinic-cases-oral-surgery-en-15.jpg
      - /uploads/clinic-cases-oral-surgery-en-16.jpg
      - /uploads/clinic-cases-oral-surgery-en-17.jpg
      - /uploads/clinic-cases-oral-surgery-en-18.jpg
      - /uploads/clinic-cases-oral-surgery-en-19.jpg
      - /uploads/clinic-cases-oral-surgery-en-20.jpg
      - /uploads/clinic-cases-oral-surgery-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>EXPOSURE OF DENTAL IMPLANTS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>LOWER THIRD MOLAR EXTRACTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TOOTH EXTRACTION, BONE GRAFT AND IMMEDIATE IMPLANT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MULTIPLE EXODONTICS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FRENECTOMY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ALVEOLAR REGENERATION WITH AUTOLOGOUS BONE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPACTED CANINE EXPOSURE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sinus Lift </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>EXCISIONAL BIOPSY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>APICECTOMY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PLATELET-RICH FIBRIN (PRF) IN IMPLANT DENTISTRY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MANDIBULAR TORUS REMOVAL </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MAXILLARY SINUS AUGMENTATION AND DENTAL IMPLANTS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>GUIDED TISSUE REGENERATION </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>VESTIBULAR BONE BOARD RESTORATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ROOTS SIMPLE EXTRACTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SURGICAL RESECTION OF FIBROMATOUS LESION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>HEALING CAP</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CORTICOTOMY FOR EXPANSION OF RESIDUAL RIDGE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER THIRD MOLAR ODONTECTOMY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-oral-surgery-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MAXILLARY FIRST MOLAR EXTRACTION </h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">"I WAS SUBJECTED TO SEVERAL EXTRACTIONS AND TWO BONE GRAFTS AS A PRIOR PREPARATION TO THE PLACEMENT OF DENTAL IMPLANTS, AND TRULY, I COULD NOT GO BETTER WITH DR. GARABÁN. HE IS VERY SUBTLE IN HIS WORK AND HIS GREAT SKILL AND PROFESSIONAL EXPERIENCE IS IMMEDIATELY PERCEIVED".</p>
  images:
    portrait: /uploads/quotes-oral-surgery-portrait-en.jpg
    landscape: /uploads/quotes-oral-surgery-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
