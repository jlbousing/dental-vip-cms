---
templateKey: annex-page
language: en
redirects: >
  /especialidades/ortodoncia/aparatos-esteticos/
title: Aesthetic Braces
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-aesthetic-braces.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 46%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: top
  type: List
  content: >
    ""
  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >
        <span><i class="icon-instagram"></i></span>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title"> AESTHETIC BRACES</h1>
    <p>The new technologies in Orthodontics
    have allowed, when the appearance is important, the use of materials more comfortable
    and discreet than steel, and that help the patient to smile without problems while
    advancing his treatment.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-aesthetic-braces.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>In DENTAL VIP We Are True Experts in Aesthetic Orthodontics</h1>
    <br>
    <h2 >20 years of experience and more than 2,000 cases treated successfully
    support us.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: reverse
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Because we do not want to interfere with your lifestyle!</h2>
      <p class="dv-div-text
      dv-pb-15">The vast majority of patients want a therapeutic option that straightens their teeth quickly and effectively, that offers a neat and beautiful appearance, and that in addition; responds to their particular needs.
      </p> <p class="dv-div-text text-right">
      One in five people applying for Orthodontics are adults and require an effective and truly aesthetic solution. Ceramic and crystalline devices of recognized quality are suitable for almost all types of known malocclusion, work continuously throughout the treatment and never limit work activity, social life or sports practice</p>
    image:
      display: true
      size: 40px
      src: /uploads/icon-brackets-usa.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-aesthetic-braces.jpg

customBlocks:
  display: true
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2>Ceramic Brackets</h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>They are the most used and economical aesthetic brackets
          on the market. Porcelain Appliances are exact reproductions of traditional metal
          brackets, but of a color similar to that of teeth.</p>
    - header:
        display: true
        content: >
          <h2> Sapphire Brackets </h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-shappire-brackets.jpg
      footer:
        display: true
        content: >
          <p  class="top-title">These braces are practically invisible, since they
          are manufactured with Sapphire Crystal, a transparent and shiny element that
          combined with other minerals becomes very resistant.</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title">Other Procedures</h1>
  procedures:
    - title: >
        <h5>  3D DIAGNOSIS AND PLANNING </5>
      to: /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> ORAL REHABILITATION</h5>
      to: "/en/specialties/prosthodontics/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5>  DIGITAL SMILE DESIGN</5>
      to: /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> CAD-CAM TECHNOLOGY</5>
      to: /en/specialties/prosthodontics/cad-cam-technology/
      img: /uploads/procedures-cad-cam.jpg
    - title: >
        <h5> PORCELAIN VENEERS</5>
      to: /en/specialties/aesthetic-dentistry/porcelain-veneers/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5> IMPLANT SUPPORTED RESTORATIONS </5>
      to: en/specialties/dental-implants/implant-supported-restorations/
      img: /uploads/procedures-dental-implants.jpg
---

<div class="container">
<div class="row">
<div class="item np left">

## Because we take care of the image of our patients!

We fully understand that many teenagers, professionals, models, artists
and adults refuse to exhibit metal devices on their teeth. Therefore, we always
offer the alternatives that best fit their needs.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-advertisement.png)

</div>
<div class="item np right">

## Because we want to satisfy all your expectations!

Beautiful, radiant and natural smiles at any age. In recent years, the use
of aesthetic appliances has significantly increased the acceptance and satisfaction
rates of all our Orthodontic treatments.

</div>
</div>
</div>
