---
templateKey: annex-page
language: en
redirects: >
  /especialidades/ortodoncia/cuide-sus-brackets/
title: Take Care of Your Brackets!
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-brackets-care.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 55%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: top
  type: List
  content: >
    ""
  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >
        <span><i class="icon-instagram"></i></span>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">Take Care of Your Brackets!</h1>
    <p>If you have started an Orthodontic
    Treatment, you need to know the following information and comply with these simple
    indications:</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-brackets-care.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>Which Toothbrush is the Most Suitable?</h1>
    <br>
    <h2 >One of soft filaments and V-shaped profile that generates a triple
    cleaning effect: appliances, teeth and gums.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Oral irrigators</h2>
      <p class="dv-div-text
        dv-pb-15">The complexity of removing oral biofilm <em>(bacterial plaque)</em>
      in highly retentive areas such like the ones that form with Orthodontic appliances
      can easily generate gingival inflammation, tooth decay and halitosis. A large
      number of clinical trials have shown that, under these conditions, Waterpik<sup>®</sup>
      oral irrigators eliminate 99.9% of plaque deposited on teeth, gums and hard-to-reach
      places; thanks to the direct application of a pulsatile jet of common water or
      mouthwash.</p> <p class="dv-div-text text-left"> In our opinion, the Irrigation
      Technique will always be indicated as an adjunctive resource in the hygiene of
      patients with Orthodontics, Fixed Prosthesis and Dental Implants.</p>
    image:
      display: true
      size: 70px
      src: /uploads/icon-waterpik.jpg

    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-brackets-care.png

customBlocks:
  display: true
  type: column
  blocks:
    - header:
        display: true
        content: >
          <span class="icon">1</span>
          <p>The first 3 or 4 days
          after the installation of the brackets are somewhat uncomfortable because it
          is normal to experience some sensation of "burning or itching" in the gums and
          some pain or discomfort at the time of biting and chewing. Also, it is possible
          that ulcers, aphthas or sores appear inside the mouth <em>(remember that the
          appliances are a foreign body for your organism)</em>, mainly on the tongue,
          lips and cheeks. If necessary, you can take an analgesic such as acetaminophen
          or paracetamol and use the Orthodontic wax that we have provided, placing a
          small piece of it on the bracket that is bothering you.</p>
      body:
        display: true
        images:
          - src: /uploads/brackets-care-steps-1.jpg
          - src: /uploads/brackets-care-steps-2.jpg
      footer:
        display: true
        content: >
          <span class="icon">2</span>
          <p>You will be able
          to eat practically everything, but it is essential to avoid food and hard objects
            that could detach the devices or bend the wires, a situation that would cause
            significant delays in treatment. Therefore, do not touch the brackets with your
            fingers, pencils or pens, or bite your nails or use wooden chopsticks. All these
            habits could damage Orthodontic appliances. Also avoid biting foods directly,
            always cut or chop them first with your hands or cutlery before putting them
            in your mouth. Also avoid chewing gum or sticky foods and biting excessively
            hard things such as bone, ice, seeds and similar objects. IT IS JUST A MATTER
          OF COMMON SENSE!</p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">Other Procedures</h1>
  procedures:
    - title: >
        <h5>  AESTHETIC BRACKETS </h5>
      to: /en/specialties/orthodontics/aesthetic-braces/
      img: /uploads/procedures-brackets.jpg
    - title: >
        <h5>DIGITAL SMILE DESIGN</h5>
      to: /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> 3D DIAGNOSIS AND PLANNING </h5>
      to: /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> PORCELAIN VENEERS</h5>
      to: /en/specialties/aesthetic-dentistry/porcelain-veneers/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5>  ORAL REHABILITATION </h5>
      to: >
        /en/specialties/prosthodontics
      img: /uploads/procedures-rehabilitation.jpg
    - title: >
        <h5>IMPLANT SUPPORTED RESTORATIONS </h5>
      to: /en/specialties/dental-implants/implant-supported-restorations/
      img: /uploads/procedures-dental-implants.jpg
---

<div class="container">
<div class="row alt">
<div class="item np left par">

<span class="icon-number" style="
    font-size: 50px;
">3</span>

During the treatment
      it is important to practice a very careful oral hygiene. The appliances cause
      more dental plaque and food debris to be retained in the teeth, therefore it
      is then necessary to devote some more time to their cleaning. If your teeth
      and gums do not brush well, they will swell, bleed and a bad smell will come
      out of your mouth.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-3.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-icon-brush"></i></span>

You have to brush immediately after
            each meal <em>(normally, just as you did before having appliances)</em>,
            paying special attention to the area of the teeth between the braces and
            gums, as it is there where more food is retained. If for some reason you
            cannot do it, at least rinse thoroughly with plenty of water or mouthwash.
            Fluoride paste should be used and the brush changed regularly, when you
            notice that the bristles begin to bend and wear out.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-4.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-interdental-brush_1"></i></span>

At least once a day <em>(if you
can more times even better) </em>look at yourself in a mirror after brushing
and then use the interdental brush to access the space between teeth,
wires and brackets. This small cone-shaped brush must be operated vertically,
from the bottom up and between all the brackets and bands that you have
in your mouth. Check that all spaces and appliances remain clean and free
of food debris.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-5.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-dental-floss"></i></span>

Finally, use dental floss <em>(Special
            for Prosthetics and Orthodontics) </em>to clean the deepest areas of the
            proximal surfaces of the teeth. Insert it directly above the wire arch,
            first hugging the surface of one tooth and then that of the other. If
            it is your desire, complement the process with the mouthwash of your preference.

</div>

</div>
</div>
