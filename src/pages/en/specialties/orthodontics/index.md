---
templateKey: specialties-page
language: en
redirects: /especialidades/ortodoncia/
title: Orthodontics
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-orthodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">Orthodontics</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-orthodontics"></i></h1>
    <p class="thin"><em>Take care of your health and protect your investment. Many clinics and
    companies delegate their treatments to auxiliary turn staff. Inform yourself before
    and demand to be always attended by the same Orthodontist.</em></p>

# Description Section
article:
  content: >
    <p>The smile is the light of our face, allows us to interact better with
    people, cause a good impression and makes persons show more empathy when it comes
    to interacting with us. <strong>Smiling often will change your life, improve your
    mood, your charisma and your self-esteem.</strong></span></p> <p>Considering the
    beauty of the smile as the great north and supreme objective of Orthodontics,
    in DENTAL VIP we use with great success the STRAIGHT WIRE TECHNIQUE with preadjusted
    devices according to the philosophy and prescription of Dr. Ronald Roth for the
    treatment of malocclusions and dental malpositions. <strong>This technique </strong></span><em><strong>(Straight
    Wire System)</strong></em></span><strong> uses very light forces that allow us
    to move the teeth with total accuracy in the 3 planes of the space</strong></span>,
    achieving ideal positions perfectly compatible with the most demanding standards
    of contemporary dental aesthetics. In that sense, we can use conventional or <strong>aesthetic
    brackets</strong></span>; it all depends on your preference.</span></p> <p>In
    the initial phase of DIAGNOSIS we use <strong>digital photographs, panoramic radiographs
    and computerized cephalometries</strong></span> <em>(Ricketts, Jarabak, Downs
    and Steiner)</em> to plan all orthodontic mechanotherapy and achieve highly predictable,
    aesthetic and functional results.</p> <p>Additionally, we routinely handle <strong>craniofacial
    growth modification techniques</strong> for the treatment of skeletal malocclusions
    in children and adolescents, through the use of DENTOFACIAL ORTHOPEDICS and functional
    jaw orthopedics devices <em>(removable appliances)</em> that allow us to recover
    the facial and biological balance lost by developmental disorders.<strong> In
    the extreme cases of severe dentofacial deformities a combined Orthodontic-Surgical
    approach is already necessary</strong>, so that the execution of the treatment
    implies the joint and coordinated participation with the Maxillofacial Surgeon,
    who will be responsible for repositioning the affected maxillary structures through
    the various procedures of ORTHOGNATHIC SURGERY.</p>
  img: /uploads/aside-orthodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      It Is Not Enough with to Just Align the Teeth. The sum of the characterizations
    and small details that can be achieved in the finishing phase will define the
    excellence of the result. That is why in some people the teeth will look really
    beautiful, and in others, just straight.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. José Miguel Gómez Díez</strong>
    details: >
      <strong>
        Orthodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-orthodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-orthodontics.jpg
  mobilePosition: '{"mobilePosition320":"-860px","mobilePosition360":"-952px","mobilePosition375":"-1241px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>What is Orthodontics?</h4>
      content: >-
        <p>It is the Specialty of Dentistry responsible for the prevention,
        diagnosis and treatment of malocclusions and dentofacial deformities. It
        is based on the knowledge of the craniofacial growth and development
        process and the domain of biomechanics, a science that deals with studying
        and controlling the vectors, intensities, senses and physical effects of
        the forces that need to be applied on the teeth to move them and correct
        their position.</span></p>
    - title: <h4>What is a malocclusion?</h4>
      content: >-
        <p>It is any irregular arrangement of the teeth and jaws that affect the
        aesthetic and masticatory function of the person. There are dental
        malocclusions and skeletal malocclusions, that is why is so important the
        DIAGNOSIS in Orthodontics. The most common dental malocclusion is the
        crowding and is due to a discrepancy between the size of the teeth and the
        size of the jaws. Skeletal ones derived from alterations of facial growth
        and development, and are characterized by abnormalities of shape, size and
        position of the maxillary bones.</span></p>
    - title: <h4>What is a dentofacial deformity?</h4>
      content: >-
        <p>It is a skeletal and dental malocclusion of such magnitude that it also
        compromises the facial aesthetics of the person. In these cases
        orthodontic treatment alone is not enough and must be combined with
        Orthognathic Surgery procedures so that the Maxillofacial Surgeon can
        reposition the dysplastic maxillary structures.</span></p>
    - title: <h4>Why is it important to correct malocclusions and align teeth?</h4>
      content: >-
        <p>Because socially it is very favorable to have a good appearance, and
        also, because in large part; oral health depends on a correct occlusion or
        bite. Straight and aligned teeth are easier to clean and less susceptible
        to cavities and periodontal diseases.</span></p>
    - title: <h4>Do I need a Dentist or an Orthodontist?</h4>
      content: >-
        <p>During the Dentistry degree barely some basic concepts about
        Orthodontics are taught, therefore, additional Specialization studies
        </span><em>(</em></span><em>University Postgraduate degree of 2 or 3 years
        duration</em><em>) </em></span>are required to be truly trained in the
        area. Keep this always in mind, avoid failures and bad experiences that
        are unfortunately very common. Short courses and Orthodontic fellowships
        are excellent for the General Dentist to learn to detect and intercept
        some malocclusions, but in no way quialify him to carry out corrective
        Orthodontic treatments.</span></p>
    - title: <h4>What is the best age to start an Orthodontic treatment?</h4>
      content: >-
        <p>There is no specific age, it all depends on the type and severity of
        the problem. </span>It is ideal to attend to consultation during the mixed
        dentition period </span><em>(between 7 and 9 years old)</em></span> to
        rule out any dental alteration, and especially skeletal; since growth
        modification </span><em>(Orthopedics)</em></span> is only possible before
        maturity or sexual development. Purely dental malocclusions can be treated
        at any stage of life.</span></p>
    - title: <h4>How long does a treatment of this type last?</h4>
      content: >-
        <p>It also depends on the case, but as a general rule the time varies
        between 18 and 24 months. In those patients with severe abnormalities the
        duration may be longer by virtue of the degree of skeletal and dental
        involvement.</span></p>
    - title: <h4>Is it always necessary to extract permanent teeth?</h4>
      content: >-
        <p>Of course not! This is determined based on the dento-maxillary
        discrepancy or degree of dental crowding. Extractions are indicated in
        approximately 30% of cases.</span></p>
    - title: "<h4>Which are better, fixed or removable appliances?</h4>"
      content: >
        <p>Fixed appliances or brackets are the only dispositives capable of
        accurately moving teeth in the 3 planes of space. Removable appliances are
        used to modify growth, to "move" bones and achieve orthopedic changes, but
        they are very bad for aligning teeth. Each type has its indications and
        specific uses.</span></p>
    - title: >-
        <h4>What are the differences between metal brackets and aesthetic
        brackets?</h4>
      content: >-
        <p>Mainly the color and the material. The aesthetic brackets are made of
        plastic, porcelain </span><em>(white)</em></span> or sapphire crystals
        </span><em>(transparent),</em></span> which makes them virtually
        imperceptible to the naked eye. Another important difference to consider
        is the cost, since the aesthetic brackets of proven quality triple in
        value to those of stainless steel.</span></p>
    - title: <h4>Are there other alternatives that are even more aesthetic?</h4>
      content: >
        <p>At present there has been an impressive rebound of old techniques that
        were in disuse. Lingual Orthodontics and the so-called "invisible
        Orthodontics" without brackets, with pure plastic splints, are offered
        with excessive enthusiasm. We consider that they have great limitations
        and are only effective in some extremely simple cases, so they should be
        considered only under a very objective and professional
        criterion.</span></p>
    - title: <h4>Is it very uncomfortable to use brackets?</h4>
      content: >-
        <p>Usually the first 3 or 4 days following the installation of the braces
        are somewhat annoying because it is normal to experience some pain when
        eating and chewing. After this period, the inconveniences are usually
        limited to occasional minor discomforts; mainly caused by the friction of
        the appliances with the inner part of the lips and cheeks. For this you
        will be provided with a special wax that you must place on the bracket
        that is bothering you and thus solve the problem.</span></p>
    - title: <h4>Do they need to be placed on all teeth?</h4>
      content: >-
        <p>In the vast majority of cases yes. To obtain optimal results it is also
        necessary to place bands </span><em>(metal rings)</em></span> around the
        molars, since these are the ones that provide the necessary anchorage to
        perform and control the vast majority of dental movements contemplated in
        the treatment planning phase.</span></p>
    - title: <h4>Is it normal for teeth to stain or pigment around braces?</h4>
      content: >-
        <p>Not at all! Generally this is the first sign of a poorly performed
        treatment in which the excesses of adhesive were not removed when
        cementing the dispositives. These excesses are extremely harmful because
        in a short time they cause tooth decay, dental decalcification and severe
        gingival inflammation.</span></p>
    - title: >-
        <h4>Is it necessary to have some kind of special care during
        treatment?</h4>
      content: >-
        <p>Of course that oral hygiene is a key factor for its success. It is
        essential to brush after each meal, use the interdental brush, floss and
        mouthwash. Periodic checks with the General Dentist, and in some cases,
        with the Periodontist; are also recommended, so that teeth and gums remain
        healthy at all times.</span></p>
    - title: <h4>What things can not be eaten with dental appliances?</h4>
      content: >-
        <p>You can practically eat everything, however, it is advisable to avoid
        some excessively hard foods such as ice and bones, and sticky like chewing
        gum; since they can easily detach brackets, bands and other
        devices.</span></p>
    - title: <h4>How often are Orthodontic checks performed?</h4>
      content: "<p>Generally every 4 weeks, except for special circumstances.</span></p>"
    - title: <h4>Will my teeth really look perfect?</h4>
      content: >-
        <p>In 99% of the cases the results obtained are completely satisfactory as
        long as the treatment is planned based on a right diagnosis and the
        patient regularly attends his control appointments. Sometimes it is
        necessary to complement these results with procedures of dental
        aesthetics, whitening or periodontal plastic surgery; so that the teeth
        look really spectacular.</span></p>
    - title: <h4>Could they move after removing the braces?</h4>
      content: >-
        <p>Dental alignment is a dynamic and changing entity over time, that is,
        teeth tend to move throughout life, with or without prior Orthodontics; so
        the retention phase is the longest and most complex of an orthodontic
        treatment and requires excellent collaboration by the patient.</span></p>
    - title: <h4>What are the retainers and what are they for?</h4>
      content: >-
        <p>They are custom-made devices that are placed at the time of removing
        the brackets. They can be fixed or removable and are intended to keep the
        teeth in their final position, preventing them from moving and misaligning
        again. If they are removable, they are used 24 hours a day at the
        beginning, but then it is usually enough to put them only to
        sleep.</span></p>

cases:
  title: >
    <h1>Orthodontics - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-orthodontics-en-01.jpg
      - /uploads/clinic-cases-orthodontics-en-02.jpg
      - /uploads/clinic-cases-orthodontics-en-03.jpg
      - /uploads/clinic-cases-orthodontics-en-04.jpg
      - /uploads/clinic-cases-orthodontics-en-05.jpg
      - /uploads/clinic-cases-orthodontics-en-06.jpg
      - /uploads/clinic-cases-orthodontics-en-07.jpg
      - /uploads/clinic-cases-orthodontics-en-08.jpg
      - /uploads/clinic-cases-orthodontics-en-09.jpg
      - /uploads/clinic-cases-orthodontics-en-10.jpg
      - /uploads/clinic-cases-orthodontics-en-11.jpg
      - /uploads/clinic-cases-orthodontics-en-12.jpg
      - /uploads/clinic-cases-orthodontics-en-13.jpg
      - /uploads/clinic-cases-orthodontics-en-14.jpg
      - /uploads/clinic-cases-orthodontics-en-15.jpg
      - /uploads/clinic-cases-orthodontics-en-16.jpg
      - /uploads/clinic-cases-orthodontics-en-17.jpg
      - /uploads/clinic-cases-orthodontics-en-18.jpg
      - /uploads/clinic-cases-orthodontics-en-19.jpg
      - /uploads/clinic-cases-orthodontics-en-20.jpg
      - /uploads/clinic-cases-orthodontics-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER AND LOWER CROWDING  </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SKELETAL CLASS II MALOCCLUSION </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SEVERE CROWDING AND AESTHETIC BRACKETS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>EXTRACTION OF FOUR FIRST PREMOLARS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ECTOPIC MAXILLARY CANINE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CROWDING WITHOUT EXTRACTIONS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BIMAXILLARY DENTOALVEOLAR PROTRUSION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SELF-LIGATING BRACES </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ORTHODONTIC IN THE MIXED DENTITION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER DENTO-MAXILLARY DISCREPANCY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER EXTRACTION CASE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTAL DEFORMITY AND SEVERE CROWDING</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DEEP BITE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER  </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER  </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER   </h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">I BELIEVED DIFFICULT TO GET GOOD RESULTS IN A CASE AS COMPLEX AS MINE.
    I USED FIXED DEVICES PER YEAR AND A HALF SO AS TO THEY COULD OPERATE ME AND CORRECT
    THE POSITION OF THE MAXILLARY. NOW I AM ANOTHER, THE DOCTORS ALIGNED MY TEETH
    AND TRANSFORMED MY FACE. SOMETHING FANTASTIC!".</p>
  images:
    portrait: /uploads/quotes-orthodontics-portrait-en.jpg
    landscape: /uploads/quotes-orthodontics-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>

          <p>
            Contact us to study your case, plan the treatment and be able to issue you a tight budget. We guarantee that by visiting the city of Caracas you can save up to 70% on High-End dental treatments.
          </p>

          
        </div>
      image: /uploads/aside-dental-tourism-map.png
      specialCaseImage:
        - name: aside-dental-tourism-map.svg
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-orthodontics-invisalign.jpg
      content: >
        <h1 class="bebas">A HAPPY AND TRANSPARENT FINISH!</h1>
        <p>When removing the brackets, it is always
          necessary to use an artificial device that keeps the teeth in their new position.
          For being almost invisible, comfortable and easy to use, a vacuum thermoformed
          transparent plastic retainer is always our best recommendation.</p>
        <br/>
      footer:
        icon:
          display: true
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/sections-orthodontics-asthetic-braces.jpg
      content: >
        <h1 class="bebas">AESTHETIC BRACES</h1>
        <p>Thanks to their color, texture and transparency
          Porcelain or Sapphire Crystals Aesthetic Brackets mimetize with dental enamel
          and are particularly suitable for those persons, who due to their work or social
          activity, want a very low profile treatment but of proven clinical quality.</p>
        <br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: true
          to: /en/specialties/orthodontics/aesthetic-braces/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-orthodontics-brackets-care.jpg
      content: >
        <h1 class="bebas">TAKE CARE OF YOUR BRACKETS!</h1>
          <p><b>Indications for people with fixed appliances.  </b></br></br>Never
        forget that an adequate oral hygiene technique and the permanent collaboration
        of the patient with his treatment, are indispensable requirements for its success.</p>
        <br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: true
          to: /en/specialties/orthodontics/take-care-of-your-brackets/
          placeholder: <span>MORE INFO</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
