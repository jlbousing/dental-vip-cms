---
templateKey: specialties-page
language: en
redirects: /especialidades/implantes-dentales/
title: Dental Implants
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-dental-implants.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 64%
  content:
    position: center
    body: >
      <h1 class="bebas">DENTAL IMPLANTS</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-dental-implants"></i></h1>
    <p class="thin"><em>The use of Osseointegrated Dental Implants in the Oral Rehabilitation of
    partially or totally edentulous patients has been fully consolidated as a reliable
    and highly predictable alternative in the long term.</em></p>

# Description Section
article:
  content: >
    <p>Undoubtedly, one of the most spectacular advances of the modern dentistry
    is the development of Dental Implants. These, <b>are small titanium devices in
    the form of a cylinder and that, due to their high degree of biocompatibility,
    are capable of osseointegrate into the maxillary structures to “ideally” replace
    teeth lost</b> due to decay, trauma and periodontal disease; thus avoiding the
    use of old ones removable prostheses.</p> <p>With the LAST GENERATION IMPLANTS
    <b>the surgical procedure of placement is quite simple, fast and painless.</b>
    However, it must always be performed by Specialists in Oral or Maxillofacial Surgery,
    since the technique to be used is quite sensitive and requires extensive surgical
    and anatomical knowledge of tissues and oral structures.</p> <p><b>Another essential
    factor in Advanced Implantology is the restorative aspect.</b> If the prosthesis
    that is then placed on the implant is not well designed, the harmful forces generated
    by chewing will inevitably lead to its failure. Hence the importance and necessity
    of MULTIDISCIPLINARY TREATMENT in Oral Rehabilitation with Osseointegrated Dental
    Implants. <b>In our clinic the second phase of the treatment or prosthetic phase
    is always in charge of the Prosthodontics Specialist Dentist</b>, since no one
    better than him is able to handle the complicated principles and foundations of
    occlusion physiology, that is to say, in the way that implant and prosthesis should
    related to the rest of the teeth when the patient comes into function to eat and
    chew.</p> <p>Finally, and in pro of the long-term success of the treatment, it
    is also imperative to highlight the importance of <b>complying with a good consensual
    periodontal maintenance protocol</b>, which at 6-month intervals, allows us to
    keep healthy gums and prosthetic elements in perfect aesthetic and structural
    condition.</p>
  img: /uploads/aside-dental-implants.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      It is truly gratifying to perceive day by day how Dental Implants improve
    people''s health and quality of life. The fact of being able to eat and chew with
    comfort, speak normally and smile without fear; are benefits of invaluable repercussion.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Oral Surgeon
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-dental-implants.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-dental-implants.jpg
  mobilePosition: '{"mobilePosition320":"-884px","mobilePosition360":"-1021px","mobilePosition375":"-1276px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>What is osseointegration?</h4>
      content: >-
        <p>It is defined as the biological process by which a functional ankylosis
        is achieved between an implant and its recipient bone. The implants are
        made of titanium, biocompatible and osteoinducer metal, capable of
        stimulating osteoblast differentiation and initiating the process of
        healing and bone regeneration <em>(osteogenesis)</em> around it. This
        phenomenon is what allows an implant to fix firmly to the bone, remain
        stable over time and can be safely used as a base or pillar of dental
        prostheses.</p>
    - title: <h4>When are Osseointegrated Dental Implants indicated?</h4>
      content: >-
        <p>When it is necessary to replace one, several or even all the teeth of
        the mouth. Unitary implants avoid the need to have to carve, wear and
        compromise the adjacent teeth that ones have been lost to make a
        traditional fixed bridge. We place an implant, a crown and ready, in this
        way a much more biological and conservative rehabilitation is achieved.
        When several or all of the teeth are missing, multiple implants are placed
        and then individual crowns or exclusively implant supported fixed
        prostheses are cemented or screwed on them.</p>
    - title: <h4>Are there contraindications to implant treatment?</h4>
      content: >-
        <p>As a general rule, they are the same that prevent other surgical
        procedures such as blood clotting disorders, recent history of acute
        myocardial infarction or stroke, severe immunosuppression, malignant
        tumors undergoing chemotherapy or radiotherapy, severe rheumatoid
        arthritis, uncontrolled diabetes mellitus and bone diseases such as
        osteoporosis and osteomalacy. Also, it is important to assess other
        additional factors such as the intake of bisphosphonates in women,
        presence of acute periodontal infection and severe smoking habits.</p>
    - title: <h4>So they are contraindicated in smokers?</h4>
      content: >-
        <p>Cigarette does not constitute an absolute contraindication for implant
        rehabilitation, however, it is scientifically proven that smoking causes
        peripheral vascular vasoconstriction that significantly alters the process
        of bone healing and regeneration. So that in these patients the chances of
        success are reduced from approximately 98% to 65% of cases.</p>
    - title: <h4>How are they placed?</h4>
      content: >-
        <p>Through a small surgery, very little invasive, a perforation or
        surgical bed is created in the thickness of the alveolar bone. Next, the
        implant is introduced by screwing it under pressure, so that its surface
        is in intimate contact with the underlying bone tissue. Making simple
        analogy we can say that the surgical phase is quite similar to the way of
        placing a ramplug on a wall. In some cases it is not even necessary to
        take stitches.</p>
    - title: <h4>Is this procedure very painful?</h4>
      content: >-
        <p>Not at all!, as long as it is performed under a proper local anesthetic
        technique. Our Oral Surgeon handles excellent surgical protocols and
        powerful anesthetics that make the procedure simple, fast and painless.
        The postoperative period is usually very similar to a dental
        extraction.</p>
    - title: <h4>Any prerequisites for the intervention?</h4>
      content: >-
        <p>Almost the same as for any other oral surgery act. Good general health,
        a preoperative profile that assesses the ability of coagulation and tissue
        scarring and prophylactic premedication with broad-spectrum antibiotics,
        usually amoxicillin with clavulanic acid or cephalosporins in allergic
        patients.</p>
    - title: <h4>Other special surgeries may be necessary?</h4>
      content: >-
        <p>Unfortunately in some cases the quantity, quality and density of the
        recipient bone may be deficient and it is usually necessary to place
        autogenous or synthetic bone grafts to improve the local environment and
        avoid a possible failure. Another relatively common maneuver is the
        elevation of the maxillary sinus or SINUS LIFT, since many times its
        descent or pneumatization limits the placement of implants in the
        posterior sector of the upper jaw.</p>
    - title: <h4>Are all implants the same?</h4>
      content: >
        <p>No, they vary considerably in diameter, length, conicity, type of
        connection and surface treatment. Previous radiographic studies, conical
        beam volumetric tomography <em>(Cone Beam)</em> and the Surgeon's clinical
        experience are key factors for your choice. Likewise, it is important to
        use trademarks recognized and backed by long-term research studies that
        guarantee their performance and functional longevity. In our clinic, and
        according to their level of quality, we offer you medium-high and high-end
        dental implants.</p> <p>We do not work with low cost or clonic
        implants!</p>
    - title: <h4>What are immediate implants?</h4>
      content: >-
        <p>They are those that are placed at the same time of dental extraction to
        avoid resorption of the alveolar ridge and favor the aesthetic results of
        the final restoration. An additional advantage is that they significantly
        shorten the total treatment time, since it is not necessary to wait for
        the 4 or 5 months involved in the process of bone healing and neoformation
        of the dental alveolus after extraction.</p>
    - title: <h4>Are Dental Implants very expensive?</h4>
      content: >-
        <p>Expensive is something that implies giving an overestimated value to
        something that does not have it. Although by their nature <em>(pure
        titanium)</em> implants are more expensive than other dental alternatives,
        the fact of being able to eat and smile practically as with natural teeth
        makes them deserving of the highest value in any cost-benefit
        relationship.</p>
    - title: <h4>What is an All-on-Four system?</h4>
      content: >-
        <p>It is a technique that allows the total fixed rehabilitation with
        implants in the upper or lower jaw of the fully edentulous patient. It is
        based on the strategic placement of only four <em>(4)</em> implants, a
        fact that considerably reduces the final costs of the treatment.</p>
    - title: <h4>How aesthetic do implants look?</h4>
      content: >-
        <p>The implants are not visible, they are embedded inside the bone, so
        that the aesthetic responsibility lies with the definitive prosthesis.
        This should be made to meet all relevant functional requirements, and
        also, to provide the patient with teeth that look truly natural, of a
        color, shape and size that project harmony and proportionality,
        individually characterized and capable of reflecting and translucent
        light. These are all key factors for the aesthetic success of the
        treatment.</p>
    - title: <h4>What is the prosthetic phase?</h4>
      content: >-
        <p>It is the second phase of a rehabilitation with Dental Implants. It
        must always be in charge of the Prosthodontics Specialist, who will
        design, prepare and install the definitive implant-supported prosthesis,
        always fully respecting all biomechanical principles of occlusion and
        dental aesthetics. Individual crowns, fixed bridges, complete
        rehabilitations, hybrid prostheses and overdentures are the known
        alternatives.</p>
    - title: <h4>Are all implant prostheses fixed?</h4>
      content: >-
        <p>Although it is possible to make removable overdentures, we consider
        that the effort and investment involved in this type of procedure deserves
        a fixed prosthesis that truly improves the quality of life of the person.
        In our clinic, whenever possible, we try to make them all fixed, with the
        exception of the provisional ones used during the osseointegration
        period.</p>
    - title: >-
        <h4>Can the crown or fixed prosthesis be placed immediately, that is, on
        the same day as the surgery?</h4>
      content: >-
        <p>Yes we can with the so-called immediate loading implants, however, for
        this we must meet a series of ideal conditions sometimes difficult to
        find. The traditional and even more common protocol is that of deferred
        load, which includes a period of 3-4 months of waiting between the
        surgical and prosthetic phases to allow complete maturation of the
        peri-implant bone tissue <em>(osseointegration)</em>. During that period a
        removable provisional prosthesis is usually made to temporarily solve the
        aesthetic problem.</p>
    - title: "<h4>Is it a definitive treatment, for a lifetime?</h4>"
      content: >-
        <p>It would be irresponsible to offer a general estimate for all cases,
        however, there are patients who have been implant prostheses for more than
        30 years. Obviously everything depends on the general state of health,
        prior planning of the case, surgical implantation technique, device
        quality, aseptic procedure, correct distribution of masticatory loads,
        oral hygiene, patient habits and, very important; assistance to subsequent
        periodic reviews to rule out or treat on time incipient infectious
        processes, mismatches or failures of prosthetic components.</p>
    - title: <h4>Is there a possibility of rejection of a Dental Implant?</h4>
      content: >-
        <p>There is no described cases of allergy or toxicity to titanium in the
        literature, so there can be no rejection itself. It may happen that an
        implant is not properly osseointegrated by an infectious process or
        localized trauma <em>(usually by a premature or poorly developed
        prosthesis)</em> and it is necessary to replace it with another one to
        solve the problem. The success rate today, with High-End Dental Implants,
        is greater than 98% of cases.</p>
    - title: <h4>What is peri-implantitis?</h4>
      content: >-
        <p>Implants and natural teeth are so similar that they are susceptible to
        the same periodontal diseases. Just as there is periodontitis, there is
        peri-implantitis, both bacterial and inflammatory diseases that destroy
        the alveolar bone, cause mobility of teeth and implants, and in extreme
        cases; loss of them.</p>
    - title: <h4>How then to prevent it?</h4>
      content: >-
        <p>The first thing is to make sure the preparation and capacity of the
        professionals who will carry out your treatment. For this you do not need
        a dentist, you need a WORK TEAM consisting of a Surgeon, a Prosthodontist
        and a Periodontist who dominate and have experience in the field. Then,
        your collaboration is indispensable. Oral hygiene is the fundamental
        pillar of any restorative procedure in Dentistry, and implants are no
        exception. If you achieve always keep your implants and gums free of
        dental plaque and food debris, go regularly to regular control
        appointments and keep yourself in good general health; it is very likely
        that you will enjoy for a lifetime the benefits of this Innovative Dental
        Alternative.</p>

cases:
  title: >
    <h1>Dental Implants - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-dental-implants-en-01.jpg
      - /uploads/clinic-cases-dental-implants-en-02.jpg
      - /uploads/clinic-cases-dental-implants-en-03.jpg
      - /uploads/clinic-cases-dental-implants-en-04.jpg
      - /uploads/clinic-cases-dental-implants-en-05.jpg
      - /uploads/clinic-cases-dental-implants-en-06.jpg
      - /uploads/clinic-cases-dental-implants-en-07.jpg
      - /uploads/clinic-cases-dental-implants-en-08.jpg
      - /uploads/clinic-cases-dental-implants-en-09.jpg
      - /uploads/clinic-cases-dental-implants-en-10.jpg
      - /uploads/clinic-cases-dental-implants-en-11.jpg
      - /uploads/clinic-cases-dental-implants-en-12.jpg
      - /uploads/clinic-cases-dental-implants-en-13.jpg
      - /uploads/clinic-cases-dental-implants-en-14.jpg
      - /uploads/clinic-cases-dental-implants-en-15.jpg
      - /uploads/clinic-cases-dental-implants-en-16.jpg
      - /uploads/clinic-cases-dental-implants-en-17.jpg
      - /uploads/clinic-cases-dental-implants-en-18.jpg
      - /uploads/clinic-cases-dental-implants-en-19.jpg
      - /uploads/clinic-cases-dental-implants-en-20.jpg
      - /uploads/clinic-cases-dental-implants-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>9 IMPLANTS AND UPPER PARTIAL HYBRID PROSTHESIS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FULL MOUTH REHABILITATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANT SUPPORTED FIXED PARTIAL PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>REPLACEMENT OF LOWER INCISORS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>All-On-Four </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANTS IN CORRESPONDENCE WITH 1.2 AND 2.2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>2 IMPLANTS AND LOWER FIXED BRIDGE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>HEALING CAPS AND 6 METAL-PORCELAIN CROWNS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>REMOVABLE OVERDENTURE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANTATION IN CORRESPONDENCE WITH 2.1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>HEALING SCREW AND ANTERIOR CROWN</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER WITH IMPLANTS AND CERAMIC CROWNS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UNITARY IMPLANT IN BICUSPID REGION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ABUTMENTS OR PROSTHETIC PILLARS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>METAL-ACRYLIC HYBRID PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SUPERIOR HYBRID DENTURE OVER 8 IMPLANTS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sinus Lift</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CONVENTIONAL FIXED PROSTHESIS AND UNITARY IMPLANT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANT SUPPORTED FIXED PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BIMAXILLARY FIXED REHABILITATION WITH DENTAL IMPLANTS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANT IN ZONE OF HIGH AESTHETIC COMMITMENT</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">NOW THAT I HAVE IMPLANTS I CANNOT UNDERSTAND HOW I HAVE BEEN SO LONG USING
    REMOVABLE DENTURES. FOR AN AFRAID FEAR OF A SMALL SURGERY, I SPENT YEARS HIDING
    MY SMILE, COVERING MY MOUTH WITH MY HANDS SO THAT THE PLASTIC AND THE HOOKS OF
    THE DENTURES WERE NOT SEEN".</p>
  images:
    portrait: /uploads/quotes-dental-implants-portrait-en.jpg
    landscape: /uploads/quotes-dental-implants-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-hexagon-dental-implants.jpg
      content: >
        <h1 class="bebas">WHY ZIMMER BIOMET?</h1>
        <p>Our brand of preference for being one of
        the 5 best worldwide among the more than 500 currently marketed. Because we
        know that High-End Dental Implants are a guarantee of excellence, quality and
        longevity!</p><br/>
      footer:
        icon:
          display: true
          img: /uploads/sections-icons-hexagon-dental-implants.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/sections-dental-implants-3d-diagnosis.jpg
      content: >
        <h1 class="bebas">3D DIAGNOSIS AND PLANNING</h1>
        <p>The appearance and development of advanced
        volumetric visualization techniques has meant for the dental community the possibility
        of accessing 3D reconstruction to safely determine the exact position in which
        each device should be implanted.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-hexagon-dental-implants.jpg
        link:
          display: true
          to: /en/specialties/dental-implants/3d-diagnosis-and-planning/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-dental-implants-implant-supported-restorations.jpg
      content: >
        <h1 class="bebas">IMPLANT SUPPORTED RESTORATIONS</h1>
        <p>After the osseointegration period, the implants
        are uncovered and the definitive prosthesis previously contemplated is fabricated.
        Depending on the case, it may be total or partial, fixed or removable and made
        of metal-porcelain, metal-acrylic, total ceramic or thermopolymerizable polyacrylic
        resin.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-hexagon-dental-implants.jpg
        link:
          display: true
          to: /en/specialties/dental-implants/implant-supported-restorations/
          placeholder: <span>MORE INFO</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
