---
templateKey: annex-page
language: en
redirects: >
  /especialidades/implantes-dentales/protesis-sobre-implantes
title: Implant Supported Restorations
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-implant-supported-prosthesis.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 65%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: List
  content: >

  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >

        <div class="content-wrapper">

        <div class="title-icon">
          <i class="icon-individual-crown icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Single crown</h2>
        </div>

        <p class="dv-srv-pr">Although clinical and laboratory work is much more
        complex than that of a dento-supported crown or cap <em>(on a natural tooth)</em>,
        it is the most basic restoration that can be made on a osseointegrated implant.
        They are indicated in cases of unit implants and can be made of metal-porcelain,
        Lithium Disilicate or Zirconia Oxide <em>(high dental aesthetics)</em>.</p>
          </div>
    - img: /uploads/implant-types-fixed-bridge.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-fixed-bridge icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Fixed bridge</h2>
        </div>
        <p class="dv-srv-pr">It is the ideal fixed option for edentulous gaps of
        3 or more contiguous teeth. The structure is made up of 3 or more crowns fused
        together and supported at its ends by 2 or more dental implants. They can be
        made with the same materials that are used for individual crowns.</p>
           </div>
    - img: /uploads/implant-types-full-rehabilitation.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-full-rehabilitation icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Full-arch rehabilitation</h2>
        </div>
        <p class="dv-srv-pr">It is the alternative A1 for the total absence of
        teeth. 10 or 12 ceramic crowns on the largest possible number of implants <em>(from
        8 to 12)</em>. Crowns can be individual, can be merged into sections or be part
        of a single structure. This type of prosthesis is the most similar to natural
        dentition and when biting through it provides an unequaled sense of security
        and comfort, however, it requires a great economic investment and almost intact
        maxillary bone bases.</p>
         </div>
    - img: /uploads/implant-types-hybrid-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-hybrid-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Hybrid prosthesis</h2>
        </div>
        <p class="dv-srv-pr">A hybrid prosthesis is one that combines several materials.
        It is a single fixed structure that is screwed onto 4, 6 or 8 implants and usually
        replaces all the teeth of an arcade. It can be made of metal-porcelain or metal-acrylic,
        and that in cases of maxillary atrophy or severe bone resorption, it incorporates
        artificial pink gum to restore the occlusal vertical dimension, lip support,
        dental aesthetics and facial fullness of the person.</p>
             </div>
    - img: /uploads/implant-types-overdent.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-individual-crown icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Overdenture</h2>
        </div>
        <p class="dv-srv-pr">Snap on denture is a total prosthesis of acrylic resin,
        removable and that is anchored on 2, 3 or 4 implants. It is also known as "fit
        denture" because it is based on a male and female system for insertion and removal.
        The fact of being able to be removed by the patient considerably facilitates
        the process of oral hygiene, and although it is removable, the implants provide
        it great retention and stability compared to a conventional or muco-supported
        prosthesis.</p>
           </div>
# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">IMPLANT SUPPORTED RESTORATIONS</h1>
    <p>There is a lot of types. The indication
    of one or the other will depend on the number of missing teeth, bone and gum volume,
    facial fullness, number and arrangement of the implants, aesthetic factors, occlusion,
    antagonistic arcade, prosthetic space and economic considerations.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-implant-supported-restorations.jpg
  mobilePosition: '{"mobilePosition320":"-676px","mobilePosition360":"-770px","mobilePosition375":"-938px"}'
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>We Design Your Smile with CAD-CAM Computerized Technology</h1>
    <br>
    <br>
    <h2 >Our team of Dental Technicians has more than 20 years of experience
    in Implant-Assisted Restorations.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Abutment and prosthetic devices</h2>
      <p class="dv-div-text
        dv-pb-15">The abutment or prosthetic pillar is the piece
      that establishes the structural connection between the implant and the prosthesis.
      It is screwed into the implant, and then over it, the restoration is cemented
      or screwed too. It can be prefabricated or made to measure by a process of foundry
      and metal casting.</p><p class="dv-div-text text-right">A transfer or impression
      coping device and an analog or implant´s replica are also generally required to
      be able to successfully carry out the laboratory process.</p>
    image:
      display: true
      size: 100px
      src: /uploads/icons-abutment-es.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-implant-supported-restorations.jpg
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Other Procedures</h1>
  procedures:
    - title: >
        <h5> 3D DIAGNOSIS AND PLANNING </h5>
      to: "/en/specialties/dental-implants/3d-diagnosis-and-planning/"
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> CAD-CAM TECHNOLOGY </h5>
      to: "/en/specialties/prosthodontics/cad-cam-technology/"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> DIGITAL SMILE DESIGN </h5>
      to: "/en/specialties/aesthetic-dentistry/digital-smile-design/"
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> ORAL REHABILITATION </h5>
      to: >
        /en/specialties/prosthodontics/
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> AESTHETIC BRACKETS </h5>
      to: "/en/specialties/orthodontics/aesthetic-braces/"
      img: "/uploads/procedures-brackets.jpg"
    - title: >
        <h5>  PORCELAIN VENEERS</h5>
      to: "/en/specialties/aesthetic-dentistry/porcelain-veneers/"
      img: "/uploads/procedures-veneers.jpg"
---

<div class="container">
<div class="row">
<div class="item np left">

## Impression making

It is the procedure that allows us to register, copy and reproduce in negative
and with accuracy the artificial infra and mesostructures in order to obtain
the working model for the manufacture of the definitive prosthesis. To rehabilitate
implants we handle the concept of transfer and elastomeric materials such as
silicones by addition.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-restorations.jpg)

</div>
<div class="item np right">

## Screwed or cemented prosthesis?

It depends on the case. Each one has its specific indications and utilities.
However, the appearance of new special cements for implantological use has done
that, little by little, cemented prostheses gaining ground in the selection.

</div>
</div>
<div class="row">

<div class="item center">
<br/>
<br/>

## Occlusal balance

Without fear of being wrong, we can affirm that the occlusion we give to
an implant-supported restoration, especially if it is fixed, will represent
its life insurance. The implant does not have mechanisms or "fuse" sensors that
allow it, in the event of an overload, to emit an alarm signal; as is the case
with a natural tooth through the proprioceptive elements contained in its periodontium.
Hence the importance of achieving a perfect occlusion or bite in all restorative
treatments on osseointegrated implants.

</div>

</div>
</div>
