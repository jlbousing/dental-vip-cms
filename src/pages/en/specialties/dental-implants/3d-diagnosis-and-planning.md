---
templateKey: annex-page
language: en
redirects: >
  /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
title: 3D Diagnosis and Planning
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-3d-diagnosis-and-planning.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 38%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: bottom
  type: Column
  content: >
    <div></div>
  blocks:
    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h2></h2>

# Heading Section
heading:
  display: true
  content: <h1 class="bebas title"> 3D Diagnosis and Planning</h1>
    <p>Nowadays it is routine to transfer
    treatments designed virtually to the real clinical condition of our patients,
    a fact that has marked an unprecedented advance in the field of Oral Rehabilitation.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-3d-diagnosis-and-planning.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>3D: Advanced Implantology with Prosthetic Criterion</h1>
    <br>
    <br>
    <h2 >A true Multidisciplinary approach from beginning to end, Prosthodontics
    and Surgery in close relationship...</h2>
# anex-links
anexes:
  enforce: true
  display: true
  items:
    - img: /uploads/sections-3d-radiography.jpg
      content: >
        <h2>Maximum precision in the placement of your dental implants</h2>
        <p class="dv-srv-pr dv-srv-pr-45">DENTAL VIP incorporates the latest technology in its Oral Implantology procedures, the Cone Beam Volumetric Tomography <em>(CBVT-3D Digital Scanner)</em>, a tool of high clinical value that provides three-dimensional, accurate and high quality digital images.</p>
        <p class="dv-srv-pl text-right dv-npr">The use of this resource is essential for the surgical planning of the case, particularly when implants are to be placed in the posterior sector of the mandible and/or upper jaw, since it allows us to delimit the canal of the inferior dental nerve and know the exact distance between alveolar ridge and maxillary sinus, anatomical structures that must always be respected to prevent risks, complications and operative failures.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> LOAD FILE</span>
    - img: /uploads/sections-3d-study.jpg
      content: >
        <h2>A comfortable, fast and safe study...</h2>
        <ul>
          <li>
            <i class="icon-check circle"></i>

            Low radiation dose, much smaller than that of a conventional CAT scan.
          </li>
          <li>
            <i class="icon-check circle"></i>

           Short exposure time, less than three minutes.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Scanned with the patient in sitting posture.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Open tomograph that provides great comfort, avoiding feelings of confinement or claustrophobia.
          </li>
          <li>
            <i class="icon-check circle"></i>
           Information in DICOM format that allows, through special software, a unique procedure of visualization and therapeutic planning.
          </li>
        </ul>

      footer:
        icon:
          display: true
          img: /uploads/icon-caresteam.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Clinical advantages of the CONE BEAM in the practice of oral implantology</h2>
          <p class="dv-div-title text-right"></p> <p class="dv-div-text text-right">It allows
        to detect anatomical structures, evaluate the morphology, quantity and bone quality,
        make accurate measurements of the alveolar ridge in width, height and depth, determine
        if a bone graft or a maxillary sinus lift is necessary, make surgical guides,
        select the size and model of implants, optimize their location and reduce to the
        minimum the surgical risks.</p>
        <br/> <br/>
    image:
      display: true
      size: 120px
      src: /uploads/icon-implants-logo.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-3d-diagnosis-and-planning.png
# Procedures Section
procedures:
  title: >
    <h1 class="title"> OTHER PROCEDURES</h1>
  procedures:
    - title: >
        <h5>IMPLANT SUPPORTED RESTORATIONS</h5>
      to: "/en/specialties/dental-implants/implant-supported-restorations"
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5> ORAL REHABILITATION</h5>
      to: "/en/specialties/prosthodontics/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5>CAD-CAM TECHNOLOGY</h5>
      to: "/en/specialties/prosthodontics/cad-cam-technology"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5>PORCELAIN VENEERS</h5>
      to: "/en/specialties/aesthetic-dentistry/porcelain-veneers/"
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5>DIGITAL SMILE DESIGN</h5>
      to: >
        /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5>AESTHETIC BRACKETS</h5>
      to: /en/specialties/orthodontics/aesthetic-braces/
      img: "/uploads/procedures-brackets.jpg"
---

<div class="row container">
<div class="item np left">

## Mounting and diagnostic wax-up

Its objective is to try to reproduce the final result of the future prosthesis.
With it, the Prosthodontics Specialist will be able to determine the exact number
and position of the required implants and will prepare a thermoformed plastic
guide for the time of surgery.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-3d.jpg)

</div>
<div class="item np right">

## Guided intervention with surgical splint

The splint or surgical guide is a prototype, a physical device that orients
the Surgeon during the implantation act, allowing him to place each element
according to as planned, with millimetric spatial precision and adequate axial
inclination.

</div>
</div>
