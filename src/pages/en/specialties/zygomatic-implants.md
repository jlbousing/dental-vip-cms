---
templateKey: specialties-page
language: en
redirects: /especialidades/implantes-cigomaticos/
title: Zygomatic Implants
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-zygomatic-implants.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">ZYGOMATIC IMPLANTS</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-zygomatic-implants"></i></h1>
    <p class="thin"><em>Undoubtedly, the best alternative for those patients with Large Bone Loss
    in their upper jaws and in which it is no longer possible to place conventional
    dental implants. “An optimal solution to a complex problem.</em></p>

# Description Section
article:
  content: >
    <p> When one or more teeth are lost, and they are not replaced as soon
    as possible, a slow but progressive process of reabsorption at the level of the
    maxillary bone is started immediately which reduces the amount of bone structure
    available, and necessary, for the placement of dental implants. "What is not
    used is atrophied", and it is precisely what happens to the EDENTULOUS MAXILLARY
    ALVEOLAR PROCESS, naturally, as a result of the lack of stimulation due to the
    absence of functional loads. So   <strong>    if a lot of time elapses between
    tooth mutilation and the intention of rehabilitation, it is very likely that by
    then there is no longer enough bone material capable of guaranteeing good primary
    anchorage and adequate osseointegration of conventional titanium devices,</strong>
     that a once implanted, they should function as artificial substitutes for dental
    roots and solid supports for the fixation of prosthetic structures.</p><p>
     Fortunately in many cases, when the defect of the alveolar ridge is mild and
    located, it is possible to resort without problems to the placement of autologous
    or synthetic GRAFTS as a prior or simultaneous step to the placement of dental
    implants. However, if the bone loss is very large and widespread, these procedures
    usually become very uncomfortable, complex, extensive and unpredictable; since
    they imply the need to generate a large body wound at the level of the pelvis
    or head to collect iliac crest or calotte bone blocks', high chances of rejection
    due to the own cruelty of the surgical technique and a period of tissue healing
    that could involve to 5 or 6 months of waiting, before being able to continue
    with the treatment.</p> <p>Faced with such eventualities, <strong>it is often
    preferable to forget about the maxillary and grafts, and fix the implants to other
    bones of the orofacial region</strong>, being by their compact structure and anatomical
    proximity the MALAR OR ZYGOMATIC <em>(which forms the cheekbone)</em> and Sphenoid
    bones, those of preference. So <strong>Zygomatic Implants are nothing more than
    special dental devices, much longer than conventional ones</strong> <em>(between
    30 and 55 mm. in length)</em> and that allow us to make fixed dentures without
    inconveniences even in cases of severe or advanced maxillary atrophy; and that
    is why, in conjunction with Pterygoid Implants <em>(very similar to Zygomatics
    but that anchor at the level of the pterygomaxillary suture)</em>, they are also
    known in popular slang as <strong>"Dental Implants for Patients with Little Bone".</strong>
    And it is that in short, the relative simplicity of their implantation <em>(in
    relation to the placement of multiple grafts)</em>, the absence of morbidity of
    a donor area and a much shorter and favorable period of healing for the patient,
    suggest to these LONG IMPLANTS as a simpler and safer alternative for extreme
    cases, compared to other bone regeneration and oral implantology techniques.</p>
  img: /uploads/aside-zygomatic-implants.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      The treatment with Zygomatic Implants is a procedure that involves a Maxillofacial
    Surgery intervention, in a surgical block of hospital environment, general anesthesia
    and all the life support resources that, unquestionably, merit the case.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Maxillofacial Surgeon
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-zygomatic-implants.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-zygomatic-implants.jpg
  mobilePosition: '{"mobilePosition320":"-171px","mobilePosition360":"-171px","mobilePosition375":"-171px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>What does it consist of the study and treatment planning phase?</h4>
      content: >-
        <p>It is actually very broad and covers many considerations, but one of the
        most relevant is the assessment of the state of the patient's bone and
        dental structure through a 3D volumetric tomography. This test provides the
        Specialist with valuable information and allows him, a priori, to verify
        that the person cannot really be treated with conventional dental
        implants.</p>
    - title: <h4>Which patients can undergo this oral implantology technique?</h4>
      content: >-
        <p>In principle, all those who have lost teeth and a lot of bone volume in
        their maxillary structures, and of course; aspire to a rehabilitation of
        much better quality than any option with removable dentures can offer
        them.</p>
    - title: <h4>What are the causes of bone loss?</h4>
      content: >-
        <p>They are multiple, but among the most common are age, chronic periodontal
        disease, odontogenic infections, accidents and facial injuries, traumatic
        extraction of teeth and broken roots, prolonged use of removable dentures,
        cysts and respiratory or paranasal sinus infections, resection of tumor
        processes, and of course; physiological resorption as a result of the lack
        of interest or inability to replace lost teeth in a reasonable period of
        time.</p>
    - title: <h4>Is there an age limit to qualify for the procedure?</h4>
      content: >-
        <p>None, as long as the life expectancy of the person justify the investment
        of time and money. In fact, many of our patients are over 65 years old.</p>
    - title: <h4>What percentage of success does it currently have?</h4>
      content: >-
        <p>Extraordinary!, superior to conventional implants and very close to 100%
        of cases, with an average survival rate of 95% at 10 years.</p>
    - title: <h4>Is it a painful treatment?</h4>
      content: >-
        <p>Not at all, since it is almost always done under general anesthesia. In
        addition, the postoperative period is usually very bearable, with few
        discomforts and allows the patient to rejoin his daily life in a very short
        time. Even so, the prescription of an analgesic protocol completely
        individualized will be routine and will ensure effective control of
        post-surgical pain in the most rebellious cases.</p>
    - title: <h4>Could it be done only with sedation and local anesthesia?</h4>
      content: >-
        <p>Occasionally, and only in very favorable surgical conditions. However,
        for your peace of mind and ours, at DENTAL VIP we never belittle the
        delicacy of the procedure and always prefer general anesthesia in surgical
        block and hospital environment for this type of interventions.</p>
    - title: <h4>What material are these implants made of?</h4>
      content: >-
        <p>Of titanium alloys for high performance. The degree of purity of titanium
        is determined by the maximum percentage of oxygen in which it is produced,
        being the grade 1 the highest purity, since its oxygen and iron content is
        very low. In total and today, there are about 40 degrees to name the
        different titanium alloys, ranging from pure titanium to combinations with
        vanadium, palladium, ruthenium, aluminum, tin and molybdenum. However, grade
        5 titanium alloy currently represents for more than 50% of total titanium
        use worldwide and is considered a highly biocompatible material with
        biomechanical qualities superior to pure titanium.</p>
    - title: <h4>What are their real advantages?</h4>
      content: >-
        <p>Compared to traditional oral implantology techniques, which contemplate
        heroic alveolar reconstructions with multiple autologous grafts,
        <strong>Long Implants</strong> offer the following advantages:</p> <ol> 
        <li>Avoid the high risk of rejection associated with maxillary grafts. 
        </li>  <li>They considerably reduce the total treatment time.  </li> 
        <li>They involve a much more conservative, safe and predictable procedure. 
        </li>  <li>Higher chance of success, since the malar bone is very compact
        and virtually immune to bone resorption <em>(not so the grafts)</em>.  
        </li>  <li>Lower morbidity, since it is not necessary to intervene on
        extraoral anatomical regions to collect bone.  </li>  <li>High possibility
        of immediate loading <em>(fixed prosthesis the same day)</em>.  </li> 
        <li>Shorter and more favorable postoperative for the patient.  </li> 
        <li>Best short, medium and long term forecast.  </li>  </ol> <p>In addition,
        the Zygomatic Implants protocol guarantees the barrier function,
        biomechanical stability and correct distribution of the masticatory forces
        through the maxillomalar pillars; thus avoiding overloads and occasional
        fatigue failures. </p>
    - title: <h4>What risks and contraindications exist?</h4>
      content: >-
        <p>Most studies cite sinusitis and oroantral communication as the most
        frequent postoperative complications, with an incidence of 4.93 per 100
        implants placed. However, it is considered a prevalence so low that it
        further corroborates the high success rate achieved with these implants. In
        relation to contraindications, we have that, without taking into account any
        systemic condition or disease that could prevent a surgical act, they are
        very few, and generally all associated with pre-existing pathologies or
        infections within the sinus or maxillary antrum. So that in some cases,
        interconsultation and prior treatment with an Otolaryngologist could be
        essential. </p>
    - title: <h4>How is the surgery and how long does it last?</h4>
      content: >-
        <p>The 4 most prominent techniques are that of Brånemark, that of Stella and
        Warner <em>(Sinus Slot Technique)</em>, the paramaxillary or extrasinusal
        technique and the ZAGA <em>(Zygoma Anatomy-Guided Approach)</em> technique
        of Dr. Carlos Aparicio. In the classical or Brånemark technique, Zygomatic
        Implants are placed maneuvering through the maxillary sinus, until the
        zygomatic arch is reached. In the Stella and Warner's, a more direct
        visualization of the base of the malar bone is achieved, thanks to the
        opening of a large window in the lateral wall of the paranasal antrum. In
        the extrasinusal, the implants are placed by the outside the maxillary bone,
        without involving the sinus; and in the ZAGA methodology, any possible
        combination of the 3 previous techniques is contemplated, in order to
        perform a more individualized surgery, adapted to the particular anatomy of
        each person.</p> <p>Logically, depending on the complexity of the case,
        number of devices to be implanted, type of approach selected, and of course,
        the skill of the Surgeon; between 30 and 120 minutes is the period of time
        necessary to anesthetize, make incisions, dissect tissues, stipulate
        measures and approximations, select implants, mill, drill, screw, clean and
        suture the wound.</p>
    - title: <h4>Can anyone Specialist perform this type of intervention?</h4>
      content: >-
        <p>This procedure, although not exclusive to our clinic, is not as common in
        general practice, because very few Dentists are truly trained to provide
        these treatments. The needed surgical skill for the placement of Zygomatic
        Implants requires a high level of experience, impeccable mastery of the head
        and neck anatomy and a very advanced surgical training. So only a
        Maxillofacial Surgeon, really experienced, will be the right
        professional.</p>
    - title: <h4>Could I have any mark or scar on my face?</h4>
      content: "<p>Never!, since the procedure is executed entirely by intraoral route.</p>"
    - title: <h4>Is it then necessary to remain hospitalized?</h4>
      content: >-
        <p>Very rarely, and only in case of any unforeseen or surgical complication
        that requires special care.</p>
    - title: >-
        <h4>How is the postoperative period and how long does it take for the
        patient to recover?</h4>
      content: >-
        <p>The postoperative period does not usually involve major complications, as
        long as the patient scrupulously complies with the indications provided. The
        guidelines to follow may vary according to each intervention, the type of
        approach used and the particular conditions of each case; but medication
        <em>(antibiotics, analgesics and anti-inflammatories)</em>, local
        application of cold, rest and oral hygiene are essential to achieve a
        recovery as quickly and satisfactorily as possible. Usually, between 2 and 5
        days is the time it takes for most people to restart their usual
        activities.</p>
    - title: <h4>Are these implants more expensive than normal implants?</h4>
      content: >-
        <p>Undoubtedly!, taking into account that they are up to 4 times longer than
        conventional ones and much more titanium is needed for their manufacture,
        they require operating room and general anesthesia, instruments and surgical
        components of advanced design, a longer and more invasive major surgery
        intervention and the participation of an expert Maxillofacial Surgeon.</p>
    - title: <h4>What type of prosthesis can next be made?</h4>
      content: <p>Usually a hybrid prosthesis of metal-acrylic or metal-porcelain.</p>
    - title: >-
        <h4>Are they only used to fix complete dentures, that is, when all teeth are
        missing?</h4>
      content: >-
        <p>Yes in the past, since it was a procedure designed for this purpose, with
        4 implants in total, 2 on each side of the upper jaw. However, at present
        modifications of the original technique are applied that allow the Zygomatic
        or Pterygoid Implant to be placed in a suitable position and compatible with
        a rehabilitation based on fixed partial prosthodontics <em>(crowns and
        ceramic bridges)</em>. In fact, nowadays are multiple possibilities offered
        by these implants, from the placement of a single asymmetric device, to
        countless combinations with conventional implants, logically to solve the
        vast majority of cases, and in some of them, even to reduce costs.</p>
    - title: >-
        <h4>Is it possible to place a fixed prosthesis the same day of the
        intervention, even if it is a provisional one?</h4>
      content: >-
        <p>Many times yes, since the multicortical anchor and the proven apex design
        of these implants generally provide great primary stability. However, it is
        only after surgery that it will be decided if a fixed or conventional
        prosthesis with relief in the areas where the intraoral platforms emerge is
        installed. Then, after the osseointegration period, elapsed 4, 5 or 6
        months, the final prosthetic rehabilitation is performed.</p>
    - title: >-
        <h4>After the treatment, what should I do to keep my implants for a
        lifetime?</h4>
      content: >-
        <p>Basically keep yourself in good general health, practice very good oral
        hygiene and go to the Dentist regularly. In these cases, it is highly
        recommended to carry out a lifelong follow-up <em>(clinical and
        radiographic)</em> of the patient, in order to detect in time any incipient
        infectious process or mismatch of the prosthetic components that could
        generate major evils. In addition, we usually recommend removing the
        prosthesis every 6 months to inspect and clean the implants and prosthetic
        attachments, the soft tissue that surrounds them and the base or inner face
        of the prosthetic structure. </p>

cases:
  title: >
    <h1>Zygomatic Implants - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-zygomatic-implants-en-01.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-02.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-03.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-04.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-05.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-06.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-07.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-08.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-09.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-10.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-11.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-12.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-13.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-14.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-15.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-16.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-17.jpg
      - /uploads/clinic-cases-zygomatic-implants-en-18.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>UNILATERAL ZYGOMATIC IMPLANT</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>IMMEDIATE LOADING</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>MIXED IMPLANTATION AND IMMEDIATE FUNCTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>POST-SURGICAL CONTROL AND DEFINITIVE PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>AESTHETIC RESOLUTION IN ATROPHIC ALVEOLAR RIDGE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>ZAGA TECHNIQUE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>SURGICAL AND POST-SURGICAL ASPECTS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>BRÅNEMARK'S CLASSIC TECHNIQUE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>METAL-ACRYLIC HYBRID PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>REIMPLANTATION CASE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>IMPLANT SUPPORTED TOTAL DENTURE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>RESTITUTION OF FACIAL PLENITUDE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>ALL-ON-6</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>PARAMAXILLARY OR EXTRASINUSAL TECHNIQUE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>IMPLANTATION WITH PRF</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>START OF THE PROSTHETIC PHASE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>METAL-PORCELAIN FULL ARCH HYBRID PROSTHESIS</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">THIS TECHNIQUE IS A TRUE BLESSING, AND EVEN MORE AFTER AN EXPERIENCE LIKE
    MINE, IN WHICH BY UNKNOWLEDGE OR DESIDIA, A DENTIST OF MY COUNTRY CONDEMNED ME
    TO USE FOR YEARS A REMOVABLE DENTURES THAT WERE NOT HELD BY ANYTHING IN THE WORLD".</p>
  images:
    portrait: /uploads/quotes-zygomatic-implants-portrait-en.jpg
    landscape: /uploads/quotes-zygomatic-implants-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-3d-diagnosis.png
      content: >
        <h1 class="bebas">3D DIAGNOSIS AND PLANNING</h1>
        <p>We never plan surgery with a simple radiographic
        plate, since we consider essentials the use of Computerized Axial Tomography
        <em>(CAT)</em> and a computer program that allows us analyzing structures, interpreting
        data and determining the most favorable surgical projections.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/dental-implants/3d-diagnosis-and-planning/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-sedation.png
      content: >
        <h1 class="bebas">SEDATION AND GENERAL ANESTHESIA</h1>
        <p>Always under the responsibility of a Specialist
          in Anesthesiology, resuscitation and pain therapy, with the ability to title
          the drugs administered and expertise in airway management, monitoring of vital
          signs and application of resuscitation techniques.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/sedation-and-general-anesthesia
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-prosthesis.png
      content: >
        <h1 class="bebas">IMPLANT SUPPORTED RESTORATIONS</h1>
        <p>As with conventional dental implants, the
        options available when rehabilitating Zygomatic Implants are varied. Although
        the design and confection of metal-acrylic hybrid dentures it is usually the
        most common, porcelain structures apply with total ownership.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/dental-implants/implant-supported-restorations/
          placeholder: <span>MORE INFO</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
