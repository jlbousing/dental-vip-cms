---
templateKey: specialties-page
language: en
redirects: /especialidades/estetica-dental/
title: Aesthetic Dentistry
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-aesthetic-dentistry.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 47%
  content:
    position: center
    body: >
      <h1 class="bebas">Aesthetic Dentistry</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-aestetic-dentistry"></i></h1>
    <p class="thin"><em>Biomimetics is the art of creating Dental Restorations that are in Harmony
    with Nature, through knowledge, mastery and expert manipulation of the different
    ceramic and adhesive systems available.</em></p>

# Description Section
article:
  content: >
    <p>In today's society, where the image is so important, a pleasant appearance
    often means the difference between success and failure; personal and professional.
    So that <b>a Healthy and Beautiful Smile is not a luxury or a whim but almost
    a necessity. The range of treatments at our disposal to correct Dental Imperfections
    is increasingly wide and affordable</b>, and the results are becoming faster,
    safer and more accurate. Among them we have TEETH WHITENING systems based on hydrogen
    or carbamide peroxides photoactivatables with high intensity halogen lamps <em>(ZOOM<sup>®</sup>
    system)</em> that provide us with remarkable and immediate benefits.</p> <p>In
    addition, <b>in the Specialty it is already routine to work with digital photographs
    for the study and computerized planning of the odontoesthetic treatment plan</b>.
    This phenomenon has been called SMILE DESIGN and gives us the possibility of applying
    a more scientific and less subjective criterion when restoring decayed, fractured,
    pigmented, rotated, separated or malformed teeth. On the other hand, <b>new adhesive
    materials based on nanotechnology allow us for its great versatility to modify
    the shape, color and size of the teeth at pleasure. In very few appointments the
    result is obvious and the transformation looks spectacular</b>. For the most complex
    cases we have the so-called PORCELAIN VENEERS, that are thin sheets of ceramic
    material that adhere firmly to the dental structures and that are indicated for
    the correction of aesthetic defects of greater extent and severity.</p> <p><b>Nowadays
    Aesthetic Dentistry has no limits</b>, except for the creativity, skill and preparation
    of the professional staff.</p>
  img: /uploads/aside-aesthetic-dentistry.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      The genuine smile, also called Duchenne''s Smile, is one that arises spontaneously,
    sincerely and naturally in the face. It is undoubtedly an expressive sign of well-being
    that fosters sociability and promotes communication between people.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Filomena Montemurro Tafuri</strong>
    details: >
      <strong>
        Prosthodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-aesthetic-dentistry.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-aesthetic-dentistry.jpg
  mobilePosition: '{"mobilePosition320":"-967px","mobilePosition360":"-1090px","mobilePosition375":"-1476px"}'
  content: >
    <i></i>
accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: >-
        <h4>Why is the use of clinical photography in Aesthetic Dentistry
        important?</h4>
      content: >-
        <p>An intraoral photograph provides instant visualization of the teeth in
        real time. With it we can see details difficult to appreciate with the
        naked eye, and also, allows the patient to opinion and actively
        participate in the development of your treatment plan. Another advantage
        of serial photography is the storage of images for future comparisons
        before and after treatment.</p>
    - title: <h4>What is a smile design?</h4>
      content: >-
        <p>It consists of making a detailed digital study of the dental, gingival
        and facial characteristics of a person when smiling to identify what
        changes or modifications would be favorable, from the aesthetic point of
        view, to harmonize and beautify the appearance of his smile. The clinical
        execution of the design could be very conservative, from simple
        recontoring and teeth whitening, to more invasive interventions such as
        orthodontics, gum surgery, veneers making, total-ceramic crowns, and even;
        maxillofacial surgery. It all depends on the case and the patient's
        expectations.</p>
    - title: <h4>When can and should make a smile design?</h4>
      content: >-
        <p>When the patient is of legal age, he wishes to improve his physical
        appearance and not escape his aesthetic perception of reality. It is
        really scary to see people with teeth that simulate “piano keys”,
        exaggeratedly long, white and monochromatic, without harmony with their
        own parameters and features and elaborate without taking into
        consideration that each human being is an exclusive and individual entity.
        Smile design is an invaluable tool in the field of Cosmetic Dentistry, as
        long as it is used with ethics, wisdom and true aesthetic criteria.</p>
    - title: <h4>What parameters are used to choose the shape of the new teeth?</h4>
      content: >-
        <p>Race, sex, age, personality, size and shape of the face, biotype and
        gingival state, smile line, type of occlusion or bite, and of course; the
        patient's preferences and tastes. It is also very important to always work
        under the precept of the golden or divine proportion, try as much as
        possible that the new restorations keep in tune with one of the most used
        parameters in plastic surgery and aesthetic medicine, which suggests that
        a proportional ratio of 1/1,618 between the size of nearby and
        well-defined elements, it will guarantee success in the expression and
        projection of harmony, beauty and naturalness.</p>
    - title: <h4>When is teeth whitening indicated?</h4>
      content: >-
        <p>Like any aesthetic treatment, the indication of whether or not to
        perform a teeth whitening is determined by the patient's own perception
        and needs. This procedure allows us to remove stains and dental
        pigmentations of extrinsic origin and clarify the color tone of the entire
        smile.</p>
    - title: <h4>When is it contraindicated?</h4>
      content: >-
        <p>When the patient has cavities, active periodontal diseases or extreme
        dental sensitivity. In addition, in cases of intrinsic or excessively dark
        spots such as those caused by tetracyclines, internal pulp hemorrhages,
        alterations in the histological formation of enamel and dentin; and those
        caused as a result of corrosion of old metal restorations. In these cases,
        the realization of veneers or ceramic crowns would be indicated to achieve
        the desired color change.</p>
    - title: <h4>Why should a Dentist do it?</h4>
      content: >-
        <p>Home bleaching that is sold in stores and pharmacies has a much lower
        concentration than those used in consultation. This phenomenon is due to
        the fact that the dental trays for application are universal and not
        custom-made, a condition that makes it impossible to avoid contact of the
        bleaching peroxide with the gums and mucous membranes of the patient. If
        these products had a professional concentration, they would cause great
        damage and caustic lesions to periodontal tissues.</p>
    - title: <h4>How bleaches carbamide peroxide?</h4>
      content: >-
        <p>Upon contact with saliva, it decomposes into hydrogen peroxide and
        urea. Due to its low molecular weight, hydrogen peroxide easily penetrates
        enamel prisms and dentinary tubules, inside which it is metabolized by
        certain enzymes such as catalase, peroxidase and hydroperoxidase;
        releasing oxygen molecules as the final product that soften and eliminate
        pigments and interplasmic wastes. The high intensity blue light acts as a
        catalyst providing energy to the bleaching solution to accelerate its
        diffusion and oxidation within the dental structure.</p>
    - title: >-
        <h4>How much can teeth whiten and how long do the results obtained
        last?</h4>
      content: >-
        <p>The color of the teeth is genetically determined and evaluated with a
        standardized 15-tone color guide. Thanks to a bleaching with carbamide
        peroxide we can clarify between 1 and 10 shades, which means that a very
        yellowish or grayish smile can return to show off its original white. The
        results usually last between 2 and 7 years, everything depends on the
        dietary, hygienic and social habits of the patient. In smokers, regular
        consumers of dark drinks <em>(cola, coffee and tea)</em> and diets high in
        citrus fruits <em>(kiwi or pineapple)</em>, the duration of the result can
        be considerably compromised.</p>
    - title: <h4>Is teeth whitening a painful procedure?</h4>
      content: >-
        <p>At all! It is a conservative treatment that does not even require
        dental anesthesia, however, in many cases it can generate a temporary
        increase in dental sensitivity. This hypersensitivity is considered normal
        and is controlled with the use of special dentifrices and fluoride-based
        desensitizing gels.</p>
    - title: <h4>What is the dental reshaping or cosmetic contouring?</h4>
      content: >-
        <p>Over the years, the teeth wear out and suffer microfractures that alter
        their natural shape and age the appearance of the person. With only make
        small and accurate modifications of the edges and dental angles it is
        possible to immediately recover the lost morphology and the original
        appearance of the smile. It is a painless, fast, safe and very economical
        procedure.</p>
    - title: >-
        <h4>What are the most innovative restorative materials in Dental
        Aesthetics?</h4>
      content: >
        <p>In the last decade the development of dental biomaterials has been
        amazing. Today we have composites or dental resins composed of nanometric
        filling material that provide excellent physical and aesthetic properties,
        and with which it is possible to achieve anterior and posterior direct
        adhesive restorations <em>(made in the mouth by the Dentist)</em>,
        perfectly smooth and polished; of brightness, color and texture similar to
        tooth enamel. They are indicated for the treatment of caries, fractures
        and aesthetic defects of little extension.</p>

        <p>For the manufacture of veneers, crowns and inlays in the laboratory,
        the great novelty is the use of ceramics <em>(popularly called
        porcelains)</em> based on Zirconium Oxide <em>(ZrO<sub>2</sub>)</em> and
        Lithium Disilicate <em>(LS<sub>2</sub>)</em>, with very high resistance to
        bending and fracture that makes it possible to completely dispense with
        the use of base metal alloys, and consequently; drastically improve the
        transparency, fluorescence and opalescence properties of the restored
        teeth. Among the most used state-of-the-art ceramics are:
        DC-Zircon<sup>®</sup> <em>(DCS)</em>, Cercon<sup>®</sup>
        <em>(Dentsply)</em>, In-Ceram<sup>®</sup> YZ <em>(Vita)</em>,
        Procera<sup>®</sup> Zirconia <em>(Nobel Biocare)</em>, Lava<sup>®</sup>
        <em>(3M Espe)</em> and IPS e.max<sup>®</sup> <em>(Ivoclar)</em>; among
        others.</p>
    - title: <h4>What are inlays and onlays?</h4>
      content: >-
        <p>They are a type of indirect restorations <em>(manufactured in the
        laboratory)</em> that are used to aesthetically reconstruct endodontically
        treated or heavily destroyed posterior teeth. Represents an excellent
        alternative to the typical dental crown and according to its extension
        they are classified as inlays, onlays and overlays. They are usually made
        with feldspathic or zirconia-based porcelains providing an extraordinary
        aesthetic appearance.</p>
    - title: <h4>What is a veneer?</h4>
      content: >-
        <p>Dental veneers are direct restorations made of composite, or fine
        porcelain sheets that adhere exclusively to the anterior surface or
        vestibular face of the teeth to improve their shape and appearance. Dental
        veneers are used to correct fractured, stained, misaligned, worn,
        separated or malformed teeth.</p>
    - title: "<h4>How do inlays, veneers and crowns differ?</h4>"
      content: >-
        <p>Inlays and veneers are partial coating restorations, that is, they only
        replace a part or portion of the tooth; while crowns restore or replace
        the entire visible part thereof. Veneers are indicated on the anterior
        teeth, the inlays and onlays on the posterior ones and the crowns on any
        tooth of the oral cavity.</p>
    - title: "<h4>What is then better for a front tooth, a veneer or a crown?</h4>"
      content: >-
        <p>It all depends on the clinical conditions and requirements of the case.
        Before making a decision, the Aesthetic Dentist must evaluate factors such
        as the degree of structural integrity of the organ, appearance of the
        neighboring teeth, properties and characteristics of the enamel, pulp
        vitality, periodontal state, oral hygiene index and social and functional
        habits of the patient; among others. In the end, each type of restoration
        has its specific indications and only a successful professional criterion,
        depending on the case, will give us the answer.</p>
    - title: <h4>How do these restorations stick to teeth?</h4>
      content: >-
        <p>Cementing is one of the most important steps because the long-term
        duration of restorations in mouth depends largely on it. It is essential
        that the clinician know and master the different adhesive systems for each
        ceramic material and each type of restoration. The cemented adhesive
        technique promotes the formation of strong chemical and mechanical bonds
        between tooth and restoration.</p>
    - title: <h4>Are all veneers of porcelain?</h4>
      content: >-
        <p>No! They can also be made of composite resins by direct technique, in a
        single session and without the intervention of the dental laboratory,
        however; they are much less aesthetic, less resistant and have a shorter
        shelf life than ceramic ones. The latters can remain up to 20 years in the
        mouth.</p>
    - title: "<h4>If by smiling I show a large amount of gum, what can be done?</h4>"
      content: >-
        <p>It depends on the case and its etiology. If the cause of the gingival
        smile is the over-eruption of the upper incisors, Orthodontics is the
        treatment of choice, to level the arch, intrude the teeth and achieve the
        apical migration of their marginal gum. If the problem is generated by an
        altered passive eruption, by a true excess of gum over the clinical crowns
        of the teeth, gingivectomy or surgical removal of the abnormal tissue will
        be the solution. However, if the condition is due to an alteration of the
        facial development, an excessive vertical growth of the upper jaw
        <em>(VME)</em>, only the surgical impact of the same through an
        intervention of Maxillofacial Orthognathic Surgery <em>(Le Fort I
        osteotomy)</em> will solve the problem.</p>
    - title: <h4>What is a periodontal plastic surgery or gingivoplasty?</h4>
      content: >-
        <p>Every smile is made up of 3 basic elements: teeth, lips and gums.
        Gingivoplasty is a procedure that allows correcting the shape, size and
        thickness of the gums, so that they look thin, delineated and in perfect
        harmony with the teeth. Similarly, it is also possible to almost
        completely modify the appearance, size and thickness of the lips through a
        small cosmetic surgery called Cheiloplasty, usually performed by Doctors
        Specializing in Orofacial Plastic Surgery.</p>

cases:
  title: >
    <h1>Aesthetic Dentistry - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-aesthetic-dentistry-en-01.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-02.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-03.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-04.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-05.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-06.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-07.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-08.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-09.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-10.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-11.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-12.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-13.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-14.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-15.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-16.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-17.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-18.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-19.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-20.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FRACTURE AND DENTAL MALPOSITION CORRECTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MAXILLARY LATERAL INCISORS RECONSTRUCTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>LITHIUM DISILICATE RESTORATIONS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TOTAL AESTHETIC REHABILITATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PERIODONTAL PLASTIC SURGERY AND LAMINATE VENEERS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IPS E.MAX<sup>®</sup> PORCELAIN VENEERS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CLOSING OF SPACES WITH COMPOSITE RESINS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>WHITENING AND CERAMIC VENEERS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>100% METAL FREE CERAMIC STRUCTURES</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SMILE DESIGN </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>LED WHITENING AND COMPOSITE VENEER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SILVER AMALGAM REPLACEMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>BEFORE AND AFTER</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">I AM WONDERFUL WITH MY NEW SMILE, THE CHANGE WAS DRASTIC AND SPECTACULAR.
    MY TEETH WAS VERY ROUND, SMALL, YELLOW AND SOMETHING SEPARATED. NOW THEY ARE BEAUTIFUL
    LIKE THOSE OF CELEBRITIES. NO DOUBT A TREATMENT THAT EXCEEDED ALL MY EXPECTATIONS".</p>
  images:
    portrait: /uploads/quotes-aesthetic-dentistry-portrait-en.jpg
    landscape: /uploads/quotes-aesthetic-dentistry-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-aesthetic-dentistry-zoom.jpg
      content: >
        <h1 class="bebas">FOR A WHITER AND BEAUTIFUL TEETH!</h1>
        <p>We have the best LED <em>(Light Emitting
        Diode)</em> cold light system for teeth whitening. In just a 45-minute session
        it is able to clarify the color of the teeth in up to eight shades, without
        any potential risk and with truly amazing results.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/aesthetic-dentistry/teeth-whitening/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-aesthetic-dentistry-dds.jpg
      content: >
        <h1 class="bebas">DIGITAL SMILE DESIGN (DSD)</h1>
        <p>For emotional patients who give great value
        to their appearance, for those who know well the effects of their smile on others
        and who wish to go beyond the strict and outdated concept of health as a simple
        absence of disease.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/aesthetic-dentistry/digital-smile-design/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-aesthetic-dentistry-veneers.jpg
      content: >
        <h1 class="bebas">PORCELAIN VENEERS</h1>
        <p>The aesthetic option par excellence to improve
        the general appearance of the smile or correct existing defects with conservative
        restorations that recreate the natural appearance of the teeth and provide a
        resistance comparable to tooth enamel.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /en/specialties/aesthetic-dentistry/porcelain-veneers/
          placeholder: <span>MORE INFO</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
