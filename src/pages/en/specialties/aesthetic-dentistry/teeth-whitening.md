---
templateKey: annex-page
language: en
redirects: >
  /especialidades/estetica-dental/blanqueamiento-dental/
title: Teeth Whitening
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-teeth-whitening.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 35%
  content:
    position: center
    body: >
      <i></i>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">TEETH WHITENING</h1>
    <p>It consists of the treatment of dental
    surfaces with chemical agents and high intensity light sources that remove and
    eliminate pigments that uglify the color of the smile.</p>

# Gallery Section
listGallery:
  display: true
  position: bottom
  type: Column
  content: >
    <h2>Your Teeth + White in Just 8 Steps:</h2>
    <p class="subtitle">
        Previous dental prophylaxis and
        polishing of the enamel with rubber cones and ultrafine grain pastes, will be
        very few the steps <br> that will separate you from a beautiful and radiant appearance:
    </p>
  blocks:
    - img: /uploads/teeth-whitening-list-01.png
      number: 1
      title: >
        <h3>Initial color registration</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">It must
          be done visually and always under a natural light source, without the presence
          of lipstick and with the help of a standardized guide of at least 15 shades.</p>

    - img: /uploads/teeth-whitening-list-02.png
      number: 2
      title: >
        <h3>Desensibilización</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">The application
          of a desensitizing agent 10 minutes before the procedure is very advisable
          to prevent, or at least relieve, the possible postoperative discomfort.</p>

    - img: /uploads/teeth-whitening-list-03.png
      number: 3
      title: >
        <h3>Gingival barrier</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">>Placement of a
          polyacrylic resin layer that protects the gums and soft tissues from chemical
          burns and caustic injuries during the procedure.</p>

    - img: /uploads/teeth-whitening-list-04.png
      number: 4
      title: >
        <h3>Dosage</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Preparation of the bleaching
          gel, generally based on peroxides, according to the concentrations previously
          defined in the evaluation, diagnosis and treatment planning phases.</p>

    - img: /uploads/teeth-whitening-list-05.png
      number: 5
      title: >
        <h3>Gel application</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">In abundant quantity
          and directly on the surfaces of the front or vestibular faces of the teeth,
          taking care not to exceed the limits of the protective barriers..</p>

    - img: /uploads/teeth-whitening-list-06.png
      number: 6
      title: >
        <h3>Photoactivation</h3><p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Through partial
          periods of 15 minutes and blue spectrum LED light with wavelengths between
          380 and 515 nanometers, and power equal to or greater than 400 mW/cm2.</p>

    - img: /uploads/teeth-whitening-list-07.png
      number: 7
      title: >
        <h3>Removal and cleaning</h3><p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Aspiration
          of the chemical with high suction machine, gingival barrier detachment and
          washing of the work field with water and abundant irrigation.</p>

    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h3>Final assessment</h3><p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Objective analysis
          of the results obtained. Quantification of changes in hue or chroma, value
          or luminosity and saturation or intensity.</p>

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-teeth-whitening.png
  content: >
    <i></i>

# Form Section
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

# Slogan Section
slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>Light Up Your Smile Today!</h1>
    <br>
    <h2 > Look younger and show off the fastest, safest and most demanded aesthetic treatment of the moment.</h2>

# Aside Section
anexes:
  display: true
  enforce: false
  items:
    - img: /uploads/sections-teeth-whitening-homemade.png
      content: >
        <h2>Teeth whitening at home</h2>
        <p class="dv-srv-pr dv-srv-pr-45">It is indicated in the simplest cases. The technique usually consists of using a gel and a transparent plastic splint custom made that is left on the teeth for a certain period of time, usually during the nights to sleep.</p>
          <p class="dv-srv-pr dv-srv-pr-45">However, performing the act only once will not yield greater results. It must be a constant process that can last several days or weeks, depending on the concentration of the product. Its main drawback is that the results tend to be unstable and not very durable. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-teeth-whitening-clinic.png
      content: >
        <h2>Teeth whitening in clinic</h2>
          <p class="dv-srv-pl dv-srv-pl-pr">Also called by light or photoactivation.
        This is a much more advanced procedure, in which in a single 45-minute session,
        notable results are usually achieved.</p><p class="dv-srv-pl dv-srv-pl-pr">The
        high degree of concentration of the peroxide used, makes it important to carefully
        protect the gums with a special barrier or cover. Next, the bleaching gel is
        applied, whose properties are increased thanks to the energy supplied by a high
        intensity LED light, which allows variable adjustments to provide maximum safety
        and comfort to the patient.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-teeth-whitening-mixed.png
      content: >
        <h2>Combined teeth whitening</h2>
        <p>It is surely the most
        effective of all, since it combines the benefits of photoactivation and at home
        whitening techniques.</p> <p class="dv-srv-pr dv-srv-pr-45">It usually runs
        in 3 phases. The first ambulatory, where the patient applies the bleaching product
        at home for a period of 10 to 15 days. The second in clinic, where one or two
        sessions are performed with high concentration photosensitive agents. And the
        third at home, where under strict professional supervision, small and sporadic
        touch-ups are indicated to guarantee the durability of the results obtained. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-teeth-whitening-intern.png
      content: >
        <h2>Internal whitening</h2>
        <p>It is an excellent alternative
        to treat dyschromias in non-vital teeth  <em>(with root canal treatment)</em>,
        since it represents a conservative and very economical intervention compared
        to other more complex procedures such as adhesive restorations, porcelain veneers
        and ceramic crowns. </p><p class="dv-srv-pr dv-srv-pr-45">It usually involves
        the endodontic approach of the tooth, sealing of the root canal, application
        of bleaching solutions of the highest concentration, washing and final filling.
        Several clinical sessions are usually required to achieve the desired color. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>MORE INFO</span>
# Article Section
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Benefits of a professional teeth whitening</h2></p>
      <p class="dv-div-text dv-pb-15">Undoubtedly, the main benefit is the personal
      satisfaction of having a more attractive dentition that allows the patient to
      feel comfortable and pleased with his smile, self-confident and better valued
      by society. In addition, it is irrefutable the fact that a white and neat teeth
      immediately project an appearance of good hygiene and personal care.</p><p class="dv-div-text
      dv-pb-15">However, aesthetic and psychological factors are not the only advantages,
      since numerous studies have demonstrated the effects of carbamide peroxide as
      a local antiseptic and its important action in reducing plaque and periodontal
      disease indexes; considerably promoting the health of teeth and gums.</p>
    image:
      display: true
      size: 40px
      src: /uploads/sections-icons-aesthetic-dentistry.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-teeth-whitening.png
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title">Other Procedures</h1>
  procedures:
    - title: >
        <h5>DIGITAL SMILE DESIGN</h5>
      to: >
        /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> PORCELAIN VENEERS</h5>
      to: >
        /en/specialties/aesthetic-dentistry/porcelain-veneers/
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5> ORAL REHABILITATION</h5>
      to: >
        /en/specialties/prosthodontics/
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5>  3D DIAGNOSIS AND PLANNING</h5>
      to: >
        /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> AESTHETIC BRACKETS</h5>
      to: >
        /en/specialties/orthodontics/aesthetic-braces/
      img: "/uploads/procedures-brackets.jpg"
    - title: >
        <h5> IMPLANT SUPPORTED RESTORATIONS</h5>
      to: >
        /en/specialties/dental-implants/implant-supported-restorations/
      img: "/uploads/procedures-dental-implants.jpg"
---
