---
templateKey: annex-page
language: en
redirects: >
  /especialidades/estetica-dental/diseno-digital-de-sonrisa/
title: Digital Smile Design
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-digital-smile-design-en.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 66%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: bottom
  type: Column
  content: >
    <div></div>
  blocks:
    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h2></h2>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">DIGITAL SMILE DESIGN (DSD)</h1>
    <p>It is a computing tool that allows
    us, through a digital photographic protocol, to record and study your proportions
    and dentofacial features, plan your ideal smile and simulate it graphically.</p>

parallax:
  display: true
  portraitPosition: center
  mobilePosition: '{"mobilePosition320":"-527px","mobilePosition360":"-600px","mobilePosition375":"-773px"}'
  img: /uploads/parallax-digital-smile-design.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consult Us Right Now!</h1>
  background: /uploads/parallax-form-annexed-pages.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>NOW YOU CAN SEE THE FINAL RESULT BEFORE STARTING YOUR TREATMENT!</h1>
    <br>
    <br>
    <h2 >Definitely, Smile Design forever revolutionized the studio
     and clinical practice of Aesthetic Dentistry.</h2>
# anex-links
anexes:
  enforce: true
  display: true
  items:
    - img: /uploads/sections-dds-shoot.jpg
      content: >
        <h2>Images capture</h2>
        <p class="dv-srv-pr dv-srv-pr-45">We are fully convinced
        that clinical photography is for Dental Aesthetics what radiography is for Dentistry
        in general. "A picture says more than a thousand words" and "a picture shows
        more than a thousand details”.</p>    <p class="dv-srv-pr dv-srv-pr-45">We
        use a digital reflex camera to record the patient's mouth and face from various
        angles and profiles. To do a good job it is essential that intra and extraoral
        photographs have high resolution, good lighting, sharpness and depth of field.
        Normally we handle under JPG format.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> LOAD FILE</span>
    - img: /uploads/sections-dds-processing.jpg
      content: >
        <h2>Computer processing</h2>
        <p class="dv-srv-pl dv-srv-pl-pr">>A professional image
        processing software allows us to measure and modify the position, shape and
        dimensions of teeth and gums in order to reproduce the best possible aesthetic
        result, of course, under the golden ratio criterion <i>(1/1.618)</i> and always
        within the biologically viable limits.</p>    <p class="dv-srv-pl dv-srv-pl-pr">In
        DENTAL VIP we never use templates or serial prototypes. Our designs are completely
        personalized, realistic and adjusted to the true clinical condition of the person.
        <strong>We only show what we can truly achieve!</strong></p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Digital mock-up</h2>
        <p>It is the final projection,
        simulation and photomontage of the new smile. In it, you will be able to appreciate
        in detail the benefits of each and every one of the interventions suggested
        by our team of Specialists. If there were still doubt, we would then proceed
        to obtain a real plaster cast to over it reproduce the design contemplated in
        laboratory wax and duplicate it directly in the patient's mouth with a special
        acrylic resin. This splint or acrylic prototype could be used for up to a week
        and be subjected to the consideration of friends and close family.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>

articleBlock:
  direction: reverse
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Advantages of our DSD protocol</h2>
      <ul>
      <li> <i class="icon-check circle"></i>It incorporates innovative and invaluable diagnostic and therapeutic planning tools. </li>
      <li><i class="icon-check circle"></i>Promotes the active and decisive participation of the patient in his treatment.</li>
      <li><i class="icon-check
      circle"></i>It allows us to experiment with different shapes, sizes and colors of teeth; before choosing the definitive ones.</li>
      <li><i
      class="icon-check circle"></i>It facilitates communication and interaction between our team of Dentists.</li>
      <li><i class="icon-check circle"></i>It makes possible the transmission of exact graphic indications to the dental technician.</li>
      <li><i class="icon-check circle"></i>It guarantees that the confection of indirect ceramic restorations be a faithful reflection of our professional wishes and criteria.</li>
      <li><i class="icon-check
      circle"></i>Highly predictable, accurate and satisfactory results.</li></ul>
    image:
      display: false
      size: 50px
      src: /uploads/sections-icons-aesthetic-dentistry.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-digital-smile-design.jpg
# Procedures Section
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>

procedures:
  title: >
    <h1 class="title">Other Procedures</h1>
  procedures:
    - title: >
        <h5>PORCELAIN VENEERS</h5>
      to: >
        /en/specialties/aesthetic-dentistry/porcelain-veneers/
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5> 3D DIAGNOSIS AND PLANNING</h5>
      to: >
        /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> ORAL REHABILITATION</h5>
      to: >
        /en/specialties/prosthodontics/
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5>IMPLANT SUPPORTED RESTORATIONS</h5>
      to: >
        /en/specialties/dental-implants/implant-supported-restorations
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5>CAD-CAM TECHNOLOGY</h5>
      to: >
        /en/specialties/prosthodontics/cad-cam-technology/
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5>AESTHETIC BRACKETS</h5>
      to: >
        /en/specialties/orthodontics/aesthetic-braces/
      img: "/uploads/procedures-brackets.jpg"
---

<div class="container">
<div class="row">
<div class="item np left">

## Approval and consent

The DSD concept involves the patient in the transformation process of his own smile, making him a co-designer of the treatment and allowing him to express his expectations to the work team, to finally achieve his absolute compliance and authorization for the clinical procedure.

</div>

<div class="item np right">

## Intraoral execution

Periodontal plastic surgery, teeth whitening, cosmetic contouring, adhesive restorations, veneers and total-ceramic crowns are the most common clinical interventions in a Smile Design. Usually, the participation of two or more Specialist Dentists is necessary.

</div>
</div>

![Hopper The Rabbit](/img/dds-custom-block-smile.jpg)

<div class="row">
<div class="item np center">

## Dental Morphology and Visagism: The Art of Customizing a Smile

It is unquestionable that to enhance the beauty of a person we must
always seek harmony between his visage, his facial features and the shape of his
teeth. But with the technique of Visagism we go even so further and seek to project
the patient's personality through the geometric pattern of his smile. Today we
know that our unconscious and emotional understanding tends to associate oval
shapes with sensitive and melancholic characters, triangular shapes with dynamic
and extroverted people, rectangular ones with strong and dominant temperaments
and square ones with serious and discrete individuals.

</div>

</div>

<div class="row fb">
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-oval.png)

<p class="dv-div-text">DOMINANT CENTRALS</p>
<p class="dv-div-text">ROUNDED CANINES</p>
<p class="dv-div-text">DELICATE LATERALS</p>
<p class="dv-div-text">ROUNDED ARCH</p>
      
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-triangular.png)

<p class="dv-div-text">ASCENDING SMILE LINE</p>
<p class="dv-div-text">CONVERGENT AXES</p>
<p class="dv-div-text">INCLINED CANINES</p>
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-rectangular.jpg)

<p class="dv-div-text">DOMINANT CENTRALS</p>
<p class="dv-div-text">FLAT INCISAL EDGES</p>
<p class="dv-div-text">AGGRESSIVE CANINES</p>
<p class="dv-div-text">VERTICAL AXES</p>
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dsd-custom-block-square.png)

<p class="dv-div-text">ABSENCE OF DOMINANCE</p>
<p class="dv-div-text">DIVERGENT AXES</p>
<p class="dv-div-text">HORIZONTAL LAYOUT</p>
</div>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
