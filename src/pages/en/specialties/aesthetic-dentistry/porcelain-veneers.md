---
templateKey: annex-page
language: en
redirects: >
  /especialidades/estetica-dental/carillas-de-porcelana/
title: Porcelain Veneers
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-porcelain-veneers.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 40%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: columnb
  content: >
    <h2>When are they indicated?</h2>
    <p class="subtitle">In the presence of aesthetic
    anomalies that compromise the appearance of the person. Below we present various
    <br>  clinical conditions susceptible to this type of dental restoration:</p>
  blocks:
    - img: /uploads/porcelain-veneers-1.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Diastemas or interdental spaces.</p>
    - img: /uploads/porcelain-veneers-2.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Pigmentations or irreversible stains.</p>
    - img: /uploads/porcelain-veneers-3.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Mild malpositions.</p>
    - img: /uploads/porcelain-veneers-4.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Black triangles or gingival embrasures.</p>
    - img: /uploads/porcelain-veneers-5.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Coronary fractures.</p>
    - img: /uploads/porcelain-veneers-6.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Alterations in shape, color and size.</p>
    - img: /uploads/porcelain-veneers-7.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Old or defective restorations.</p

    - img: /uploads/porcelain-veneers-8.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Need for permanent bleaching.</p>

# Heading Section
heading:
  display: true
  content: <h1 class="bebas title">PORCELAIN VENEERS</h1>
    <p>Veneers or porcelain facets are thin
    ceramic sheets between 0.8 and 1.5 mm. of thickness that, adhered to the front
    surface of the teeth, are capable of completely modifying their shape, texture,
    color and size.</p>

parallax:
  display: true
  portraitPosition: center
  mobilePosition: -595px
  img: /uploads/parallax-porcelain-veneers.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1> 10 Days will be Enough to Transform Your Life!</h1>
    <br>
    <br>
    <br>
    <h2 >Visit us and in only 3 or 4 work sessions we can design and create that smile you have always dreamed of.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Maximum adhesion and resistance!<</h2>
      <p class="dv-div-text
      dv-pb-15">We always entrust our work to the cementing agents
      of the 3M multinational. <strong>RelyX™ Ultimate </strong>is an innovative adhesive
      resin cement and dual polymerization that was developed thinking exclusively about
      the own needs of vitreous ceramics, and that consequently, guarantees us an excellent
      clinical performance. </p> <p class="dv-div-text dv-pb-15">Thanks to this new
      system, our patients can always be calm, confident and proud of their new smile.</p>
    image:
      display: true
      size: 40px
      src: /uploads/icon-relyx.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-porcelain-veneers.jpg
# Procedures Section
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
procedures:
  title: >
    <h1 class="title">Other Procedures</h1>
  procedures:
    - title: >
        <h5>DIGITAL SMILE DESIGN </h5>
      to: >
        /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5>3D DIAGNOSIS AND PLANNING </h5>
      to: >
        /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> ORAL REHABILITATION </h5>
      to: >
        /en/specialties/prosthodontics/
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> CAD-CAM TECHNOLOGY </h5>
      to: >
        /en/specialties/prosthodontics/cad-cam-technology/
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> IMPLANT SUPPORTED RESTORATIONS </h5>
      to: >
        /en/specialties/dental-implants/implant-supported-restorations/
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5> AESTHETIC BRACKETS</h5>
      to: >
        /en/specialties/orthodontics/aesthetic-braces/
      img: "/uploads/procedures-brackets.jpg"
---

<div class="row container">
<div class="item">

![Hopper The Rabbit](/img/gallery-blocks-dds-cut.jpg)

## Should be teeth preparated?

In 85% of cases it will be indispensable to make a little wear. Otherwise,
a restoration with unacceptable over-contour would be generated, or with insufficient ceramic´s thickness, weak, with high risk of fracture and unable to mask the aesthetic background defect.

</div>
<div class="item">

![Hopper The Rabbit](/img/gallery-blocks-dds-lasting.jpg)

## How many years will my veneers last?

Although the longevity of IPS e.max<sup>®</sup> ceramic will always
depend on multiple factors such as oral hygiene, compliance with periodic checkups
and control of occlusal habits, among others; we estimate that between 10 and
15 years will be the lifespan for the vast majority of our designs.

</div>
</div>
