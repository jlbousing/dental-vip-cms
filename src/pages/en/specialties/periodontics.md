---
templateKey: specialties-page
language: en
redirects: /especialidades/periodoncia/
title: Periodontics
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-periodonthics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 43%
  content:
    position: center
    body: >
      <h1 class="bebas">PERIODONTICS</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-periodontics"></i></h1>
    <p class="thin"><em>The main cause of Periodontitis is dental plaque, an accumulation of food
    debris and bacteria that is deposited on the surface of teeth and gums when the
    person lacks an adequate oral hygiene technique.</em></p>

# Description Section
article:
  content: >
    <p>This Dental Specialty is focusing in the prevention, diagnosis and treatment
    of all infectious diseases <em>(GINGIVITIS and PERIODONTITIS)</em> that attack
    the teeth fixation apparatus: gums, alveolar bone, cement and periodontal ligament;
    and which are responsible for up to 80 % of dental losses in subjects over 35
    years old. <strong>It is not enough with have healthy teeth, the health of the
    gums is as or more important than staying caries free.</strong></p> <p><strong>Usually,
    treatment involves the elimination and control of local infectious irritants such
    as bacterial plaque and dental calculus</strong> through deep tartrectomies <em>(cleanings)</em>
    and the teaching of proper oral hygiene techniques. In more advanced cases it
    is usually necessary to implement more complex surgical procedures that involve
    scaling&nbsp;and&nbsp;root planing, periodontal flaps, bone grafts, membranes
    placement and the so-called GUIDED TISSUE REGENERATION.</p> <p><strong>Periodontal
    Disease is almost always asymptomatic in its early stages, which is why it is
    so dangerous and destructive</strong>. When the first signs such as brushing bleeding,
    tooth separation and dental mobility appear; we are generally in the presence
    of advanced phases of it. <strong>Without specialized treatment all the support
    bone is lost and then the teeth will loosen and fall </strong><strong><br /> irremedibly</strong><strong>.</strong></p>
    <p>Another additional benefit of this Specialty is that we can obtain from the
    so-called PERIODONTAL PLASTIC SURGERY, that includes a set of techniques that
    allow the Dentist <strong>to modify the gum's contour, thickness and shape</strong>;
    achieving invaluable aesthetic effects for patients. Generally this type of surgery
    is complementary to treatments of orthodontics, whitening and dental aesthetics.</p>
  img: /uploads/aside-periodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Current scientific evidence links Periodontal Diseases (PD) with other systemic
    pathologies such as diabetes, chronic obstructive pulmonary disease, pneumonia,
    rheumatoid arthritis, kidney failure, obesity, metabolic syndrome, ischemic cardiovascular
    disease and pregnancy disorders.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Javier Martínez Téllez</strong>
    details: >
      <strong>
        General Dentist - Periodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-periodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-periodontics.jpg
  mobilePosition: '{"mobilePosition320":"-919px","mobilePosition360":"-952px","mobilePosition375":"-1241px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>How is a healthy gum?</h4>
      content: >
        <p>Under normal conditions, the gum is fine, thin, resilient, firmly adhered
        to the underlying bone tissue and arranged in such a way that its contours
        faithfully the cervical surface of the teeth; forming a well-defined
        scalloping. It must cover the entire root, be coral pink and have a pointed
        surface texture very similar to the peel of an orange.</p>
    - title: <h4>How are periodontal diseases classified?</h4>
      content: >
        <p>Although their scientific classification is extremely extensive and
        complex, it would suffice to know that of all periodontal diseases
        gingivitis is the most common, mild and with best prognosis; since its
        evolution only affects the marginal gum and its damage is completely
        reversible. But if it is not treated on time, it advances and attacks the
        alveolar bone, causing infection, reabsorption and loss of it
        <em>(Periodontitis)</em> with formation of periodontal sacs, pus and
        recession of the gum. When losing their support bone, the teeth gradually
        loosen until falling. Although Periodontitis can be localized or
        generalized, chronic, aggressive, necrotizing, associated with systemic
        diseases, drugs, endo-periodontal lesions or other acquired conditions; the
        most important thing is that the Specialist knows to recognize to which of
        the variants he faces to be able to establish an adequate and effective
        periodontal treatment that offers the greatest chance of success.</p>
    - title: <h4>What are their causes?</h4>
      content: >
        <p>They are essentially of bacterial etiology. Although the accumulation of
        dental plaque on the teeth is decisive for the onset and progression of
        periodontal disease, its severity and response to treatment will be the
        result of the interaction of a large number of modifying, contributing
        and/or predisposing factors. So, in the end, we can say that it is a
        pathology of multifactorial origin.</p>
    - title: <h4>At what age do they usually appear?</h4>
      content: >
        <p>With the exception of gingivitis, periodontal diseases are rarely
        diagnosed before 15 years old, although to do it, is under forms very severe
        and aggressive that seriously threaten teething, and even; the general
        health of the child. They usually appear in adults, starting their first
        manifestations at an early age, around 25 years old. The younger the
        individual is at the time of appearing, the more severe their evolution will
        probably be and complex their treatment. There is no doubt that the
        incidence of periodontopathies is directly proportional to the age of
        people.</p>
    - title: <h4>What other factors can trigger them?</h4>
      content: >
        <p>Tobacco, alcohol and stress. Also some medications and systemic diseases
        such as diabetes, osteoporosis or severe immunosuppression states.
        Defective, over-contoured or poorly adjusted restorations, crooked or
        crowded teeth <em>(malocclusions)</em> and poor oral hygiene are also
        factors that trigger and stimulate the pathogenic potential of periodontal
        diseases.</p>
    - title: >-
        <h4>Is there any relationship between periodontal disease and
        pregnancy?</h4>
      content: >
        <p>Pregnancy causes many hormonal changes that exponentially increase the
        risk of periodontal disease in women, and this in turn, is associated with
        preeclampsia, low birth weight and premature birth. Periodontal disease, by
        suppose a deposit of microorganisms and their toxic products, can trigger a
        response with systemic risk. Therefore, expectant women should always seek
        immediate care for periodontal disease and thus reduce the risk of pre and
        postnatal complications.</p>
    - title: <h4>How can  periodontopathies be prevented?</h4>
      content: >
        <p>Keeping gums healthy throughout life should not be as difficult as it
        seems. We only need constancy in the hygiene and care of our mouth, to
        prevent the formation of calculus and bacterial plaque. In addition,
        periodic visits to the Dentist are essential to achieve a deeper cleaning,
        especially in those places of difficult access for both, the brushes and
        dental floss.</p>
    - title: <h4>How can I know if my gum is already sick?</h4>
      content: >
        <p>The most obvious signs are spontaneous bleeding or to tooth brushing, the
        appearance of pus with a bad taste or smell of the mouth, redness or
        retraction of the gum, change in the position of the teeth, sensitivity,
        pain and dental mobility. The definitive diagnosis should always be
        accomplished by the Dentist after performing a thorough clinical examination
        and a complete radiological study.</p>
    - title: <h4>Is periodontal disease curable?</h4>
      content: >
        <p>It depends on the variant. Gingivitis is completely reversible and
        curable. However, different forms of periodontitis cause irreversible damage
        to the dental insertion apparatus. Considering the moderate and advanced
        variants of PD as chronic and controllable entities, but with a high risk of
        recurrence, it is difficult to speak of total curing. A patient with
        periodontitis, apart from learning, mastering and conscientiously applying
        the various oral hygiene techniques, must maintain a regular lifetime
        control with his Periodontist, to stabilize his condition over time and
        achieve what we know today as a "reduced but healthy periodontium"; a
        periodontium affected to some extent but with a capacity for regeneration
        and no evidence of active disease. Under this condition, natural dentition
        may be preserved for many more years.</p>
    - title: <h4>Can I recover lost bone?</h4>
      content: >
        <p>Sometimes the defects produced in the maxillary structure due to
        periodontal disease have very specific characteristics that enable its bone
        regeneration. Guided tissue regeneration procedures are applied in different
        ways, either in the form of filler material, resorbable membranes or with
        substances derived from organic proteins that stimulate bone growth. These
        procedures are complex and require a high professional qualification, but we
        reiterate, they are only possible in some very specific occasions; generally
        in well-defined vertical defects.</p>
    - title: <h4>Is the dental mobility produced by periodontitis reversible?</h4>
      content: >
        <p>Mobility usually persists despite periodontal treatment because,
        unfortunately, the general level of bone never recovers. The teeth that show
        mobility at the time of diagnosis could be lost in the short or medium term
        despite the treatment, and it is the reason that justifies the need for
        early diagnosis and control of the disease.</p>
    - title: <h4>Are antibiotics useful in general therapy?</h4>
      content: >
        <p>Antibiotics are substances capable of eliminating or inactivating
        bacteria, and periodontal disease as an infection, is susceptible to their
        mechanisms of action. However, and due to its chronic nature, it is not
        feasible to use them continuously in cases of periodontitis, since their
        prolonged use would generate bacterial resistance and unacceptable side
        effects on the organism. Currently, it is recommended to use antibiotics
        only against the most aggressive variants, in the acute phase, for a
        well-defined period of time and always with the help of a
        culture-antibiogram that reveals the most effective drug for each particular
        situation.</p>
    - title: <h4>What is the difference between a tartrectomy and a root planing?</h4>
      content: >
        <p>Prophylaxis, cleaning or tartrectomy are the same thing, they are three
        terms that are used interchangeably to refer to the professional removal of
        plaque and supragingival calculus, that one which is visible and that is
        formed on the exposed surface of the tooth, above the gum. However, in
        patients with bone loss we must do a much deeper cleaning that eliminates
        the periodontal sacs, the tartar that is inside them and the infected root
        cement. This procedure is known as scaling and root planing. Another variant
        is periodontal curettage, a surgical technique that goes beyond and not only
        removes infected cement, but also part of the soft tissue of the periodontal
        pocket&rsquo;s wall. It is a technique that is currently used only in very
        particular situations.</p>
    - title: <h4>How is a periodontal pocket or pathological sac?</h4>
      content: >
        <p>In health conditions, the gum joins the tooth through an epithelial
        insertion forming a groove <em>(gingival groove)</em> of between 1 and 3 mm.
        deep. A periodontal pocket is a degenerative lesion of the periodontium, an
        abnormal space <em>(greater than 4 mm.)</em> that forms between the gum and
        the tooth, at the level of the gingival groove; as a result of the constant
        accumulation of plaque and bacteria below of the gum. Due to their depth,
        the presence of pathological sacs completely impede the removal of dental
        plaque by the patient and favors the formation of tartar, the destruction of
        the supporting bone and the aggravation of periodontal disease.</p>
    - title: <h4>What does it consist of and when is a flap surgery indicated?</h4>
      content: >
        <p>A periodontal flap is a gum portion that is surgically separated from the
        underlying tissues to allow access, visibility and cleanliness of the bone
        and root surfaces. We could consider it essentially as an open subgingival
        curettage. It is one of the most used treatments in advanced periodontitis
        and its basic objectives are to remove calculus and plaque from inaccessible
        areas, eliminate or reduce periodontal pockets, ensure a more favorable
        tissue contour and increase the amount of attached gum. In addition, in some
        patients with severe retractions, exposed roots or mucogingival defects; a
        flap allows us to move the gum to different positions to correct lesions by
        default.</p>
    - title: <h4>When is a bone graft placed?</h4>
      content: >
        <p>The surgical techniques currently available only allow to graft bone in
        some types of periodontal lesions and, often, only partially. The unique
        defects in which the scientific literature has demonstrated the
        effectiveness of grafts are vertical type lesions <em>(circumscribed defects
        of 1, 2 or 3 walls) </em>and those of furca in lower molars. Unfortunately,
        regenerative procedures fail when attempts are made to treat horizontal
        lesions <em>(the most common in periodontal disease) </em>or any furca
        defect in the upper molars. Conversely, bone grafts are highly effective in
        increasing the quantity and quality of bone in the recipient bed as a
        procedure prior to the placement of dental implants.</p>
    - title: <h4>How are gingival recessions or retractions treated?</h4>
      content: >
        <p>The first thing, before performing any type of surgical intervention, is
        to eliminate the cause that has caused the gum retraction: aggressive
        brushing, excessive consumption of acidic substances, occlusal trauma,
        parafunctional habits and/or permanent accumulation of local irritants
        <em>(plaque bacterial and dental calculus). </em>Then, continuity is given
        to treatment with mucogingival surgeries such as coronal advancement flaps
        to &ldquo;stretch&rdquo; the gum and cover the exposed root and/or with
        sub-epithelial connective grafts taken from the palatine mucosa.</p>
    - title: <h4>What is the periodontal support or maintenance phase?</h4>
      content: >
        <p>It is the phase of periodontal therapy during which the disease is
        monitored to control its etiological and risk factors. It is considered from
        the clinical point of view, an active therapy, which should have the same
        professional attention and importance as the initial or attack therapy. Its
        main objective is to prevent the progression and recurrence of periodontal
        disease in patients who have been previously treated. It usually requires
        periodic visits, every 3 months at the beginning, to rule out the presence
        of local irritants, clinical signs of inflammation and to verify the full
        mastery over execution of the different oral hygiene techniques.</p>
    - title: <h4>What to do if I finally lose some teeth?</h4>
      content: >
        <p>If you went to treatment late or if your periodontitis could not be
        adequately controlled, you may lose or have lost teeth as a result of it. In
        that case, it is convenient that you replace them immediately to avoid
        functional overloads of those ones who still remain in the mouth, a fact
        that would significantly accelerate the general evolution of the disease.
        The best way to replace your teeth is by means of fixed prosthesis, since
        removable devices usually damage the gum and the remaining natural teeth, so
        they should be placed only if there is no possibility of making a fixed
        one.</p>
    - title: <h4>If I have periodontitis, can I opt for dental implants?</h4>
      content: >
        <p>Of course yes!, and it is in fact the ideal alternative, but never in
        cases of active periodontal disease. First it is necessary to treat and
        control it so that the implants behave equal and have the same chances of
        success as in a healthy patient. If we take into account, that from the
        periodontal point of view, dental implants are subject to the same risks of
        infection as natural teeth; we will understand why it is essential, to
        prevent implants from running the same fate, that a person who has lost
        their teeth due to failures of oral hygiene be trained and qualified in the
        techniques of brushing and flossing.</p>

cases:
  title: >
    <h1>Periodontics - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-periodontics-en-01.jpg
      - /uploads/clinic-cases-periodontics-en-02.jpg
      - /uploads/clinic-cases-periodontics-en-03.jpg
      - /uploads/clinic-cases-periodontics-en-04.jpg
      - /uploads/clinic-cases-periodontics-en-05.jpg
      - /uploads/clinic-cases-periodontics-en-06.jpg
      - /uploads/clinic-cases-periodontics-en-07.jpg
      - /uploads/clinic-cases-periodontics-en-08.jpg
      - /uploads/clinic-cases-periodontics-en-09.jpg
      - /uploads/clinic-cases-periodontics-en-10.jpg
      - /uploads/clinic-cases-periodontics-en-11.jpg
      - /uploads/clinic-cases-periodontics-en-12.jpg
      - /uploads/clinic-cases-periodontics-en-13.jpg
      - /uploads/clinic-cases-periodontics-en-14.jpg
      - /uploads/clinic-cases-periodontics-en-15.jpg
      - /uploads/clinic-cases-periodontics-en-16.jpg
      - /uploads/clinic-cases-periodontics-en-17.jpg
      - /uploads/clinic-cases-periodontics-en-18.jpg
      - /uploads/clinic-cases-periodontics-en-19.jpg
      - /uploads/clinic-cases-periodontics-en-20.jpg
      - /uploads/clinic-cases-periodontics-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>CONNECTIVE TISSUE GRAFT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>GENERALIZED CHRONIC PERIODONTITIS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>SUPERIOR DENTAL SPLINTING</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>1ST PHASE OF THE PERIODONTAL TREATMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>GINGIVAL RECESSIONS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>ALTERED PASSIVE ERUPTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>PERIODONTAL PLASTIC SURGERY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>GINGIVAL GRAFT AND TOTAL CERAMIC CROWN </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>PRE-IMPLANT BONE REGENERATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>PROPHYLAXIS AND TEETH WHITENING </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>RESTORATIONS ON PERIODONTAL DISEASE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>CORONAL REPOSITIONING FLAP</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>ADJUVANT TREATMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>DRUG-INDUCED GINGIVAL HYPERPLASIA</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>GINGIVECTOMY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>DIASTEMAS BY PERIODONTAL DISEASE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>ROUTINE TARTRECTOMY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>SUBGINGIVAL CURETTAGE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>MELANIC GINGIVAL PIGMENTATIONS </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>MUCOGINGIVAL SURGERY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-periodontics-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>POCKET REDUCTION SURGERY</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">I HAD GONE TO VARIOUS DENTISTS AND NEVER TOLD ME THAT I SUFFERED FROM THE
    GUMS. AS I KNEW SOMETHING WAS NOT GOING WELL, I VISITED DENTAL VIP IN SEARCH OF
    A NEW OPINION. THEY MADE ME A COMPLETE RADIOGRAPHIC STUDY AND FOUND ME MODERATE
    PERIODONTITIS. FORTUNATELY I AM ALREADY UNDER TREATMENT!".</p>
  images:
    portrait: /uploads/quotes-periodontics-portrait-en.jpg
    landscape: /uploads/quotes-periodontics-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
