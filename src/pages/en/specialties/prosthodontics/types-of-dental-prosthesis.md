---
templateKey: annex-page
language: en
redirects: >
  /especialidades/protesis/tipos-de-protesis
title: Types Of Dental Prosthesis
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-prosthesis-types.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 33%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: List
  content: >

  blocks:
    - img: /uploads/implant-types-Implant-supported-prosthesis.jpg
      number: 1
      title: >

        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-implant-supported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Implant-supported prosthesis</h2>
        </div>

        <p class="dv-srv-pr">Is the one that is made on dental implants and it is, in the vast majority of cases, the ideal type of prosthesis. Implants allow
        us to elaborate fixed and individualized restorations without having to carve,
        wear and compromise other healthy teeth. In addition, it is the only fixed alternative
        in cases of edentulous gaps without posterior pillars, small amount or total
        absence of remaining teeth.</p>
            </div>
    - img: /uploads/implant-types-prosthetic-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
          <div class="title-icon">
          <i class="icon-types-prosthetic-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Tooth-supported prosthesis</h2>
        </div>

        <p class="dv-srv-pr">It is also known as conventional fixed prosthesis
        and is cemented on the teeth adjacent to the empty space. It was undoubtedly
        the best option before the appearance of dental implants. For its elaborate
        it is necessary to carve stumps and wear the teeth that will it serve as a base
        <em>(pillars)</em> at each end, even when they are intact and completely healthy.</p>
          </div>
    - img: /uploads/implant-types-dentomucosupported-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-dentomucosupported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Dento-muco-supported prosthesis</h2>
        </div>
        <p class="dv-srv-pr">It is a removable partial denture made of acrylic
        <em>(plastic material)</em>, metal-acrylic or thermoplastic nylon. It can be
        inserted and removed by the patient himself. It rests on the oral mucosa and
        is retained by a system of extracoronal elements or hooks that surround some
        of the teeth still present. Although due to its aesthetic and functional limitations
        it moves away from the ideal prosthetic prototype, it still having valid as
        a provisional denture, and even definitive; in cases of surgical contraindication
        or economic limitation.</p>
            </div>
    - img: /uploads/implant-types-mucosupported prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-mucosupported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Muco-supported prosthesis</h2>
        </div>

        <p class="dv-srv-pr">It is the popular "complete denture" and is still
        used when there is no tooth left in the mouth. It has very little retention
        and stability, although some people get used to and tolerate it. There is currently
        the possibility of anchoring it to 2, 3 or 4 implants to transform it into a
        more comfortable and functional implant-assisted overdenture, of course, as
        long as the patient''s bone and systemic conditions allow it.</p>
                </div>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">TYPES OF DENTAL PROSTHESIS</h1>
     <p>There is more than one prosthetic solution
     for the same clinical case, each with advantages and disadvantages. It will be
     our work in consultation to expose them in detail and guide you during the selection
     process.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-prosthesis-types.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>We Have the Latest Generation Ceramic Systems...</h1>
    <br>
    <br>
    <h2 >In DENTAL VIP we guarantee the most careful selection of dental materials
    for your Oral Rehabilitation.</h2>

# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
      icon:
        display: false
        img: /uploads/sections-facilities.jpg
      link:
        display: true
        to: /especialidades/estetica-dental/carillas-de-porcelana/
      placeholder: <span>Más Información</span>
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">A truly biological and conservative approach</h2>
      <p class="dv-div-text
      dv-pb-15">We believe that the routine of
      wearing out or overloading healthy teeth with the sole intention of replacing
      other absent ones, should already be a thing of the past. Compromise the health,
      integrity and longevity of its pillars, as it happens with the various variants
      of the conventional prosthesis, it was the price forced to pay before the discovery
      and development of osseointegration. </p> <p class="dv-div-text text-right">We
      are convinced that, under favorable conditions, the implants are the ideal and
      biological solution par excellence in the field of Oral Rehabilitation. In addition
      to replacing teeth, they preserve the maxillary bone and prevent the appearance
      of new pathologies in the remaining natural dentition.</p>
    image:
      display: false
      size: 100px
      src: /uploads/icons-abutment-es.jpg
    link:
      display: true
      placeholder: MORE INFO
      to: >
        /en/specialties/dental-implants/
  img: /uploads/aside-prosthesis-types.jpg
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Other Procedures</h1>
  procedures:
    - title: >
        <h5> CAD-CAM TECHNOLOGY</h5>
      to: /en/specialties/prosthodontics/cad-cam-technology
      img: /uploads/procedures-cad-cam.jpg
    - title: >
        <h5> 3D DIAGNOSIS AND PLANNING </h5>
      to: /en/specialties/dental-implants/3d-diagnosis-and-planning/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> PORCELAIN VENEERS </h5>
      to: /en/specialties/aesthetic-dentistry/porcelain-veneers
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5> IMPLANT SUPPORTED RESTORATIONS  </h5>
      to: /en/specialties/dental-implants/implant-supported-restorations
      img: /uploads/procedures-dental-implants.jpg
    - title: >
        <h5> DIGITAL SMILE DESIGN</h5>
      to: /en/specialties/aesthetic-dentistry/digital-smile-design/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> AESTHETIC BRACKETS </h5>
      to: /en/specialties/orthodontics/aesthetic-braces/
      img: /uploads/procedures-brackets.jpg
---

<div class="row container">
<div class="item full">

![Hopper The Rabbit](/img/gallery-blocks-aesthetic.jpg)

## Aesthetics and function!

<b>Two big objectives</b>. Achieve restorations that look beautiful,
radiant and natural, that harmonize with your face, lips and gums, that highlight
your smile, that are comfortable to talk, eat and chew and that remain untouched
over time.

</div>
<div class="item full">

![Hopper The Rabbit](/img/gallery-blocks-security.jpg)

## Security and trust!

<b>Two great benefits</b>. Every human being needs to feel good and
self-confident. A healthy and attractive teeth will allow you to smile without
any complex, increase your self-esteem and will make you enjoy a full life,
without limits and full of satisfactions.

</div>
</div>
