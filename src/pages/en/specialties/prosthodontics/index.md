---
templateKey: specialties-page
language: en
redirects: /especialidades/protesis/
title: Prosthodontics
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-prosthodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 44%
  content:
    position: center
    body: >
      <h1 class="bebas">Prosthodontics</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-prosthodontics"></i></h1>
    <p class="thin"><em>The psychological consequences associated to the Absence of Teeth generate
    a lot of insecurity in people and behavioral changes capable of limiting, and
    even destroying; their social, emotional and labor relationships.</em></p>

# Description Section
article:
  content: >
    <p>It is well known that the integrity of the dental arches is a key factor
    in maintaining the balance and correct functioning of the entire STOMATOGNATHIC
    SYSTEM conformed by teeth, jaws, masticatory muscles and temporomandibular joints.
    <strong>If one or more teeth are lost and are not replaced as soon as possible,
    the pieces adjacent to the empty spaces are inclined and move towards them altering
    the occlusion and masticatory function</strong>. In addition, new spaces can begin
    to appear between the anterior teeth <em>(diastemas)</em> that seriously compromise
    dental aesthetics.</p> <p><strong>Prosthodontics is undoubtedly one of the Specialties
    with greater range of action within current Dentistry.</strong> It can encompass
    from the reconstruction of a single partially destroyed tooth to bimaxillary COMPLETE
    REHABILITATION in totally edentulous patients.</p> <p>Within the prosthetic arsenal
    at our disposal we find the well-known and popular removable dentures, conventional
    fixed prostheses and the <strong>sophisticated implant-supported restorations</strong>.
    Without any discussion, the last are currently, in all their types and versions;
    the ones of first choice. Although the function has traditionally been the primary
    objective of the Prosthodontist, nowadays aesthetics have the same relevance and
    trends have led us to make <strong>totally aesthetic and metal-free devices </strong>such
    as the thermoplastic nylon flexible prostheses Valplast<sup>®</sup> type and the
    <strong>impressive crowns and bridges of Zirconia or Lithium Disilicate</strong>
    made with CAD-CAM COMPUTER TECHNOLOGY.</p> <p>Said the above, it is not difficult
    to understand <strong>the importance it has of the participation of a Specialist,
    </strong>of a Prosthodontist who guarantees the aesthetic and functional success
    of his restorative treatment, who be really capable to design and manufacture
    prosthetic devices that allow you to speak, eat and smile comfortably and without
    any type of limitation.</p>
  img: /uploads/aside-prosthodontics-correct.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Our mission consists in to spread the benefits of oral health and promote
    the Emotional Power of a Beautiful Smile, so that all patients can enjoy to the
    fullest of Dentistry, of success and of life in general.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Filomena Montemurro Tafuri</strong>
    details: >
      <strong>
        Prosthodontist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-prosthodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-prosthodontics.jpg
  mobilePosition: '{"mobilePosition320":"-687px","mobilePosition360":"-835px","mobilePosition375":"-1089px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>What is a dental prosthesis?</h4>
      content: >-
        <p>It is an anatomical device that simulates or contains artificial teeth
        that replace the crowns of very destroyed, absents or missing natural
        teeth. Depending on their extension they can be individual, partial or
        total; and according to its support and retention, fixed or removable.</p>
    - title: <h4>Why are gums health so important for any prosthetic device?</h4>
      content: >-
        <p>The durability of any type of prosthesis will depend on its structural
        integrity and the health of its pillars. Regardless of whether they are
        supported by natural teeth, stumps or dental implants, they are all
        anchored and retained by the support periodontal tissues; by the bone
        tissue and gum mainly. If there is not good hygiene, they could become
        infected, inflamed, reabsorb the alveolar bone, loosen and fall the
        pillars; to finally, lose the dental prosthesis prematurely.</p>
    - title: <h4>How long does it take to make a dental prosthesis?</h4>
      content: >-
        <p>It depends a lot on the case and type of rehabilitation. The lapses can
        vary from 2 or 3 days for provisional ones to 6 or 7 months for those that
        are manufactured over implants, of course, taking into account the
        osseointegration period. As a general rule and under normal conditions, 3
        weeks is the average period of time required to do tests and complete the
        vast majority of our definitive restorations; whether fixed or
        removable.</p>
    - title: <h4>How is a conventional removable prosthesis?</h4>
      content: >-
        <p>They are &ldquo;remove and put&rdquo; devices that can be inserted and
        removed by the patient, rest on the oral mucosa, and if they are partial;
        are retained with hooks that surround some natural teeth. They are usually
        made with chromium-cobalt metal alloys, thermopolymerizable acrylic resin
        and thermoplastic nylon by injection. Due to their great aesthetic and
        functional limitations we only indicate them as provisional or
        transitional structures in extensive rehabilitations, or definitive; only
        in those cases where really no other alternative applies.</p>
    - title: <h4>What care should be taken with such a denture?</h4>
      content: >-
        <p>Periodic professional controls and scrupulous oral hygiene to avoid
        periodontal diseases and caries in the support teeth, particularly in the
        areas where the hooks are located. It is also necessary to clean the
        prosthesis after each meal, using a soft bristle brush and toothpaste. We
        always recommend removing the denture to sleep, so that the oral support
        mucosa has an opportunity for revascularization and regeneration,
        essential biological phenomena to prevent the occurrence and recurrence of
        inflammatory, degenerative or infectious lesions.</p>
    - title: <h4>When should a "remove and put" denture be replaced?</h4>
      content: >-
        <p>When any of its structural components is fractured, when some pillar is
        lost, when the reabsorption of the basal bone that supports it is
        noticeable or when its necessary periodic adjustments are not able to
        guarantee its stability and retention. We must understand that traditional
        removable dentures are very poor from the biomechanical point of view,
        they always generate undesirable forces on the anchor teeth and cause
        resorption of their supporting tissues; all circumstances that
        considerably limit their longevity or useful life.</p>
    - title: <h4>How is a tooth-supported fixed prosthesis?</h4>
      content: >-
        <p>The classic fixed prosthesis is cemented to the remaining natural
        teeth, previously worn and turned into stumps. They usually consist of a
        porcelain coated metal core, although it is currently possible to make
        them from pure ceramics or upon a white core based on Zirconia. Generally,
        due to their greater naturalness and translucency, the total-ceramic
        structures are indicated for the anterior teeth and, due to their greater
        resistance, those the metal core for the posterior ones.</p>
    - title: <h4>What is a stump in Dentistry?</h4>
      content: >-
        <p>It is a natural tooth that has been carved and worn to serve as a
        pillar to a crown or conventional fixed prosthesis. When the remaining
        dental structure is scarce or weakened, it will be necessary to make an
        &ldquo;artificial stump&rdquo; that consists of two portions, a radicular
        post that is housed inside the root canal <em>(endodontic treated and
        desobturated previously)</em> and a coronal portion that replaces to the
        dentin lost and reinforces the existing one. Artificial stumps <em>(post
        and core)</em> can be made with fiberglass-based materials or by foundry
        and casting of high-strength metal alloys.</p>
    - title: <h4>And stumps carving does not harm teeth?</h4>
      content: >-
        <p>It depends on the case. If the teeth are already very decayed or
        fractured, on the contrary, the carving and subsequent reconstruction of
        the stump will reinforce them, allow their aesthetic restoration and
        guarantee their permanence in the mouth. It would be different if we had
        to carve completely healthy teeth with the sole purpose of replacing other
        absent teeth using conventional fixed prostheses. Today the main advantage
        of dental implants is that they allow us to keep intact the neighboring
        teeth to the edentulous spaces.</p>
    - title: >-
        <h4>Is it always necessary to perform a root canal before carving a
        stump?</h4>
      content: >-
        <p>Not always, but in the vast majority of cases. The mechanical drilling
        applied to a vital tooth to wear it and turn it into a stump constitutes a
        great physical aggression and generates so much friction and heat that
        usually, and in the short or medium term, it causes irreversible
        inflammation, degeneration and death of its pulp tissue; always
        accompanied by extreme sensitivity and pain. Such a condition would put
        the permanent fixed restorations at risk, since they would then have to be
        detached or perforated to endodontically treat their pillars. Preventive
        endodontics for prosthetic reasons is currently a widely accepted clinical
        criterion worldwide.</p>
    - title: <h4>How many types of porcelain are used in fixed prosthodontics?</h4>
      content: >-
        <p>Chemically, porcelains or dental ceramics can be grouped into three
        large families: feldspathic, aluminous and zirconia based. The feldspats
        of vitreous matrix <em>(based on feldspar and quartz) </em>are the most
        aesthetic but most fragile of all, so they are mainly used for the coating
        of metal or ceramic skeletons. The aluminous <em>(with a high content of
        aluminum oxide) </em>are very resistant but very opaque and not very
        aesthetic, which is why they are currently reserved only for the
        preparation of copings and internal structures, being necessary to cover
        them with porcelains of less alumina for achieve a good mimicry with the
        natural tooth. Zirconia ceramics are the most novel and are composed of
        highly sintered zirconium oxide <em>(ZrO<sub>2</sub>),</em> which makes
        them highly resistant to flexion and fracture, and therefore, the ideal
        materials for making ceramic prostheses in areas of high mechanical
        compromise. However, like high-strength aluminous, these ceramics are very
        opaque <em>(they do not have a vitreous phase),</em> and therefore they
        are used only to manufacture the core of the restoration, that is to say;
        they must also be covered with conventional porcelains to achieve a good
        aesthetic result.</p>
    - title: <h4>Are fixed prostheses for a lifetime?</h4>
      content: >-
        <p>Influence so many, but so many factors in the longevity of a fixed
        rehabilitation that no professional is able to predict, for sure, its
        duration in years. We consider that in favorable conditions, between 15
        and 20 years is on average the useful life time for the vast majority of
        cases. Once the cycle has been completed, the rehabilitation may be
        ideally replaced by dental implants, or failing that, replicated; as long
        as the pillars offer favorable conditions for it.</p>
    - title: <h4>What cares should be taken with a fixed prosthesis?</h4>
      content: >-
        <p>Scrupulous oral hygiene, common sense to avoid improperly biting
        excessively hard objects, permanent use of a protective night splint and
        periodic professional controls. It is essential, apart from normal
        brushing, the constant use of the interdental brush, special dental floss,
        mouthwash and oral irrigator. A Waterpik<sup>®</sup> is the best
        complement for oral hygiene of people with dental implants, fixed
        prosthesis and orthodontics. We always recommend to our patients a routine
        check every 6 or 12 months to verify the splint and evaluate their
        periodontal condition, occlusal function, stability, integrity and
        marginal sealing of the restorations.</p>
    - title: <h4>How is special dental floss for prostheses?</h4>
      content: >-
        <p>It usually consists of 3 portions. A first rigid section to be inserted
        directly below the prosthesis, a second spongy section to clean around the
        restoration, between pillars and between the interdental spaces; and a
        third section without wax to remove the plaque from the gingival
        sulcus.</p>
    - title: <h4>How is an implant-assisted or implant-supported restoration?</h4>
      content: >-
        <p>It is the one that is retained exclusively by dental implants. There
        are several types and they are basically made with the same materials as
        traditional prostheses. Structurally they are designed under a male-female
        connection system, in which the implants house the abutments or prosthetic
        pillars inside them, and over these; we fit, cement or screw the
        artificial teeth.</p>
    - title: <h4>Why are prostheses over dental implants better?</h4>
      content: >-
        <p>In essence because they are totally independent, self-sufficient
        structures and that are closest to the ideal prosthetic prototype. They do
        not rest or retain on the patient's natural teeth, do not compromise them,
        do not generate harmful forces or functional overloads, and consequently;
        replace lost teeth without any negative effect on present ones, a
        condition impossible to achieve with any type of conventional prosthesis.
        In addition, it is the only fixed alternative for those patients who have
        lost all or most of their teeth.</p>
    - title: <h4>How many types of implant-supported restorations are there?</h4>
      content: >-
        <p>In essence, 3 types: fixed of metal-porcelain or total ceramic, hybrids
        of metal-acrylic or metal-porcelain and removable overdentures. In the
        DENTAL IMPLANTS section you can find a very specific description of each
        of them and their variants.</p>
    - title: >-
        <h4>Which is better, work over a restorable tooth root or replace it at
        once whit a dental implant?</h4>
      content: >-
        <p>Whenever possible we should preserve natural teeth. Although dental
        implants currently represent a high quality restorative alternative, they
        will never exceed the biological properties and functional expectations of
        a well-rehabilitated natural piece. When a single tooth has suffered a
        fracture or great destruction of its clinical crown due to caries, but its
        root preserves good integrity, favorable length and adequate periodontal
        support; it will always be preferable, and without any discussion, its
        treatment and restoration with endodontics, post and core and porcelain
        crown.</p>
    - title: >-
        <h4>But, how do we know if a natural tooth can be successfully
        restored?</h4>
      content: >-
        <p>Studying the case thoroughly through intraoral examination,
        radiographic evaluation and a lot of clinical criterion. Everything will
        depend on the periapical conditions that it presents, on the amount of
        tooth tissue remaining, on its periodontal state, on its aesthetic
        requirements, on its root morphology, on its location in the dental arch,
        on the occlusal loads to which it will be exposed and on the whether it
        will work as an individual crown or as a fixed bridge pillar.</p>
    - title: <h4>Can conventional prostheses be replaced by dental implants?</h4>
      content: >-
        <p>Of course yes!, and it is in fact the most advisable to protect the
        natural teeth that remain in the mouth, but of course; as long as the
        patient's general health allows it, let us have enough maxillary bone for
        the pretended implantation and the person is willing to make a new effort
        and a new investment in time and money.</p>

cases:
  title: >
    <h1>Prosthodontics - Clinical Cases</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-prosthodontics-en-01.jpg
      - /uploads/clinic-cases-prosthodontics-en-02.jpg
      - /uploads/clinic-cases-prosthodontics-en-03.jpg
      - /uploads/clinic-cases-prosthodontics-en-04.jpg
      - /uploads/clinic-cases-prosthodontics-en-05.jpg
      - /uploads/clinic-cases-prosthodontics-en-06.jpg
      - /uploads/clinic-cases-prosthodontics-en-07.jpg
      - /uploads/clinic-cases-prosthodontics-en-08.jpg
      - /uploads/clinic-cases-prosthodontics-en-09.jpg
      - /uploads/clinic-cases-prosthodontics-en-10.jpg
      - /uploads/clinic-cases-prosthodontics-en-11.jpg
      - /uploads/clinic-cases-prosthodontics-en-12.jpg
      - /uploads/clinic-cases-prosthodontics-en-13.jpg
      - /uploads/clinic-cases-prosthodontics-en-14.jpg
      - /uploads/clinic-cases-prosthodontics-en-15.jpg
      - /uploads/clinic-cases-prosthodontics-en-16.jpg
      - /uploads/clinic-cases-prosthodontics-en-17.jpg
      - /uploads/clinic-cases-prosthodontics-en-18.jpg
      - /uploads/clinic-cases-prosthodontics-en-19.jpg
      - /uploads/clinic-cases-prosthodontics-en-20.jpg
      - /uploads/clinic-cases-prosthodontics-en-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SEVERE TOOTH WEAR </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>FIXED PARTIAL PROSTHESIS REPLACEMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SUBSTITUTION OF REMOVABLE DENTURES</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ADVANCED ORAL REHABILITATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DENTAL EXTREME MAKEOVER</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>TOOTH-SUPPORTED FIXED PROSTHESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CERAMIC DESIGN WITH CAD-CAM TECHNOLOGY </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SPLINTING IN PERIODONTAL DISEASE </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PROSTHETIC RETREATMENT </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>VALPLAST<sup>®</sup></h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>UPPER LATERAL INCISORS AGENESIS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>1ST RESTORATIVE PHASE IN A COMPLEX CASE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>PORCELAIN JACKET CROWNS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>EXTREME BRUXISM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>AESTHETIC AND FUNCTIONAL RECONSTRUCTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IPS E.MAX<sup>®</sup> CAD CERAMIC SYSTEM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ALL-ON-FOUR</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MIXED REHABILITATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>IMPLANT-SUPPORTED CROWNS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MAXILLARY CENTRAL INCISOR ALL-CERAMIC CROWN</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-en-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MUCO-SUPPORTED COMPLETE DENTURE</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">I NEGLECTED ME MANY YEARS AND MY TEETH WERE A TRUE DISASTER. I NEEDED EXTRACTIONS,
    SOME ENDODONTICS, DENTAL IMPLANTS AND VARIOUS ZIRCONIA CROWNS. ALTHOUGT THE TREATMENT
    LAST ALMOST 6 MONTHS, THE SPECIALISTS PRACTICALLY REMAKED MY MOUTH".</p>
  images:
    portrait: /uploads/quotes-prosthodontics-portrait-en.jpg
    landscape: /uploads/quotes-prosthodontics-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-prosthodontics-ceramic.jpg
      content: >
        <h1 class="bebas">All Ceramic... All You Need!</h1>
        <p>A system that allows us to select the most
          appropriate metal-free ceramic material for each situation, depending on the
          initial indication and resistance requirements. Lithium Disilicate for individual
          restorations and Zirconium Oxide for extensive structures.</p><br/>
      footer:
        icon:
          display: true
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/sections-prosthodontics-prosthesis-types.jpg
      content: >
        <h1 class="bebas">TYPES OF DENTAL PROSTHESIS</h1>
        <p>Partial and total, fixed and removable,
            conventional and implant-assisted, acrylic and porcelain. Multiple are the alternatives
            to replace the teeth that unfortunately have been lost, or that are impossible
            to keep for longer in the mouth.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: true
          to: /en/specialties/prosthodontics/types-of-dental-prosthesis/
          placeholder: <span>MORE INFO</span>
    - img: /uploads/sections-prosthodontics-cad-cam.png
      content: >
        <h1 class="bebas">CAD-CAM TECHNOLOGY</h1>
        <p>It is the most innovative technology available
          in fixed prosthodontics and represents a great advance in relation to conventional
          Dentistry. We use it to make porcelain inlays, crowns and bridges, prosthesis
          over implants and in other indirect restorative treatments.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: true
          to: /en/specialties/prosthodontics/cad-cam-technology/
          placeholder: <span>MORE INFO</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
