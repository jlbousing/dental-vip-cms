---
templateKey: specialties-page
language: en
redirects: /especialidades/sedacion-y-anestesia-general/
title: Sedation and General Anesthesia
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-sedation-and-general-anesthesia.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">SEDATION AND GENERAL ANESTHESIA</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-sedation-and-general-anesthesia"></i></h1>
    <p class="thin"><em>Their main objectives are to Guarantee the Patient's Well-Being and Comfort,
    completely suppressing the pain, physical discomfort and psychological stress
    associated with any medical or surgical act.</em></p>

# Description Section
article:
  content: >
    <p>Despite all the advances that Dentistry has experienced in recent years,
    <strong>the fear and anxiety caused by dental treatment continue to be two very
    common aspects in patients around the world</strong>. So much so, that through
    recent statistical studies it has been possible to demonstrate that up to 25%
    of adults avoid or postpone attending the Dentist's office for awe of suffering
    pain, constituting the circumstance a true psychological barrier that, sometimes;
    impedes receive the necessary care and preserve oral health. And although behavior
    management strategies are useful in the vast majority of cases, they are not always
    effective, especially when facing <strong>individuals who are excessively nervous,
    apprehensive or who have disabilities that prevent them from cooperating.</strong>.
    And it is precisely in these vicissitudes, in which the interaction between the
    professional and the mourner fails, and in which the usual conditioning techniques
    seem not to work, when we should consider the possibility of applying alternative
    clinical resources to MANAGE ANXIETY and preoperative stress.</p> <p><strong>Currently,
    local anesthesia by infiltration continues to be without any discussion, the simplest,
    more frequent and more effective method for dispensing and ensuring a pain-free
    dental treatment</strong>; however, it is also unquestionable the fact that it
    is a procedure that per se can cause discomfort and rejection, due to the need
    to apply one or several injections inside the mouth.</p> <p>And it is definitely
    in those situations, in which it is not even possible to tolerate a puncture,
    in which we must resort to other anesthetic techniques such as <strong>Conscious
    Sedation, which represents a valid, relevant and very effective pharmacological
    option for fear, stress and pain control</strong>; making possible the dental
    treatment and avoiding the typical confrontations with the pediatric patient or
    hypersensitive adult. In addition, an ATRAUMATIC EXPERIENCE, will virtually guarantee
    the voluntary return of these people to their regular periodic checks during childhood
    and adulthood.</p>
  img: /uploads/aside-sedation-and-general-anesthesia.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Both Conscious Sedation (CS) and General Anesthesia (GA) are safe and reliable
    procedures, logically, as long as they are practiced under ideal conditions of
    infrastructure and clinical equipment, and of course; under the responsibility
    of a Highly Specialized Medical Team.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Anesthesiologist
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-sedation-and-general-anesthesia.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-sedation-and-general-anesthesia.jpg
  mobilePosition: '{"mobilePosition320":"-166px","mobilePosition360":"-193px","mobilePosition375":"-309px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Frequently Asked Questions
  items:
    - title: <h4>Between local and general anesthesia, which is preferable?</h4>
      content: >
        <p>Because of its great effectiveness, safety and almost absolute absence of
        side effects, trunk or infiltrative local anesthesia is the one of choice
        for routine interventions in Dentistry and minor oral surgery in outpatient
        settings; while inhalation or intravenous conscious sedation it is the best
        option for special or excessively apprehensive patients. General anesthesia
        is only necessary in cases of major surgery such as the orthognathic and
        maxillofacial.</p>
    - title: <h4>What is General Anesthesia?</h4>
      content: >
        <p>GA can be defined as induced, controlled and reversible intoxication of
        the central nervous system that produces unconsciousness, temporary amnesia,
        analgesia, loss of sensation, muscle relaxation and suppression of autonomic
        and sensory reflexes. The drugs supplied have hypnotic properties and can be
        applied in different ways, but the inhalation and intravenous routes are
        usually used.</p>
    - title: <h4>What is Conscious Sedation?</h4>
      content: >
        <p>It is an anesthetic technique in which patients are given the combination
        of two or more medications that cause a mild depression of the central
        nervous system, without loss of consciousness, but with alteration of it. It
        has an effect on pain, since by decreasing anxiety the painful threshold is
        raised, it facilitates the administration of the local anesthetic without
        the person knowing and, at the correct dose; it also produces amnesia, so
        that the patient has little or nothing to remember about the procedure.</p>
    - title: <h4>What is the difference between both and which is better?</h4>
      content: >
        <p>Basically that in sedation reflexes, breathing control and responsiveness
        to tactile and verbal stimuli are preserved. In general anesthesia, the
        patient is asleep in a deep sleep, and airway protection and assisted
        ventilation are frequently required. In addition, cardiovascular function is
        usually also altered, so constant monitoring of vital signs is pertinent.
        None is better than another, and simply each one has its indications. In
        Dentistry, general anesthesia for more extensive, complex and invasive
        procedures, such as maxillofacial surgery or the placement of zygomatic
        implants for example; and sedation, for the vast majority of oral
        interventions in particular circumstances.</p>
    - title: <h4>What kind of drugs are usually used to sedate a patient?</h4>
      content: >
        <p>Nitrous Oxide combined with Oxygen
        <em>(N</em><sub><em>2</em></sub><em>O:O</em><sub><em>2</em></sub><em>)
        </em>by inhalation, and intravenously Benzodiazepines such as Diazepam and
        Midazolam, Propofol, Sodium Thiopental, Fentanyl, Ketamine, Etomidate and/or
        any combination thereof. In addition, the local anesthetic of preference,
        usually those belonging to the amide group; like Lidocaine or
        Mepivacaine.</p>
    - title: <h4>Is sedation a 100% safe procedure?</h4>
      content: >
        <p>The performance of any medical act can always have adverse or undesirable
        effects, and the CS is not the exception. Among the possible risks we can
        find allergic reactions, aspiration of secretions at the pulmonary level,
        hypoxia, hypoventilation, obstruction of the airway by foreign bodies and
        abnormal reactions of the autonomic nervous system. However, the
        aforementioned complications rarely occur, and in such a case; the presence
        of an Anesthesiologist will guarantee a quick and safe resolution of
        them.</p>
    - title: <h4>Are there contraindications to this anesthesia technique?</h4>
      content: >
        <p>Very few, among which we find: history of hypersensitivity prior to the
        procedure, respiratory failure, severe hepatic insufficiency, pregnancy,
        lactation, alcoholism, drug use, psychotic diseases, intestinal occlusions,
        some cases of glaucoma and/or any other systemic condition that
        contraindicates the use of narcotics.</p>
    - title: >-
        <h4>If I am very nervous and I am just going to fix a tooth, can you sleep
        me completely?</h4>
      content: >
        <p>Of course, yes; however, we would have to assess very well if the cost
        and complexity of the treatment would truly compensate you in such a simple
        case. The correct thing is that before considering sedation, it becomes
        pertinent to exhaust all persuasive and related to the conditioning of
        behavior means, which often achieve to minimize anxiety levels and radically
        modify the disposition towards treatment.</p>
    - title: <h4>In which cases is a sedation or deeper anesthesia recommendable?</h4>
      content: >
        <p>There is really no clear consensus on the indications for the use of
        these techniques in Dentistry, however, they depend on the objective and
        subjective analysis of multiple factors associated with the patient, the
        professional and the treatment. Among the most common indications we
        find:</p> <ol> <li>Children or adults with previous traumatizing
        medical-dental experiences, and in which it is not possible to achieve
        positive communication or the necessary cooperation for treatment.</li>
        <li>Patients allergic to local anesthetics.</li> <li>Children and adults
        with blood dyscrasias, since infiltrative or trunk anesthesia could cause
        bleeding in the latero-pharyngeal spaces.</li> <li>People with mental
        retardation, psychomotor, genetic or musculoskeletal disorders; which
        prevent conventional treatment in a state of consciousness.</li>
        <li>Patients with congenital heart disease in which extensive treatment is
        to be performed or that contemplates the removal of septic processes,
        multiple restorations or maxillofacial surgery.</li> <li>Medically
        compromised patients and that their general condition requires anxiety
        relief to prevent greater risks.</li> <li>Situations in which we determine
        that local anesthesia will not achieve the desired effect due to the size,
        location of the lesion and/or duration of the procedure; for example, the
        placement of multiple implants in both jaws.</li> <li>Odontophobic patients
        or with real panic to the Dentist.</li> </ol>
    - title: <h4>Can these interventions produce any side effects?</h4>
      content: >
        <p>It is very difficult, since the drugs are generally used with very well
        controlled doses that are completely metabolized in a short time, allowing
        the patient to wake up normally, as if it were a long nap. However, in order
        to discharge; the person must be conscious and oriented, hemodynamic and
        respiratoryly stable and without the need for help to walking.</p>
    - title: <h4>Is it necessary to do some type of study or previous evaluation?</h4>
      content: >
        <p>The preanesthetic assessment <em>(PA)</em> is a study protocol that
        allows the evaluation of the physical condition and the risk of the patient,
        to then establish an anesthetic plan according to their particular
        conditions and thus reduce the possibility of complications. The PA is
        mandatory, has demonstrated its importance and significance in the field of
        Anesthesiology and is a major element of safety in medical care. Recent
        studies have shown that the lack of assessment of the status of anesthetic
        patients influences on more than 70% of intraoperative accidents and were
        the cause of some deaths occurred.

        The preoperative consultation must take place several days before the
        scheduled intervention. The contemplated period should allow the completion
        of the complementary tests and pertinent external interconsultations,
        respiratory therapy sessions if required, the abstinence of tobacco and/or
        alcohol, and even; the administration of some specific nutritional
        contribution.</p>
    - title: <h4>Can the CS be applied freely in the dental office?</h4>
      content: >
        <p>AT OUR CRITERION NEVER!, unless the center has facilities, equipments and
        materials that guarantee proper patient care, and that include at least: an
        operating room environment attached to the dental box, anesthesia appliances
        and equipments, artificial airways and endotracheal tubes of all diameters,
        intravenous catheters, valves and respiratory assistance bags, laryngeal
        masks of all sizes, oropharyngeal and nasopharyngeal cannulas of various
        diameters, medical gas cylinders, advanced monitoring system,
        electrocardiograph, laryngoscope and video laryngoscope, fiberoptic
        bronchoscope for difficult intubations, peripheral nerves stimulator,
        oxygen supply systems, gas purification system, aspiration system,
        cardiopulmonary resuscitation equipment <em>(CPR),</em> recovery room and
        emergency electrical power plant with a minimum autonomy of 3 continuous
        hours. In addition, in the cases of GA, it is essential that the operating
        room is integrated into a private medical clinic or large hospital, with
        intensive therapy, a permanent multidisciplinary medical team and trained
        auxiliary personnel to attend to any possible eventuality.</p>

        <p>With sincerity, we do not know in our capital city any dental clinic that
        meets these demands 100% and is truly able to provide a safe and effective
        environment for sedation treatments, and even less; with general anesthesia.
        At DENTAL VIP we never put our patients lives at risk and we always prefer
        to intervene these cases in external physical spaces to our usual
        infrastructure.</p>
    - title: <h4>What safety or precautionary measures should I take?</h4>
      content: >
        <p>Basically fasting, to avoid regurgitation and pulmonary aspiration of
        gastric contents after induction of anesthesia, during the course of the
        procedure or in the immediate postoperative period. Current preoperative
        fasting protocols coincide in the duration of the period of time during
        which no substance should be ingested, indicating; 2 hours for clear liquids
        and 6 hours for more solid foods. It is understood by clear liquids only
        water, fruit juice without pulp, carbonated drinks, light tea and black
        coffee.</p>
    - title: <h4>Should an Anesthesiologist always be present?</h4>
      content: >
        <p>OF COURSE! The responsibility of a patient under sedation should always
        be on charge of a Specialist in Anesthesiology, resuscitation and pain
        therapy, with experience in infiltrative techniques, ability to title the
        drugs that are administered and expertise in airway management, monitoring
        of vital signs and application of resuscitation techniques. The presence of
        an Anesthesiologist can mean the difference between life and death, so
        clear!</p>
    - title: <h4>Should he intubate me and put on an artificial respirator?</h4>
      content: >
        <p>Only in cases of general anesthesia. However, when applying a CS, all
        professional and life support resources that allow to face any eventuality
        and safeguard the patient&acute;s life must always be at hand, and among
        which; the endotracheal tubes and mechanical ventilation devices are
        indispensable.</p>
    - title: <h4>Are the costs of this type of anesthesia very high?</h4>
      content: >
        <p>They undoubtedly add to the account and raise the final amount of the
        bill. The occupation and use of a truly designed and equipped infrastructure
        for this purpose, the intervention of an Anesthesiologist with his auxiliary
        staff and the transfer of dental equipments and materials necessary to meet
        the therapeutic objective, are variables that will always have a
        considerable economic impact.</p>
    - title: >-
        <h4>If you are going to sedate me, can I go alone or should I be
        accompanied?</h4>
      content: >
        <p>It is necessary to always be accompanied, since it is very useful and
        comforting to receive physical and emotional support after the procedure,
        and in addition; it is essential to delegate the responsibility of the move
        to the place of residence. After sedation, feelings of clumsiness, confusion
        and disorientation are frequent.</p>
    - title: <h4>How long will it take me to wake up after the intervention?</h4>
      content: >
        <p>Conscious sedation is a technique that allows the rapid return of the
        patient to his normal state, which makes it possible to discharge him
        without further delay after finishing the procedure. However, it is common
        to experience a certain degree of drowsiness and tiredness, which is why we
        recommend waiting, at least; one hour before leaving the facilities.</p>
    - title: <h4>What postoperative care should I have?</h4>
      content: >
        <p>Basically postpone for 24 hours any activity that requires mental
        coordination, balance or equilibrium; such as: driving, operating heavy
        machinery, making complex calculations or any other function that demands
        psychomotor precision.</p>
    - title: <h4>Later, is it necessary to keep some kind of rest?</h4>
      content: >
        <p>None additional to the one that merits the routine postoperative in
        anesthesiology<em> (24 hours)</em> and the dental treatment dispensed.</p>

cases:
  title: >
    <h1>Conscious Sedation - Gallery</h1>
  display: true
  lightbox:
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-01.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-02.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-03.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-04.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-05.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-06.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-07.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-08.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-09.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-10.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-11.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-12.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-13.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-14.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-15.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-16.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-17.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-en-18.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #4</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #5</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #6</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #7</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #8</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #9</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #10</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #11</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #12</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #13</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #14</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #15</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #16</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #17</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/clinic-cases-sedation-and-general-anesthesia-en-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>CS GALLERY #18</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">AS IN MANY PEOPLE OF MY GENERATION, THE FEAR OF THE DENTISTS WAS PRODUCED
    BY A BAD EXPERIENCE IN CHILDHOOD, BUT THANKS TO SEDATION; I LOST THE PANIC AND
    I HAD BEEN ABLE TO BE TREATED WITH MULTIPLE DENTAL IMPLANTS".</p>
  images:
    portrait: /uploads/quotes-sedation-and-general-anesthesia-portrait-en.jpg
    landscape: /uploads/quotes-sedation-and-general-anesthesia-landscape-en.jpg
form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
