---
templateKey: contact-page
language: en
redirects: /contacto/
title: Contact
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-contact.png
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Contact</h1>

# Heading Section
heading:
  display: true
  content: <h1 class="title">We Are at Your Service</h1>
    <p>If you need additional information,
    want to make an inquiry, suggestions or reserve space on agenda; we can attend
    you by phone, using the form below or sending an email to  <a href="mailto:info@dentalvipcaracas.com"
    style="color:#91c508">info@dentalvipcaracas.com</a></p>

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-contact.png
  content: >
    <h1 class="big">Follow Us</h1>
    <h2>On our blog and social networks</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/en/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">News, Articles, Topical Advices and Much More …</h2>

# Procedures Section
amenities:
  title: >
    <h1 class="title">It Will Be an Honor to Receive Your Visit!</h1>
  procedures:
    - title: <h5>CARACAS SUBWAY</h5>
      to: /en/the-clinic/dental-implants/
      img: /uploads/procedures-subway.jpg
      content: >
        <p>
          <span>
          <i class="icon icon-subway"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>

          </span>
          <br></br>
          Line 1   <br class="visible-xs visible-sm visible-md visible-lg">
         Chacao Station  <br class="hidden-xs hidden-sm visible-md visible-lg">
         Exit: Francisco de Miranda Ave./<br class="hidden-xs hidden-sm visible-md
        visible-lg">Los Maristas St.</em>
        </p>
    - title: <h5>PARKING</h5>
      to: /en/specialties/orthodontics/
      img: /uploads/procedures-parking.jpg
      content: >
        <p>
          <span>
            <i class="icon icon-instagram"></i>
          </span>
          <br></br>
          Multicentro Empresarial del Este Parking <br class="visible-xs visible-sm visible-md visible-lg">
          Accesses by : <br class="hidden-xs hidden-sm visible-md visible-lg">
          Francisco de Miranda Ave. <em>(east direction).</em> <br class="hidden-xs hidden-sm visible-md visible-lg">
          Libertador Ave. <em>(west direction).</em>
        </p>
    - title: <h5>WI-FI ZONE</h5>
      to: /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-wifi.jpg
      content: >
        <p>
          <span>
            <i class="icon icon-instagram"></i>
          </span>
          <br></br>
          We Will Keep You Always Connected! <br class="visible-xs visible-sm visible-md visible-lg">
          INTERNET <br class="hidden-xs hidden-sm visible-md visible-lg">
         Broadband Service Available to Our <br class="hidden-xs hidden-sm visible-md visible-lg"> Distinguished Visitors.
        </p>
---
