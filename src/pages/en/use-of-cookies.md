---
templateKey: advise-page
description: >
  some seo
language: en
title: Use OF COOKIES
redirects: /uso-de-cookies/
published: true
keywords:
  - default
---

# USE OF COOKIES

## Cookies policy

The website [https://www.dentalvipcaracas.com/](/en/) owned by DENTAL VIP, Especialidades Odontológicas s.c. uses cookies.

A cookie is a piece of text that a web server can store on a user's hard drive. Cookies are not programs, only data with a name, so they cannot cause damage to computing devices.

Cookies are designed to offer a much more personalized browsing experience to internet users. These allow, among other things, that a website stores information on a user's computer and then retrieves it, that the page loads much faster for recurring visitors and that it offers them information that may be of real interest to them; depending on the use they make and the content of it.

Frequently, web pages use third-party cookies in order to create statistics and advertising profiles of users. This activity is completely legal, as long as they have our consent.

If you do not want to receive cookies, please configure your internet browser to erase them from your device's hard drive, block them or notify you if they are installed. To continue without changes in the configuration of cookies, simply continue browsing the website.

## Consent

The cookies we use do not store personal data or any type of information that can identify you, unless you want to register voluntarily in order to use the services that we put at your disposal or to receive information about promotions and contents of your interest.

By browsing and continuing on our website, you indicate that you are consenting to the use of the aforementioned cookies, and in the conditions contained in this Cookies Policy. If you do not agree, send an email to [info@dentalvipcaracas.com](mailto:info@dentalvipcaracas.com)

## How to block or delete installed cookies

You can allow, block or delete the cookies installed on your computer by configuring your browser options. You can find information on how to do it, in relation to the most common browsers, in the links included below:

Explorer: [https://support.microsoft.com/en-us/kb/278835](https://support.microsoft.com/en-us/kb/278835)

Chrome: [https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DAndroid&hl=en](https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DAndroid&hl=en)

Firefox: [https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox](https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox)

Safari: [https://support.apple.com/en-us/HT201265](https://support.apple.com/en-us/HT201265)

## Modifications

The website [https://www.dentalvipcaracas.com/](/en) owned by DENTAL VIP, Especialidades Odontológicas s.c. can modify this Cookies Policy according to the legal requirements, as well as, in order to adapt it to the demands dictated by any of the different public institutions of the state.

For this reason, we advise users to visit it periodically. When significant changes occur in this Cookies Policy, they will be communicated immediately, either through the web or through registered emails.
