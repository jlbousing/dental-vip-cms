---
templateKey: clinic-page
language: en
redirects: /la-clinica/instalaciones/
title: Facilities
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-facilities.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Facilities</h1>

# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2><span><i class="icon-instagram"></i></span> In the clinic…</h2>

# Heading Section
heading:
  display: true
  content: <h1 class="title">A Space Designed for Your Tranquility</h1>
    <p>A unique facilities, an exceptional technological equipment and an ideal environment for our patients to enjoy a different Dentistry with the highest level of comfort, hygiene and safety; always looking for the best possible result.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>

# Quote Section
testimonial:
  display: false
  color: "#ededed"
  content: >
    <p class="content">THE RESPECT FOR THE LIFE AND INTEGRITY OF THE HUMAN PERSON, THE PROMOTION AND PRESERVATION OF HEALTH, AS A COMPONENT OF DEVELOPMENT AND SOCIAL WELFARE, AND ITS EFFECTIVE PROJECTION TO THE COMMUNITY; CONSTITUTE IN ALL CIRCUMSTANCES THE PRINCIPAL DUTY OF THE DENTIST".
    </p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait-en.jpg
    landscape: /uploads/quotes-phillosophy-en.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Follow Us</h1>
    <h2>On our blog and social networks</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/en/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">News, Articles, Topical Advices and Much More …</h2>

# Gallery Section
gallery:
  display: true
  type: gridGallery
  carousel:
    display: true
    placeholder: ROTATE THE DEVICE TO ENLARGE THE IMAGES
    items:
      - /uploads/lightbox-facilities-01-en.png
      - /uploads/lightbox-facilities-02-en.png
      - /uploads/lightbox-facilities-03-en.png
      - /uploads/lightbox-facilities-04-en.png
      - /uploads/lightbox-facilities-05-en.png
      - /uploads/lightbox-facilities-06-en.png
      - /uploads/lightbox-facilities-07-en.png
      - /uploads/lightbox-facilities-08-en.png
      - /uploads/lightbox-facilities-09-en.png
      - /uploads/lightbox-facilities-10-en.png
      - /uploads/lightbox-facilities-11-en.png
      - /uploads/lightbox-facilities-12-en.png
      - /uploads/lightbox-facilities-13-en.png
      - /uploads/lightbox-facilities-14-en.png
      - /uploads/lightbox-facilities-15-en.png
      - /uploads/lightbox-facilities-16-en.png
      - /uploads/lightbox-facilities-17-en.png
      - /uploads/lightbox-facilities-18-en.png
      - /uploads/lightbox-facilities-19-en.png
      - /uploads/lightbox-facilities-20-en.png
      - /uploads/lightbox-facilities-21-en.png
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-01.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> MULTICENTRO EMPRESARIAL DEL ESTE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-02.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> MAIN ACCESS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-03.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> RECEPTION AREA</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-04.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> WAITING ROOM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-05.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> WAITING ROOM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-06.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> WAITING ROOM</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-07.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> TOILETS</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-08.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> CENTRAL CORRIDOR</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-09.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-10.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> INTERVIEW AREA</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-11.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-12.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> CLINICAL UNIT </h3
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-13.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-14.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> STERILIZATION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-15.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-16.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> RADIOLOGY</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-17.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-18.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> HIGH SUCTION</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-19.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> DENTAL ROOM 3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-20.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> LED LIGHTING</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-21.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3> LABORATORY</h3>

# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>
# Procedures Section
procedures:
  title: >
    <h1 class="title">One Specialty for Each Treatment!</h1>
  procedures:
    - title: <h5>dental implants</h5>
      to: >
        /en/specialties/dental-implants/
      img: /uploads/procedures-implants.png
    - title: <h5>Orthodontics</h5>
      to: >
        /en/specialties/orthodontics/
      img: /uploads/procedures-orthodontics.png
    - title: <h5>Aesthetic Dentistry</h5>
      to: >
        /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-aesthetic-dentistry.png
---
