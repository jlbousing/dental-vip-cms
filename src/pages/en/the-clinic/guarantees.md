---
templateKey: payment-options-page
language: en
redirects: /la-clinica/garantias/
title: Guarantees
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-guarantees.png
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Guarantees</h1>

banner: >
  <div class="banner">
    <aside>
      <p>
        <span style="margin-bottom: 25px;display: block;">Is Your Quote Too Expensive For Your Dental Treatment?</span>
        SAVE UP TO 70% <span style="color: #333;">WE CAN HELP YOU!</span>
      </p>
    </aside>
    <aside>
     <span class="travel">
        <i class="icon-travel"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
      </span>
    </aside>
  </div>

boxes:
  display: false
  boxes:
    - title: <h5>UNITED MEDICAL CREDIT</h5>
      to: /la-clinica/implantes-dentales/
      img: /uploads/procedures-united-medical-credit.png
    - title: <h5>DENEFITS</h5>
      to: /especialidades/ortodoncia/
      img: /uploads/procedures-denefits.png
    - title: <h5>MY MEDICAL FUNDING</h5>
      to: /especialidades/estetica-dental/
      img: /uploads/procedures-my-medical-founding.png
---

<h1>Our Guarantee Policy</h1>

<div class="row">
  <div>
    <p>
      Often, many patients overlook the issue of guarantees when deciding which
      dental clinic to choose and, at our discretion; it is undoubtedly one of
      the most important factors that differentiates one center from another.
    </p>
    <p>
      DENTAL VIP can offer you the best guarantee of any Dental Clinic in
      Caracas because we trust in the high level of our work, in the quality of
      the materials we use and in the precision of the dental technicians who
      labor in our prosthetic laboratory.
    </p>
    <p>
      If one of our guaranteed treatments fails structurally, it will be
      replaced free of charge for you by our Dentists, of course; under the
      terms and conditions detailed further down. This gives you that peace of
      mind that if anything is to happen, we will be here to solve it!
    </p>
  </div>
  <div class="icon">
    <i class="icon-schedule"> </i>
  </div>
</div>

<div class="message" style="margin-top:3.2rem !important">
  <p class="big">In the case of retreatment in a foreign patient, we will cover the costs of accommodation, but not those of transfer or airfare.</p>
</div>
<p>
  Below is an overview of the guarantee times for the different types of dental solutions we offer:
</p>
  <br />
  <br />
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 17%;"></span>
    </div>
    <div class="title">REPAIRS</div>
    <div class="time">6 months</div>
  </div>
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 34%;"></span>
    </div>
    <div class="title">FILLINGS</div>
    <div class="time">1 year</div>
  </div>
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 68%;"></span>
    </div>
    <div class="title">REMOVABLE SOLUTIONS</div>
    <div class="time">2 years</div>
  </div>
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 68%;"></span>
    </div>
    <div class="title">PORCELAIN VENEERS</div>
    <div class="time">2 years</div>
  </div>
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 68%;"></span>
    </div>
    <div class="title">FIXED SOLUTIONS</div>
    <div class="time">2 years</div>
  </div>
  <div class="percentaje">
    <div class="progress-bar">
      <span class="progress-bar-fill" style="width: 100%;"></span>
    </div>
    <div class="title">DENTAL IMPLANTS <i>(ZIMMER BIOMET)</i></div>
    <div class="time">3 years</div>
  </div>
<br />
<p>
 The main requirement to enforce the warranty right in cases of dental implants, fixed prosthodontics and porcelain veneers is the constant use of the night splint supplied at the end of the treatment. For this purpose, it must be presented upon arrival for its evaluation and to verify, according to its degree of natural wear; the correct use of it. No guarantee will be valid if the device is not presented, if it is not adjusted correctly due to lack of use or if a splint other than that made by our Specialists is detected.
</p>
<p>
  Another requisite for the guarantee to be valid is to attend an annual check-up and professional dental cleaning. Although you can have your regular checkups in your home country, to maintain the guarantee; you must send us an email with the bill of the treatment, a complete set of intraoral photographs and a panoramic X-ray to be archived in your file and be aware of your clinic condition. The timelessness in the execution of this requirement or in the reception of said records will automatically cancel any responsibility in terms of guarantee.
</p>
<p>
    Circumstances that can endanger the longevity of dental work are mainly chronic diseases, bad habits and oral hygiene deficiencies. In these cases, two annual checkups are obligatory for the guarantee to be valid; and one of them has to be done at our clinic. With some specific risk factors we cannot offer any type of guarantee, but we will always inform you of this beforehand.
</p>
<div class="message red">
  <h1>Excluding risk factors:</h1> 
  <ul style="font-weight: 300;"> 
    <li>Immune or degenerative pathologies.</li> 
    <li>Accidents and severe trauma.</li> 
    <li>Systemic diseases that have a detrimental effect on the body such as uncontrolled diabetes, epilepsy, osteoporosis and acquired immunodeficiency syndrome <em>(AIDS)</em>; among others.</li> 
    <li>Radio and chemotherapy.</li> 
    <li>Heavy smoking.</li> 
  </ul>
</div>
<p>The guarantee is invalidated if the patient or a third party modifies, without our express consent, the prosthesis or the dental work performed. Similarly, plastic and acrylic parts, temporary solutions, extensions and coatings are not covered; as well as no future endodontics or pulp inflammation treatment.</p>
<p>Occasionally, the teeth suffer trauma during the preparation of crowns and bridges, and that subsequently; merit root canal treatments. As such circumstance is unpredictable in the vast majority of cases, DENTAL VIP cannot be held responsible for any endodontic pathology that is not evident at the time of the initial consultation and diagnosis.</p>
<p>Any expense or amount incurred in another dental clinic, such as the final cementation of a definitive fixed prosthesis; is beyond the coverage of this Guarantee Policy.</p>
<p>Although if any complication or unforeseen occurs we will always seek conciliation as the preferred procedure to agree and resolve any dispute or clinical situation, we reserve the right to submit the treatment provided to the evaluation of independent professional experts; of course, only in those cases of doubt or disagreement in terms of guarantees.</p>
<br>
<br>
<h2  class="left section-title">
  <b>What does the guarantee include?
</b>
</h2>
<p>Although we would love to be able to guarantee our treatments for a lifetime, it is truly impossible and somewhat utopian; since the length of time of any dental rehabilitation will depend on a large number of factors totally unrelated to our work: the genetics and innate predisposition to certain pathological states, the eating habits, the present and future general health status, the appearance, development and evolution of systemic, bone, hormonal, degenerative, neoplastic or mental diseases; among others, the osteoporosis and associated intake of bisphosphonates in women, the consumption of drugs and other medications, the exposure to radiation, the suffering from strokes and facial trauma, the development of bruxism and other parafunctional habits, the true knowledge, mastery, skill and execution of the different oral hygiene techniques; the frequency of brushing, the constant use of the interdental brush and dental floss, the regular use of the occlusal splint and the attendance to scheduled periodic check-ups; are all conditions and activities only inherent to the patient, and therefore, under your sole and absolute responsibility.</p>
<p> So dental guarantees can only cover the costs <i>(partial or total)</i> of redo a treatment when it is necessary to repeat it, within a reasonable period of time, due to factors inherent to the professional and/or to the biomaterials used that directly prove failures of diagnosis, planning, execution and quality of care provided. </p>
<p> DENTAL VIP offers a very sincere and patient friendly Guarantee Policy. In case you have to make it effective and reside outside of Venezuela, we will organize your trip in the usual conditions and we will cover the following: </p>
<ul class="check-list">
  <li>
    <i class="icon-check circle"></i
    ><span
      >Flight booking.</span
    >
  </li>
  <li>
    <i class="icon-check circle"></i
    ><span
      >Hotel booking.</span
    >
  </li>
  <li>
    <i class="icon-check circle"></i
    ><span
      >Airport pick-up and drop off.</span
    >
  </li>
  <li>
    <i class="icon-check circle"></i
    ><span
      >Accommodation up to a maximum of 7 nights.</span
    >
  </li>
  <li>
    <i class="icon-check circle"></i
    ><span
      >Cost-free dental retreatment.</span
    >
  </li>
</ul>
<h2  class="left section-title">
  <b>How is the procedure performed?
</b>
</h2>
<p>
  In the case of any unforeseen, you should immediately go to the consultation if you live in the city; or if not, send photos and a brief description of the problem through our contact forms. If it is necessary to return to Caracas, we will examine your medical history and the images provided to try to determine why the failure occurred, plan the retreatment and make effective; if applicable, the Guarantee Policy already described.
</p>
<br>
