---
templateKey: clinic-page
language: en
redirects: /la-clinica/por-que-elegirnos/
title: Why choose us
description: >
  seo description
published: true
tags:
  - default tag
keywords:
  - default keyword
# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-why-choose-us.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: 70% 50%
  content:
    position: center
    body: >
      <h1>Why choose us</h1>

# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/hero-home.jpg
      content: <h2><span><i class="icon-instagram"></i></span> Alguna Vaina</h2>
        <p>gfiaej'osigja'eosijg'eijers'gjv</p>

# Heading Section
heading:
  display: true
  content: <h1 class="title">10 Reasons that Will Make the Difference</h1>
    <p>Making decisions is often complicated,
    but as rational beings we are, we will always decant for that alternative, which
    a priori; maximizes our personal well-being.</p>

# List Section
list:
  display: true
  items:
    - content: >
        <h3>Renowned Team of Specialist Dentists</h3>
        <p>High Level University Professionals with Postgraduate Studies in the
        different areas of Clinical Dentistry. All Specialties in the Same Place.</p>
    - content: >
        <h3>Modern and Comfortable Facilities</h3>
        <p>Quiet, Relaxed and Maximum Comfort Environment with Internet Service
        (Wi-Fi zone). Private and Totally Independent Dental Rooms.</p>
    - content: >
        <h3>The Best Technology</h3>
        <p>We have the Latest Generation Equipments Worldwide. We are always at
        the Forefront of Dental Innovation.</p>
    - content: >
        <h3>Experience and Professional Ethics</h3>
        <p>Dentists with Ample Trajectory and Unquestionable Vocation. A Human
        Team Truly Committed to what it Does.</p>
    - content: >
        <h3>Fully Personalized Attention</h3>
        <p>We are not a center of mass attention, and therefore, we do not delegate
        functions. You Will Always Be Attended by Your Trusted Specialist.</p>
    - content: >
        <h3>Organization and Minimum Waiting Time</h3>
        <p>We value and respect your time, that is why, We Work Under a Prior
        Appointment System that allows us to Optimize Our Service.</p>
    - content: >
        <h3>Excellent Location</h3>
        <p>We are in the Chacao Municipality, in the "center of the east" of the
        Capital City, within a Business Urban Complex of great economic and commercial
        activity.</p>
    - content: >
        <h3>Security and Easy Access</h3>
        <p>Numerous Private Surveillance, more than 2,000 Parking Positions at
        your disposal and pedestrian entrance from the Underground System Metro de Caracas.</p>
    - content: >
        <h3>Fair and Truly Competitive Prices</h3>
        <p> Surely the Bests in High Standing Dentistry.</p>
    - content: >
        <h3>Financing and Payment Facilities</h3>
        <p>Flexibility in the Collection of Professional Fees, Special Financing
        Plans and Commercial Point of Sale for payment with Debit and Credit Cards.</p>

# Quote Section
testimonial:
  display: false
  color: red
  content: >
    <p>THE RESPECT FOR THE LIFE AND INTEGRITY OF THE HUMAN PERSON, THE PROMOTION AND PRESERVATION OF HEALTH, AS A COMPONENT OF DEVELOPMENT AND SOCIAL WELFARE, AND ITS EFFECTIVE PROJECTION TO THE COMMUNITY; CONSTITUTE IN ALL CIRCUMSTANCES THE PRINCIPAL DUTY OF THE DENTIST".</p>
  images:
    portrait: /uploads/lightbox-smiles-01.jpg
    landscape: /uploads/lightbox-smiles-01.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-why-choose-us.jpg
  content: >
    <h1>Do You Live Outside of Venezuela?</h1>
    <p class="dv-subtitle text-center text-white">
      We are also an option!
    </p>
    <p class="text-left dv-subtitle">
      At present our country has become an important destination
      of Dental Tourism and there are already many foreign patients who visit us to
      receive First Level Health Care.
    </p>

    <p class="text-left dv-subtitle">
      Our recognized Quality of Care and the possibility of Saving Large Sums of Money in complex oral treatments are two competitive advantages difficult to ignore.
    </p>

    <p class="text-left dv-subtitle">
      For your convenience we have a privileged location and two excellent
      hotel infrastructures located less than 50 meters from the clinic. CHACAO SUITES
      and SHELTER SUITES offer nice and comfortable rooms, parking, restaurants and
      other services that will facilitate and make enjoyable your short stay in the
      city of Caracas.
    </p>
    <br>
    <h1>Thinking of traveling and visiting us?</h1>
    <h3>Know More About Our Clinical Acting Protocol.</h3>
    <br>
    <a class="link" href="/en/foreign-patients/">MORE INFO</a>

# Gallery Section
gallery:
  display: false
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
  items:
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>

# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">One Specialty for Each Treatment!</h1>
  procedures:
    - title: <h5>dental implants</h5>
      to: >
        /en/specialties/dental-implants/
      img: /uploads/procedures-implants.png
    - title: <h5>Prosthodontics</h5>
      to: >
        /en/specialties/prosthodontics/
      img: /uploads/procedures-prosthesis.jpg
    - title: <h5>Aesthetic Dentistry</h5>
      to: >
        /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-aesthetic-dentistry.png
---
