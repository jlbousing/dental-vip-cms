---
templateKey: clinic-page
language: en
redirects: /la-clinica/tecnologia/
title: Technology
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-technology.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Technology</h1>

# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2><span><i class="icon-instagram"></i></span> In the clinic…</h2>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title">A Last Generation Clinical Equipment</h1>
    <p>In DENTAL VIP we have the most modern equipments and the most advanced dental technology to offer you and your whole family cutting-edge and proven quality dental treatments.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>

# Quote Section
testimonial:
  display: false
  color: '#ededed'
  content: >
    <p class="content">THE RESPECT FOR THE LIFE AND INTEGRITY OF THE HUMAN PERSON, THE PROMOTION AND PRESERVATION OF HEALTH, AS A COMPONENT OF DEVELOPMENT AND SOCIAL WELFARE, AND ITS EFFECTIVE PROJECTION TO THE COMMUNITY; CONSTITUTE IN ALL CIRCUMSTANCES THE PRINCIPAL DUTY OF THE DENTIST".
    </p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait-en.jpg
    landscape: /uploads/quotes-phillosophy-en.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Follow Us</h1>
    <h2>On our blog and social networks</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/en/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">News, Articles, Topical Advices and Much More …</h2>

# Gallery Section
gallery:
    display: true
    type: staticGallery
    carousel:
      display: false
      placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
      items:
        - /uploads/lightbox-facilities-01.png
    items:
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-01.jpg
      placeholder: none
      body: >
        <h3>DENTAL LASER</h3>
        <p>The EPIC™ diode laser is a state-of-the-art surgical and therapeutic
        device, designed to perform a wide variety of soft tissue procedures, teeth whitening
        and temporary pain relief.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-02.jpg
      placeholder: none
      body: >
        <h3>HIGH INTENSITY LED LAMP</h3>
        <p class="dv-text-feat">The ZOOM Advanced Power is a cold light lamp
        for teeth whitening that emits UV light at wavelengths close to blue <i>(between
        365-500 nm)</i> and that provides energy to accelerate the diffusion and oxidation
        of bleaching solutions within the dental structure.</p>  
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-03.jpg
      placeholder: none
      body: >
        <h3>CAD-CAM SYSTEM</h3>
        <p>Computerized technology that allows 3D scanning, digitalization and
        data transfer to software that designs and manufactures any type of fixed ceramic
        restoration through the activation and control of a robotic milling system.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-04.jpg
      placeholder: none
      body: >
        <h3>KODAK RVG 5100 RADIOVISIOGRAPHY</h3>
        <p>Computerized digital radiology that minimizes the generation of X-rays
        and provides high-definition instantaneous images with more than 20 pairs of visible
        lines per millimeter, thus expanding the technical capabilities of diagnosis and
        clinical evaluation.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-05.jpg
      placeholder: none
      body: >
        <h3> INTRAORAL CAMERA</h3>
        <p>Captures and stores high-resolution digital images that allow us to
        appreciate in detail the initial oral condition of the patient, design the smile,
        compare and evaluate the final results of the esthetic dental treatment.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-06.jpg
      placeholder: none
      body: >
        <h3>ERGONOMIC AND BIARTICULATED ARMCHAIRS</h3>
        <p>Last generation dental units with wide and enveloping anatomical endorsements
        that provide great comfort to the patient, reduce the level of stress and avoid
        tensions or postural discomfort during treatment.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-07.jpg
      placeholder: none
      body: >
        <h3>UV CURING LAMP</h3>
        <p>Wireless halogen light device used to polymerize photosensitive plastic
        materials in dental restoration processes and operative dentistry.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-08.jpg
      placeholder: none
      body: >
        <h3>NSK CLEAN HEAD SYSTEM®</h3>
        <p>High speed rotary equipment specially manufactured to prevent the entry
        and accumulation of oral fluids and other contaminants, guaranteeing asepsis in
        all types of dental procedures.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-09.jpg
      placeholder: none
      body: >
        <h3>APEX LOCATOR</h3>
        <p>Electronic instrument that basing on the frequency, resistance and electrical
        impedance is able to locate the greatest apical constriction of the root and determine
        the length of work within the root canal.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-10.jpg
      placeholder: none
      body: >
        <h3>BIOSONIC® S1 ULTRASONIC SCALER</h3>
        <p class="dv-text-feat"> Powerful piezoelectric ultrasonic scaler that
        facilitates the elimination of dental calculus, fillings, defective crowns and
        the irrigation and mechanical instrumentation of root canals in endodontic therapy.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-11.jpg
      placeholder: none
      body: >
        <h3>VACUUM SYSTEM</h3>
        <p class="dv-text-feat">Vacuum machine for the thermoforming of myo-relaxing
        plates, whitening splints, occlusal splints for bruxism, mouth guards, base plates,
        individual trays, orthodontic retainers, surgical guides and provisional crowns.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-12.jpg
      placeholder: none
      body: >
        <h3>DIGITAL DENTISTRY</h3>
        <p class="dv-text-feat">Extraoral digital camera, smile design software
        and LED monitors to optimize communication with the patient and teamwork.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-13.jpg
      placeholder: none
      body: >
        <h3>SURGICAL IMPLANT UNIT</h3>
        <p class="dv-text-feat">Electric motor for surgery and oral implantology
        designed under an intuitive handling concept, with mechanized function and automatic
        torque control that facilitate the correct conformation of the surgical bed, insertion
        and screwing of dental implants.</p>
        
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-14.jpg
      placeholder: none
      body: >
        <h3>SANDBLASTER</h3>
        <p class="dv-text-feat">Sandblasting and microabrasion technology for
        the internal preparation of metals and ceramic restorations, adjustment and settlement
        of crowns, optimization of proximal contacts and removal of oxides and residual
        cements.</p>
      action: false
    - link:
        display: false
        to: /
      image: /uploads/gallery-technology-15.jpg
      placeholder: none
      body: >
        <h3>PROGRAMAT® P310</h3>
        <p class="dv-text-feat">Modern dental porcelain furnace equipped with
        varied programs of sintered and QTK2 muffle technology that guarantee a homogeneous
        distribution of heat, optimal atomic coalescence and the life cycle extension
        of the various ceramic elements.</p>
      action: false
    
# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">One Specialty for Each Treatment!</h1>
  procedures:
    - title: <h5>dental implants</h5>
      to: >
        /en/specialties/dental-implants/
      img: /uploads/procedures-implants.png
    - title: <h5>Prosthodontics</h5>
      to: >
        /en/specialties/prosthodontics/
      img: /uploads/procedures-prosthesis.jpg
    - title: <h5>Aesthetic Dentistry</h5>
      to: >
        /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-aesthetic-dentistry.png
---
