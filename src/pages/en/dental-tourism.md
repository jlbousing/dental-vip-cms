---
templateKey: dental-tourism
language: en
redirects: /turismo-dental/
title: Dental Tourism
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-dental-tourism.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 83%
  content:
    position: center
    body: >
      <h1>Save Up to 70% on High-End<br>Dental Treatments</h1>
prices:
  footer:
    image: /img/icons-OANDA.png
    title: >
      <p>Prices expressed in American dollars<em>(USD)</em>.<br>Convert them
      immediately to your currency with:<p>
    to: https://www1.oanda.com/currency/converter/
  rows:
    - title: Dental Implants
      icon: icon-dental-implants
      rows:
        - procedure: >
            <p>3D Cone Beam Scanner</p>
          price: $ 60
          currency: USD
        - procedure: >
            <p>Conscious Sedation <em>(optional)</em></p>
          price: $ 500
          currency: USD
        - procedure: >
            <p>Single Dental Implant</p>
          price: $ 700
          currency: USD
        - procedure: >
            <p>Multiple Dental Implants</p>
          price: $ 650
          currency: USD each
        - procedure: >
            <p>Sinus Lift</p>
          price: $ 850
          currency: USD
        - procedure: >
            <p>Synthetic Bone Graft</p>
          price: $ 200
          currency: USD per cc
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: "/en/specialties/dental-implants/\n"
    - title: Zygomatic Implants
      icon: ZIGOMATICOS
      rows:
        - procedure: >
            <p>General Anesthesia</p>
          price: $ 650
          currency: USD
        - procedure: >
            <p>Hospital Fees</p>
          price: $ 800
          currency: USD
        - procedure: >
            <p>Unilateral Zygomatic Implant</p>
          price: $ 2,800
          currency: USD
        - procedure: >
            <p>Bilateral Zygomatic Implants</p>
          price: $ 2,500
          currency: USD each
        - procedure: >
            <p>Quad Zygoma</p>
          price: $ 9,800
          currency: USD
        - procedure: >
            <p>Temporary Denture</p>
          price: $ 580
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/zygomatic-implants/
    - title: Implant Supported Restorations
      icon: ICON-IMPLANT SUPPORT
      rows:
        - procedure: >
            <p>Porcelain Fused to Metal Crown</p>
          price: $ 480
          currency: USD
        - procedure: >
            <p>CAD/CAM Zirconia Crown</p>
          price: $ 570
          currency: USD
        - procedure: >
            <p>Implant Supported Bridge</p>
          price: $ 480
          currency: USD per unit
        - procedure: >
            <p>Snap on Denture</p>
          price: $ 1,350
          currency: USD
        - procedure: >
            <p>All on Four</p>
          price: $ 2,600
          currency: USD
        - procedure: >
            <p>Fixed Hybrid Denture</p>
          price: $ 2,600
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/dental-implants/implant-supported-restorations/
    - title: Aesthetic Dentistry
      icon: icon-aestetic-dentistry
      rows:
        - procedure: >
            <p>Digital Smile Design</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p>Gum Contouring Surgery</p>
          price: $ 120
          currency: USD per quadrant
        - procedure: >
            <p>Dental Reshaping</p>
          price: $ 50
          currency: USD per arch
        - procedure: >
            <p>LED Teeth Whitening</p>
          price: $ 180
          currency: USD
        - procedure: >
            <p>Composite Direct Veneer</p>
          price: $ 100
          currency: USD
        - procedure: >
            <p>IPS e.max Ceramic Veneer</p>
          price: $ 490
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: "/en/specialties/aesthetic-dentistry/\n"
    - title: Endodontics
      icon: icon-endodontics
      rows:
        - procedure: >
            <p>Root Canal Therapy</p>
          price: $ 190
          currency: USD
        - procedure: >
            <p>Root Canal Retreatment</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p>Post Space Preparation</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p>Post and Core Build Up</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p>Apicoectomy or Root Resection</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p>MTA Surgical Retrofilling</p>
          price: $ 100
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/endodontics/
    - title: Oral Surgery
      icon: icon-oral-surgery
      rows:
        - procedure: >
            <p>Simple Extraction</p>
          price: $ 45
          currency: USD
        - procedure: >
            <p>Surgical Odontectomy</p>
          price: $ 80
          currency: USD
        - procedure: >
            <p>3rd Molar Extraction</p>
          price: $ 150
          currency: USD
        - procedure: >
            <p>Lingual or Labial Frenectomy</p>
          price: $ 100
          currency: USD
        - procedure: >
            <p>Alveoloplasty</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p>Oral Lesion Removal and Biopsy</p>
          price: $ 180
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/oral-surgery/
    - title: Conventional Prosthesis
      icon: icon-prosthodontics
      rows:
        - procedure: >
            <p>Metal-Porcelain Crowns and Bridges</p>
          price: $ 430
          currency: USD per unit
        - procedure: >
            <p>IPS e.max Crowns and Bridges</p>
          price: $ 480
          currency: USD per unit
        - procedure: >
            <p>Zirconia CAD/CAM Crowns and Bridges</p>
          price: $ 550
          currency: USD per unit
        - procedure: >
            <p>Removable Partial Denture</p>
          price: $ 450
          currency: USD
        - procedure: >
            <p>Regular Total Denture</p>
          price: $ 550
          currency: USD
        - procedure: >
            <p>Occlusal Guard</p>
          price: $ 180
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: "/en/specialties/prosthodontics/\n"
    - title: General Dentistry
      icon: icon-general-dentistry
      rows:
        - procedure: >
            <p>General Cleaning</p>
          price: $ 40
          currency: USD
        - procedure: >
            <p>Deep Cleaning and Prophylaxis</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p>Composite Filling 1 Surface</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p>Composite Filling 2 Surfaces</p>
          price: $ 70
          currency: USD
        - procedure: >
            <p>Composite Filling 3 Surfaces</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p>Ceramic Inlay/Onlay</p>
          price: $ 420
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/general-dentistry/
    - title: Periodontics
      icon: icon-periodontics
      rows:
        - procedure: >
            <p>Scaling and Root Planing</p>
          price: $ 120
          currency: USD per quadrant
        - procedure: >
            <p>Open Curettage</p>
          price: $ 180
          currency: USD per quadrant
        - procedure: >
            <p>Mucogingival Surgery</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p>Gum Grafting</p>
          price: $ 180
          currency: USD per tooth
        - procedure: >
            <p>Gingivectomy</p>
          price: $ 120
          currency: USD per quadrant
        - procedure: >
            <p>Guided Tissue Regeneration</p>
          price: $ 350
          currency: USD per defect
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: /en/specialties/periodontics/
    - title: Orthodontics
      icon: icon-orthodontics
      rows:
        - procedure: >
            <p>Metal Braces</p>
          price: $ 500
          currency: USD (Upper and Lower)
        - procedure: >
            <p>Porcelain Braces</p>
          price: $ 1,000
          currency: USD (Upper and Lower)
        - procedure: >
            <p>Sapphire Braces</p>
          price: $ 1,500
          currency: USD (Upper and Lower)
        - procedure: >
            <p>Bonded Retainer</p>
          price: $ 180
          currency: USD
        - procedure: >
            <p>Essix or Hawley Retainer</p>
          price: $ 150
          currency: USD
        - procedure: >
            <p>Dentofacial Orthopedic Devices</p>
          price: $ 1,800
          currency: USD
      link:
        image: /img/icon-oral-surgery.jpg
        title: MORE INFO
        to: "/en/specialties/orthodontics/\n"
routes:
  title: <h1> Most Frequent Routes</h1>
  image: travel
  icons:
    clock: clock
    currency: currency
  footer: >-
    When planning your trip, we will need to check the availability of non-stop
    flights and current rates.
  departures:
    - from: >
        From: BOGOTA
      flag: co
      time: >
        1h : 46m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: BRIDGETOWN
      flag: bb
      time: >
        1h : 33m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: GEORGETOWN
      flag: gy
      time: >
        1h : 48m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: KINGSTOWN
      flag: vc
      time: >
        1h : 21m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: MADRID
      flag: es
      time: >
        9h : 12m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: MIAMI
      flag: us
      time: >
        3h : 14m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: PANAMA
      flag: pa
      time: >
        2h : 17m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: PORT OF SPAIN
      flag: tt
      time: >
        1h : 14m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: TORONTO
      flag: ca
      time: >
        5h : 18m
      cost: 875.00 USD
      visa: No Visa Required

    - from: >
        From: WILLEMSTAD
      flag: cw
      time: >
        0h : 51m
      cost: 875.00 USD
      visa: No Visa Required

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Dental Tourism in Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>An Attractive Reality!</h2>
          <p>
          Eat, smile and enjoy as before. Do not compromise your quality of life anymore and take advantage of the great benefits that globalization offers in the health sector. Take our attention proposal seriously and overcome once and for all any barrier that prevents you from showing white, healthy and beautiful teeth.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accessibility</h2>
          </div>
          <div class="content">
            <p>
            Huge possibility of materializing aesthetic, extensive or highly complex dental treatments; often unattainable for the vast majority of people in many countries of Europe, North, Central and South America and the Caribbean Islands.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Immediacy</h2>
          </div>
          <div class="content">
            <p>
            Your appointment instantly, without delays or waiting lists. Exclusive, intensive and multidisciplinary care to cover in record time all the oral requirements previously established in the diagnostic and therapeutic planning phases.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Top Quality</h2>
          </div>
          <div class="content">
            <p>
            At your disposal the experience of the best local team of Specialist Dentists, the most modern and comfortable facilities and the latest technology worldwide. A widely recognized service and in accordance with the highest international standards in oral health.
            </p>
          </div>

gallerySteps:
  title: >
    <h1> Making Denstistry Care Simple and Affordable </h1>
  steps:
    - title: >
        <h3>Contact and Submission of Basic Dental Studies</h3>
      image: /uploads/steps-1-dental-tourism.png
    - title: >
        <h3>Treatment Plan and Budgeting</h3>
      image: /uploads/steps-2-dental-tourism.png
    - title: >
        <h3>Travel, Reception and Accommodation</h3>
      image: /uploads/steps-3-dental-tourism.png
    - title: >
        <h3>Complementary Diagnostic Tests</h3>
      image: /uploads/steps-4-dental-tourism.png
    - title: >
        <h3>Intensive Dental Treatment</h3>
      image: /uploads/steps-5-dental-tourism.png
    - title: >
        <h3> New Smile and Homecoming</h3>
      image: /uploads/steps-6-dental-tourism.png

# Heading Section
heading:
  display: true
  content: <h1 class="title">Visit Us in Caracas and Get a VIP Smile!</h1>
    <p>There are already thousands of patients
    who have found in Venezuela the best solution to their oral health needs, thanks
    to our reasonable cost structure and easy access to first level dental services.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-dental-tourism.png
  content: >
    <h1 class="bold">Dental Extreme Makeover in Just 1 or 2 Weeks!</h1>
    <h2  class="bold">A truly intensive and multidisciplinary treatment.</h2>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-annexed-pages.jpg

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-dental-tourism-calculator.png
      content: >
        <h1>Do you already have a dental quote?</h1>
        <p>Have you visited several Dentists and still cannot find a treatment option that is truly within your means? Want to get more out of your money? </p>
        <p class="dv-srv-pl text-left dv-npr">Send us your quote to improve it and submit it to your consideration. We will surely surprise you!</p> <br>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: true
          to: mailto:info@dentalvipcaracas.com?Subject=I%20have%20a%20budget
          placeholder: <span> LOAD FILE</span>
    - img: /uploads/sections-dental-tourism-hotel.png
      content: >
        <h1>Safety and comfortable hotels</h1>
        <p class="dv-srv-pl text-left dv-npr">Our privileged location allows us to have two excellent hotel infrastructures located less than 50 meters from the clinic. CHACAO SUITES and SHELTER SUITES offer nice and comfortable rooms, restaurants, private security and other services that will facilitate and make enjoyable your brief stay in the city of Caracas. </p>
        <p class="dv-srv-pl text-left dv-npr">ESTIMATED COST PER DAY: 50.00 USD <em>(meals not included)</em>.</p> 
        <br>
        <div class="link-box"><a href="http://www.hotelchacaosuites.com/index.php?idioma=en" class="link">CHACAO SUITES</a> &nbsp;&nbsp;&nbsp; <a class="link">SHELTER SUITES</a></div>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-dental-tourism-24.png
      content: >
        <h1>Full travel assistance and permanent support</h1>
        <p>Our administrative staff is able to provide you support in everything related to air tickets, airport-hotel-airport transfers and accommodation procedures. </p>
        <p class="dv-srv-pl text-right dv-npr">Personalized attention 24 hours a day and the permanent accompaniment of a bilingual guide, if you wish, are an integral part of our conception of service, of our effort to make your experience as comfortable, fruitful and pleasant as possible.</p>
        <p class="dv-srv-pl text-right dv-npr">We will try to make you feel at home!</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">We Are Just One Step Away!</h1>
  procedures:
    - title: >
        <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Professional Staff</h5>
      to: /en/professional-staff/
      img: /uploads/procedures-professionals.png
---
