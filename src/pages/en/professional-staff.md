---
templateKey: professionals-page
language: en
redirects: /profesionales/
title: Professional Staff
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/profesionales-banner.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 14%
  content:
    position: center
    body: >
      <h1 class="dark">Professional Staff</h1>

# Parallax
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads//uploads/parallax-form-professionals.jpg

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title">Maximum Training and Experience!</h1>
    <p>A recognized team of Specialist Dentists
    with extensive fourth level academic training, long care trajectory and solid
    leadership in the profession; completely identified with excellence and optimum
    quality of service.</p>
staff:
  title: >
    <h1>Clinic, Laboratory and Administration Staff</h1>
  cards:
    - img: /uploads/personal-angelo-sansone-ruggero.jpg
      content: >
        <h5>TPD. Angelo Sansone Ruggero</h5>
        <p> Fixed Prosthesis and Dental Ceramics</p>
    - img: /uploads/personal-denis-diaz-alvarez.jpg
      content: >
        <h5>TPD. Denis Díaz Álvarez</h5>
        <p>Acrylics and Removable Dentures</p>
    - img: /uploads/personal-vanesa-hernandez.jpg
      content: >
        <h5>TPD. Vanesa Hernández</h5>
        <p>Functional Devices and Active Plates</p>
    - img: /uploads/personal-aymara-guillen-portillo.jpg
      content: >
        <h5>Aymara Guillén Portillo</h5>
        <p>Reception and Patient Care</p>
    - img: /uploads/personal-maria-betancourt-matos.jpg
      content: >
        <h5>María Betancourt Matos</h5>
        <p>Dental Hygienist</p>
    - img: /uploads/personal-gisela-garcia.jpg
      content: >
        <h5>Gisela García</h5>
        <p>Dental Hygienist</p>
    - img: /uploads/personal-paola-rivas.jpg
      content: >
        <h5>Paola Rivas</h5>
        <p>Financial Management</p>
    - img: /uploads/personal-esteban-garrido.jpg
      content: >
        <h5>Lic. Esteban Garrido</h5>
        <p>Administration and Accounting</p>
    - img: /uploads/personal-maria-jose-tirado.jpg
      content: >
        <h5>María José Tirado</h5>
        <p>Office Coordination and Social Media</p>
# anex-links
professionals:
  display: true
  items:
    - img: /uploads/professionals-dr-castor-jose-garaban-povea.png
      content: >
        <h2 class="light">Dr. Castor José Garabán Povea</h2>
        <h3 class="bebas boxed">ORAL SURGERY - DENTAL IMPLANTS</h3>
        <ul>
        <li class="nl"><strong>Doctor of Dental Surgery</strong>&nbsp;<em>(Universidad Central de Venezuela, 1994)</em>.</li>
        <li class="nl"><strong>Specialist Degree in Oral Surgery</strong>&nbsp;<em>(Universidad Central de Venezuela, 2006)</em>.</li>
        <li class="nl">Postgraduate Course in Oral Implantology and Implant Prosthodontics.</li>
        <li class="nl">Diploma in Osseointegration and Peri-Implant Bone Regeneration.</li>
        <li class="nl">Permanent training in Conservative and Microinvasive Surgical Techniques. </li>
        <li class="nl">Clinical Expert in the management of multiple systems of Advanced Oral Implantology. </li>
        <li class="nl">Academic Assistant Professor at the Faculty of Dentistry, U.C.V. </li>
        <li class="nl">Member of Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Member of Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Member of Sociedad Venezolana de Cirugía Buco-Maxilofacial&nbsp;<em>(S.V.C.B.M.F.)</em>.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-castor-jose-garaban-povea-studies.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-filomena-montemurro-tafuri.png
      content: >
        <h2 class="light">Dr. Filomena Montemurro Tafuri</h2>
        <h3 class="bebas boxed">PROSTHESIS - DENTAL AESTHETICS</h3>
        <ul>
        <li class="nl"><strong>Doctor of Dental Surgery</strong>&nbsp;<em>(Universidad Santa María, 2001)</em>.</li>
        <li class="nl"><strong>Specialist in Prosthodontics</strong>&nbsp;<em>(Collegio dei Docenti di Odontoiatria, Italy, 2003).</em></li>
        <li class="nl">Advanced course in Aesthetic and Restorative Dentistry&nbsp;<em>(U.S.M., 2004).</em></li>
        <li class="nl">Master in design and confection of Implant Supported Restorations.</li>
        <li class="nl">Residence in applied clinical Prosthodontics and Implantology.</li>
        <li class="nl">Multiple training seminars in Smile Design and CAD-CAM Technologies.</li>
        <li class="nl">Diploma in Dental Ceramics, Occlusion and Operative Dentistry.</li>
        <li class="nl">Private practice limited to the area of Dental Aesthetics, Prosthetics and Oral Rehabilitation.</li>
        <li class="nl">Member of Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Member of Colegio de Odontólogos Metropolitano.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-filomena-montemurro-tafuri-studies.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-javier-martinez-tellez.png
      content: >
        <h2 class="light">Dr. Javier Martínez Téllez</h2>
        <h3 class="bebas boxed">GENERAL DENTISTRY - PERIODONTICS</h3>
        <ul>
        <li class="nl"><strong>Doctor of Dental Surgery</strong>&nbsp;<em>(Universidad Central de Venezuela, 2000).</em></li>
        <li class="nl"><strong>Postgraduate Course in Integral Stomatology of the Adult</strong>&nbsp;<em>(Universidad Santa María, 2004).</em></li>
        <li class="nl"><strong>Specialist Degree in Periodontics</strong>&nbsp;<em>(Universidad Central de Venezuela, 2014).</em></li>
        <li class="nl">Aspiring to the degree of DOCTOR OF DENTISTRY.</li>
        <li class="nl">Diploma in Advanced Guided Tissue Regeneration Techniques.</li>
        <li class="nl">Author of several articles in national and international journals.</li>
        <li class="nl">Practice focused on the prevention and treatment of Periodontal Pathology.</li>
        <li class="nl">Member of Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Member of Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Member of Sociedad Venezolana de Periodontología.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-javier-martinez-tellez-studies.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dr-jose-miguel-gomez-diez.png
      content: >
        <h2 class="light">Dr. José Miguel Gómez Díez</h2>
        <h3 class="bebas boxed">ORTHODONTICS - DENTOFACIAL ORTHOPEDICS</h3>
        <ul>
            <li class="nl"><strong>Doctor of Dental Surgery</strong>&nbsp;<em>(Universidad Central de Venezuela, 1996).</em></li>
            <li class="nl"><strong>Master Degree in Orthodontics</strong>&nbsp;<em>(Universidad Autónoma de Tamaulipas, Mexico, 2003).</em></li>
            <li class="nl">Fellowship Program in Clinical Orthodontics.</li>
            <li class="nl">Straight Wire System Certification Course.</li>
            <li class="nl">Advanced training in Occlusion and Craniomandibular Dysfunction.</li>
            <li class="nl">Numerous stays of academic formation in Spain, Mexico and USA.</li>
            <li class="nl">Exclusive dedication to the Specialty of Orthodontics and Dentofacial Orthopedics.</li>
            <li class="nl">Member of Colegio de Odontólogos de Venezuela.</li>
            <li class="nl">Member of Colegio de Odontólogos Metropolitano.</li>
            <li class="nl">Member of Colegio de Odontólogos del Estado Miranda.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dr-jose-miguel-gomez-diez-studies.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>
    - img: /uploads/professionals-dra-vianka-xaviera-torres.png
      content: >
        <h2 class="light">Dr. Vianka Xaviera Torres</h2>
        <h3 class="bebas boxed">GENERAL DENTISTRY - ENDODONTICS</h3>
        <ul>
        <li class="nl"><strong>Doctor of Dental Surgery</strong>&nbsp;<em>(Universidad Central de Venezuela, 2000).</em></li>
        <li class="nl"><strong>Endodontics Postgraduate Course</strong>&nbsp;<em>(Universidad Autónoma de Tamaulipas, Mexico, 2003)</em>.</li>
        <li class="nl">Diploma in Pharmacotherapy and Immunopharmacology.</li>
        <li class="nl">Clinical Expert in the management of Endo-Periodontal and Endo-Prosthetic Lesions.</li>
        <li class="nl">Advanced training in the practical use of Nickel-Titanium Rotary Systems.</li>
        <li class="nl">Certified formation in Thermoplastic Condensation and Obturation Techniques.</li>
        <li class="nl">Attendance at more than 50 theoretical and practical courses of the Specialty.</li>
        <li class="nl">Member of Colegio de Odontólogos de Venezuela.</li>
        <li class="nl">Member of Colegio de Odontólogos Metropolitano.</li>
        <li class="nl">Member of Colegio de Odontólogos del Estado Miranda.</li>
        </ul>
      footer:
        icon:
          display: true
          img: /uploads/professionals-dra-vianka-xaviera-torres-studies.jpg
        link:
          display: false
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">Top Quality Dental Center!</h1>
  procedures:
    - title: <h5>Why Choose Us</h5>
      to: /en/the-clinic/why-choose-us/
      img: /uploads/procedures-why-choose-us.png
    - title: <h5>Facilities</h5>
      to: /en/the-clinic/facilities/
      img: /uploads/procedures-facilities.jpg
    - title: <h5>Technology</h5>
      to: /en/the-clinic/technology/
      img: /uploads/procedures-technology.jpg
---
