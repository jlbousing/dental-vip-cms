---
templateKey: foreign-patients-page
language: en
redirects: /pacientes-del-exterior/
title: Foreign Patients
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-foreign-patients.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">FOREIGN
        PATIENTS</h1>

form:
  title: >
    <h1>Consult Us Right Now!</h1>
  background: /uploads/parallax-form-professionals.jpg

# Heading Section
altHeading:
  title: >
    <p>In DENTAL VIP we have been consequents for years with all the patients who visit us from anywhere in Venezuela and the world. Our team understands, appreciates, thanks and honors the great effort of all those people who travel great distances in search of Specialized Dental Care by offering them a special protocol of care <em>(widely known in the USA as "Dental Extreme Makeover")</em> that contemplates two phases:
    </p>
  columns:
    - head: >
        <div class="dv-title-circle text-center">Phase 1</div>
      body: >
        <p> Contact, execution and reception of the basic dental studies and relevant diagnostic evaluations.
        <br />
        <br />
         Case study, DIAGNOSIS, planning and referral of a specific treatment proposal; including of course, an estimate of professional fees or dental quote.
        </p>
    - head: >
        <div class="dv-title-circle text-center">Phase 2</div>
      body: >
        <p>Displacement, reception and accommodation.</p>
        <br />
        <br />
            <p class="dv-content-phase">INTENSIVE AND MULTIDISCIPLINARY DENTAL TREATMENT to be able to cover in record time <em>(1 or 2 weeks)</em> all the oral requirements previously established.
        </p>

heading:
  display: true
  content: >
    <h1 class="title">Your Health: A Good Reason for Travel</h1>
    <p>Globalization has made Dental Tourism
    a wonderful tool for access to First Level Dentistry. Due to its recognized trajectory
    in the sector and reasonable cost structure, Venezuela undoubtedly consolidates
    itself as one of the best destinations worldwide.</p>

    <ul class="maps">
    <li>
        <span><i class="icon-north-america"></i></span>
        <p class="dv-content">Save more than</p> <p class="dv-content-number">70%</p>
      <p class="dv-content">IF YOU LIVE IN</p> <p class="dv-content-country">United
      States or canada</p>
    </li>
    <li>
        <span><i class="icon-europe"></i></span>
       <p class="dv-content">Save more than</p> <p class="dv-content-number">50%</p>
      <p class="dv-content">If you live in</p> <p class="dv-content-country">SPAIN,
      ITALY OR PORTUGAL</p>

    </li>
    <li>
        <span><i class="icon-south-america"></i></span>
        <p class="dv-content">Save more than</p> <p class="dv-content-number">50%</p>
      <p class="dv-content">if you live in</p> <p class="dv-content-country">CENTRAL,
      SOUTH AMERICA OR CARIBBEAN ISLANDS</p>

    </li>
    </ul>

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>Do You Already Have a Dental Quote?</h1>
    <br>
    <h2 >Send it to us via e-mail to homologate it and submit it for your consideration.  We
    will surely surprise you!</h2>

hostSection:
  bg: /uploads/host-bg.jpg
  title: <h1 style="text-align:center;">Transfers and Accommodation</h1>
  body: >
    <p style="word-break:break-word;">Our privileged location allows us to also have two excellent hotel infrastructures
    located less than 50 meters from the clinic. CHACAO SUITES and SHELTER SUITES
    have comfortable rooms, parking, restaurants and other services that will facilitate
    and make pleasant, if necessary, your brief stay in the city of Caracas. If it
    is your desire, our administrative staff is able to provide support in everything
    related to air ticket office, airport-hotel-airport transfers and accommodation
    procedures.</p>
  columns:
    - title: <p>Chacao Suites</p>
      link: http://www.hotelchacaosuites.com/index.php?idioma=en
      img: /img/host-chacao-suites.jpg
    - title: <p>Shelter Suites</p>
      link: https://www.hotel-shelter.com/reserva/
      img: /img/host-shelter-suites.jpg

# Procedures Section
procedures:
  title: >
    <h1 class="title">One Specialty for Each Treatment!</h1>
  procedures:
    - title: <h5>dental implants</h5>
      to: >
        /en/specialties/dental-implants/
      img: /uploads/procedures-implants.png
    - title: <h5>Prosthodontics</h5>
      to: >
        /en/specialties/prosthodontics/
      img: /uploads/procedures-prosthesis.jpg
    - title: <h5>Aesthetic Dentistry</h5>
      to: >
        /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-aesthetic-dentistry.png
---
