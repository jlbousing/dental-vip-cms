---
templateKey: advise-page
language: en
title: Legal Notice
description: >
  Seo description
redirects: /aviso-legal/
published: true
keywords:
  - default
---

# LEGAL NOTICE

## Identifying data

You are visiting the website [https://www.dentalvipcaracas.com](/en) owned by DENTAL VIP, Especialidades Odontológicas s.c. _(hereinafter, THE OWNER)_, with registered office at Multicentro Empresarial del Este, Miranda Tower, Nucleus A, 14th Floor, Office A-143, Chacao, Caracas, Venezuela. Registered at the Public Registry of the Municipality of Chacao on 09/14/2012, under number 45, folio 366 and volume 42 of the protocol for 2012.

You can contact THE OWNER by any of the following means:  
Phones: +58 _(212)_ 261.3732 / 261.3331 / 261.5251  
Email: [info@dentalvipcaracas.com](mailto:info@dentalvipcaracas.com)

## Web hosting

DigitalOcean, LLC is a startup provider of cloud computing infrastructure services that offers a platform focused on software developers.  
Address: 101 Avenue of the Americas 10th Floor New York, NY 10013 United States.  
Phone: 1-347-903-7918  
Email address: [team@info.digitalocean.com](mailto:team@info.digitalocean.com)  
Website: https://www.digitalocean.com/

## General users

These conditions _(hereinafter, Legal Notice)_ are intended to regulate the use of the website that THE OWNER makes available to the general public.

The access and/or use of this website attributes the condition of USER, who accepts, from said access and/or use, the general conditions reflected here. The aforementioned conditions will be universally applicable and mandatory compliance.

## Portal use

[https://www.dentalvipcaracas.com](/en/) exposes a multitude of information, services, programs and data _(hereinafter, THE CONTENTS)_ on the internet, belonging to THE OWNER or its licensors; and to which THE USER may have access.

THE USER assumes responsibility for the use of the portal. This responsibility extends to the registration that is necessary to access certain services or contents. In said registry, THE USER will be responsible for providing truthful and lawful information. As a result of this registration, THE USER could be provided with a password for which he will be responsible, committing himself to make diligent and confidential use of it.

THE USER undertakes to make appropriate use of the contents and services that THE OWNER offers through its portal, and with an enunciative but not limiting nature, not to use them to:

- To fall into illegal activities or contrary to good faith and public order.

- Disseminate content or propaganda racist, xenophobic, pornographic, of apology of terrorism or that attempts against human rights.

- Causing damage to the physical and logical systems of DENTAL VIP, Especialidades Odontológicas s.c., its suppliers or third parties, introducing or spreading computer viruses or any other physical or logical systems that are likely to cause the aforementioned damages.

- Try to access and use the email accounts of other users, modify or manipulate their messages.

- Use the website or the information it contains for commercial, political, advertising or similar purposes, and particularly, sending unsolicited emails.

THE OWNER reserves the right to withdraw all comments and contributions that violate respect for the dignity of the person, that are discriminatory, xenophobic, racist, pornographic, that threaten youth or childhood, order or public safety or that, in his opinion; they are not suitable for publication. In any case, THE OWNER will not be responsible for the opinions expressed by users through forums, chats, social networks or other digital participation tools.

## Data protection

Everything related to the data protection policy is contained in the [Privacy Policy](/en/privacy-policy/) document.

## Contents. Intellectual and industrial property

THE OWNER owns all the intellectual and industrial property rights of his website, as well as the elements contained therein: images, photographs, sound, audio, video, software or texts, brands or logos, color combinations, structure and design, selection of materials used, codes, computer programs necessary for its operation, access and use; among others.

All Rights Reserved This website is protected by copyright, trademarks, service marks, patents and other property rights; in addition to those derived from current laws. Unauthorized use of any copyrighted material, trademarks or any other intellectual property without the express written consent of the owner, is strictly prohibited.

## Exclusion of guarantees and responsibility

THE USER acknowledges that the use of the website, its contents and services is carried out under its sole responsibility. Specifically, and by title merely enunciative, THE OWNER assumes no responsibility in the following areas:

1.  The availability of the operation of the website, its services, contents and its quality or interoperability.

2.  The purpose for which the website serves the objectives of THE USER.

3.  The infringement of the current legislation by THE USER or third parties and, in particular, of the intellectual and industrial property rights that are owned by other persons or entities.

4.  The existence of malicious codes or any other harmful computer element that could damage the USER's or third party's computer system. It is up to THE USER, in any case, to have adequate tools for the detection and disinfection of these elements.

5.  Fraudulent access to the contents or services by unauthorized third parties, or, when appropriate; the capture, elimination, alteration, modification or manipulation of messages and communications of any kind that said third parties could make.

6.  The correctness, veracity, topicality and usefulness of the content and services offered; and the subsequent use made of them by THE USER. THE OWNER will use all reasonable efforts and means to provide up-to-date and reliable information.

7.  Damage to computer equipments during access to the website and damage to USERS when they originate from failures or disconnections in telecommunications networks that interrupt the service.

8.  Damages or losses arising from circumstances that occur due to a fortuitous event or force majeure.

In case there are forums, in the use of the same or other similar spaces, it should be taken into account that the messages will reflect only the opinion of THE USER who sends them, which is solely responsible. THE OWNER is not responsible for the content of the messages sent by THE USER.

## Modification of this legal notice and duration

THE OWNER reserves the right to make, without prior notice, the modifications he deems appropriate in his portal; being able to change, delete or add as many content and services as he wants, such as the way in which they are represented or located in his portal.

The validity of the aforementioned conditions will be based on their exposure and will be valid until they are modified by others duly published.

## Links

In the event that links or hyperlinks to other internet sites are included in [https://www.dentalvipcaracas.com](/en), THE OWNER will not exercise none type of control over said sites and contents. In no case, THE OWNER will assume any responsibility for the contents of any link belonging to a third-party website, nor will it guarantee the technical availability, quality, reliability, accuracy, breadth, veracity, validity and constitutionality of any matter or information contained in any of such hyperlinks and other internet sites. Similarly, the inclusion of these external connections will not imply any type of association, merger or participation with the connected entities.

## Exclusion rights

THE OWNER reserves the right to deny or withdraw access to the portal and/or the services offered without prior warning, at its own request or from a third party, to those USERS who breach the content of this Legal Notice.

## Generalities

THE OWNER will pursue the breach of these conditions, as well as any improper use of his portal, exercising all civil and criminal actions that may correspond him by law.

## Applicable law and jurisdiction

The relationship between THE OWNER and THE USER will be governed by current Venezuelan regulations. All disputes and claims arising from this Legal Notice will be resolved by Venezuelan courts and tribunals.

## Minor users

[https://www.dentalvipcaracas.com](/en) directs its services to USERS over 18 years old. People under this age are not authorized to use our services and should not, therefore, send us their personal data. We inform you that if such circumstance occurs, DENTAL VIP, Especialidades Odontológicas s.c. will not be responsible for the possible consequences that could be derived from the breach of the warning that this clause establishes.
