---
templateKey: contact-page
language: es
redirects: /en/contact/
title: Contacto
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-contact.png
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Contacto</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title">Estamos para Servirle</h1>
    <p>Si necesita información adicional,
    desea realizar una consulta, hacer sugerencias o reservar espacio en agenda; podemos
    atenderle vía telefónica, mediante el uso del formulario contiguo o enviando un
    mensaje de correo electrónico a <a href="mailto:info@dentalvipcaracas.com" style="color:#91c508">info@dentalvipcaracas.com</a></p>

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-contact.png
  content: >
    <h1 class="big">Síganos</h1>
    <h2>En nuestro blog y redes sociales</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">Noticias, Artículos, Consejos de Actualidad y Mucho Más...</h2>

# Procedures Section
amenities:
  title: >
    <h1 class="title">¡Será un Honor Recibir Su Visita!</h1>
  procedures:
    - title: <h5>METRO DE CARACAS</h5>
      to: /en/the-clinic/dental-implants/
      img: /uploads/procedures-subway.jpg
      content: >
        <p>
          <span>
            <i class="icon icon-instagram"></i>
          </span>
          <br></br>
          Línea 1 <br class="visible-xs visible-sm visible-md visible-lg">
          Estación Chacao <br class="hidden-xs hidden-sm visible-md visible-lg">
          Salida: Av. Francisco de Miranda/Calle los <br class="hidden-xs hidden-sm visible-md visible-lg">Maristas.
        </p>
    - title: <h5>Parking</h5>
      to: /en/specialties/orthodontics/
      img: /uploads/procedures-parking.jpg
      content: >
        <p>
          <span>
            <i class="icon icon-instagram"></i>
          </span>
          <br></br>
          Estacionamiento Multicentro Empresarial del Este
          <br class="visible-xs visible-sm visible-md visible-lg" />
          Accesos por:
          <br class="hidden-xs hidden-sm visible-md visible-lg" />
          Av. Francisco de Miranda <em>(Sentido Este).</em>
          <br class="hidden-xs hidden-sm visible-md visible-lg" />
          Av. Libertador <em>(Sentido Oeste).</em>
        </p>
    - title: <h5>ZONA WI-FI</h5>
      to: /en/specialties/aesthetic-dentistry/
      img: /uploads/procedures-wifi.jpg
      content: >
        <p>
          <span>
            <i class="icon icon-instagram"></i>
          </span>
          <br></br>
          ¡Le Mantendremos Siempre Conectado! <br class="visible-xs visible-sm visible-md visible-lg">
          INTERNET <br class="hidden-xs hidden-sm visible-md visible-lg">
          Servicio de Banda Ancha Disponible para Nuestros <br class="hidden-xs hidden-sm visible-md visible-lg"> Distinguidos Visitantes.
        </p>
---
