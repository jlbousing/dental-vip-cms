---
templateKey: home-page
language: es
redirects: /en/
title: Odontología Especializada en Caracas
siteName: DENTAL VIP, Especialidades Odontológicas s.c.
description: En DENTAL VIP ponemos a su disposición la experiencia del mejor equipo de Odontólogos Especialistas, las más modernas y cómodas instalaciones y la última tecnología a nivel mundial.
keywords:
  - default keyowrd
published: true
tags:
  - default tag
ogImage: https://imagenes.dentalvipcaracas.com/open-graph-p%c3%a1gina-inicio.jpg

# Hero Section
hero:
  background:
    scaleOnReveal: true
    img: /uploads/hero-home.jpg
    isParallax: false
  anim:
    display: true
    type: left
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: bottom left-aligned
    body: >
      <h2 class="wrapped">Salud, Belleza y Función</h2>
      <br>
      <h3 class="no-mob">¡Una Especialidad para Cada Tratamiento!</h3>
      <h1>INNOVACIÓN Y PRESTIGIO EN ODONTOLOGÍA</h1>
      <br>
      <br>

# Brand Section
brand:
  logo: /uploads/logo.svg
  title: >
    <h4 class="light">CENTRO DE ESTÉTICA Y REHABILITACIÓN ORAL</h4>
  main: >
    <p>¡Bienvenidos a nuestro espacio en la red!</p>
    <p>
      En DENTAL VIP ponemos a su disposición la experiencia del mejor
      equipo de Odontólogos Especialistas, las más modernas y cómodas
      instalaciones y la última tecnología a nivel mundial.
    </p>
    <p>
      Somos un grupo humano verdaderamente comprometido con lo que
      hace, capaz de brindar un servicio de salud integral,
      personalizado y de alto valor científico; enfocado siempre en la
      ética, responsabilidad y sentido social de nuestra labor.
    </p>

  partners:
    - image: /uploads/logo-footer.jpg

  footer: >
    CARACAS - VENEZUELA

# Gallery Section
gallery:
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
      - /uploads/lightbox-smiles-02.jpg
      - /uploads/lightbox-smiles-03.jpg
      - /uploads/lightbox-smiles-04.jpg
      - /uploads/lightbox-smiles-05.jpg
      - /uploads/lightbox-smiles-06.jpg
      - /uploads/lightbox-smiles-07.jpg
      - /uploads/lightbox-smiles-08.jpg
      - /uploads/lightbox-smiles-09.jpg
      - /uploads/lightbox-smiles-10.jpg
      - /uploads/lightbox-smiles-11.jpg
      - /uploads/lightbox-smiles-12.jpg
      - /uploads/lightbox-smiles-13.jpg
      - /uploads/lightbox-smiles-14.jpg
      - /uploads/lightbox-smiles-15.jpg
      - /uploads/lightbox-smiles-16.jpg
      - /uploads/lightbox-smiles-17.jpg
      - /uploads/lightbox-smiles-18.jpg
      - /uploads/lightbox-smiles-19.jpg
      - /uploads/lightbox-smiles-20.jpg
      - /uploads/lightbox-smiles-21.jpg
      - /uploads/lightbox-smiles-22.jpg
      - /uploads/lightbox-smiles-23.jpg
      - /uploads/lightbox-smiles-24.jpg
  items:
    - link:
        display: true
        to: /la-clinica/por-que-elegirnos/
      image: /img/gallery-why.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Por Qué Elegirnos</h3>
        <p class="dv-text-feat"> Nuestra trayectoria es su mejor garantía</p> 
        <p class="dv-text-feat-100">
          Conozca las 10 razones que nos distinguen de la competencia y
          conforman nuestra propuesta de valor.
        </p>
    - link:
        display: true
        to: /la-clinica/instalaciones/
      image: /img/gallery-facilities.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>La Clínica en Imágenes</h3>
        <p class="dv-text-feat">
          Un ambiente relajado, tranquilo y de máximo confort...
        </p>
    - link:
        display: true
        to: /la-clinica/tecnologia/
      image: /img/gallery-technology.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Dotación y Tecnología</h3>
        <p class="dv-text-feat">
          ¡A la vanguardia en equipos y procesos digitales!
        </p>
    - link:
        display: true
        to: /profesionales/
      image: /img/gallery-professionals.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Staff profesional</h3>
        <p class="dv-text-feat">¡Su boca en manos de expertos!</p>
    - link:
        display: false
        to: /
      image: /img/gallery-smiles.png
      action: true
      placeholder: >
        <span> Ver galeria </span>
      body: >
        <h3>Galería de Sonrisas</h3>
         <p class="d-none d-lg-block dv-text-feat">
          ¡Pasión por la belleza... Devoción por la naturalidad!
        </p> <p class="dv-text-feat-100">
          Una pequeña muestra de lo que podemos hacer por Usted: Odontología
          moderna, integral y especializada.
        </p>
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>
        <p class="dv-text-feat">Un protocolo especial de atención</p> 
        <p class="dv-text-feat-100">
          Somos consecuentes con todos los pacientes que nos visitan desde
          cualquier parte de Venezuela y el mundo.
        </p>
# Quote component structure
quote:
  body: >
    <p>
      <strong>
        La Calidad en Odontología no es fruto de la casualidad.
      </strong>
      <br />
      <em>
        Es siempre el resultado de la búsqueda de la excelencia profesional y de
        un esfuerzo inteligente por el mejoramiento continuo. La capacitación permanente
        en nuevas tendencias y filosofías terapéuticas, la honestidad, el uso eficiente
        de los recursos y el trato cordial a las personas generan los mayores niveles
        de satisfacción en todos nuestros pacientes.
      </em>
    </p>
  footer:
    author: >
      <strong>María José Tirado</strong>
    details: >
      <strong>
        Coordinación Clínica
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

# Statistics Section
statistics:
  image: /uploads/parallax-stadistics.jpg
  items:
    - number: 20
      title: >
        <h1>Años de<br> experiencia</h1>
    - number: 580000
      title: >
        <h1>Intervenciones<br> electivas</h1>
    - number: 9500
      title: >
        <h1>casos<br> resueltos</h1>

# Features Section
features:
  title: >
    <h1 class="title">
      ¿Qué Ventajas le Ofrece Nuestra Exclusiva Metodología de Trabajo?
    </h1>
  description: >
    <p>
      Un novedoso enfoque multidisciplinario que posibilita la gestión
      integradora del conocimiento y potencia la capacidad resolutiva de
      nuestro equipo asistencial.
      <br />
      <strong>¡Todas las Especialidades en el Mismo Lugar!</strong>
    </p>
  features:
    - to: /especialidades/cirugia-bucal/
      img: /uploads/specialties-oral-surgery.png
      title: >
        <h3 class="title bebas bold"> Cirugía Bucal</h3>
      description: >
        <p> Prevención, diagnóstico y tratamiento de toda la patología quirúrgica propia o asociada a dientes, mucosas, labios, encías y huesos maxilares.</p>
    - to: /especialidades/implantes-dentales/
      img: /uploads/specialties-dental-implants.png
      title: >
        <h3 class="title bebas bold"> Implantes</h3>
      description: >
        <p> Reposición de los dientes perdidos con dispositivos biocompatibles
        de titanio que viabilizan la rehabilitación oral fija de los pacientes parcial
        o totalmente edéntulos.</p>
    - to: /especialidades/estetica-dental/
      img: /uploads/specialties-dental-aesthetics.png
      title: >
        <h3 class="title bebas bold"> Estética Dental</h3>
      description: >
        <p> Análisis y estudio de las características dentogingivales para corregir
        defectos cosméticos y embellecer el aspecto general de la dentadura.</p>
    - to: /especialidades/endodoncia/
      img: /uploads/specialties-endodontics.png
      title: >
        <h3 class="title bebas bold"> Endodoncia</h3>
      description: >
        <p>Limpieza, desinfección y conformación de los conductos radiculares
        como paso previo a los múltiples procedimientos de prótesis y restauración dental.</p>
    - to: /especialidades/ortodoncia/
      img: /uploads/specialties-orthodontics.png
      title: >
        <h3 class="title bebas bold"> Ortodoncia</h3>
      description: >
        <p> Brackets y aparatología funcional para la corrección biomecánica
        de maloclusiones, malposiciones dentales y deformidades dentofaciales.</p>
    - to: /especialidades/protesis/
      img: /uploads/specialties-prosthesis.png
      title: >
        <h3 class="title bebas bold">Prótesis y Rehabilitación Oral</h3>
      description: >
        <p> Diseño y confección de coronas cerámicas y estructuras protésicas
        que restituyen la integridad de las arcadas dentales, estética y función masticatoria.</p>

# Testimonials Section
testimonials:
  title: Testimonios y Valoraciones
  items:
    - img: "/img/testimonials-mohammad.png"
      testimonial:
        “Tras 11 implantes y 11 coronas cerámicas en el centro, he tenido
        siempre muy buenas experiencias. Honestamente quedé muy satisfecho con los servicios
        de DENTAL VIP. Los implantes son un gran acierto, y de lo único que me arrepiento
        es de no haberlo hecho antes. Gracias a todos, y especialmente al Dr. Garabán,
        por su paciencia, calidad humana y ética profesional”."
      position: Empresario
      name: Mohammad Byherzade
    - img: "/img/testimonials-viviana.png"
      testimonial:
        "“Antes me daba vergüenza sonreír porque no estaba conforme con el
        aspecto de mi dentadura. Desde que visité DENTAL VIP me siento más cómoda y
        segura de mi misma. Me hicieron un diseño digital de sonrisa, me cortaron las
        encías, me blanquearon los dientes y me colocaron cuatro carillas de porcelana
        verdaderamente espectaculares. Les recomiendo con los ojos cerrados”."
      position: Médico Cirujano
      name: Viviana Hernández
    - img: "/img/testimonials-carlos.png"
      testimonial:
        "“Luego de dos tratamientos de Ortodoncia y más de 5 años con brackets
        pensé que mi caso ya no tenía solución. Casi sin esperanzas, y solo por los
        resultados que observé en mi prima Marta, decidí visitar al Dr. José Miguel
        Gómez. En solo 3 meses con mis nuevos aparatos noté una mejoría impresionante,
        y finalmente, pude recuperar mi sonrisa. Aprendí que, definitivamente, lo barato
        sale muy caro”."
      position: Fotógrafo
      name: Carlos Gutiérrez
    - img: "/img/testimonials-brenda.png"
      testimonial:
        "“Quería colocarme implantes dentales y no sabía en quién confiar.
        Solicité varias opiniones y finalmente me decidí por DENTAL VIP, y no porque
        fuese un centro reconocido, sino porque me ofrecieron las mejores garantías
        y me transmitieron mucha seguridad. Ahora que acabé el tratamiento puedo decir
        que quedé encantada y que todo el equipo me pareció muy cercano y profesional”."
      position: Ingeniero
      name: Brenda Uzcátegui
    - img: "/img/testimonials-jose.png"
      testimonial:
        "“Me vi en la necesidad de ir a la clínica para tratar algunas caries
        y hacerme dos tratamientos de conducto. Recomendaría a DENTAL VIP por la asesoría,
        por el fácil acceso a su locación, por la receptividad de su personal, por sus
        impecables instalaciones, y sobre todo; porque los procedimientos fueron ejecutados
        sin ningún tipo de dolor. Muy buenos Odontólogos”."
      position: Contador Público
      name: José Luis Dávila

# Procedures Section
procedures:
  title: >
    <h1 class="title">Procedimientos Destacados</h1>
  procedures:
    - title: >
        <h5>Prótesis sobre implantes</h5>
      to: /especialidades/implantes-dentales/protesis-sobre-implantes/
      img: /uploads/procedures-dental-implants.jpg
    - title: >
        <h5>Diagnóstico y Planificación 3D</h5>
      to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5>Carillas de porcelana</h5>
      to: /especialidades/estetica-dental/carillas-de-porcelana/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5>Tecnología cad-cam</h5>
      to: /especialidades/protesis/tecnologia-cad-cam/
      img: /uploads/procedures-cad-cam.jpg
    - title: >
        <h5>Diseño digital de sonrisa</h5>
      to: /especialidades/estetica-dental/diseno-digital-de-sonrisa
      img: /uploads/procedures-design.jpg
    - title: >
        <h5>Brackets Estéticos</h5>
      to: /especialidades/ortodoncia/aparatos-esteticos/
      img: /uploads/procedures-brackets.jpg
---
