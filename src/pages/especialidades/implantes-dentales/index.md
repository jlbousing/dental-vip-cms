---
templateKey: specialties-page
language: es
redirects: /en/specialties/dental-implants/
title: Implantes Dentales en Caracas
siteName: DENTAL VIP, Especialidades Odontológicas s.c.
description: ¡Coma, sonría y disfrute como antes! Recupere ya sus dientes y olvídese para siempre de esas obsoletas e incómodas dentaduras removibles. Con seguridad los implantes dentales mejorarán su atractivo físico, su carisma y su autoestima.
keywords:
  - default keyowrd
published: true
tags:
  - default tag
ogImage: https://imagenes.dentalvipcaracas.com/open-graph-pagina-implantes-dentales.jpg
# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-dental-implants.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 64%
  content:
    position: center
    body: >
      <h1 class="bebas">Implantes Dentales</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-dental-implants"></i></h1>
    <p class="thin"><em>La utilización de Implantes Dentales Oseointegrados en la Rehabilitación
    Oral de los pacientes parcial o totalmente edéntulos ha sido plenamente consolidada
    como una alternativa confiable y altamente predecible a largo plazo.</em></p>

# Description Section
article:
  content: >
    <p>Indudablemente, uno de los progresos más espectaculares de la Odontología
    moderna es el desarrollo de los Implantes Dentales. Estos, <strong>son pequeños
    dispositivos de titanio en forma de cilindro y que, por su alto grado de biocompatibilidad,
    son capaces de oseointegrarse a las estructuras maxilares para sustituir “idealmente”
    los dientes perdidos  </strong>por caries, traumatismos y enfermedad periodontal;
    evitando así el uso de las antiguas prótesis removibles.</p><p>Con los IMPLANTES
    DE ÚLTIMA GENERACIÓN  <strong>el procedimiento quirúrgico de colocación es bastante
    simple, rápido e indoloro.  </strong>Sin embargo, debe ser siempre ejecutado por
    Especialistas en Cirugía Bucal o Maxilofacial, ya que la técnica a emplear es
    bastante sensible y requiere de amplios conocimientos quirúrgicos y anatómicos
    de los tejidos y estructuras bucales.</p><p><strong>Otro factor esencial en la
    Implantología Avanzada es el aspecto restaurador. </strong> Si la prótesis que
    se coloca luego sobre el implante no está bien diseñada, las fuerzas nocivas generadas
    por la masticación conducirán irremediablemente a su fracaso. De aquí la importancia
    y necesidad del TRATAMIENTO MULTIDISCIPLINARIO en la Rehabilitación Bucal con
    Implantes Dentales Oseointegrados.  <strong>En nuestra clínica la segunda fase
    del tratamiento o fase protésica está siempre a cargo del Odontólogo Especialista
    en Prostodoncia, </strong> ya que nadie mejor que él es capaz de manejar los complicados
    principios y fundamentos de fisiología de la oclusión, es decir, de la manera
    como deben relacionarse ese implante y esa prótesis con el resto de los dientes
    cuando el paciente entra en función para comer y masticar.</p><p>Finalmente, y
    en pro del éxito a largo plazo del tratamiento, es también imperativo destacar
    la importancia del <strong> cumplimiento de un buen protocolo consensuado de mantenimiento
    periodontal, </strong> que a intervalos de 6 meses, nos permita conservar las
    encías sanas y los elementos protésicos en perfecto estado estético y estructural.
    </p>
  img: /uploads/aside-dental-implants.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
     Es verdaderamente gratificante percibir día a día como los Implantes Dentales
    mejoran la calidad de vida y la salud de las personas. El hecho de poder comer
    y masticar con comodidad, hablar con normalidad y sonreír sin temor; son beneficios
    de invalorable repercusión.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
       Cirujano Bucal
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-dental-implants.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-dental-implants.jpg
  mobilePosition: '{"mobilePosition320":"-884px","mobilePosition360":"-1021px","mobilePosition375":"-1276px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿Qué es la oseointegración?</h4>
      content: >-
        <p>Se define como el proceso biológico mediante el cual se logra una
        anquilosis funcional entre un implante y su hueso receptor. Los implantes
        son de titanio, metal biocompatible y osteoinductor, capaz de estimular la
        diferenciación de osteoblastos e iniciar el proceso de cicatrización y
        regeneración ósea <em>(osteogénesis)</em> a su alrededor. Este fenómeno es
        el que permite que un implante se fije con firmeza al hueso, permanezca
        estable a lo largo del tiempo y pueda ser utilizado con seguridad como
        base o pilar de prótesis dental.</p>
    - title: <h4>¿Cuándo están indicados los Implantes Dentales Oseointegrados?</h4>
      content: >-
        <p>Cuando sea necesario reponer uno, varios o incluso todos los dientes de
        la boca. Los implantes unitarios evitan la necesidad de tener que tallar,
        desgastar y comprometer los dientes contiguos al que se ha perdido para
        confeccionar un puente fijo tradicional. Colocamos un implante, una corona
        y listo, de esta manera se logra una rehabilitación mucho más biológica y
        conservadora. Cuando faltan varios o todos los dientes, se colocan
        implantes múltiples y luego sobre ellos se cementan o atornillan coronas
        individuales o prótesis fijas exclusivamente implantosoportadas.</p>
    - title: <h4>¿Existen contraindicaciones al tratamiento con implantes?</h4>
      content: >-
        <p>Por norma general, son las mismas que impiden ejecutar otros
        procedimientos quirúrgicos como trastornos de coagulación sanguínea,
        historia reciente de infarto agudo al miocardio o accidente
        cerebrovascular, inmunosupresión severa, tumoraciones malignas en
        tratamiento con químio o radioterapia, artritis reumatoide severa,
        diabetes mellitus no controlada y enfermedades óseas como osteoporosis y
        osteomalacia. También, es importante valorar otros factores adicionales
        como la ingesta de bifosfonatos en mujeres, presencia de infección
        periodontal aguda y hábitos tabáquicos severos.</p>
    - title: <h4>¿De modo que están contraindicados en fumadores?</h4>
      content: >-
        <p>El cigarrillo no constituye una contraindicación absoluta para la
        rehabilitación con implantes, sin embargo, está comprobado científicamente
        que el hábito de fumar provoca una vasoconstricción vascular periférica
        que altera significativamente el proceso de cicatrización y regeneración
        ósea. De tal forma que en estos pacientes las posibilidades de éxito se
        ven reducidas de un 98% a un 65% de los casos aproximadamente.</p>
    - title: <h4>¿Cómo se colocan?</h4>
      content: >-
        <p>A través de una pequeña cirugía, muy poco invasiva, se crea una
        perforación o lecho quirúrgico en el espesor del hueso alveolar.
        Seguidamente, se introduce el implante enroscándolo a presión, de modo que
        su superficie quede en íntimo contacto con el tejido óseo subyacente.
        Haciendo analogía simple podemos decir que la fase quirúrgica es bastante
        parecida a la forma de colocar un ramplug en una pared. En algunos casos
        ni siquiera es necesario tomar puntos de sutura.</p>
    - title: <h4>¿Es muy doloroso este procedimiento?</h4>
      content: >-
        <p>En lo absoluto!, siempre y cuando se efectúe bajo una adecuada técnica
        de anestesia local. Nuestro Cirujano Bucal maneja excelentes protocolos
        quirúrgicos y potentes anestésicos que hacen el procedimiento simple,
        rápido e indoloro. El postoperatorio suele ser muy similar al de una
        extracción dental.</p>
    - title: <h4>¿Algún requisito previo a la intervención?</h4>
      content: >-
        <p>Casi los mismos que para cualquier otro acto de cirugía oral. Buen
        estado de salud general, un perfil preoperatorio que evalúe la capacidad
        de coagulación y cicatrización tisular y premedicación profiláctica con
        antibióticos de amplio espectro, generalmente amoxicilina con ácido
        clavulánico o cefalosporinas en pacientes alérgicos.</p>
    - title: <h4>¿Podrían ser necesarias otras cirugías especiales?</h4>
      content: >-
        <p>Lamentablemente en algunos casos la cantidad, calidad y densidad del
        hueso receptor pueden ser deficientes y suele ser entonces necesario
        colocar injertos óseos autógenos o sintéticos para mejorar el entorno
        local y evitar un posible fracaso. Otra maniobra relativamente común es la
        elevación del seno maxilar o SINUS LIFT, ya que muchas veces su descenso o
        neumatización limita la colocación de implantes en el sector posterior del
        maxilar superior.</p>
    - title: <h4>¿Son iguales todos los implantes?</h4>
      content: >
        <p>No, varían considerablemente en diámetro, longitud, conicidad, tipo de
        conexión y tratamiento de superficie. Los estudios radiográficos previos,
        las tomografías volumétricas de haz cónico <em>(Cone Beam)</em> y la
        experiencia clínica del Cirujano son factores claves para su elección. De
        igual forma, es importante utilizar marcas comerciales reconocidas y
        respaldadas por estudios de investigación a largo plazo que garanticen su
        desempeño y longevidad funcional. En nuestra clínica, y según su nivel de
        calidad, ponemos a su disposición implantes de gama media-alta y gama
        alta.</p> <p>¡No trabajamos con implantes clónicos o de bajo coste!</p>
    - title: <h4>¿Qué son los implantes inmediatos?</h4>
      content: >-
        <p>Son aquellos que se colocan en el mismo momento de la extracción dental
        para evitar la reabsorción del reborde alveolar y favorecer los resultados
        estéticos de la restauración final. Una ventaja adicional es que acortan
        considerablemente el tiempo total de tratamiento, al no ser necesario
        esperar los 4 o 5 meses que conlleva el proceso de cicatrización y
        neoformación ósea del alvéolo después de la extracción.</p>
    - title: <h4>¿Son muy caros los Implantes Dentales?</h4>
      content: >-
        <p>Caro es algo que implica dar un valor sobreestimado a algo que no lo
        tiene. Aunque por su naturaleza <em>(titanio puro)</em> los implantes son
        más costosos que otras alternativas odontológicas, el hecho de poder comer
        y sonreír prácticamente igual que con los dientes naturales les hace
        merecedores del más alto valor dentro de cualquier relación
        costo-beneficio.</p>
    - title: <h4>¿Qué es un sistema All-on-Four?</h4>
      content: >-
        <p>Es una técnica que permite la rehabilitación total fija con implantes
        en el maxilar superior o inferior del paciente totalmente edéntulo. Se
        fundamenta en la colocación estratégica de solo cuatro <em>(4)</em>
        implantes, hecho que disminuye considerablemente los costos finales del
        tratamiento.</p>
    - title: <h4>¿Qué tan estéticos lucen los implantes?</h4>
      content: >-
        <p>Los implantes no son visibles, quedan incrustados en el interior del
        hueso, de modo que la responsabilidad estética recae en la prótesis
        definitiva. Esta debe ser confeccionada para satisfacer todos los
        requerimientos funcionales pertinentes, y además, para proveer al paciente
        de dientes que luzcan verdaderamente naturales, de un color, forma y
        tamaño que proyecte armonía y proporcionalidad, caracterizados
        individualmente y con capacidad de reflejar y traslucir la luz. Todos
        estos son factores claves para el éxito estético del tratamiento.</p>
    - title: <h4>¿En qué consiste la fase protésica?</h4>
      content: >-
        <p>Es la segunda fase de una rehabilitación con Implantes Dentales. Debe
        estar siempre a cargo del Especialista en Prostodoncia, quien diseñará,
        confeccionará e instalará la prótesis implantosoportada definitiva,
        respetando siempre a cabalidad todos los principios biomecánicos de
        oclusión y estética dental. Coronas individuales, puentes fijos,
        rehabilitaciones completas, prótesis híbridas y sobredentaduras son las
        alternativas conocidas.</p>
    - title: <h4>¿Todas las prótesis sobre implantes son fijas?</h4>
      content: >-
        <p>Aunque es posible confeccionar sobredentaduras removibles, consideramos
        que el esfuerzo e inversión que implica este tipo de procedimiento merece
        una prótesis fija que verdaderamente mejore la calidad de vida de la
        persona. En nuestra clínica, siempre y cuando sea posible, intentamos que
        todas sean fijas, a excepción de las provisionales utilizadas durante el
        período de oseointegración.</p>
    - title: >-
        <h4>¿Se puede colocar la corona o prótesis fija inmediatamente, es decir,
        el mismo día de la cirugía?</h4>
      content: >-
        <p>Sí se puede con los llamados implantes de carga inmediata, sin embargo,
        para esto se deben reunir una serie de condiciones ideales algunas veces
        difíciles de encontrar. El protocolo tradicional y todavía más común es el
        de carga diferida, que contempla un lapso de entre 3 y 4 meses de espera
        entre las fases quirúrgica y protésica para permitir la completa
        maduración del tejido óseo periimplantar <em>(oseointegración)</em>.
        Durante ese período generalmente se confecciona una prótesis provisional
        removible que solvente temporalmente el problema estético.</p>
    - title: "<h4>¿Es un tratamiento definitivo, para toda la vida?</h4>"
      content: >-
        <p>Sería irresponsable ofrecer una estimación general para todos los
        casos, sin embargo, hay pacientes que llevan prótesis sobre implantes
        desde hace más de 30 años. Obviamente todo depende del estado de salud
        general, planificación previa del caso, técnica quirúrgica de
        implantación, calidad del dispositivo, asepsia en el procedimiento,
        correcta distribución de cargas masticatorias, higiene oral, hábitos del
        paciente y, muy importante; asistencia a las revisiones periódicas
        posteriores para descartar o tratar a tiempo procesos infecciosos
        incipientes, desajustes o fallas de los componentes protésicos.</p>
    - title: <h4>¿Existe la posibilidad de rechazo a un Implante Dental?</h4>
      content: >-
        <p>No hay en la literatura ningún caso descrito de alergia o toxicidad al
        titanio, por lo que no puede existir un rechazo propiamente dicho. Puede
        ocurrir que un implante no se oseointegre adecuadamente por un proceso
        infeccioso o trauma localizado <em>(generalmente por una prótesis
        prematura o mal elaborada)</em> y sea necesario sustituirlo por otro para
        solventar el problema. El porcentaje de éxito en la actualidad, con
        Implantes de Gama Alta, es superior al 98% de los casos.</p>
    - title: <h4>¿Qué es la periimplantitis?</h4>
      content: >-
        <p>Los implantes y los dientes naturales son tan parecidos que son
        susceptibles a las mismas enfermedades periodontales. Al igual que existe
        la periodontitis, existe la periimplantitis, ambas enfermedades
        bacterianas e inflamatorias que destruyen el hueso alveolar, causan
        movilidad de dientes e implantes, y en casos extremos; pérdida de los
        mismos.</p>
    - title: <h4>¿Cómo hacer entonces para prevenirla?</h4>
      content: >-
        <p>Lo primero es cerciorarse de la preparación y capacidad de los
        profesionales que llevarán a cabo su tratamiento. Para esto Usted no
        necesita un odontólogo, usted necesita un EQUIPO DE TRABAJO conformado por
        un Cirujano, un Prostodoncista y un Periodoncista que dominen y tengan
        experiencia en la materia. Luego, es indispensable su colaboración. La
        higiene oral es el pilar fundamental de todo procedimiento restaurador en
        Odontología, y los implantes no son la excepción. Si Usted logra mantener
        siempre sus implantes y encías libres de placa dental y restos
        alimenticios, acude con regularidad a las citas periódicas de control y se
        conserva en buen estado de salud general; es muy probable que logre
        disfrutar para toda la vida los beneficios de esta Innovadora Alternativa
        Odontológica.</p>

cases:
  title: >
    <h1>Implantes Dentales - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-dental-implants-es-01.jpg
      - /uploads/clinic-cases-dental-implants-es-02.jpg
      - /uploads/clinic-cases-dental-implants-es-03.jpg
      - /uploads/clinic-cases-dental-implants-es-04.jpg
      - /uploads/clinic-cases-dental-implants-es-05.jpg
      - /uploads/clinic-cases-dental-implants-es-06.jpg
      - /uploads/clinic-cases-dental-implants-es-07.jpg
      - /uploads/clinic-cases-dental-implants-es-08.jpg
      - /uploads/clinic-cases-dental-implants-es-09.jpg
      - /uploads/clinic-cases-dental-implants-es-10.jpg
      - /uploads/clinic-cases-dental-implants-es-11.jpg
      - /uploads/clinic-cases-dental-implants-es-12.jpg
      - /uploads/clinic-cases-dental-implants-es-13.jpg
      - /uploads/clinic-cases-dental-implants-es-14.jpg
      - /uploads/clinic-cases-dental-implants-es-15.jpg
      - /uploads/clinic-cases-dental-implants-es-16.jpg
      - /uploads/clinic-cases-dental-implants-es-17.jpg
      - /uploads/clinic-cases-dental-implants-es-18.jpg
      - /uploads/clinic-cases-dental-implants-es-19.jpg
      - /uploads/clinic-cases-dental-implants-es-20.jpg
      - /uploads/clinic-cases-dental-implants-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>9 Implantes y Prótesis Parcial Híbrida Superior </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Rehabilitación Oral Completa </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Parcial Fija Implantosoportada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reposición de Incisivos Inferiores</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>All-On-Four </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implantes en Correspondencia con 1.2 y 2.2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>2 Implantes y Puente Fijo Inferior</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Healing Caps y 6 Coronas Metal-Porcelana</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sobredentadura Removible</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implantación en Correspondencia con el 2.1 </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tornillo de Cicatrización y Corona Anterior</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después con Implantes y Coronas Cerámicas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implante Unitario en Región de Bicúspides</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Abutments o Pilares Protésicos</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Total Híbrida Metal-Acrílico</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Híbrida Superior Sobre 8 Implantes </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sinus Lift</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Fija Convencional e Implante Unitario</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Fija Superior Implantosoportada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Rehabilitación Bimaxilar con Implantes Dentales</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-dental-implants-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implante en Zona de Alto Compromiso Estético</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">AHORA QUE LLEVO IMPLANTES NO PUEDO ENTENDER CÓMO ESTUVE TANTO TIEMPO USANDO
    DENTADURAS REMOVIBLES. POR UN MIEDO TONTO A UNA PEQUEÑA CIRUGÍA, PASÉ AÑOS OCULTANDO
    MI SONRISA, TAPÁNDOME LA BOCA CON LA MANO PARA QUE NO ME VIERAN EL PLÁSTICO Y
    LOS GANCHOS DE LOS PUENTES".</p>
  images:
    portrait: /uploads/quotes-dental-implants-portrait.jpg
    landscape: /uploads/quotes-dental-implants.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-hexagon-dental-implants.jpg
      content: >
        <h1 class="bebas">¿POR QUÉ ZIMMER BIOMET?</h1>
        <p>Nuestra marca de preferencia por ser una
            de las 5 mejores a nivel mundial de entre las más de 500 que se comercializan
            en la actualidad. ¡Porque sabemos que los Implantes Dentales de Alta Gama son
            garantía de excelencia, calidad y longevidad!</p><br/>
      footer:
        icon:
          display: true
          img: /uploads/zimmer-biomet-logo.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-dental-implants-3d-diagnosis.jpg
      content: >
        <h1 class="bebas">Diagnóstico y Planificación 3D</h1>
        <p>La aparición y desarrollo de técnicas avanzadas
            de visualización volumétrica ha supuesto para la comunidad dental la posibilidad
            de acceder a la reconstrucción 3D para determinar con seguridad la posición
            exacta en la que debe ser implantado cada dispositivo.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-hexagon-dental-implants.jpg
        link:
          display: true
          to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
          placeholder: <span> Más Información</span>
    - img: /uploads/sections-dental-implants-implant-supported-restorations.jpg
      content: >
        <h1 class="bebas">Prótesis Sobre Implantes</h1>
        <p>Transcurrido el período de oseointegración,
            se destapan los implantes y se confecciona la prótesis definitiva previamente
            contemplada. Según el caso, podrá ser total o parcial, fija o removible y de
            metal-porcelana, metal-acrílico, totalcerámica o resina poliacrílica termopolimerizable.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-hexagon-dental-implants.jpg
        link:
          display: true
          to: /especialidades/implantes-dentales/protesis-sobre-implantes/
          placeholder: <span> Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
