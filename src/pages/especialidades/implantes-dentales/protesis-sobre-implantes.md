---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/dental-implants/implant-supported-restorations/
title: Prótesis sobre Implantes
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-implant-supported-prosthesis.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 65%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: List
  content: >

  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >

        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-individual-crown icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Corona individual</h2>
        </div>

        <p class="dv-srv-pr">Aunque el trabajo clínico y de laboratorio es mucho
          más complejo que el de una corona o funda dentosoportada <em>(sobre un diente
          natural)</em>, es la restauración más básica que se puede confeccionar sobre
          un implante oseointegrado. Están indicadas en casos de implantes unitarios y
          pueden ser de metal-porcelana, Disilicato de Litio u Óxido de Zirconio <em>(alta
          estética dental).</em></p>
          </div>
    - img: /uploads/implant-types-fixed-bridge.jpg
      number: 1
      title: >

        <div class="content-wrapper">

        <div class="title-icon">
          <i class="icon-fixed-bridge icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Puente fijo</h2>
        </div>
        <p class="dv-srv-pr">Es la opción fija ideal para brechas edéntulas de
          3 o más dientes contiguos. La estructura está conformada por 3 o más coronas
          fusionadas entre sí y soportada en sus extremos por 2 o más implantes dentales.
          Pueden ser elaborados con los mismos materiales que se utilizan para las coronas
          individuales.</p>
           </div>
    - img: /uploads/implant-types-full-rehabilitation.jpg
      number: 1
      title: >

        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-full-rehabilitation icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Rehabilitación completa</h2>
        </div>
        <p class="dv-srv-pr">Es la alternativa A1 para la ausencia total de dientes.
        10 o 12 coronas cerámicas sobre el mayor número de implantes factible <em>(de
        8 a 12)</em>. Las coronas pueden ser individuales, pueden estar fusionadas por
        secciones o formar parte de una sola estructura. Este tipo de prótesis es el
        más parecido a la dentición natural y al morder proporciona una inigualable
        sensación de seguridad y confort, sin embargo, requiere de una gran inversión
        económica y de unas bases óseas maxilares casi intactas.</p>
         </div>
    - img: /uploads/implant-types-hybrid-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-hybrid-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Prótesis híbrida</h2>
        </div>
        <p class="dv-srv-pr">Una prótesis híbrida es aquella que combina varios
            materiales. Es una sola estructura fija que se atornilla sobre 4, 6 u 8 implantes
            y que por lo general reemplaza a todos los dientes de una arcada. Puede ser
            de metal-porcelana o metal-acrílico, y que en casos de atrofia maxilar o reabsorción
            ósea severa, incorpora encía artificial de color rosa para reestablecer la dimensión
            vertical oclusal, soporte labial, estética dental y plenitud facial de la persona.</p>
             </div>
    - img: /uploads/implant-types-overdent.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-overdent icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Sobredentadura</h2>
        </div>
        <p class="dv-srv-pr">Es una prótesis total de resina acrílica, removible
          y que se ancla sobre 2, 3 o 4 implantes. Se le conoce como "dentadura de encaje"
          porque se basa en un sistema macho y hembra para su inserción y remoción. El
          hecho de poder ser retirada por el paciente facilita considerablemente el proceso
          de higiene oral, y aunque es removible, los implantes le proporcionan gran retención
          y estabilidad en comparación con una prótesis convencional o mucosoportada.</p>
           </div>
# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">Prótesis sobre Implantes</h1>
    <p>Existen varios tipos. La indicación
    de uno u otro dependerá de la cantidad de dientes ausentes, volumen de hueso y
    encía, plenitud facial, número y disposición de los implantes, factores estéticos,
    oclusión, arcada antagonista, espacio protésico y consideraciones económicas.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-implant-supported-restorations.jpg
  mobilePosition: '{"mobilePosition320":"-676px","mobilePosition360":"-770px","mobilePosition375":"-938px"}'
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>Diseñamos Su Sonrisa con Tecnología Computarizada CAD-CAM</h1>
    <br>
    <br>
    <h2 >Nuestro equipo de Técnicos Dentales cuenta con más de 20 años de experiencia
    en Restauraciones Implantoasistidas.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Abutment y dispositivos protésicos</h2>
      <p class="dv-div-text
      dv-pb-15">El abutment o pilar protésico es la pieza que
        establece la conexión estructural entre el implante y la prótesis. Se enrosca
        en el implante, y luego sobre él, se cementa o atornilla la restauración. Puede
        ser prefabricado o hecho a la medida por un proceso de fundición y colado metálico.</p><p
        class="dv-div-text text-right">Un transfer o dispositivo de transferencia y un
        análogo de laboratorio son también generalmente requeridos para poder llevar a
        feliz término el procedimiento de laboratorio.</p>
    image:
      display: true
      size: 100px
      src: /uploads/icons-abutment-es.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-implant-supported-restorations.jpg
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: "/especialidades/implantes-dentales/diagnostico-y-planificacion-3d/"
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> Tecnología cad-cam </h5>
      to: "/especialidades/protesis/tecnologia-cad-cam/"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> Diseño digital de sonrisa </h5>
      to: "/especialidades/estetica-dental/diseno-digital-de-sonrisa/"
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Brackets Estéticos </h5>
      to: "/especialidades/ortodoncia/aparatos-esteticos/"
      img: "/uploads/procedures-brackets.jpg"
    - title: >
        <h5> Carillas de porcelana </h5>
      to: "/especialidades/estetica-dental/carillas-de-porcelana/"
      img: "/uploads/procedures-veneers.jpg"
---

<div class="container">
<div class="row">
<div class="item np left">

## Toma de impresión

Es el procedimiento que nos permite registrar, copiar y reproducir en negativo y con exactitud las infra y mesoestructuras artificiales con el objetivo de obtener el modelo de trabajo para la confección de la prótesis definitiva. Para rehabilitar implantes manejamos el concepto de transferencia y materiales elastoméricos como las siliconas por adición.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-restorations.jpg)

</div>
<div class="item np right">

## ¿Prótesis atornillada o cementada?

Depende del caso. Cada una tiene sus indicaciones y utilidades específicas. Sin embargo, la aparición de nuevos cementos especiales para uso implantológico va dando como resultado que, poco a poco; las prótesis cementadas vayan ganando terreno en la selección.

</div>
</div>
<div class="row">

<div class="item center">

## Equilibrio oclusal

Sin temor a equivocarnos, podemos afirmar que la oclusión que le procuremos a una restauración implantosoportada, sobre todo si es fija; representará su seguro de vida. El implante no posee mecanismos o sensores "fusibles" que le permitan, ante una sobrecarga, emitir una señal de alarma; tal y como sucede con un diente natural a través de los elementos propioceptores contenidos en su periodonto. De allí la importancia de lograr una perfecta oclusión o mordida en todos los tratamientos restauradores sobre implantes oseointegrados.

</div>

</div>
</div>
