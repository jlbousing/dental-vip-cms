---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/dental-implants/3d-diagnosis-and-planning/
title: Diagnóstico y Planificación 3D
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-3d-diagnosis-and-planning.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 38%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: bottom
  type: Column
  content: >
    <div></div>
  blocks:
    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h2></h2>

# Heading Section
heading:
  display: true
  content: <h1 class="bebas title">Diagnóstico y Planificación 3D</h1>
    <p>En la actualidad es rutina transferir
    tratamientos diseñados virtualmente a la condición clínica real de nuestros pacientes,
    hecho que ha marcado un avance sin precedentes en el campo de la Rehabilitación
    Oral.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-3d-diagnosis-and-planning.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>3D: Implantología Avanzada con Criterio Protésico</h1>
    <br>
    <br>
    <h2 >Un verdadero enfoque Multidisciplinario de principio a fin, Prostodoncia
    y Cirugía en estrecha relación...</h2>
# anex-links
anexes:
  enforce: true
  display: true
  items:
    - img: /uploads/sections-3d-study.jpg
      content: >
        <h2>¡Máxima precisión en la colocación de sus implantes dentales!</h2>
        <p class="dv-srv-pr dv-srv-pr-45">DENTAL VIP incorpora la tecnología de última generación en
        sus procedimientos de Implantología Oral, la Tomografía Volumétrica Digital
        de Haz Cónico <em>(CBVT-Cone Beam Volumetric Tomography)</em>, una herramienta
        de alto valor clínico que proporciona imágenes tridimensionales, precisas y
        de alta calidad digital.</p><p class="dv-srv-pl text-right dv-npr">La utilización
        de este recurso es esencial para la planificación quirúrgica del caso, particularmente
        cuando se van a colocar implantes en el sector posterior de la mandíbula y/o
        maxilar superior, ya que nos permite delimitar el conducto del nervio dentario
        inferior y conocer la distancia exacta entre reborde alveolar y seno maxilar,
        estructuras anatómicas que deben ser siempre respetadas para prevenir riesgos,
        complicaciones y fracasos operatorios.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> LOAD FILE</span>
    - img: /uploads/sections-3d-radiography.jpg
      content: >
        <h2>Un estudio cómodo, rápido y seguro...</h2>
        <ul>
          <li>
            <i class="icon-check circle"></i>

            Baja dosis de radiación, mucho menor a la de un TAC convencional.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Corto tiempo de exposición, inferior a tres minutos.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Escaneado con el paciente en postura sentada.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Tomógrafo abierto que proporciona gran comodidad, evitando sensaciones de
            encierro o claustrofobia.
          </li>
          <li>
            <i class="icon-check circle"></i>

            Información en formato DICOM que permite, a través de un software especial,
            un procedimiento único de visualización y planificación terapéutica.
          </li>
        </ul>

      footer:
        icon:
          display: true
          img: /uploads/icon-caresteam.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Ventajas clínicas de la CONE BEAM en la
      práctica de la implantología oral</h2>
      <p class="dv-div-title text-right"></p> <p class="dv-div-text text-right">Permite
        detectar estructuras anatómicas, evaluar la morfología, cantidad y calidad ósea,
        realizar mediciones exactas del reborde alveolar en ancho, alto y profundidad,
        determinar si es necesario un injerto óseo o un levantamiento de seno maxilar,
        confeccionar guías quirúrgicas, seleccionar el tamaño y modelo de los implantes,
        optimizar su localización y reducir al mínimo los riesgos quirúrgicos.</p>
        <br/> <br/>
    image:
      display: true
      size: 120px
      src: /uploads/icon-implants-logo.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-3d-diagnosis-and-planning.png
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Prótesis sobre implantes</h5>
      to: "/especialidades/implantes-dentales/protesis-sobre-implantes/"
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Tecnología cad-cam</h5>
      to: "/especialidades/protesis/tecnologia-cad-cam/"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> Carillas de porcelana</h5>
      to: "/especialidades/estetica-dental/carillas-de-porcelana/"
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5> Diseño digital de sonrisa</h5>
      to: "/especialidades/estetica-dental/diseno-digital-de-sonrisa/"
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> Brackets Estéticos</h5>
      to: "/especialidades/ortodoncia/aparatos-esteticos/"
      img: "/uploads/procedures-brackets.jpg"
---

<div class="row container">
<div class="item np left">

## Montaje del caso y encerado diagnóstico

Su objetivo es intentar reproducir el resultado final de la futura prótesis.
Con él, el Especialista en Prostodoncia podrá determinar el número y posición
exacta de los implantes requeridos y confeccionará una guía plástica termoformada
para el momento de la cirugía.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-3d.jpg)

</div>
<div class="item np right">

## Intervención guiada con férula quirúrgica

La férula o guía quirúrgica es un prototipo, un dispositivo físico que orienta
al Cirujano durante el acto de implantación, permitiéndole colocar cada elemento
según lo dispuesto, con precisión espacial milimétrica e inclinación axial adecuada.

</div>
</div>
