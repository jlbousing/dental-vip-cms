---
templateKey: specialties-page
language: es
redirects: /en/specialties/endodontics/
title: Endodoncia
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-endodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 50%
  content:
    position: center
    body: >
      <h1 class="bebas">Endodoncia</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-endodontics"></i></h1>
    <p class="thin"><em>De estar indicado, un Tratamiento de Conducto realizado a tiempo es la
    única alternativa viable para evitar la odontectomía o extracción de un diente
    comprometido.</em></p>

# Description Section
article:
  content: >
    <p><p>Todo estímulo o ente agresivo capaz de comprometer la integridad estructural
    de un diente es también capaz de causar daño, infección o necrosis de su tejido
    pulpar. <strong>La pulpa dental o “nervio” de un diente es un tejido conectivo
    especializado que se aloja en su parte interna a todo lo largo de la corona y
    raíz.</strong> Cuando un traumatismo, un desgaste dentario excesivo o el avance
    indiscriminado de una caries no tratada alcanza, expone o infecta ese tejido pulpar,
    y queremos por supuesto conservar el órgano dentario; no queda otra alternativa
    que <strong>extirparlo para luego desinfectar y ensanchar mecánicamente los conductos
    radiculares vacíos de modo que sean capaces de recibir un material de obturación
    inerte, estéril y biocompatible</strong> que los ocupe por completo, evite el
    dolor y la propagación del proceso infeccioso. En esto consiste un TRATAMIENTO
    DE CONDUCTO. <strong>Si no es realizado a tiempo, pueden formarse grandes abscesos
    faciales, flegmones o procesos celulíticos extremadamente dolorosos y agresivos
    </strong> que pueden diseminarse rápidamente y comprometer seriamente otras estructuras
    vitales de la región orofacial, y en casos extremos; hasta la vida del paciente.</p><p>Dado
    lo intrincada y variable que resulta la anatomía radicular de los dientes y el
    hecho de que los conductos son pequeñas cavidades inaccesibles a la vista,<strong>
    es un campo de trabajo considerablemente hostil y difícil, </strong> por ende
    es necesario que el Odontólogo posea amplios conocimientos anatómicos de la MORFOLOGÍA
    RADICULAR y haya desarrollado un sentido de percepción táctil muy especial que
    le permitan <strong>localizar, acceder, instrumentar y rellenar los conductos
    con suma precisión, sin margen de error posible.</strong> Es de nuestro parecer
    que tales cualidades solo se logran cursando estudios formales de la Especialidad
    y limitando la práctica profesional exclusivamente al área de Endodoncia.</p><p><strong>Nuestra
    Endodoncista cuenta con los últimos avances tecnológicos como los sistemas rotatorios
    de alta velocidad para la instrumentación mecánica de conductos, </strong>localizador
    electrónico de ápices radiculares, radiología digital computarizada o RADIOVISIOGRAFÍA
    que reduce al mínimo la emisión de rayos X y modernos sistemas de obturación termoplástica
    que disminuyen casi a cero los índices de fracaso clínico y la necesidad de retratamientos,
    cirugías endodónticas periapicales, apicectomías y extracciones dentales.</p>
  img: /uploads/aside-endodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Una adecuada e inmediata Restauración Post-Endodoncia es el factor clave
    para prevenir reinfecciones, fracturas y pérdidas dentarias. En muchos casos una
    corona cerámica será el tratamiento de primera elección.
      </em>
    </p>
  footer:
    author: >
      <strong>Dra. Vianka Xaviera Torres</strong>
    details: >
      <strong>
        Endodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-endodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-endodontics.jpg
  mobilePosition: '{"mobilePosition320":"-144px","mobilePosition360":"-144px","mobilePosition375":"-144px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿Cómo es un diente por dentro?</h4>
      content: >-
        <p>Está conformado en toda su extensión por una cavidad o espacio que
        aloja a un “nervio” o tejido pulpar, y que se divide en dos porciones; la
        coronaria y la radicular. La porción coronaria se denomina cámara pulpar y
        aloja a la pulpa coronaria. La porción radicular se denomina conducto
        radicular, se extiende hasta el vértice o ápice de la raíz y está también
        ocupada por tejido conectivo especializado o pulpa dental.</p>
    - title: <h4>¿Cuántas raíces y conductos tienen?</h4>
      content: >-
        <p>Existen dientes monoradiculares, biradiculares y multiradiculares. De
        acuerdo a la posición que ocupen en la arcada suelen tener una, dos o tres
        raíces. Dentro de cada raíz puede haber uno o dos conductos.</p>
    - title: <h4>¿Cómo saber si se necesita una Endodoncia?</h4>
      content: >-
        <p>Generalmente por la sintomatología. El dolor espontáneo, la
        hipersensibilidad constante a los estímulos térmicos, la molestia por
        ingesta de azúcares y la presencia de abscesos dentales suelen ser claros
        indicios de pulpitis y/o necrosis pulpar.</p>
    - title: <h4>¿Qué es una pulpitis?</h4>
      content: >-
        <p>Es la inflamación de la pulpa dental. Puede ser reversible o
        irreversible. Se considera irreversible cuando, a pesar de ser vital, la
        pulpa haya perdido por completo su capacidad de regeneración.</p>
    - title: <h4>¿Qué es una necrosis pulpar?</h4>
      content: >-
        <p>Es la muerte del nervio de un diente como consecuencia final de una
        inflamación crónica, muy aguda o traumática de su tejido pulpar.
        Generalmente, el proceso se inicia en la fracción más coronal de la pulpa
        para luego extenderse a su porción radicular.</p>
    - title: >-
        <h4>¿Se puede realizar un Tratamiento de Conducto en presencia de dolor o
        inflamación aguda?</h4>
      content: >-
        <p>Muchas veces es difícil. A menudo es necesario prescribir antibióticos,
        analgésicos y antiinflamatorios antes de poder acceder a los conductos. En
        otros casos, se hace pertinente drenar abscesos y tratar otros procesos
        apicales como requisito previo de la terapia radicular.</p>
    - title: >-
        <h4>¿Cuántas sesiones se necesitan para culminar una Endodoncia
        convencional?</h4>
      content: >-
        <p>Depende de varios factores. De la sintomatología, de si es pulpa viva o
        pulpa muerta, de si hay o no presencia de exudado y del número de raíces y
        conductos a tratar; entre otros. Por norma general, los tratamientos se
        completan en una, dos o tres sesiones clínicas.</p>
    - title: <h4>¿Existe alguna otra alternativa a la terapia endodóntica?</h4>
      content: <p>Solo la extracción dental.</p>
    - title: <h4>¿Es necesario colocar anestesia?</h4>
      content: >-
        <p>¡Por supuesto! Por lo general son tejidos infectados, inflamados o
        necróticos, sumamente sensibles a la manipulación clínica.
        Afortunadamente, un buen diagnóstico previo y una adecuada técnica de
        anestesia local troncular o infiltrativa, serán suficientes para llevar a
        cabo el procedimiento sin mayor incomodidad para el paciente.</p>
    - title: >-
        <h4>¿Por qué se recomienda siempre aislar el diente a tratar con un dique
        de goma?</h4>
      content: >-
        <p>El aislamiento absoluto del campo operatorio permite mantener en todo
        momento las condiciones de asepsia y facilita los procedimientos de
        antisepsia. Además de evitar el ingreso de saliva <em>(sustancia rica en
        bacterias)</em> al interior del conducto, el dique de goma mejora la
        visibilidad de la zona e impide que el paciente pueda aspirar o deglutir
        instrumentos y productos químicos durante el tratamiento.</p>
    - title: <h4>¿Qué es la conductometría?</h4>
      content: >-
        <p>Consiste en determinar la longitud exacta entre la constricción apical
        de cada conducto y el borde incisal o cara oclusal del diente en
        tratamiento. Esta medida constituye la longitud de trabajo y debe ser
        respetada en todo momento para evitar complicaciones y fracasos
        terapéuticos.</p>
    - title: >-
        <h4>¿Cuáles son los objetivos de la instrumentación o preparación
        biomecánica?</h4>
      content: >-
        <p>Extirpar la pulpa, eliminar residuos y material necrótico, ensanchar el
        conducto y darle forma cónica para su completa desinfección y adecuada
        obturación.</p>
    - title: >-
        <h4>¿Con qué se rellenan u obturan los conductos radiculares ya
        vacíos?</h4>
      content: >-
        <p>A lo largo de la historia se han utilizado una gran variedad de
        materiales con tal finalidad, sin embargo, ninguno ha demostrado cumplir
        con todos los requisitos deseables del material ideal. En la actualidad la
        gutapercha sigue siendo el más utilizado, un tipo de goma polimérica
        parecida al caucho y que se extrae de un árbol originario de las islas del
        archipiélago Malayo. Adicionalmente, se coloca un cemento que garantice el
        sellado apical, generalmente a base de hidróxido de calcio.</p>
    - title: >-
        <h4>¿Cómo saber si un Tratamiento de Endodoncia fue correctamente
        realizado?</h4>
      content: >-
        <p>Básicamente por la ausencia de sintomatología y por la evidencia
        radiográfica. Un Tratamiento de Conducto bien realizado debe reflejar una
        obturación que se prolongue hasta la constricción apical de la raíz, que
        carezca de luces en su interior, que tenga cuerpo y volumen suficiente,
        que sea compacta en toda su extensión y que reproduzca fielmente la forma
        cónica del conducto que ocupa.</p>
    - title: <h4>¿Cuáles complicaciones podrían presentarse?</h4>
      content: >-
        <p>Si el Especialista no está bien capacitado y atento a su trabajo,
        podría ensanchar en exceso las paredes radiculares y debilitar el diente,
        podría fracturar instrumentos dentro del conducto, podría crear escalones
        y falsas vías, perforaciones coronarias, camerales o radiculares, podría
        impulsar restos necróticos u orgánicos al periápice y generar sub o
        sobreobturaciones.</p>
    - title: <h4>¿Qué es una sobreobturación?</h4>
      content: >-
        <p>Consiste en la extravasación de una pequeña cantidad de sellador o
        gutapercha hacia la zona periradicular, fuera de la raíz del diente.
        Afortunadamente, numerosos trabajos científicos avalan el éxito del
        tratamiento aún en los casos donde se haya producido una pequeña
        sobreobturación.</p>
    - title: <h4>¿Qué es un Retratamiento?</h4>
      content: >-
        <p>El Retratamiento o Reendodoncia es la intervención que se realiza para
        eliminar el material de relleno que se encuentra en el interior de los
        conductos de un diente ya tratado para su nueva limpieza, conformación y
        obturación. En otras palabras, es un nuevo Tratamiento de Conducto en un
        diente ya endodonciado.</p>
    - title: <h4>¿Cuándo debe realizarse?</h4>
      content: >-
        <p>Cuando por alguna razón haya fracasado la terapia endodóntica inicial,
        o bien, cuando los conductos se hayan vuelto a contaminar. Tal
        circunstancia suele ocurrir cuando no se confecciona a la brevedad la
        restauración definitiva, cuando aparezcan nuevas caries, en casos de
        enfermedad periodontal avanzada o cuando el diente se fisure y sufra
        alguna pequeña fractura. Cualquiera de estas situaciones podría derivar en
        una Reinfección.</p>
    - title: >-
        <h4>¿Son más frágiles los dientes que han sufrido un Tratamiento de
        Conducto?</h4>
      content: >-
        <p>¡Indudablemente! La pérdida de estructura dentaria coronal y de los
        mecanoreceptores pulpares los hacen menos flexibles, menos resistentes y
        más susceptibles a la fractura. Es por ello que de su adecuada e inmediata
        reconstrucción, dependerán su permanencia y longevidad.</p>
    - title: >-
        <h4>¿Cuánto tiempo se puede permanecer sin la restauración
        definitiva?</h4>
      content: >-
        <p>El menor tiempo posible. Mientras un diente endodonciado no se
        restaure, será propenso a la fractura y reinfección. En aquellos casos de
        gran destrucción, que ameriten la confección de núcleos o pernos
        artificiales, recomendamos iniciar la preparación radicular a los dos o
        tres días de culminada la Endodoncia.</p>

cases:
  title: >
    <h1>Endodoncia - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-endodontics-es-01.jpg
      - /uploads/clinic-cases-endodontics-es-02.jpg
      - /uploads/clinic-cases-endodontics-es-03.jpg
      - /uploads/clinic-cases-endodontics-es-04.jpg
      - /uploads/clinic-cases-endodontics-es-05.jpg
      - /uploads/clinic-cases-endodontics-es-06.jpg
      - /uploads/clinic-cases-endodontics-es-07.jpg
      - /uploads/clinic-cases-endodontics-es-08.jpg
      - /uploads/clinic-cases-endodontics-es-09.jpg
      - /uploads/clinic-cases-endodontics-es-10.jpg
      - /uploads/clinic-cases-endodontics-es-11.jpg
      - /uploads/clinic-cases-endodontics-es-12.jpg
      - /uploads/clinic-cases-endodontics-es-13.jpg
      - /uploads/clinic-cases-endodontics-es-14.jpg
      - /uploads/clinic-cases-endodontics-es-15.jpg
      - /uploads/clinic-cases-endodontics-es-16.jpg
      - /uploads/clinic-cases-endodontics-es-17.jpg
      - /uploads/clinic-cases-endodontics-es-18.jpg

  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Retratamiento de un Primer Molar Inferior</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reendodoncia por Filtración </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Remoción de Instrumento Fracturado </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Endodoncia Multiradicular </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Terapia Endodóntica Convencional por Caries</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tratamiento en Ápice Inmaduro</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Síndrome del Diente Fisurado</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tratamiento de Conducto Multiradicular</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Obturación con Gutapercha Termoplastificada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Biopulpectomía </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Endodoncia en un Caso de Bruxismo Severo</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Necrosis Pulpar</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Necropulpectomía</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Periodontitis Apical Aguda</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Fracaso por Mal Diagnóstico</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pulpitis Irreversible por Caries Proximal </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Endodoncias Múltiples por Razones Protésicas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /upload/clinic-cases-endodontics-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Retratamiento, Desobturación Y Núcleo Artificial</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">ACUDÍ A LA CLÍNICA PARA EXTRAERME UNA MUELA QUE SE ME HABÍA PARTIDO Y ME
    DIJERON QUE SERÍA UN GRAN ERROR HACERLO, QUE SE PODÍA SALVAR CON ENDODONCIA. AÚN
    ESCÉPTICO, INICIÉ EL TRATAMIENTO, Y AHORA LO AGRADEZCO; AUNQUE CON UNA FUNDA CERÁMICA
    AÚN CONSERVO íntegra MI DENTADURA".</p>
  images:
    portrait: /uploads/quotes-endodontics-portrait.jpg
    landscape: /uploads/quotes-endodontics.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
