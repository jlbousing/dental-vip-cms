---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/aesthetic-dentistry/teeth-whitening/
title: Blanqueamiento Dental
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-teeth-whitening.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 35%
  content:
    position: center
    body: >
      <i></i>

# Heading Section
heading:
  display: true
  content: <h1 class="bebas title">Blanqueamiento Dental</h1>
    <p>Consiste en el tratamiento de las superficies
    dentales con agentes químicos y fuentes lumínicas de alta intensidad que remueven
    y eliminan los pigmentos que afean el color de la sonrisa. </p>

# Gallery Section
listGallery:
  display: true
  position: bottom
  type: Column
  content: >
    <h2>Sus Dientes + Blancos en Solo 8 Pasos:</h2>
    <p class="subtitle">
        Previa profilaxis dental y pulido del esmalte con conos de goma y pastas de grano ultrafino, son muy pocos los pasos <br> que le separarán de una hermosa y radiante apariencia:
    </p>
  blocks:
    - img: /uploads/teeth-whitening-list-01.png
      number: 1
      title: >
        <h3>Registro del color inicial</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Se debe
        realizar de forma visual y siempre bajo una fuente de luz natural, sin la
        presencia de pintura de labios y con la ayuda de una guía estandarizada de
        al menos 15 tonos.</p>

    - img: /uploads/teeth-whitening-list-02.png
      number: 2
      title: >
        <h3>Desensibilización</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">La aplicación
        de un agente desensibilizante 10 minutos antes del procedimiento es muy aconsejable
        para prevenir, o al menos aliviar, las posibles molestias postoperatorias.</p>

    - img: /uploads/teeth-whitening-list-03.png
      number: 3
      title: >
        <h3>Barrera gingival</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Colocación de una
        capa de resina poliacrílica que protege las encías y tejidos blandos de quemaduras
        químicas y lesiones cáusticas durante el procedimiento.</p>

    - img: /uploads/teeth-whitening-list-04.png
      number: 4
      title: >
        <h3>Dosificación</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Preparación del gel
        blanqueador, generalmente a base de peróxidos, según las concentraciones previamente
        definidas en las fases de evaluación, diagnóstico y planificación del tratamiento.</p>

    - img: /uploads/teeth-whitening-list-05.png
      number: 5
      title: >
        <h3>Aplicación del gel</h3>
        <p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">En abundante
        cantidad y directamente sobre las superficies de las caras frontales o vestibulares
        de los dientes, cuidando de no traspasar los límites de las barreras protectoras.</p>

    - img: /uploads/teeth-whitening-list-06.png
      number: 6
      title: >
        <h3>Fotoactivación</h3><p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Por períodos parciales
        de 15 minutos y luz LED de espectro azul con longitudes de onda de entre 380
        y 515 nanómetros, y potencia igual o superior a 400 mW/cm2.</p>

    - img: /uploads/teeth-whitening-list-07.png
      number: 7
      title: >
        <h3>Remoción y limpieza</h3><p class="dv-div-text list" style="margin-top:-10px;margin-bottom:-4px;">Aspiración del
        químico con máquina de alta succión, desprendimiento de la barrera gingival
        y lavado del campo operatorio con agua y abundante irrigación.</p>

    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h3>Valoración final</h3><p class="dv-div-text list" style="margin-top:-4px;margin-bottom:-10px;">Análisis objetivo
        de los resultados obtenidos. Cuantificación de los cambios en matiz o croma,
        valor o luminosidad y saturación o intensidad.</p>

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-teeth-whitening.png
  content: >
    <i></i>

# Form Section
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

# Slogan Section
slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¡Ilumine Ya Su Sonrisa!</h1>
    <br>
    <br>
    <h2 > Luzca más joven y presuma del tratamiento estético más rápido, seguro
    y demandado del momento.</h2>

# Aside Section
anexes:
  display: true
  enforce: false
  items:
    - img: /uploads/sections-teeth-whitening-homemade.png
      content: >
        <h2>Blanqueamiento dental en casa</h2>
        <p class="dv-srv-pr dv-srv-pr-45">Se indica en los casos más sencillos. La técnica suele consistir en utilizar un gel y una férula plástica transparente hecha a medida que se deja sobre los dientes por un lapso de tiempo determinado, generalmente durante las noches al dormir.</p>
          <p class="dv-srv-pr dv-srv-pr-45">Sin embargo, realizar el acto una sola vez no dará mayores resultados. Debe de ser un proceso constante que puede durar varios días o semanas, dependiendo de la concentración del producto. Su principal inconveniente es que los resultados suelen ser inestables y poco duraderos. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> MÁS INFORMACIÓN</span>
    - img: /uploads/sections-teeth-whitening-clinic.png
      content: >
        <h2>Blanqueamiento
        dental en clínica</h2>
        <p class="dv-srv-pl dv-srv-pl-pr">También denominado por
        luz o fotoactivación. Se trata de un procedimiento mucho más avanzado, en el
        que en una sola sesión de 45 minutos, se logran usualmente resultados notables.</p><p
        class="dv-srv-pl dv-srv-pl-pr">El alto grado de concentración del peróxido utilizado,
        hace pertinente proteger cuidadosamente las encías con una barrera o funda especial.
        A continuación, se aplica el gel blanqueador, cuyas propiedades se potencian
        gracias a la energía suministrada por una luz LED de alta intensidad, que permite
        ajustes variables para proporcionar máxima seguridad y confort al paciente.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-teeth-whitening-mixed.png
      content: >
        <h2>Blanqueamiento
        dental combinado</h2>
        <p>Es con seguridad el más
        efectivo de todos, ya que combina las bondades de las técnicas de fotoactivación
        y blanqueamiento en casa.</p> <p class="dv-srv-pr dv-srv-pr-45">Normalmente
        se ejecuta en 3 fases. La primera ambulatoria, donde el paciente se aplica el
        producto blanqueante en su domicilio por un lapso de 10 a 15 días. La segunda
        en clínica,  donde se realizan una o dos sesiones con agentes fotosensibles
        de alta concentración. Y la tercera en casa, donde bajo estricta supervisión
        profesional, se indican pequeños y esporádicos retoques que garanticen la perdurabilidad
        de los resultados obtenidos. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-teeth-whitening-intern.png
      content: >
        <h2>Blanqueamiento
        interno</h2>

        <p class="dv-srv-pr dv-srv-pr-45">Es una excelente alternativa para
        tratar las discromías en piezas no vitales <em>(con tratamiento de conducto)</em>,
        ya que representa una intervención conservadora y muy económica frente a otros
        procedimientos más complejos como las restauraciones adhesivas, carillas de
        porcelana y coronas cerámicas. </p><p class="dv-srv-pr dv-srv-pr-45">Generalmente
        implica el abordaje endodóntico del diente, sellado del conducto radicular,
        aplicación de soluciones blanqueadoras de altísima concentración, lavado y obturación
        final. Suelen requerirse varias sesiones clínicas para alcanzar el color deseado.   </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: ""
          placeholder: <span>Más Información</span>

# Article Section
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Beneficios de un blanqueamiento dental profesional</h2>
      <p class="dv-div-text dv-pb-15">Indudablemente, el principal beneficio es la satisfacción
      personal de tener una dentición más atractiva que le permitan al paciente sentirse
      cómodo y agradado con su sonrisa, seguro de sí mismo y mejor valorado por la sociedad.
      Además, es irrefutable el hecho de que unos dientes blancos y pulcros proyectan
      de inmediato una apariencia de buena higiene y cuidado personal.</p><p class="dv-div-text
      dv-pb-15">Sin embargo, los factores estéticos y psicológicos no son las únicas
      ventajas, ya que numerosos estudios han demostrado los efectos del peróxido de
      carbamida como antiséptico oral y su importante acción en la reducción de los
      índices de placa y enfermedad periodontal; promoviendo considerablemente la salud
      de dientes y encías.</p>
    image:
      display: true
      size: 40px
      src: /uploads/sections-icons-aesthetic-dentistry.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-teeth-whitening.png
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Diseño digital de sonrisa</h5>
      to: "/especialidades/estetica-dental/diseno-digital-de-sonrisa/"
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> Carillas de porcelana</h5>
      to: "/especialidades/estetica-dental/carillas-de-porcelana/"
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Diagnóstico y Planificación 3D</h5>
      to: "/especialidades/implantes-dentales/diagnostico-y-planificacion-3d/"
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> Brackets Estéticos</h5>
      to: "/especialidades/ortodoncia/aparatos-esteticos/"
      img: "/uploads/procedures-brackets.jpg"
    - title: >
        <h5> Prótesis sobre implantes</h5>
      to: "/especialidades/implantes-dentales/protesis-sobre-implantes/"
      img: "/uploads/procedures-dental-implants.jpg"
---
