---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/aesthetic-dentistry/digital-smile-design/
title: Diseño digital de sonrisa
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-digital-smile-design.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 66%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: bottom
  type: Column
  content: >
    <div></div>
  blocks:
    - img: /uploads/teeth-whitening-list-08.png
      number: 8
      title: >
        <h2></h2>

# Heading Section
heading:
  display: true
  content:
    <h1 class="bebas title">Diseño Digital de Sonrisa (DDS)</h1>
    <p>Es una herramienta informática que
    nos permite, mediante un protocolo fotográfico digital, registrar y estudiar sus
    proporciones y rasgos dentofaciales, planificar su sonrisa ideal y simularla gráficamente.</p>

parallax:
  display: true
  portraitPosition: center
  mobilePosition: '{"mobilePosition320":"-527px","mobilePosition360":"-600px","mobilePosition375":"-773px"}'
  img: /uploads/parallax-digital-smile-design.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-annexed-pages.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¡Ahora Podrá Ver el Resultado Final Antes de Iniciar Su Tratamiento!</h1>
    <br>
    <br>
    <h2 >Definitivamente, el Diseño de Sonrisa revolucionó para siempre el estudio
    y ejercicio clínico de la Odontología Estética.</h2>
# anex-links
anexes:
  enforce: true
  display: true
  items:
    - img: /uploads/sections-dds-shoot.jpg
      content: >
        <h2>Captura de imágenes</h2>
        <p class="dv-srv-pr dv-srv-pr-45">Estamos plenamente convencidos
         de que la fotografía clínica es para la Estética Dental lo que la radiografía
          es para la Odontología en general. "Una imagen dice más que mil palabras" y
          "una fotografía muestra más de mil detalles".</p><p class="dv-srv-pr dv-srv-pr-45">Nos
          valemos de una cámara reflex digital para registrar la boca y la cara del paciente
          desde varios ángulos y perfiles. Para realizar un buen trabajo es indispensable
          que las fotografías intra y extraorales cuenten con alta resolución, buena iluminación,
          nitidez y profundidad de campo. Normalmente nos manejamos bajo formato JPG. </p>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> LOAD FILE</span>
    - img: /uploads/sections-dds-processing.jpg
      content: >
        <h2>Procesamiento informático</h2>
        <p class="dv-srv-pl dv-srv-pl-pr">Un software profesional de
        tratamiento de imágenes nos permite medir y modificar la posición, la forma
        y las dimensiones de dientes y encías con la finalidad de reproducir el mejor
        resultado estético posible, claro está, bajo el criterio de la proporción áurea
        <i>(1/1.618)</i> y siempre dentro de los límites biológicamente viables.</p><p
        class="dv-srv-pl dv-srv-pl-pr">En DENTAL VIP nunca utilizamos plantillas ni
        prototipos seriados. Nuestros diseños son completamente personalizados, realistas
        y ajustados a la verdadera condición clínica de la persona. <strong>¡Solo mostramos
        lo que verdaderamente podemos lograr!</strong></p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
        <p>Es la proyección final, simulación
        y fotomontaje de la nueva sonrisa. En él, se podrán apreciar con detalle las
        bondades de todas y cada una de las intervenciones sugeridas por nuestro equipo
        de Especialistas. Si aún hubiese duda, procederíamos entonces a obtener un molde
        real de yeso para sobre él reproducir en cera de laboratorio el diseño contemplado
        y duplicarlo directamente en la boca del paciente con una resina acrílica especial.
        Esta férula o prototipo acrílico podrá ser utilizado hasta por una semana y
        sometido a la consideración de amigos y familiares cercanos.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: ""
          placeholder: <span>Más Información</span>

articleBlock:
  direction: reverse
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Ventajas de nuestro protocolo DDS</h2>
      <ul>
      <li> <i class="icon-check circle"></i>Incorpora innovadoras e invalorables herramientas
      de diagnóstico y planificación terapéutica. </li>
      <li><i class="icon-check circle"></i>Promueve la activa y determinante participación
      del paciente en su tratamiento.</li>
      <li><i class="icon-check
      circle"></i>Nos permite experimentar con diferentes formas, tamaños y colores
      de dientes; antes de elegir los definitivos.</li>
      <li><i
      class="icon-check circle"></i>Facilita la comunicación e interacción entre nuestro
      equipo de Odontólogos.</li>
      <li><i class="icon-check circle"></i>Hace
      posible la transmisión de indicaciones gráficas exactas al técnico dental.</li>
      <li><i class="icon-check circle"></i>Garantiza que la
      confección de las restauraciones cerámicas indirectas sean reflejo fiel de nuestros
      deseos y criterios profesionales.</li>
      <li><i class="icon-check
      circle"></i>Resultados altamente predecibles, precisos y satisfactorios.</li></ul>
    image:
      display: false
      size: 50px
      src: /uploads/sections-icons-aesthetic-dentistry.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-digital-smile-design.jpg
# Procedures Section
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>

procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Carillas de porcelana </h5>
      to: "/especialidades/estetica-dental/carillas-de-porcelana/"
      img: "/uploads/procedures-veneers.jpg"
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: "/especialidades/implantes-dentales/diagnostico-y-planificacion-3d/"
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> Rehabilitación Oral </h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Prótesis sobre implantes </h5>
      to: "/especialidades/implantes-dentales/protesis-sobre-implantes/"
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5> Tecnología cad-cam </h5>
      to: "/especialidades/protesis/tecnologia-cad-cam/"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> Brackets Estéticos </h5>
      to: "/especialidades/ortodoncia/aparatos-esteticos/"
      img: "/uploads/procedures-brackets.jpg"
---

<div class="container">
<div class="row">
<div class="item np left">

## Aprobación y consentimiento

El concepto
DDS involucra al paciente en el proceso de transformación de su propia sonrisa,
haciéndole partícipe como co-diseñador del tratamiento y permitiéndole expresar
sus expectativas al equipo de trabajo, para finalmente, lograr su conformidad
absoluta y autorización para el procedimiento clínico.

</div>

<div class="item np right">

## Ejecución intraoral

Cirugía plástica periodontal, blanqueamiento
dental, recontorneado estético, restauraciones adhesivas, carillas y coronas totalcerámicas
son las intervenciones clínicas más comunes en un Diseño de Sonrisa. Por lo general,
es necesaria la participación de dos o más Odontólogos Especialistas.

</div>
</div>

![Hopper The Rabbit](/img/dds-custom-block-smile.jpg)

<div class="row">
<div class="item np center">

## Morfología Dental y Visagismo: El Arte de Personalizar una Sonrisa

Es incuestionable que para realzar la belleza de una persona debemos
siempre procurar armonía entre su rostro, sus rasgos faciales y la forma de sus
dientes. Pero con la técnica del Visagismo vamos aún más allá y buscamos proyectar
la personalidad del paciente a través del patrón geométrico de su sonrisa. Hoy
en día sabemos que nuestro entendimiento inconsciente y emocional tiende a asociar
las formas ovales con caracteres sensibles y melancólicos, las formas triangulares
con personas dinámicas y extrovertidas, las rectangulares con temperamentos fuertes
y dominantes y las cuadradas con individuos serios y discretos.

</div>

</div>

<div class="row fb">
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-oval.png)

<p class="dv-div-text">Centrales dominantes</p><p class="dv-div-text">Caninos
      redondeados</p><p class="dv-div-text">Laterales delicados</p><p class="dv-div-text">Arco
      redondeado</p>
      
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-triangular.png)

<p class="dv-div-text">Línea de sonrisa ascendente</p><p class="dv-div-text">Ejes
      convergentes</p><p class="dv-div-text">Caninos inclinados</p>
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-rectangular.jpg)

<p class="dv-div-text">Centrales dominantes</p><p class="dv-div-text">Bordes
      incisales planos</p><p class="dv-div-text">Caninos agresivos</p><p class="dv-div-text">Ejes
      verticales</p>
</div>
<div class="fb-4 item np center">

![Hopper The Rabbit](/img/dds-custom-block-cuadradra.png)

<p class="dv-div-text">Ausencia de dominancia</p><p class="dv-div-text">Ejes
      divergentes</p><p class="dv-div-text">Disposición horizontal</p>
</div>
</div>
</div>
<br>
<br>
<br>
