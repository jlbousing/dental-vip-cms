---
templateKey: specialties-page
language: es
redirects: /en/specialties/aesthetic-dentistry/
title: Estética Dental
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-aesthetic-dentistry.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 47%
  content:
    position: center
    body: >
      <h1 class="bebas">Estética Dental</h1>

# Heading Section
heading:
  display: true
  content: <h1 class="title"><i class="icon icon-aestetic-dentistry"></i></h1>
    <p class="thin"><em>La Biomimética es el arte de crear Restauraciones Dentales que estén en
    Armonía con la Naturaleza, a través del conocimiento, dominio y experta manipulación
    de los diferentes sistemas cerámicos y adhesivos disponibles.</em></p>

# Description Section
article:
  content: >
    <p>En la sociedad actual, donde la imagen es tan importante, una apariencia
    agradable muchas veces significa la diferencia entre el éxito y el fracaso; personal
    y profesional. De tal forma que <strong>una Sonrisa Sana y Bonita no es un lujo
    o capricho sino casi una necesidad. La gama de tratamientos a nuestro alcance
    para corregir las Imperfecciones Dentales es cada vez más amplia y asequible,</strong>
    y los resultados son cada vez más rápidos, seguros y precisos. Entre ellos contamos
    con sistemas de BLANQUEAMIENTO DENTAL a base de peróxido de hidrógeno o peróxido
    de carbamida fotoactivables con lámparas halógenas de alta intensidad <i>(sistema
    ZOOM<sup>®</sup>)</i> que nos proporcionan beneficios notables e inmediatos.</p><p>Además,
    <strong>en la Especialidad ya es rutina trabajar con fotografías digitales para
    el estudio y planificación computarizada del plan de tratamiento odontoestético.</strong>
    A este fenómeno se le ha denominado DISEÑO DE SONRISA y nos brinda la posibilidad
    de aplicar un criterio más científico y menos subjetivo a la hora de restaurar
    dientes cariados, fracturados, pigmentados, rotados, separados o malformados.
    Por otro lado, <strong>los nuevos materiales adhesivos basados en la nanotecnología
    nos permiten por su gran versatilidad modificar a placer la forma, color y tamaño
    de los dientes. En muy pocas citas el resultado salta a la vista y la transformación
    luce espectacular. </strong>Para los casos más complejos contamos con las llamadas
    CARILLAS DE PORCELANA, que son finas láminas de material cerámico que se adhieren
    firmemente a las estructuras dentarias y que se indican para la corrección de
    los defectos estéticos de mayor extensión y gravedad.</p><p><strong>Hoy en día
    la Odontología Estética prácticamente no tiene límites, </strong>salvo la creatividad,
    destreza y preparación del equipo profesional.</p>
  img: /uploads/aside-aesthetic-dentistry.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      La sonrisa genuina, también llamada Sonrisa de Duchenne, es aquella que surge
      en el rostro de manera espontánea, sincera y natural. Indudablemente es un signo
      expresivo de bienestar que fomenta la sociabilidad y favorece la comunicación
      entre las personas.
      </em>
    </p>
  footer:
    author: >
      <strong>Dra. Filomena Montemurro Tafuri</strong>
    details: >
      <strong>
        Prostodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-aesthetic-dentistry.jpg

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-aesthetic-dentistry.jpg
  mobilePosition: '{"mobilePosition320":"-967px","mobilePosition360":"-1090px","mobilePosition375":"-1476px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: >-
        <h4>¿Por qué es importante el uso de la fotografía clínica en Odontología
        Estética?</h4>
      content: >-
        <p>Una fotografía intraoral proporciona una visualización instantánea de
        los dientes en tiempo real. Con ella se pueden observar detalles difíciles
        de apreciar a simple vista, y además, permite que el paciente opine y
        participe activamente en la elaboración de su plan de tratamiento. Otra
        ventaja de la fotografía seriada es el almacenamiento de imágenes para
        futuras comparaciones del antes y después del tratamiento.</p>
    - title: <h4>¿En qué consiste un diseño de sonrisa?</h4>
      content: >-
        <p>Consiste en hacer un estudio digital detallado de las características
        dentales, gingivales y faciales de una persona al sonreír para identificar
        qué cambios o modificaciones serían favorables, desde el punto de vista
        estético, para armonizar y embellecer el aspecto de su sonrisa. La
        ejecución clínica del diseño podría ser muy conservadora, desde un simple
        recontorneado y blanqueamiento dental, hasta intervenciones más invasivas
        como ortodoncia, cirugía de encías, confección de carillas, coronas
        totalcerámicas, e incluso; cirugía maxilofacial. Todo depende del caso y
        de las expectativas del paciente.</p>
    - title: <h4>¿Cuándo se puede y debe realizar un diseño de sonrisa?</h4>
      content: >-
        <p>Cuando el paciente sea mayor de edad, desee mejorar su apariencia
        física y no escape su percepción estética de la realidad. Es
        verdaderamente aterrador ver a personas con dientes que simulan “teclas de
        piano”, exageradamente largos, blancos y monocromáticos, sin armonía con
        sus parámetros y rasgos propios y elaborados sin tomar en consideración
        que cada ser humano es una entidad exclusiva e individual. El diseño de
        sonrisa es una herramienta invalorable en el campo de la Odontología
        Cosmética, siempre y cuando se utilice con ética, sensatez y verdadero
        criterio estético.</p>
    - title: >-
        <h4>¿Qué parámetros se emplean para elegir la forma de los nuevos
        dientes?</h4>
      content: >-
        <p>Raza, sexo, edad, personalidad, tamaño y forma de la cara, biotipo y
        estado gingival, línea de la sonrisa, tipo de oclusión o mordida, y por
        supuesto; las preferencias y gustos particulares del paciente. También es
        muy importante trabajar siempre bajo el precepto de la proporción áurea o
        divina, tratar en lo posible de que las nuevas restauraciones guarden
        sintonía con uno de los parámetros más utilizados en cirugía plástica y
        medicina estética, aquel que sugiere que una relación proporcional de
        1/1,618 entre el tamaño de elementos próximos y bien definidos, será
        garantía de éxito en la expresión y proyección de armonía, belleza y
        naturalidad.</p>
    - title: <h4>¿Cuándo está indicado un blanqueamiento dental?</h4>
      content: >-
        <p>Como todo tratamiento estético, la indicación de realizar o no un
        blanqueamiento dental está determinada por la percepción y necesidades
        propias del paciente. Este procedimiento nos permite eliminar manchas y
        pigmentaciones dentarias de origen extrínseco y aclarar el tono del color
        de toda la dentadura.</p>
    - title: <h4>¿Cuándo está contraindicado?</h4>
      content: >-
        <p>Cuando el paciente tenga caries, enfermedades periodontales activas o
        sensibildad dental extrema. Además, en los casos de manchas intrínsecas o
        excesivamente oscuras como las ocasionadas por tetraciclinas, hemorragias
        pulpares internas, alteraciones en la formación histológica del esmalte y
        dentina; y las originadas como producto de la corrosión de antiguas
        restauraciones metálicas. En estos casos, estaría indicada la realización
        de carillas o coronas cerámicas para lograr el cambio de color
        deseado.</p>
    - title: <h4>¿Por qué debe realizarlo un Odontólogo?</h4>
      content: >-
        <p>Los blanqueamientos caseros que se expenden en tiendas y farmacias
        poseen una concentración muy inferior a los que se utilizan en consulta.
        Tal fenómeno obedece al hecho de que las cubetas para su aplicación son
        universales y no están confeccionadas a medida, condición que imposibilita
        evitar el contacto del peróxido blanqueador con las encías y mucosas del
        paciente. Si esos productos tuvieran una concentración profesional,
        causarían grandes daños y lesiones cáusticas a los tejidos
        periodontales.</p>
    - title: <h4>¿Cómo blanquea el peróxido de carbamida?</h4>
      content: >-
        <p>Al entrar en contacto con la saliva, se descompone en peróxido de
        hidrógeno y urea. Por su bajo peso molecular, el peróxido de hidrógeno
        penetra con facilidad los prismas de esmalte y canalículos dentinarios, en
        cuyo interior es metabolizado por ciertas enzimas como la catalasa,
        peroxidasa e hidroperoxidasa; liberándose como producto final moléculas de
        oxígeno que reblandecen y eliminan los pigmentos y desechos
        interplasmáticos. La luz azul de alta intensidad actúa como catalizador
        suministrándole energía a la solución blanqueadora para acelerar su
        difusión y oxidación dentro de la estructura dentaria.</p>
    - title: >-
        <h4>¿Qué tanto se pueden blanquear los dientes y cuánto duran los
        resultados obtenidos?</h4>
      content: >-
        <p>El color de los dientes está determinado genéticamente y se evalúa con
        una guía cromática estandarizada de 15 tonos. Gracias a un blanqueamiento
        con peróxido de carbamida podemos aclarar entre 1 y 10 tonos, lo que
        implica que una sonrisa muy amarillenta o grisácea pueda volver a lucir su
        blanco original. Los resultados suelen perdurar entre 2 y 7 años, todo
        depende de los hábitos alimenticios, higiénicos y sociales del paciente.
        En individuos fumadores, consumidores habituales de bebidas oscuras
        <em>(colas, café y té)</em> y con dietas altas en cítricos <em>(kiwi o
        piña)</em>, la duración del resultado puede verse considerablemente
        comprometida.</p>
    - title: <h4>¿Es el blanqueamiento dental un procedimiento doloroso?</h4>
      content: >-
        <p>¡En lo absoluto! Es un tratamiento conservador que ni siquiera requiere
        anestesia dental, sin embargo, en muchos casos puede generar un aumento
        pasajero de la sensibilidad dentaria. Esta hipersensibilidad se considera
        normal y es controlada con el uso de dentífricos especiales y geles
        desensibilizantes a base de flúor.</p>
    - title: <h4>¿En qué consiste el dental reshaping o recontorneado estético?</h4>
      content: >-
        <p>Con el paso de los años los dientes se desgastan y sufren
        microfracturas que alteran su forma natural y envejecen el aspecto de la
        dentadura. Con solo realizar pequeños desgastes de los bordes y ángulos
        dentarios es posible recuperar de inmediato la morfología perdida y la
        apariencia original de la sonrisa. Es un procedimiento indoloro, rápido,
        seguro y muy económico.</p>
    - title: >-
        <h4>¿Cuáles son los materiales restauradores más novedosos en Estética
        Dental?</h4>
      content: >
        <p>En la última década ha sido asombroso el desarrollo de los
        biomateriales odontológicos. Hoy en día contamos con composites o resinas
        compuestas por material de relleno nanométrico que proveen excelentes
        propiedades físicas y estéticas, y con las cuales es posible lograr
        restauraciones adhesivas directas <em>(hechas en boca por el
        Odontólogo)</em> anteriores y posteriores, perfectamente lisas y pulidas;
        de brillo, color y textura similar al esmalte dental. Se indican para el
        tratamiento de caries, fracturas y defectos estéticos de poca
        extensión.</p> <p>Para la fabricación de carillas, coronas e
        incrustaciones en el laboratorio, la gran novedad es la utilización de
        cerámicas <em>(popularmente llamadas porcelanas)</em> a base de Óxido de
        Zirconio <em>(ZrO<sub>2</sub>)</em> y Disilicato de Litio
        <em>(LS<sub>2</sub>)</em>, con altísima resistencia a la flexión y
        fractura que permiten prescindir por completo del uso de aleaciones
        metálicas de base, y en consecuencia; mejorar drásticamente las
        propiedades de transparencia, fluorescencia y opalescencia de los dientes
        restaurados. Entre las cerámicas de última generación más utilizadas
        están: DC-Zircon<sup>®</sup> <em>(DCS)</em>, Cercon<sup>®</sup>
        <em>(Dentsply)</em>, In-Ceram<sup>®</sup> YZ <em>(Vita)</em>,
        Procera<sup>®</sup> Zirconia <em>(Nobel Biocare)</em>, Lava<sup>®</sup>
        <em>(3M Espe)</em> e IPS e.max<sup>®</sup> <em>(Ivoclar)</em>; entre
        otras.</p>
    - title: <h4>¿Qué es una incrustación?</h4>
      content: >-
        <p>Es un tipo de restauración indirecta <em>(se fabrica en el
        laboratorio)</em> que se utiliza para reconstruir estéticamente dientes
        posteriores endodonciados o muy destruidos. Es una excelente alternativa a
        la típica corona dental y según su extensión se clasifican en inlays,
        onlays y overlays. Generalmente se confeccionan con porcelanas
        feldespáticas o zirconiosas proporcionando un aspecto estético
        extraordinario.</p>
    - title: <h4>¿Qué es una carilla?</h4>
      content: >-
        <p>Las carillas dentales son restauraciones directas hechas de composite,
        o bien, finas láminas de porcelana que se adhieren exclusivamente sobre la
        superficie anterior o cara vestibular de los dientes para mejorar su forma
        y apariencia. Las carillas dentales se usan para corregir dientes
        fracturados, manchados, desalineados, desgastados, separados o
        malformados.</p>
    - title: >-
        <h4>¿En qué se diferencian las incrustaciones, las carillas y las
        coronas?</h4>
      content: >-
        <p>Las incrustaciones y carillas son restauraciones de recubrimiento
        parcial, es decir, solo sustituyen una parte o porción del diente;
        mientras que las coronas restauran o reemplazan toda la parte visible del
        mismo. Las carillas se indican en los dientes anteriores, las
        incrustaciones en los posteriores y las coronas en cualquier diente de la
        cavidad bucal.</p>
    - title: >-
        <h4>¿Qué es mejor entonces para un diente frontal, una carilla o una
        corona?</h4>
      content: >-
        <p>Todo depende de las condiciones y exigencias clínicas del caso. Antes
        de tomar una decisión, el Odontólogo Estético debe evaluar factores como
        el grado de integridad estructural del órgano, aspecto de los dientes
        vecinos, propiedades y características del esmalte, vitalidad pulpar,
        estado periodontal, índice de higiene oral, hábitos sociales y funcionales
        del paciente; entre otros. Al final, cada tipo de restauración tiene sus
        indicaciones específicas y solo un acertado criterio profesional, según el
        caso, nos dará la respuesta.</p>
    - title: <h4>¿Cómo se pegan estas restauraciones a los dientes?</h4>
      content: >-
        <p>El cementado es uno de los pasos más importantes porque de él depende
        en gran medida la duración a largo plazo de las restauraciones en boca. Es
        fundamental que el clínico conozca y domine los distintos sistemas
        adhesivos para cada material cerámico y cada tipo de restauración. El
        cementado adhesivo promueve la formación de fuertes uniones químicas y
        mecánicas entre diente y restauración.</p>
    - title: <h4>¿Son todas las carillas de porcelana?</h4>
      content: >-
        <p>¡No! Pueden fabricarse también de resinas compuestas mediante técnica
        directa, en una sola sesión y sin intervención del laboratorio dental, sin
        embargo; son mucho menos estéticas, menos resistentes y su vida útil menor
        que las de cerámica. Estas últimas pueden permanecer hasta 20 años en
        boca.</p>
    - title: >-
        <h4>Si al sonreír muestro gran cantidad de encía, ¿qué se puede
        hacer?</h4>
      content: >-
        <p>Depende del caso y su etiología. Si la causa de la sonrisa gingival es
        la sobreerupción de los incisivos superiores, la Ortodoncia es el
        tratamiento de elección, para nivelar la arcada, intruir los dientes y
        lograr la migración apical de su encía marginal. Si el problema es
        generado por una alteración de la erupción pasiva, por un verdadero exceso
        de encía sobre las coronas clínicas de los dientes, la gingivectomía o
        remoción quirúrgica del tejido anómalo será la solución. Ahora bien, si la
        condición obedece a una alteración del desarrollo facial, a un crecimiento
        vertical excesivo del maxilar superior <em>(EVM)</em>, solo la impacción
        quirúrgica del mismo mediante una intervención de Cirugía Ortognática
        Maxilofacial <em>(osteotomía de Le Fort I)</em> solventará el
        problema.</p>
    - title: >-
        <h4>¿En qué consiste una cirugía plástica periodontal o
        gingivoplastía?</h4>
      content: >-
        <p>Toda sonrisa está conformada por 3 elementos básicos: dientes, labios y
        encías. La gingivoplastía es un procedimiento que permite corregir la
        forma, tamaño y grosor de las encías, de modo que luzcan finas, delineadas
        y en perfecta armonía con los dientes. De igual forma, es también posible
        modificar casi por completo el aspecto, tamaño y grosor de los labios a
        través de una pequeña cirugía estética denominada Queiloplastía,
        habitualmente practicada por Médicos Especialistas en Cirugía Plástica
        Orofacial.</p>

cases:
  title: >
    <h1>Estética Dental - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-aesthetic-dentistry-es-01.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-02.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-03.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-04.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-05.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-06.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-07.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-08.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-09.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-10.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-11.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-12.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-13.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-14.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-15.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-16.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-17.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-18.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-19.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-20.jpg
      - /uploads/clinic-cases-aesthetic-dentistry-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Corrección de Fracturas y Malposición Dental </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reconstrucción de Incisivos Laterales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Restauraciones con Disilicato de Litio</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Rehabilitación Estética Total</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Cirugía Plástica Periodontal y Facetas Laminadas </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Carillas de Porcelana en IPS E.Max<sup>®</sup></h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Cierre de Espacios con Resinas Compuestas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Blanqueamiento y Carillas Cerámicas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Estructuras Cerámicas 100% Libres de Metal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Diseño de Sonrisa </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Blanqueamiento LED y Carilla de Resina</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reemplazo de Amalgama de Plata</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-aesthetic-dentistry-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">ESTOY MARAVILLADA CON MI NUEVA SONRISA, EL CAMBIO FUE DRÁSTICO Y ESPECTACULAR.
    MIS DIENTES ERAN MUY REDONDOS, PEQUEÑOS, AMARILLOS Y ALGO SEPARADOS. AHORA SON
    BELLOS COMO LOS DE LAS CELEBRITIES. SIN DUDA UN TRATAMIENTO QUE SUPERÓ CON CRECES
    TODAS MIS EXPECTATIVAS".</p>
  images:
    portrait: /uploads/quotes-aesthetic-dentistry-portrait.jpg
    landscape: /uploads/quotes-aesthetic-dentistry.jpg

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-aesthetic-dentistry-zoom.jpg
      content: >
        <h1 class="bebas">Para unos Dientes ¡Más Blancos y Hermosos!</h1>
        <p>Contamos con el mejor sistema LED <em>(Light
        Emitting Diode)</em> de luz fría para blanqueamiento dental. En solo una sesión
        de 45 minutos es capaz de aclarar el color de los dientes hasta en ocho tonos,
        sin ningún tipo de riesgo potencial y con unos resultados verdaderamente sorprendentes.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/blanqueamiento-dental/
          placeholder: <span> Más Información</span>
    - img: /uploads/sections-aesthetic-dentistry-dds.jpg
      content: >
        <h1 class="bebas">Diseño Digital de
        Sonrisa (DDS)</h1>
        <p>Para pacientes emocionales que dan gran
            valor a su apariencia, para aquellos que conocen bien los efectos de su sonrisa
            en los demás y que desean ir más allá del estricto y anticuado concepto de salud
            como simple ausencia de enfermedad.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-aesthetic-dentistry-veneers.jpg
      content: >
        <h1 class="bebas">Carillas de Porcelana</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
