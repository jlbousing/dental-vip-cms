---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/aesthetic-dentistry/porcelain-veneers/
title: Carillas de porcelana
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-porcelain-veneers.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 40%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: columnb
  content: >
    <h2>¿Cuándo están indicadas?</h2>
    <p class="subtitle">En presencia de anomalías estéticas
    que comprometan la apariencia de la persona.  A continuación presentamos diversas
    <br> condiciones clínicas susceptibles a este tipo de restauración dental:</p>
  blocks:
    - img: /uploads/b&a-1.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Diastemas o separaciones interdentales.</p>
    - img: /uploads/b&a-2.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Manchas o tinciones irreversibles.</p>
    - img: /uploads/b&a-3.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Malposición leve.</p>
    - img: /uploads/b&a-4.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Triángulos negros o troneras gingivales.</p>
    - img: /uploads/b&a-5.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Fracturas coronarias.</p>
    - img: /uploads/b&a-6.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Alteraciones de forma, color y tamaño.</p>
    - img: /uploads/b&a-7.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Restauraciones antiguas o defectuosas.</p

    - img: /uploads/b&a-8.jpg
      number: 1
      title: >
        <p class="dv-div-text list"><i class="icon-check circle"></i>Necesidad de blanqueamiento permanente.</p>

# Heading Section
heading:
  display: true
  content: <h1 class="bebas title">Carillas de Porcelana</h1>
    <p>Las carillas o facetas de porcelana
    son finas láminas de cerámica de entre 0.8 y 1.5 mm. de espesor que, adheridas
    a la superficie frontal de los dientes, son capaces de modificar por completo
    su forma, textura, color y tamaño.</p>

parallax:
  display: true
  portraitPosition: center
  mobilePosition: '{"mobilePosition320":"-759px","mobilePosition360":"-595px","mobilePosition375":"-742px"}'
  img: /uploads/parallax-porcelain-veneers.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¡10 Días nos serán Suficientes para Transformar Su Vida!</h1>
    <br>
    <br>
    <br>
    <h2 >Visítenos y en solo 3 o 4 sesiones de trabajo podremos diseñar y crear
    esa sonrisa que siempre ha soñado.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">¡Máxima adhesión y resistencia!</h2>
      <p class="dv-div-text
      dv-pb-15">Siempre confiamos nuestro trabajo a los agentes cementantes de la multinacional
      3M. <strong>RelyX™ Ultimate</strong> es un innovador cemento de resina adhesiva
      y polimerización dual que fue desarrollado pensando exclusivamente en las necesidades
      propias de la cerámica vítrea, y que en consecuencia, nos garantiza un excelente
      desempeño clínico.</p><p class="dv-div-text dv-pb-15">Gracias a este novedoso
      sistema, nuestros pacientes podrán estar siempre tranquilos, seguros y orgullosos
      de su nueva sonrisa.</p>
    image:
      display: true
      size: 40px
      src: /uploads/icon-relyx.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-porcelain-veneers.jpg
# Procedures Section
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Diseño digital de sonrisa </h5>
      to: "/especialidades/estetica-dental/diseno-digital-de-sonrisa/"
      img: "/uploads/procedures-design.jpg"
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: "/especialidades/implantes-dentales/diagnostico-y-planificacion-3d/"
      img: "/uploads/procedures-diagnostic.jpg"
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Tecnología cad-cam </h5>
      to: "/especialidades/protesis/tecnologia-cad-cam/"
      img: "/uploads/procedures-cad-cam.jpg"
    - title: >
        <h5> Prótesis sobre implantes </h5>
      to: "/especialidades/implantes-dentales/protesis-sobre-implantes/"
      img: "/uploads/procedures-dental-implants.jpg"
    - title: >
        <h5> Brackets Estéticos </h5>
      to: "/especialidades/ortodoncia/aparatos-esteticos/"
      img: "/uploads/procedures-brackets.jpg"
---

<div class="row container">
<div class="item">

![Hopper The Rabbit](/img/gallery-blocks-dds-cut.jpg)

## ¿Es necesario tallar los dientes?

En el 85% de los casos será indispensable hacer un pequeño desgaste.
De lo contrario, se generaría una restauración con sobrecontorno inaceptable,
o bien, con un espesor de cerámica insuficiente, débil, con alto riesgo de fractura
e incapaz de enmascarar el defecto estético de fondo.

</div>
<div class="item">

![Hopper The Rabbit](/img/gallery-blocks-dds-lasting.jpg)

## ¿Cuántos años durarán mis carillas?

Aunque la longevidad de la cerámica IPS e.max<sup>®</sup> va siempre
a depender de múltiples factores como higiene oral, cumplimiento de chequeos
periódicos y control de hábitos oclusales, entre otros; estimamos que entre
10 y 15 años será el tiempo de vida útil para la gran mayoría de nuestros diseños.

</div>
</div>
