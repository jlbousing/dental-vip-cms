---
templateKey: specialties-page
language: es
redirects: /en/specialties/periodontics/
title: Periodoncia
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-periodonthics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 43%
  content:
    position: center
    body: >
      <h1 class="bebas">Periodoncia</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-periodontics"></i></h1>
    <p class="thin"><em>La causa principal de la Periodontitis es la placa dental, un acúmulo de
    restos alimenticios y bacterias que se deposita sobre la superficie de dientes
    y encías cuando la persona carece de una adecuada técnica de higiene oral.</em></p>

# Description Section
article:
  content: >
    <p>Esta Especialidad Odontológica se encarga de la prevención, diagnóstico
    y tratamiento de todas las enfermedades infecciosas <i>(GINGIVITIS y PERIODONTITIS)</i>
    que atacan al aparato de fijación de los dientes: encías, hueso alveolar, cemento
    y ligamento periodontal; y que son las responsables hasta del 80% de las pérdidas
    dentarias en sujetos mayores de 35 años.  <strong>No basta con unos dientes sanos,
    la salud de las encías es tan o más importante que permanecer libre de caries.
    </strong></p><p><strong>Usualmente, el tratamiento consiste en la eliminación
    y control de los irritantes infecciosos locales como la placa bacteriana y cálculo
    dental </strong> a través de tartrectomías <i>(limpiezas)</i> profundas y la enseñanza
    de adecuadas técnicas de higiene oral. En los casos más avanzados suele entonces
    ser necesario implementar procedimientos quirúrgicos más complejos que involucran
    los curetajes y raspados radiculares, levantamiento de colgajos invasivos, injertos
    óseos, colocación de membranas y la llamada REGENERACIÓN TISULAR GUIADA.</p><p><strong>La
    Enfermedad Periodontal es casi siempre asintomática en sus fases iniciales, por
    eso es tan peligrosa y destructiva. </strong> Cuando ya aparecen los primeros
    signos como sangramiento al cepillado, separación de los dientes y movilidad dental;
    estamos generalmente en presencia de fases avanzadas de la misma.  <strong>Sin
    tratamiento especializado se pierde todo el hueso de sostén y entonces los dientes
    se aflojarán y caerán irremediablemente. </strong></p><p>Otro beneficio adicional
    de esta Especialidad es el que podemos obtener de la llamada CIRUGÍA PLÁSTICA
    PERIODONTAL, que abarca un conjunto de técnicas que permiten al Odontólogo  <strong>modificar
    el contorno, grosor y forma de las encías;  </strong>logrando efectos estéticos
    invalorables para los pacientes. Generalmente este tipo de cirugía es complementaria
    a tratamientos de ortodoncia, blanqueamiento y estética dental.</p>
  img: /uploads/aside-periodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      La evidencia científica actual vincula a las Enfermedades Periodontales (EP)
    con otras patologías sistémicas como diabetes, enfermedad pulmonar obstructiva
    crónica, neumonía, artritis reumatoide, insuficiencia renal, obesidad, síndrome
    metabólico, enfermedad cardiovascular isquémica y trastornos del embarazo.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Javier Martínez Téllez</strong>
    details: >
      <strong>
        Odontólogo General - Periodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-periodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-periodontics.jpg
  mobilePosition: '{"mobilePosition320":"-919px","mobilePosition360":"-952px","mobilePosition375":"-1241px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿Cómo es una encía sana?</h4>
      content: >-
        <p>En condiciones de normalidad, la encía es fina, delgada, resiliente,
        firmemente adherida al tejido óseo subyacente y dispuesta de tal forma que
        contornea fielmente a la superficie cervical de los dientes; conformando
        un festoneado bien definido. Debe cubrir la totalidad de la raíz, ser de
        color rosa coral y poseer una textura superficial puntillada muy similar a
        la cáscara de una naranja.</p>
    - title: <h4>¿Cómo se clasifican las enfermedades periodontales?</h4>
      content: >-
        <p>Aunque su clasificación científica es sumamente amplia y compleja,
        bastaría con saber que de todas las enfermedades periodontales la
        gingivitis es la más común, leve y de mejor pronóstico; ya que su
        evolución solo afecta a la encía marginal y sus daños son totalmente
        reversibles. Pero si no es tratada a tiempo, avanza y ataca al hueso
        alveolar, causando infección, reabsorción y pérdida del mismo
        <em>(Periodontitis)</em> con formación de bolsas o sacos periodontales,
        pus y recesión de la encía. Al perder su hueso de sostén, los dientes se
        van aflojando progresivamente hasta caerse. A pesar de que la
        Periodontitis puede ser localizada o generalizada, crónica, agresiva,
        necrotizante, asociada a enfermedades sistémicas, a fármacos, a lesiones
        endoperiodontales o a otras afecciones adquiridas; lo más importante es
        que el Especialista sepa reconocer a cuál de las variantes se enfrenta
        para poder instaurar un tratamiento periodontal adecuado, eficaz y que
        ofrezca las mayores posibilidades de éxito.</p>
    - title: <h4>¿Cuáles son sus causas?</h4>
      content: >-
        <p>Son en esencia de etiología bacteriana. Aunque la acumulación de placa
        bacteriana en los dientes es determinante para el inicio y progresión de
        la enfermedad periodontal, su gravedad y respuesta al tratamiento será el
        resultado de la interacción de gran cantidad de factores modificadores,
        contribuyentes y/o predisponentes. Así, que al final, podemos afirmar que
        es una patología de origen multifactorial.</p>
    - title: <h4>¿A qué edad suelen aparecer?</h4>
      content: >-
        <p>Con excepción de la gingivitis, las enfermedades periodontales rara vez
        se diagnostican antes de los 15 años de edad, aunque de hacerlo, es bajo
        formas muy severas y agresivas que amenazan seriamente a la dentición, e
        incluso; a la salud general del niño. Normalmente aparecen en adultos,
        iniciando sus primeras manifestaciones a temprana edad, en torno a los 25
        años. Cuanto más joven sea el individuo al momento de aparecer, más severa
        será probablemente su evolución y complejo su tratamiento. Es indudable
        que la incidencia de periodontopatías guarda una relación directamente
        proporcional con la edad de las personas.</p>
    - title: <h4>¿Qué otros factores pueden desencadenarlas?</h4>
      content: >-
        <p>El tabaco, el alcohol y el estrés. También algunos medicamentos y
        enfermedades sistémicas como diabetes, osteoporosis o estados de
        inmunosupresión severa. Las restauraciones defectuosas, sobrecontorneadas
        o mal ajustadas, los dientes torcidos o apiñados <em>(maloclusiones)</em>
        y la mala higiene oral son también factores que desencadenan y estimulan
        el potencial patógeno de las enfermedades periodontales.</p>
    - title: <h4>¿Existe alguna relación entre enfermedad periodontal y embarazo?</h4>
      content: >-
        <p>El embarazo causa muchos cambios hormonales que aumentan
        exponencialmente el riesgo de padecer enfermedad periodontal en la mujer,
        y esta a su vez, se asocia con preeclampsia, bajo peso del bebé al nacer y
        nacimiento prematuro. La enfermedad periodontal, al suponer un depósito de
        microorganismos y sus productos tóxicos, puede desencadenar una respuesta
        con riesgo sistémico. Por tanto, las mujeres expectantes deben siempre
        procurar el tratamiento inmediato para la enfermedad periodontal y así
        reducir el riesgo de complicaciones pre y postnatales.</p>
    - title: <h4>¿Cómo pueden prevenirse las periodontopatías?</h4>
      content: >-
        <p>Mantener las encías sanas a lo largo de la vida no debería ser tan
        difícil como parece. Solo hace falta constancia en la higiene y cuidado de
        nuestra boca, para evitar la formación de sarro y placa bacteriana.
        Además, son indispensables las visitas periódicas al Odontólogo para
        lograr una limpieza más profunda, sobre todo en aquellos lugares de
        difícil acceso tanto para los cepillos como para el hilo dental.</p>
    - title: <h4>¿Cómo puedo saber si mi encía está ya enferma?</h4>
      content: >-
        <p>Los signos más evidentes son el sangrado espontáneo o al cepillado, la
        aparición de pus con mal sabor u olor de boca, enrojecimiento o retracción
        de la encía, cambio en la posición de los dientes, sensibilidad, dolor y
        movilidad dental. El diagnóstico definitivo deberá ser siempre concretado
        por el Odontólogo luego de realizar un examen clínico minucioso y un
        estudio radiológico completo.</p>
    - title: "<h4>La enfermedad periodontal, ¿es curable?</h4>"
      content: >-
        <p>Depende de la variante. La gingivitis es totalmente reversible y
        curable. Sin embargo, las diferentes formas de periodontitis causan daños
        irreversibles en el aparato de inserción dental. Considerando a las
        variantes moderadas y avanzadas de la EP como entidades crónicas y
        controlables, pero con alto riesgo de recurrencia, es difícil hablar de
        curación total. Un paciente con periodontitis, aparte de aprender, dominar
        y aplicar a conciencia las diversas técnicas de higiene oral, debe
        mantener un control periódico de por vida con su Periodoncista, para
        estabilizar su condición en el tiempo y lograr lo que hoy en día conocemos
        como un “periodonto sano reducido”; un periodonto afectado hasta cierto
        punto pero con capacidad de regeneración y sin evidencia de enfermedad
        activa. Bajo esta condición, se podrá conservar la dentición natural por
        muchos años más.</p>
    - title: <h4>¿Se puede recuperar el hueso perdido?</h4>
      content: >-
        <p>En algunas ocasiones los defectos producidos en el hueso maxilar por la
        enfermedad periodontal reúnen unas características muy específicas que
        posibilitan su regeneración ósea. Los procedimientos de regeneración
        tisular guiada se aplican de diferentes maneras, bien en forma de material
        de relleno, de membranas reabsorbibles o con sustancias derivadas de las
        proteínas orgánicas que estimulan el crecimiento óseo. Estos
        procedimientos son complejos y requieren de una alta cualificación
        profesional, pero reiteramos, no son posibles más que en algunas ocasiones
        muy puntuales; generalmente en defectos verticales bien definidos.</p>
    - title: >-
        <h4>¿Es reversible la movilidad dental producida por la
        periodontitis?</h4>
      content: >-
        <p>La movilidad suele persistir a pesar del tratamiento periodontal
        porque, lamentablemente, el nivel general de hueso jamás se recupera. Los
        dientes que presenten movilidad al momento del diagnóstico podrían
        perderse a corto o mediano plazo a pesar del tratamiento, y es la razón
        que justifica la necesidad de realizar un diagnóstico y control precoz de
        la enfermedad.</p>
    - title: <h4>¿Son útiles los antibióticos en la terapia general?</h4>
      content: >-
        <p>Los antibióticos son sustancias capaces de eliminar o inactivar
        bacterias, y la enfermedad periodontal como infección, es susceptible a
        sus mecanismos de acción. No obstante, y debido a su carácter crónico, no
        es viable usarlos de forma continua en casos de periodontitis, pues su uso
        prolongado generaría resistencia bacteriana y efectos secundarios
        inaceptables sobre el organismo. En la actualidad, se recomienda utilizar
        antibióticos solo contra las variantes más agresivas, en fase aguda, por
        un lapso de tiempo bien definido y siempre con la ayuda de un cultivo y
        antibiograma que nos revele el fármaco más efectivo para cada situación
        particular.</p>
    - title: >-
        <h4>¿Cuál es la diferencia entre una tartrectomía y un raspado
        radicular?</h4>
      content: >-
        <p>La profilaxis, limpieza o tartrectomía son la misma cosa, son tres
        términos que se utilizan indistintamente para referirse a la eliminación
        profesional de la placa y sarro supragingival, aquel que es visible y que
        se forma sobre la superficie expuesta del diente, por encima de la encía.
        Sin embargo, en los pacientes con pérdida ósea debemos hacer una limpieza
        mucho más profunda que elimine las bolsas o sacos periodontales, el sarro
        que se encuentre en su interior y el cemento radicular infectado. A ese
        procedimiento se le conoce como raspado y alisado radicular. Otra variante
        es el curetaje periodontal, una técnica quirúrgica que va más allá y que
        no solo elimina el cemento infectado, sino también parte del tejido blando
        de la pared de la bolsa periodontal. Es una técnica que actualmente se
        emplea solo en situaciones muy particulares.</p>
    - title: <h4>¿Cómo es una bolsa periodontal o saco patológico?</h4>
      content: >-
        <p>En condiciones de salud, la encía se une al diente mediante una
        inserción epitelial formando un surco <em>(surco gingival)</em> de entre 1
        y 3 mm. de profundidad. Una bolsa es una lesión degenerativa del
        periodonto, un espacio anormal <em>(mayor a 4 mm.)</em> que se forma entre
        la encía y el diente, a nivel del surco gingival; como consecuencia de la
        acumulación constante de placa y bacterias por debajo de la encía. Por su
        profundidad, la presencia de sacos patológicos imposibilita por completo
        la remoción de la placa dental por parte del paciente y favorece la
        formación de sarro, la destrucción del hueso de soporte y el agravamiento
        de la enfermedad periodontal.</p>
    - title: <h4>¿En qué consiste y cuándo se indica una cirugía a colgajo?</h4>
      content: >-
        <p>Un colgajo periodontal es una porción de encía que se separa
        quirúrgicamente de los tejidos subyacentes para permitir el acceso,
        visibilidad y limpieza del hueso y superficies radiculares. Le podríamos
        considerar en esencia como un curetaje subgingival abierto. Es uno de los
        tratamientos más utilizados en la periodontitis avanzada y tiene por
        objetivos básicos remover el cálculo y la placa de zonas inaccesibles,
        eliminar o reducir las bolsas periodontales, procurar un contorno tisular
        más favorable e incrementar la cantidad de encía adherida. Además, en
        algunos pacientes con retracciones severas, raíces expuestas o lesiones
        mucogingivales; un colgajo nos permite desplazar la encía a diferentes
        posiciones para corregir lesiones por defecto.</p>
    - title: <h4>¿Cuándo se coloca un injerto óseo?</h4>
      content: >-
        <p>Las técnicas quirúrgicas disponibles en la actualidad solo permiten
        injertar hueso en algunos tipos de lesiones periodontales y, muchas veces,
        tan solo de forma parcial. Los únicos defectos en los que la literatura
        científica ha demostrado la efectividad de los injertos son las lesiones
        de tipo vertical <em>(defectos circunscritos de 1, 2 o 3 paredes)</em> y
        las de furca en molares inferiores. Lamentablemente, los procedimientos
        regenerativos fracasan cuando se intentan tratar lesiones de tipo
        horizontal <em>(las más comunes en la enfermedad periodontal)</em> o
        cualquier defecto de furca en molares superiores. Por el contrario, los
        injertos óseos son sumamente efectivos para aumentar la cantidad y calidad
        del hueso en el lecho receptor como procedimiento previo a la colocación
        de implantes dentales.</p>
    - title: <h4>¿Cómo se tratan las recesiones o retracciones gingivales?</h4>
      content: >-
        <p>Lo primero, antes de realizar cualquier tipo de intervención
        quirúrgica, es eliminar la causa que ha provocado la retracción de la
        encía: el cepillado agresivo, consumo excesivo de sustancias ácidas,
        trauma oclusal, hábitos parafuncionales y/o cúmulo permanente de
        irritantes locales <em>(placa bacteriana y cálculo dental)</em>. Luego, se
        da continuidad al tratamiento con cirugías mucogingivales como los
        colgajos de avance coronal para “estirar” la encía y cubrir la raíz
        expuesta y/o con injertos conectivos sub-epiteliales tomados de la mucosa
        palatina.</p>
    - title: <h4>¿En qué consiste la fase de soporte o mantenimiento periodontal?</h4>
      content: >-
        <p>Es aquella fase de la terapia periodontal durante la cual la enfermedad
        es monitoreada para controlar sus factores etiológicos y de riesgo. Es
        considerada desde el punto de vista clínico, una terapia activa, que debe
        tener la misma atención e importancia profesional que la terapia inicial o
        de ataque. Su principal objetivo es el de prevenir la progresión y
        recurrencia de la enfermedad periodontal en pacientes que han sido
        tratados previamente. Usualmente requiere de visitas periódicas, cada 3
        meses al principio, para descartar la presencia de irritantes locales,
        signos clínicos de inflamación y constatar el pleno dominio y ejecución de
        las diferentes técnicas de higiene oral.</p>
    - title: <h4>¿Qué hacer si finalmente pierdo algunos dientes?</h4>
      content: >-
        <p>Si Usted acudió tarde al tratamiento o si su periodontitis no se pudo
        controlar adecuadamente, es posible que pierda o haya perdido dientes como
        consecuencia de la misma. En tal caso, es conveniente que los reponga de
        inmediato para evitar sobrecargas funcionales de los que aún permanecen en
        boca, hecho que aceleraría considerablemente la evolución general de la
        enfermedad. La mejor manera de reponer sus dientes es mediante prótesis
        fija, ya que las prótesis removibles habitualmente perjudican a la encía y
        a los dientes naturales remanentes, por lo que se deben colocar solo si no
        existe la posibilidad de confeccionar una fija.</p>
    - title: "<h4>¿Si padezco periodontitis, puedo colocarme implantes dentales?</h4>"
      content: >-
        <p>¡Por supuesto que sí!, y es de hecho la alternativa ideal, pero nunca
        en casos de enfermedad periodontal activa. Primero es necesario tratarla y
        controlarla para que los implantes se comporten igual y tengan las mismas
        probabilidades de éxito que en un paciente sano. Si tomamos en
        consideración, que desde el punto de vista periodontal, los implantes
        dentales están sometidos a los mismos riesgos de infección que los dientes
        naturales; entenderemos por qué es fundamental, para evitar que los
        implantes corran la misma suerte, que una persona que haya perdido sus
        dientes por carencias de higiene oral se entrene y capacite a conciencia
        en las técnicas de cepillado y uso del hilo dental.</p>

cases:
  title: >
    <h1>Periodoncia - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-periodontics-es-01.jpg
      - /uploads/clinic-cases-periodontics-es-02.jpg
      - /uploads/clinic-cases-periodontics-es-03.jpg
      - /uploads/clinic-cases-periodontics-es-04.jpg
      - /uploads/clinic-cases-periodontics-es-05.jpg
      - /uploads/clinic-cases-periodontics-es-06.jpg
      - /uploads/clinic-cases-periodontics-es-07.jpg
      - /uploads/clinic-cases-periodontics-es-08.jpg
      - /uploads/clinic-cases-periodontics-es-09.jpg
      - /uploads/clinic-cases-periodontics-es-10.jpg
      - /uploads/clinic-cases-periodontics-es-11.jpg
      - /uploads/clinic-cases-periodontics-es-12.jpg
      - /uploads/clinic-cases-periodontics-es-13.jpg
      - /uploads/clinic-cases-periodontics-es-14.jpg
      - /uploads/clinic-cases-periodontics-es-15.jpg
      - /uploads/clinic-cases-periodontics-es-16.jpg
      - /uploads/clinic-cases-periodontics-es-17.jpg
      - /uploads/clinic-cases-periodontics-es-18.jpg
      - /uploads/clinic-cases-periodontics-es-19.jpg
      - /uploads/clinic-cases-periodontics-es-20.jpg
      - /uploads/clinic-cases-periodontics-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Injerto Gingival</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Periodontitis Crónica Generalizada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Ferulización Superior</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>1RA Fase del Tratamiento Periodontal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Retracciones Gingivales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Erupción Pasiva Alterada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Cirugía Plástica Periodontal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Injerto y Corona Totalcerámica</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Regeneración Ósea Pre-Implantaria</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Profilaxis y Aclaramiento Dental </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Restauraciones con Enfermedad Periodontal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Colgajo de Reposicionamiento Coronal </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tratamiento Coadyuvante</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Hiperplasia Gingival por Medicamentos</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gingivectomía</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Diastemas por Enfermedad Periodontal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tartrectomía Simple </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Curetaje Subgingival</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pigmentaciones Melánicas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Cirugía Mucogingival</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-periodontics-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reducción de Sacos Patológicos </h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">HABÍA IDO A VARIOS ODONTÓLOGOS Y JAMÁS ME DIJERON QUE SUFRÍA DE LAS ENCÍAS.
    COMO SABÍA QUE ALGO NO ANDABA BIEN, VISITÉ DENTAL VIP EN BUSCA DE UNA NUEVA OPINIÓN.
    ME HICIERON UN ESTUDIO RADIOGRÁFICO COMPLETO Y ME ENCONTRARON PERIODONTITIS MODERADA.
    ¡AFORTUNADAMENTE YA ESTOY EN TRATAMIENTO!".</p>
  images:
    portrait: /uploads/quotes-periodontics-portrait.jpg
    landscape: /uploads/quotes-periodontics.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
