---
templateKey: specialties-page
language: es
redirects: /en/specialties/general-dentistry/
title: Odontología General
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-general-dentistry.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 56%
  content:
    position: center
    body: >
      <h1 class="bebas">Odontología General</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-general-dentistry"></i></h1>
    <p class="thin"><em>El Mejor Tratamiento de Cualquier Enfermedad es su Prevención y nuestro
    principal objetivo debe ser el de interponer barreras biológicas que verdaderamente
    interfieran con su desarrollo.</em></p>

# Description Section
article:
  content: >
    <p><strong>El Odontólogo General se encuentra capacitado para prevenir, diagnosticar
    y tratar los problemas comunes del público en general. </strong> Si Usted requiere
    de un CHEQUEO DE RUTINA o de Odontología Básica como eliminación de caries, limpieza,
    profilaxis o prótesis bucal sin compromiso estético, funcional o periodontal;
    un profesional de práctica general podrá prestarle un excelente servicio. Sin
    embargo, debido a que las diferentes áreas de la Odontología se han hecho cada
    vez más complejas y altamente tecnificadas,  <strong>existen numerosos casos que
    requieren de la intervención de profesionales con largos e intensivos entrenamientos
    llamados Especialistas.  </strong>Estos individuos se encuentran particularmente
    capacitados para ejecutar técnicas y procedimientos avanzados brindando resultados
    altamente predecibles y muchas veces sorprendentes para los pacientes.</p><p><strong>Un
    buen Odontólogo General se encarga de realizar el triaje y diagnóstico primario
    del paciente, </strong> resolver los problemas de higiene y RESTAURACIÓN DE CARIES
    que pueda presentar y referirle, de ser necesario, al Especialista más indicado.
    Es importante aclarar que  <strong>un verdadero Especialista debe haber cursado
    estudios de cuarto nivel o Postgrado </strong> en alguna Universidad nacional
    o extranjera reconocida, con un pensum académico que abarque las horas lectivas
    exigidas por las diferentes Sociedades Científicas del país.</p><p><strong>El
    Generalista debe poseer sólidos conocimientos de patología oral, radiología, periodoncia
    y gnatología</strong> para poder detectar la presencia de lesiones o enfermedades
    en lengua, encías, tejidos blandos y articulaciones temporomandibulares. Debe
    además ser un verdadero experto en ODONTOLOGÍA OPERATORIA, ya que usualmente <strong>es
    el que elabora las preparaciones cavitarias necesarias para el tratamiento de
    la Caries Dental,</strong> aplicando diseños conservadores y tratando de preservar
    en todo momento la vitalidad pulpar, para finalmente; devolver al órgano su anatomía
    y funcionalidad habitual mediante el proceso de restauración dental. Debe por
    tanto conocer y manejar con propiedad los diferentes sistemas adhesivos basados
    en la técnica de grabado ácido, los múltiples materiales restauradores a base
    de composite o resina compuesta y los principios más básicos y universales de
    oclusión y función masticatoria.</p>
  img: /uploads/aside-general-dentistry.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Desafortunadamente, la Caries Dental representa un serio problema de salud
      pública en nuestro medio, por cuanto más del 98% de la población Venezolana la
      ha padecido, padece o padecerá en algún momento de su vida.
      </em>
    </p>
  footer:
    author: >
      <strong>Dra. Vianka Xaviera Torres</strong>
    details: >
      <strong>
        Odontólogo General - Endodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-general-dentistry.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-general-dentistry.jpg
  mobilePosition: '{"mobilePosition320":"-700px","mobilePosition360":"-800px","mobilePosition375":"-1095px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: >-
        <h4>¿Cuáles son las enfermedades más comunes que afectan a dientes y
        encías?</h4>
      content: >-
        <p>Las patologías bucodentales más frecuentes son las caries y las
        afecciones periodontales <em>(gingivitis y periodontitis)</em>. Luego, en
        menor grado, las enfermedades infecciosas de origen bacteriano, los
        traumatismos físicos, las lesiones congénitas y el cáncer de boca. Según
        la Organización Mundial de la Salud entre el 60% y el 90% de los niños en
        edad escolar y cerca del 99% de los adultos padecen de caries dental, a
        menudo acompañada de dolor o sensación de molestia.</p>
    - title: <h4>¿Es posible evitarlas?</h4>
      content: >-
        <p>Aunque el componente genético es determinante, la incidencia de
        enfermedades bucodentales puede reducirse de forma considerable
        controlando los factores de riesgo ya conocidos. El control de la placa
        dental mediante la práctica constante de una correcta técnica de higiene
        oral, la reducción de la ingesta de azúcares, la alimentación bien
        equilibrada rica en frutas, verduras y fibra, la reducción del consumo de
        alcohol, el cese del hábito tabáquico, los controles profesionales
        periódicos y las aplicaciones tópicas de flúor impactarán favorablemente
        en la aparición y prevalencia de caries, periodontopatías y neoplasias
        bucales.</p>
    - title: <h4>¿Qué es la placa bacteriana?</h4>
      content: >-
        <p>La placa dental o bacteriana es una sustancia pegajosa
        blanco-amarillenta que se adhiere con facilidad a los dientes y que
        acumula muchas especies de microbios <em>(aerobios y anaerobios)</em>
        dentro de una matriz intercelular de origen orgánico. La placa se forma a
        partir de los restos alimenticios que no hayan sido removidos y que sirven
        de sustrato perfecto para la colonización y multiplicación de bacterias.
        La importancia de su control radica en el hecho de que es la principal
        causante de las dos infecciones más habituales de la cavidad bucal: la
        Caries Dental y la Enfermedad Periodontal.</p>
    - title: <h4>¿En qué se diferencia del cálculo o sarro dental?</h4>
      content: >-
        <p>El cálculo dental, también conocido como piedra, sarro o tártaro
        dental, es el resultado de la solidificación de la placa bacteriana por la
        precipitación y acumulación progresiva de sales minerales; calcio y
        fósforo principalmente. Por ser una materia mineralizada, rugosa y
        firmemente adherida a los dientes, aloja y retiene enormes cantidades de
        placa bacteriana en su superficie, y a diferencia de ella; no puede ser
        removida por el cepillo dental. Solo una limpieza profesional podrá
        eliminarlo.</p>
    - title: <h4>¿Es normal que sangren las encías al cepillarse los dientes?</h4>
      content: >-
        <p>¡En lo absoluto! Aunque tal fenómeno podría deberse a una condición de
        orden sistémico, la causa habitual obedece a la acumulación crónica de
        placa dental que genera una entidad patológica inflamatoria reversible
        conocida como Gingivitis, pero que agravada, conduce a la formación de
        sarro, sacos patológicos, resorción del hueso alveolar y caída de los
        dientes <em>(Periodontitis)</em>. La hemorragia de origen gingival es el
        signo más evidente de la llamada Enfermedad Periodontal.</p>
    - title: <h4>¿Por qué se produce el mal aliento?</h4>
      content: >-
        <p>La halitosis, también conocida como mal aliento, se define como el
        conjunto de olores desagradables que se emiten por la boca. Es un problema
        que afecta a una de cada tres personas y está asociado con una higiene
        bucal deficiente o con enfermedades de la cavidad oral, aunque en
        ocasiones, puede ser manifestación clínica de alguna otra enfermedad de
        corte sistémico o gastrointestinal.</p>
    - title: <h4>¿Cuál es la mejor técnica de higiene oral?</h4>
      content: >-
        <p>Aquella que se practica religiosamente después de cada comida y que
        contempla el uso del cepillo convencional, cepillo interproximal, hilo
        dental y enjuague bucal. Un cepillado óptimo toma al menos tres minutos de
        tiempo y para realizarlo adecuadamente debemos colocar las cerdas sobre la
        superficie dental y su encía contigua con una angulación de 45 grados,
        para luego, aplicar movimientos horizontales cortos y suaves prestando
        especial atención a la interfase diente-encía, a los dientes posteriores
        de difícil acceso y a las zonas donde haya obturaciones, coronas, aparatos
        de ortodoncia o implantes dentales. Jamás olvide cepillar su lengua.</p>
    - title: <h4>¿Son los cepillos eléctricos mejores que los cepillos manuales?</h4>
      content: >-
        <p>Definitivamente ambos son efectivos, con ambos tipos se puede alcanzar
        un cepillado dental óptimo y eficaz. Puede utilizar el que más le acomode,
        siempre y cuando la técnica sea adecuada. Los cepillos eléctricos pueden
        funcionar mejor que los manuales en personas con destreza limitada,
        artritis, enfermedades degenerativas, mentales o que comprometan la
        capacidad motriz.</p>
    - title: <h4>¿Cada cuánto tiempo se debe visitar al Odontólogo?</h4>
      content: >-
        <p>Normalmente se recomienda una vez al año. Afortunadamente, el proceso
        de desarrollo de caries y sarro es relativamente lento si se practica una
        higiene oral razonable, por lo que un control cada 12 meses será adecuado
        para prevenir enfermedades, detectar y tratar lesiones incipientes y
        mantener sanos boca, dientes y encías. Sin embargo, para algunas personas
        con patologías preexistentes, rehabilitaciones orales extensas, implantes
        dentales, tratamientos de ortodoncia o malos hábitos; es posible que sea
        necesaria una frecuencia mayor, que puede oscilar entre 2 y 4 veces al
        año. Un caso típico es el de aquellos pacientes que sufren de gingivitis o
        periodontitis crónica, y que suelen requerir de una terapia permanente de
        soporte periodontal.</p>
    - title: >-
        <h4>¿Por qué son necesarias varias radiografías para hacer un buen
        Diagnóstico Oral?</h4>
      content: >-
        <p>Porque exponen detalles que no son accesibles al ojo humano. Solo a
        través de las imágenes radiológicas podremos detectar las muy frecuentes
        caries proximales <em>(las que se forman en las superficies de contacto de
        los dientes)</em>, las alteraciones y resorciones del hueso alveolar e
        interproximal, las enfermedades periodontales, las afecciones pulpares y
        periapicales, las reabsorciones radiculares, los dientes incluidos, los
        tumores y quistes odontogénicos y las restauraciones defectuosas o
        permeables; entre otras anomalías. Un Diagnóstico sin el soporte de la
        evidencia radiográfica dejará siempre mucho que desear.</p>
    - title: <h4>¿Qué es una tartrectomía o profilaxis?</h4>
      content: >-
        <p>Es lo que se conoce popularmente como “limpieza dental”. La
        tartrectomía es un procedimiento operatorio que consiste en la eliminación
        mecánica de todo el cálculo y placa dental acumulada en los dientes,
        margen gingival y espacios interdentales, mediante el uso de aparatos
        ultrasónicos e instrumentos odontológicos especiales. Debe practicarse
        periódicamente, cada 6 o 12 meses, ya que existen zonas en la boca a las
        que ni siquiera un correcto cepillado es capaz de llegar.</p>
    - title: <h4>¿En qué consiste un pulido?</h4>
      content: >-
        <p>Tanto los dientes naturales como los artificiales sufren desgaste, y
        con el tiempo, adquieren ciertas rugosidades en sus capas externas que es
        recomendable eliminar. Existen diversas técnicas para ello que aportan no
        solo un beneficio estético, sino también higiénico y funcional, ya que una
        superficie lisa y pulida retiene menor cantidad de placa dental y es
        entonces más fácil de abordar y limpiar.</p>
    - title: <h4>¿Cuáles son los beneficios de una aplicación tópica de flúor?</h4>
      content: >-
        <p>Este elemento químico otorga tres beneficios principales: aumenta la
        resistencia del esmalte, es antibacteriano y promueve la remineralización.
        Los fluoruros inhiben directamente la formación de ácidos bacterianos y
        contribuyen a la incorporación de iones de calcio y fosfato en el esmalte,
        disminuyendo su susceptibilidad a la caries dental. Normalmente se aplican
        en forma de gel con el uso de cubetas especiales y directamente sobre la
        superficie dental.</p>
    - title: <h4>¿Cómo se forman las caries?</h4>
      content: >-
        <p>Si no son removidas periódicamente, las bacterias contenidas en la
        placa dental generan potentes ácidos orgánicos que atacan y desmineralizan
        los tejidos duros o inorgánicos de los dientes susceptibles. Así, en la
        superficie del esmalte, podrán formarse surcos y grietas que propiciarán
        la entrada de nuevas bacterias y la proliferación de mayor cantidad de
        placa bacteriana. Este proceso de desmineralización puede ser reversible
        en las primeras fases, sin embargo, el ataque continuado de los ácidos
        conducirá a una mayor destrucción del esmalte dental y a la creación de
        cavidades en la superficie del diente que podrían llegar a la dentina, e
        incluso, a la pulpa dental; comprometiendo entonces su vitalidad.</p>
    - title: <h4>¿Cómo se curan o eliminan?</h4>
      content: >-
        <p>Depende del desarrollo, extensión y profundidad de las mismas. Si es
        incipiente o moderada se elimina todo el tejido infectado y se obtura la
        cavidad con un material especial, generalmente a base de Composite o
        Resina Fotocurada. En estados más avanzados la caries suele afectar a la
        pulpa dental o nervio del diente y es entonces necesario realizar un
        Tratamiento de Conducto. Además, después de la Endodoncia, suele indicarse
        una Incrustación o Corona Cerámica para proteger la estructura dental
        remanente debilitada y evitar su fractura.</p>
    - title: <h4>¿Todavía se utiliza la amalgama dental?</h4>
      content: >-
        <p>Aunque la aparición de nuevos materiales adhesivos para obturación
        directa ha sido muy beneficiosa desde el punto de vista estético, de
        ninguna manera afectan la pertinencia e indicación de otros materiales
        tradicionales todavía más resistentes, más duraderos y más económicos. Es
        el caso de la amalgama de plata, que indudablemente sigue siendo la mejor
        opción en situaciones donde los dientes restaurados, por el tipo de
        mordida, queden expuestos a fuerzas o sobrecargas considerables; tal y
        como ocurre con muchos dientes posteriores, a nivel de molares.</p>
    - title: <h4>¿Qué son y cómo se colocan las Resinas o Composites?</h4>
      content: >-
        <p>Son materiales sintéticos mezclados heterogéneamente para formar un
        solo compuesto de elementos variados. Se utilizan en Odontología para
        obturar dientes porque son muy estéticos, y además, porque se adhieren
        micromecánicamente a su superficie mediante la técnica de grabado ácido.
        Las Resinas Compuestas constan de un componente orgánico polimérico
        llamado matriz y un componente inorgánico que actúa como mineral de
        relleno. Generalmente son fotosensibles y se utiliza luz halógena para su
        colocación.</p>
    - title: <h4>¿En qué momento es necesario reemplazar una obturación antigua?</h4>
      content: >-
        <p>La respuesta es muy sencilla: cuando exista sensibilidad dental, cuando
        el material esté deteriorado, roto o fracturado, cuando se observe
        filtración marginal y/o caries de recidiva o cuando tras un estudio
        radiográfico se detecte un área radiolúcida coronal que sugiera una falla
        de tipo cohesivo.</p>
    - title: <h4>¿Qué es el Bruxismo?</h4>
      content: >-
        <p>El Bruxismo es el hábito involuntario de apretar o rechinar los dientes
        sin propósitos funcionales. Afecta a entre un 10% y un 20% de la
        población, pudiendo generar dolor de cabeza, dolor de los músculos
        masticatorios, cuello y oído. El rechinamiento, si no es tratado a tiempo,
        puede llegar a desgastar o fracturar los dientes por completo,
        comprometiendo la estética y la función oclusal de la persona.</p>
    - title: <h4>¿Cuándo se indica una férula nocturna?</h4>
      content: >-
        <p>La férula dental o férula de descarga, estabilización o desprogramación
        es un dispositivo de resina acrílica transparente, duro, hecho a medida y
        que se coloca en la arcada superior o inferior <em>(según las
        características del caso)</em> del paciente, y que sirve para el
        tratamiento paliativo del Bruxismo y para reducir la hiperactividad
        muscular asociada con los trastornos oclusales, funcionales e
        inflamatorios de la articulación temporomandibular <em>(ATM)</em>.
        También, lo indicamos de rutina como aparato protector en pacientes con
        rehabilitaciones fijas extensas o implantoasistidas.</p>

cases:
  title: >
    <h1>Odontología General - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-general-dentistry-es-01.jpg
      - /uploads/clinic-cases-general-dentistry-es-02.jpg
      - /uploads/clinic-cases-general-dentistry-es-03.jpg
      - /uploads/clinic-cases-general-dentistry-es-04.jpg
      - /uploads/clinic-cases-general-dentistry-es-05.jpg
      - /uploads/clinic-cases-general-dentistry-es-06.jpg
      - /uploads/clinic-cases-general-dentistry-es-07.jpg
      - /uploads/clinic-cases-general-dentistry-es-08.jpg
      - /uploads/clinic-cases-general-dentistry-es-09.jpg
      - /uploads/clinic-cases-general-dentistry-es-10.jpg
      - /uploads/clinic-cases-general-dentistry-es-11.jpg
      - /uploads/clinic-cases-general-dentistry-es-12.jpg
      - /uploads/clinic-cases-general-dentistry-es-13.jpg
      - /uploads/clinic-cases-general-dentistry-es-14.jpg
      - /uploads/clinic-cases-general-dentistry-es-15.jpg
      - /uploads/clinic-cases-general-dentistry-es-16.jpg
      - /uploads/clinic-cases-general-dentistry-es-17.jpg
      - /uploads/clinic-cases-general-dentistry-es-18.jpg
      - /uploads/clinic-cases-general-dentistry-es-19.jpg
      - /uploads/clinic-cases-general-dentistry-es-20.jpg
      - /uploads/clinic-cases-general-dentistry-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Abrasiones Cervicales</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>fractura de amalgama</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Restauración de Caries Interproximales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Ferulización por traumatismo Dental</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tratamiento de la Caries Dental </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Cavidades Clase III y Resinas Fotocuradas</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Fija Provisional</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Dentadura Parcial Removible (DPR) Superior </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Fractura del Borde Incisal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tratamiento de Lesión en Lengua </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reemplazo de Prótesis Dentomucosoportada</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Incrustación de Porcelana</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Tartrectomía o Limpieza Dental</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reconstrucción con Composite de Microrelleno </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Corona Totalcerámica</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implante Dental y Corona de Zirconio</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Profilaxis y Pulido Dental</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Removible sin Ganchos Visibles</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>DPR de Acrílico Termopolimerizable</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pernos de Fibra y Coronas libres de Metal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-general-dentistry-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Coronas de Zirconio en Incisivos Superiores </h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">EXCELENTES PROFESIONALES, MUY DETALLISTAS A LA HORA DE DIAGNOSTICAR Y CON
    MUY BUENA DISPOSICIÓN PARA EXPONER AL PACIENTE LOS PROBLEMAS DENTALES DETECTADOS
    Y LOS PROCEDIMIENTOS PERTINENTES PARA SU INMEDIATA CORRECCIÓN. FORMAN UN EQUIPO
    DE PRIMER NIVEL".</p>
  images:
    portrait: /uploads/quotes-general-dentistry-portrait.jpg
    landscape: /uploads/quotes-general-dentistry.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
