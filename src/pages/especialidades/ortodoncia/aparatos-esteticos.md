---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/orthodontics/aesthetic-braces/
title: Aparatos Estéticos
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-aesthetic-braces.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 46%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: top
  type: List
  content: >
    ""
  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >
        <span><i class="icon-instagram"></i></span>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">Aparatos Estéticos</h1>
    <p>Las nuevas tecnologías en Ortodoncia
    han permitido, cuando la apariencia es importante, el uso de materiales más cómodos
    y discretos que el acero, y que ayudan al paciente a sonreír sin problemas mientras
    avanza su tratamiento.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-aesthetic-braces.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>En DENTAL VIP Somos Verdaderos Expertos en Ortodoncia Estética</h1>
    <br>
    <h2 >20 años de trayectoria y más de 2.000 casos tratados con éxito nos
    avalan.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: reverse
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">¡Porque no queremos interferir con su estilo de vida!</h2>
      <p class="dv-div-text
      dv-pb-15">La gran mayoría de los pacientes desea una opción terapéutica que enderece
      sus dientes con rapidez y eficacia, que ofrezca un aspecto pulcro y hermoso,
      y que además; responda ante sus necesidades particulares.
      </p> <p class="dv-div-text text-right">
      Una de cada cinco personas que solicitan Ortodoncia son adultas y requieren
      de una solución efectiva y verdaderamente estética. Los aparatos cerámicos y
      cristalinos de reconocida calidad son apropiados para casi todos los tipos
      de maloclusión conocida, trabajan de forma continua durante todo el
      tratamiento y jamás limitan la actividad laboral, la vida social o la
      práctica deportiva.</p>
    image:
      display: true
      size: 40px
      src: /uploads/icon-brackets-usa.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-aesthetic-braces.jpg

customBlocks:
  display: true
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2>Brackets Cerámicos</h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>Son los brackets estéticos más utilizados y económicos
          del mercado. Los Aparatos de Porcelana son reproducciones exactas de los brackets
          metálicos tradicionales, pero de un color similar al de los dientes.</p>
    - header:
        display: true
        content: >
          <h2>Brackets de Zafiro </h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-shappire-brackets.jpg
      footer:
        display: true
        content: >
          <p  class="top-title">Estos brackets son prácticamente invisibles, ya que
          son fabricados con el Cristal de Zafiro, un elemento transparente y brillante
          que combinado con otros minerales se hace muy resistente.</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Diagnóstico y Planificación 3D </5>
      to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Diseño digital de sonrisa </5>
      to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> Tecnología cad-cam </5>
      to: /especialidades/protesis/tecnologia-cad-cam/
      img: /uploads/procedures-cad-cam.jpg
    - title: >
        <h5> Carillas de porcelana </5>
      to: /especialidades/estetica-dental/carillas-de-porcelana/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5> Prótesis sobre implantes </5>
      to: /especialidades/implantes-dentales/protesis-sobre-implantes/
      img: /uploads/procedures-dental-implants.jpg
---

<div class="container">
<div class="row">
<div class="item np left">

## ¡Porque cuidamos la imagen de nuestros pacientes!

Entendemos perfectamente que muchos adolescentes, profesionales, modelos,
artistas y personas adultas se nieguen a exhibir aparatos metálicos en su dentadura.
Por ello, siempre ofrecemos las alternativas que mejor se ajusten a sus necesidades.

</div>

<div class="item np image">

![Hopper The Rabbit](/img/info-block-advertisement.png)

</div>
<div class="item np right">

## ¡Porque queremos satisfacer todas sus expectativas!

Sonrisas hermosas, radiantes y naturales a cualquier edad. En los últimos
años, el uso de la aparatología estética ha incrementado considerablemente los
índices de aceptación y satisfacción de todos nuestros tratamientos de Ortodoncia.

</div>
</div>
</div>
