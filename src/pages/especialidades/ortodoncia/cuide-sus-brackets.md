---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/orthodontics/take-care-of-your-brackets/
title: ¡Cuide sus brackets!
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-brackets-care.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 55%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: false
  position: top
  type: List
  content: >
    ""
  blocks:
    - img: /uploads/implant-types-individual-crown.jpg
      number: 1
      title: >
        <span><i class="icon-instagram"></i></span>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">¡Cuide sus brackets!</h1>
    <p>Si Usted ha iniciado un Tratamiento
    de Ortodoncia, es necesario que conozca la siguiente información y cumpla con
    estas sencillas indicaciones:</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-brackets-care.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¿Cuál Cepillo Dental es el Más Adecuado?</h1>
    <br>
    <h2 >Uno de filamentos suaves y perfil en forma de V que genere un triple
    efecto de limpieza: aparatos, dientes y encías.</h2>
# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>

articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Irrigadores bucales</h2>
      <p class="dv-div-text
      dv-pb-15">La complejidad de la eliminación del biofilm oral <em>(placa  bacteriana)</em>
      en zonas altamente retentivas como las que se forman con los aparatos de Ortodoncia
      puede generar con facilidad inflamación gingival, caries dental y halitosis. Gran
      cantidad de ensayos clínicos han demostrado que, bajo esas condiciones, los irrigadores
      bucales Waterpik<sup>®</sup> eliminan el 99,9% de la placa depositada sobre dientes,
      encías y lugares de difícil acceso; gracias a la aplicación directa de un chorro
      pulsátil de agua o colutorio común.

      </p> <p class="dv-div-text text-right">
        A nuestro
      entender, la Técnica de Irrigación estará siempre indicada como recurso coadyuvante
      en la higiene de pacientes portadores de Ortodoncia, Prótesis Fija e Implantes
      Dentales.</p>
    image:
      display: true
      size: 70px
      src: /uploads/icon-waterpik.jpg

    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-brackets-care.png

customBlocks:
  display: true
  type: column
  blocks:
    - header:
        display: true
        content: >
          <span class="icon">1</span>
          <p>Los primeros 3 o
            4 días posteriores a la instalación de los brackets son algo incómodos porque
            es normal experimentar alguna sensación de ardor o picor en las encías y
            cierto dolor o molestia a la hora de morder y masticar. También, es posible
            que aparezcan úlceras, aftas o llaguitas dentro de la boca <em>(recuerde que
            los aparatos son un cuerpo extraño para su organismo)</em>, principalmente en
            la lengua, labios y carrillos. En caso de ser necesario, puede tomar algún analgésico
            como acetaminofén o paracetamol y utilizar la cera para Ortodoncia que le hemos
            proporcionado, colocando un pequeño trozo de ella sobre el bracket que esté
            molestando.</p>
      body:
        display: true
        images:
          - src: /uploads/brackets-care-steps-1.jpg
          - src: /uploads/brackets-care-steps-2.jpg
      footer:
        display: true
        content: >
          <span class="icon">2</span>
          <p>Usted podrá comer
          prácticamente de todo, pero es fundamental evitar los alimentos y objetos duros
          que pudiesen despegar los aparatos o doblar los alambres, situación que provocaría
          retrasos importantes en el tratamiento. Por ello, no hay que tocar los brackets
          con los dedos, lápices o bolígrafos, ni morderse las uñas o usar palillos de
          madera. Todos estos hábitos podrían llegar a dañar los aparatos de Ortodoncia.
          Evite además morder directamente los alimentos, siempre córtelos o trocéelos
          primero con las manos o cubiertos antes de introducirlos en la boca. Evite también
          masticar chicle o alimentos pegajosos y morder cosas excesivamente duras como
          hueso, hielo, semillas y objetos similares. ¡SIMPLEMENTE ES CUESTIÓN DE SENTIDO
          COMÚN!</p>

# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Brackets Estéticos </h5>
      to: /especialidades/ortodoncia/aparatos-esteticos/
      img: /uploads/procedures-brackets.jpg
    - title: >
        <h5> Diseño digital de sonrisa </h5>
      to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> Carillas de porcelana </h5>
      to: /especialidades/estetica-dental/carillas-de-porcelana/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Prótesis sobre implantes </h5>
      to: /especialidades/implantes-dentales/protesis-sobre-implantes/
      img: /uploads/procedures-dental-implants.jpg
---

<div class="container">
<div class="row alt">
<div class="item np left par">

<span class="icon-number" style="
    font-size: 50px;
">3</span>

Durante el tratamiento
es importante practicar una higiene bucal muy cuidadosa. Los aparatos hacen
que se retengan más placa dental y restos de alimentos en los dientes, y es
entonces necesario dedicar algo más de tiempo a su limpieza. Si no se cepillan
bien los dientes y encías, estas se inflamarán, sangrarán y saldrá mal olor
de su boca.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-3.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-brush"></i></span>

Hay que cepillarse inmediatamente
después de cada comida <em>(normalmente, al igual que lo hacía antes de
tener aparatos)</em>, prestando especial atención a la zona de los dientes
que está entre los brackets y las encías, pues es allí donde más comida
se retiene. Si por algún motivo no pudiese hacerlo, al menos enjuáguese
enérgicamente con abundante agua o enjuague bucal. Se debe usar pasta
con flúor y cambiar el cepillo regularmente, cuando aprecie que las cerdas
empiecen a curvarse y desgastarse.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-4.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-interdental-brush_1"></i></span>

Por lo menos 1 vez al día <em>(si
puede más veces mejor aún)</em> véase en un espejo después de cepillarse
y utilice entonces el cepillo interdental para acceder al espacio existente
entre los dientes, alambres y brackets. Este pequeño cepillo de forma
cónica debe ser accionado en sentido vertical, de abajo hacia arriba y
entre todos los brackets y bandas que tenga en la boca. Compruebe que
todos los espacios y aparatos queden limpios y libres de restos alimenticios.

</div>

</div>
<div class="row alt">
<div class="item np left image">

![Hopper The Rabbit](/img/brackets-care-steps-5.jpg)

</div>
<div class="item np left">

<span style="
    font-size: 50px;
"><i class="icon-dental-floss"></i></span>

Finalmente, utilice el hilo dental
<em>(Especial para Prótesis y Ortodoncia)</em> para limpiar las zonas
más profundas de las superficies proximales de los dientes. Introdúzcalo
directamente por encima del arco de alambre, abrazando primero la superficie
de un diente y luego la del otro. Si es su deseo, complemente el proceso
con el enjuague bucal de su preferencia.

</div>

</div>
</div>
