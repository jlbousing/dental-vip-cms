---
templateKey: specialties-page
language: es
redirects: /en/specialties/orthodontics/
title: Ortodoncia Especializada en Caracas
siteName: DENTAL VIP, Especialidades Odontológicas s.c.
ogImage: https://imagenes.dentalvipcaracas.com/open-graph-pagina-ortodoncia.jpg
description: Tratamientos Exclusivos y Personalizados con Brackets Estéticos o Convencionales. Ortodoncia Interceptiva, Correctiva y Prequirúrgica. ¡20 Años de Experiencia!
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-orthodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">Ortodoncia</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-orthodontics"></i></h1>
    <p class="thin"><em>Cuide su salud y proteja su inversión. Muchas clínicas y empresas delegan
    sus tratamientos al personal auxiliar de turno. Infórmese antes y exija ser atendido
    siempre por el mismo Ortodoncista.</em></p>

# Description Section
article:
  content: >
    <p>La sonrisa es la luz de nuestro rostro, nos permite interactuar mejor
    con las personas, causar una buena impresión y hace que la gente muestre más empatía
    a la hora de relacionarse con nosotros. <strong> Sonreír con frecuencia le cambiará
    la vida, mejorará su estado de ánimo, su carisma y su autoestima. </strong></p><p>Considerando
    la belleza de la sonrisa como el gran norte y objetivo supremo de la Ortodoncia,
    en DENTAL VIP nos valemos con gran éxito de la TÉCNICA DE ARCO RECTO con aparatos
    preajustados según la filosofía y prescripción del Dr. Ronald Roth para el tratamiento
    de las maloclusiones y malposiciones dentarias.  <strong>Esta técnica <i>(Straight
    Wire System)</i> emplea fuerzas muy ligeras que nos permiten mover los dientes
    con total exactitud en los 3 planos del espacio,  </strong>logrando posiciones
    ideales perfectamente compatibles con los estándares más exigentes de la estética
    dental contemporánea. En tal sentido, podemos utilizar brackets convencionales
    o <strong> brackets estéticos; </strong>todo depende de su preferencia.</p><p>En
    la fase inicial de DIAGNÓSTICO empleamos  <strong>fotografías digitales, radiografías
    panorámicas y cefalometrías computarizadas </strong> <i>(Ricketts, Jarabak, Downs
    y Steiner)</i> para planificar toda la mecanoterapia ortodóncica y lograr resultados
    altamente predecibles, estéticos y funcionales.</p><p>Adicionalmente, manejamos
    de rutina  <strong>técnicas de modificación del crecimiento craneofacial  </strong>para
    el tratamiento de maloclusiones esqueléticas en niños y adolescentes, a través
    del uso de aparatos de ORTOPEDIA DENTOFACIAL y ortopedia funcional de los maxilares
    <i>(aparatos removibles)</i> que nos permiten recuperar el equilibrio facial y
    biológico perdido por las alteraciones del desarrollo.  <strong>En los casos extremos
    de deformidades dentofaciales severas es ya entonces necesario un abordaje combinado
    Ortodóncico-Quirúrgico, </strong> de modo que la ejecución del tratamiento implica
    la participación conjunta y coordinada con el Cirujano Maxilofacial, quien se
    encargará de reposicionar las estructuras maxilares afectadas a través de los
    diversos procedimientos de CIRUGÍA ORTOGNÁTICA.</p>
  img: /uploads/aside-orthodontics.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      No Basta con Solo Alinear los Dientes. La suma de las caracterizaciones y
    pequeños detalles que se puedan lograr en la fase de acabado, definirán la excelencia
    del resultado. Es por ello que en algunas personas los dientes lucirán realmente
    hermosos, y en otras, simplemente derechos.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. José Miguel Gómez Díez</strong>
    details: >
      <strong>
        Ortodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-orthodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-orthodontics.jpg
  mobilePosition: '{"mobilePosition320":"-860px","mobilePosition360":"-952px","mobilePosition375":"-1241px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿En qué consiste la Ortodoncia?</h4>
      content: >-
        <p>Es la Especialidad de la Odontología que se encarga de la prevención,
        diagnóstico y tratamiento de las maloclusiones y deformidades
        dentofaciales. Se fundamenta en el conocimiento del proceso de crecimiento
        y desarrollo craneofacial y en el dominio de la biomecánica, ciencia que
        se ocupa de estudiar y controlar los vectores, intensidades, sentidos y
        efectos físicos de las fuerzas que es necesario aplicar sobre los dientes
        para desplazarlos y corregir su posición.</p>
    - title: <h4>¿Qué es una maloclusión?</h4>
      content: >-
        <p>Es cualquier disposición irregular de los dientes y maxilares que
        afectan la estética y la función masticatoria de la persona. Existen
        maloclusiones dentarias y maloclusiones esqueléticas, por eso es tan
        importante el DIAGNÓSTICO en Ortodoncia. La maloclusión dentaria más común
        es el apiñamiento y se debe a una discrepancia entre el tamaño de los
        dientes y el tamaño de los maxilares. Las esqueléticas derivan de las
        alteraciones del crecimiento y desarrollo facial, y se caracterizan por
        anomalías de forma, tamaño y posición de los huesos maxilares.</p>
    - title: <h4>¿Qué es una deformidad dentofacial?</h4>
      content: >-
        <p>Es una maloclusión esquelética y dental de tal magnitud que compromete
        además la estética facial de la persona. En estos casos el solo
        tratamiento ortodóncico no es suficiente y debe ser combinado con
        procedimientos de Cirugía Ortognática para que el Cirujano Maxilofacial
        pueda reposicionar quirúrgicamente las estructuras maxilares
        displásicas.</p>
    - title: >-
        <h4>¿Por qué es importante corregir las maloclusiones y alinear los
        dientes?</h4>
      content: >-
        <p>Porque socialmente es muy favorable tener buena apariencia, y además,
        porque en gran parte; la salud bucal depende de una correcta oclusión o
        mordida. Unos dientes derechos y alineados son más fáciles de limpiar y
        menos susceptibles a las caries y enfermedades periodontales.</p>
    - title: <h4>¿Necesito un Odontólogo o un Ortodoncista?</h4>
      content: >-
        <p>Durante la carrera de Odontología apenas se imparten algunos conceptos
        básicos acerca de Ortodoncia, por lo tanto, se requieren estudios
        adicionales de Especialización <em>(Postgrado Universitario de 2 o 3 años
        de duración)</em> para estar verdaderamente capacitado en el área. Tenga
        esto siempre presente, evite fracasos y malas experiencias que
        lamentablemente son muy comunes. Los cursos cortos y diplomados de
        Ortodoncia son excelentes para que el Odontólogo General aprenda a
        detectar e interceptar algunas maloclusiones, pero de ninguna manera le
        capacitan para llevar a cabo tratamientos de Ortodoncia correctiva.</p>
    - title: <h4>¿A qué edad se debe iniciar un tratamiento de Ortodoncia?</h4>
      content: >-
        <p>No existe una edad específica, todo depende del tipo y severidad del
        problema. Es ideal acudir a consulta durante el período de dentición mixta
        <em>(entre los 7 y 9 años de edad)</em> para descartar cualquier
        alteración dental, y sobre todo esquelética; ya que la modificación del
        crecimiento <em>(Ortopedia)</em> solo es posible antes del desarrollo o
        madurez sexual. Las maloclusiones puramente dentarias pueden ser tratadas
        en cualquier etapa de la vida.</p>
    - title: <h4>¿Cuánto tiempo dura un tratamiento de este tipo?</h4>
      content: >-
        <p>Depende también del caso, pero por norma general el tiempo oscila entre
        los 18 y 24 meses. En aquellos pacientes con anomalías severas la duración
        puede ser mayor en virtud del grado de compromiso esquelético y
        dental.</p>
    - title: <h4>¿Es siempre necesario extraer dientes permanentes?</h4>
      content: >-
        <p>¡Por supuesto que no! Esto se determina en base a la discrepancia
        dento-maxilar o grado de apiñamiento dental. Las extracciones son
        indicadas en el 30% de los casos aproximadamente.</p>
    - title: "<h4>¿Cuáles son mejores, los aparatos fijos o los removibles?</h4>"
      content: >-
        <p>Los aparatos fijos o brackets son los únicos dispositivos capaces de
        mover con precisión los dientes en los 3 planos del espacio. Los aparatos
        removibles se usan para modificar el crecimiento, para “mover” los huesos
        y lograr cambios ortopédicos, pero son muy malos para alinear dientes.
        Cada tipo tiene sus indicaciones y usos específicos.</p>
    - title: >-
        <h4>¿Qué diferencias hay entre los brackets metálicos y los brackets
        estéticos?</h4>
      content: >-
        <p>Principalmente el color y el material. Los brackets estéticos están
        fabricados de plástico, porcelana <em>(blancos)</em> o cristales de zafiro
        <em>(transparentes)</em>, lo que los hace prácticamente imperceptibles a
        simple vista. Otra diferencia importante a considerar es el costo, ya que
        los brackets estéticos de comprobada calidad triplican en valor a los de
        acero inoxidable.</p>
    - title: <h4>¿Existen otras alternativas que sean aún más estéticas?</h4>
      content: >-
        <p>En la actualidad ha habido un repunte impresionante de antiguas
        técnicas que estaban en desuso. La Ortodoncia lingual y la llamada
        "Ortodoncia invisible" sin brackets, con puras férulas plásticas, son
        ofrecidas con excesivo entusiasmo. Consideramos que tienen grandes
        limitaciones y son únicamente efectivas en algunos casos extremadamente
        simples, por lo que deben ser consideradas solo bajo un criterio muy
        objetivo y profesional.</p>
    - title: <h4>¿Es muy incómodo utilizar brackets?</h4>
      content: >-
        <p>Usualmente los primeros 3 o 4 días siguientes a la instalación de los
        aparatos son algo molestos porque es normal experimentar cierto dolor al
        comer y masticar. Transcurrido este período, los inconvenientes suelen
        limitarse a pequeñas molestias ocasionales; principalmente causadas por el
        roce de los aparatos con la parte interna de labios y mejillas. Para esto
        se le proporcionará una cera especial que debe colocar sobre el bracket
        que esté molestando y así solventar el problema.</p>
    - title: <h4>¿Es necesario colocarlos en todos los dientes?</h4>
      content: >-
        <p>En la gran mayoría de los casos sí. Para obtener resultados óptimos es
        también necesario colocar bandas <em>(anillos metálicos)</em> en los
        molares, ya que son estos los que proporcionan el anclaje necesario para
        efectuar y controlar la gran mayoría de los movimientos dentales
        contemplados en la fase de planificación del tratamiento.</p>
    - title: >-
        <h4>¿Es normal que se manchen o pigmenten los dientes alrededor de los
        brackets?</h4>
      content: >-
        <p>¡Para nada! Generalmente este es el primer signo de un tratamiento mal
        realizado en el que no fueron retirados los excesos de adhesivo a la hora
        de cementar los aparatos. Estos excesos son sumamente perjudiciales porque
        en corto tiempo producen caries, descalcificaciones dentarias e
        inflamación gingival severa.</p>
    - title: >-
        <h4>¿Es necesario tener algún tipo de cuidado especial durante el
        tratamiento?</h4>
      content: >-
        <p>Por supuesto que la higiene oral es un factor clave para el éxito del
        mismo. Es fundamental cepillarse después de cada comida, utilizar el
        cepillo interdental, hilo dental y enjuague bucal. También se recomiendan
        chequeos periódicos con el Odontólogo General, y en algunos casos; con el
        Periodoncista, de modo que dientes y encías permanezcan sanos en todo
        momento.</p>
    - title: <h4>¿Qué cosas no se pueden comer con aparatos dentales?</h4>
      content: >-
        <p>Prácticamente podrá comer de todo, sin embargo, es recomendable evitar
        algunos alimentos excesivamente duros como el hielo y los huesos, y
        pegajosos como el chicle; ya que pueden desprender con facilidad los
        brackets, bandas y demás dispositivos.</p>
    - title: <h4>¿Con qué frecuencia se realizan los controles de Ortodoncia?</h4>
      content: "<p>Generalmente cada 4 semanas, salvo circunstancias especiales.</p>"
    - title: <h4>¿Realmente quedan los dientes perfectos?</h4>
      content: >-
        <p>En el 99% de los casos los resultados obtenidos son completamente
        satisfactorios siempre y cuando el tratamiento se planifique en base a un
        diagnóstico acertado y el paciente acuda regularmente a sus citas de
        control. Algunas veces es necesario complementar estos resultados con
        procedimientos de estética dental, blanqueamiento o cirugía plástica
        periodontal; de modo que los dientes luzcan realmente espectaculares.</p>
    - title: <h4>¿Se pudieran mover después de retirar los aparatos?</h4>
      content: >-
        <p>La alineación dental es una entidad dinámica y cambiante a lo largo del
        tiempo, es decir, los dientes tienden a moverse durante toda la vida, con
        o sin Ortodoncia previa; por eso la fase de retención es la más larga y
        compleja de un tratamiento ortodóncico y requiere de una excelente
        colaboración por parte del paciente.</p>
    - title: <h4>¿Qué son y para qué sirven los retenedores?</h4>
      content: >-
        <p>Son unos dispositivos que se colocan al momento de retirar los
        brackets. Pueden ser fijos o removibles y tienen por finalidad mantener
        los dientes en su posición final, evitando que se muevan y desalineen
        nuevamente. Si son removibles, se usan al principio las 24 horas del día,
        pero luego suele ser suficiente colocárselos solo para dormir.</p>

cases:
  title: >
    <h1>Ortodoncia - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/clinic-cases-orthodontics-es-01.jpg
      - /uploads/clinic-cases-orthodontics-es-02.jpg
      - /uploads/clinic-cases-orthodontics-es-03.jpg
      - /uploads/clinic-cases-orthodontics-es-04.jpg
      - /uploads/clinic-cases-orthodontics-es-05.jpg
      - /uploads/clinic-cases-orthodontics-es-06.jpg
      - /uploads/clinic-cases-orthodontics-es-07.jpg
      - /uploads/clinic-cases-orthodontics-es-08.jpg
      - /uploads/clinic-cases-orthodontics-es-09.jpg
      - /uploads/clinic-cases-orthodontics-es-10.jpg
      - /uploads/clinic-cases-orthodontics-es-11.jpg
      - /uploads/clinic-cases-orthodontics-es-12.jpg
      - /uploads/clinic-cases-orthodontics-es-13.jpg
      - /uploads/clinic-cases-orthodontics-es-14.jpg
      - /uploads/clinic-cases-orthodontics-es-15.jpg
      - /uploads/clinic-cases-orthodontics-es-16.jpg
      - /uploads/clinic-cases-orthodontics-es-17.jpg
      - /uploads/clinic-cases-orthodontics-es-18.jpg
      - /uploads/clinic-cases-orthodontics-es-19.jpg
      - /uploads/clinic-cases-orthodontics-es-20.jpg
      - /uploads/clinic-cases-orthodontics-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Apiñamiento Superior e Inferior </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Maloclusión Clase II Esquelética</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Apiñamiento Severo y Brackets Estéticos</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracción de 4 Premolares </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Canino Superior Ectópico </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sin Extracciones</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Biprotrusión Dentoalveolar </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Brackets Autoligables</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Ortodoncia en Dentición Mixta</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Discrepancia Dentomaxilar Superior </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracciones Superiores</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Deformidad Dental y Apiñamiento</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Mordida Profunda </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-orthodontics-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Antes y Después  </h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">CREÍA DIFÍCIL OBTENER BUENOS RESULTADOS EN UN CASO TAN COMPLEJO COMO EL
    MÍO. USÉ APARATOS FIJOS POR AÑO Y MEDIO PARA QUE ME PUDIERAN OPERAR Y CORREGIR
    LA POSICIÓN DE LOS MAXILARES. AHORA SOY OTRA, LOS DOCTORES ENDEREZARON MIS DIENTES
    Y TRANSFORMARON MI CARA. ¡ALGO FANTÁSTICO!".</p>
  images:
    portrait: /uploads/testimonio-07-portrait.jpg
    landscape: /uploads/testimonio-07.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>

          <p>
            Contáctenos para estudiar su caso, planificar el tratamiento y poder emitirle un presupuesto ajustado. Le garantizamos que visitando la ciudad de Caracas podrá ahorrar hasta un 70% en tratamientos dentales de Alta Gama.
          </p>
          
        </div>
      image: /images/aside-dental-tourism-map.png
      specialCaseImage:
        - name: aside-dental-tourism-map.svg
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-orthodontics-invisalign.jpg
      content: >
        <h1 class="bebas">¡Un Final Feliz y Transparente!</h1><p>Al retirar los brackets, es siempre necesario
        utilizar un dispositivo artificial que mantenga los dientes en su nueva posición.
        Por ser casi invisible, cómodo y fácil de usar, un retenedor plástico transparente
        termoformado al vacío es siempre nuestra mejor recomendación.</p><br/>
      footer:
        icon:
          display: true
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/sections-orthodontics-asthetic-braces.jpg
      content: >
        <h1 class="bebas">Aparatos Estéticos</h1>
        <p>Gracias a su color, textura y transparencia
            los Brackets Estéticos de Porcelana o Cristales de Zafiro se mimetizan con el
            esmalte dental y son particularmente propicios para aquellas personas, que por
            su trabajo o actividad social, deseen un tratamiento de muy bajo perfil pero
            comprobada calidad clínica.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: true
          to: /especialidades/ortodoncia/aparatos-esteticos/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-orthodontics-brackets-care.jpg
      content: >
        <h1 class="bebas">¡Cuide sus Brackets!</h1>

          <p><b>Indicaciones para personas con aparatos
          fijos. </b></br></br>Jamás olvide que una adecuada técnica de higiene oral y
          la permanente colaboración del paciente con su tratamiento, son requisitos indispensables
          para el éxito del mismo.</p><br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-orthodontics-icon-invisalign.png
        link:
          display: true
          to: /especialidades/ortodoncia/cuide-sus-brackets/
          placeholder: <span>Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
