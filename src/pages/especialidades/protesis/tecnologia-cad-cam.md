---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/prosthodontics/cad-cam-technology/
title: Tecnología CAD-CAM
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-cad-cam.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 50%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: Column
  isGalleryCadCam: true
  content: >
    <h2>¿Cómo funciona?</h2>
    <p class="subtitle">
    <strong>CAD</strong> <em>(Computer Aided Design)</em> <strong>CAM</strong> <em>(Computer Aided Manufacturing).</em> El proceso paso a paso en nuestras instalaciones:
    </p>
  blocks:
    - img: /uploads/cad-cam-list-1.jpg
      style: "test"
      number: 1
      title: >
        <h3>Preparación intraoral</h3>
        <p class="dv-div-text list">Exposición
        de implantes dentales, selección y ubicación de dispositivos de transferencia,
        tallado de muñones, definición de líneas de terminación y análisis del espacio
        interoclusal.</p>

    - img: /uploads/cad-cam-list-2.jpg
      number: 2
      title: >
        <h3>Toma de impresión</h3>
        <p class="dv-div-text list">Empacado del hilo
          separador y registro de preparaciones protésicas con materiales elastoméricos
          de alta fidelidad, generalmente a base de siliconas por adición.</p>
    - img: /uploads/cad-cam-list-3.jpg
      number: 3
      title: >
        <h3>Vaciado y troquelado del modelo maestro</h3>
        <p class="dv-div-text
          list">Individualización de tallados y pilares protésicos para proveer mejor
          acceso y visualización de sus márgenes y límites biológicos.</p>
    - img: /uploads/cad-cam-list-4.jpg
      number: 4
      title: >
        <h3>Escaneo 3D y digitalización</h3>
        <p class="dv-div-text list">Extrapolación
        digital tridimensional del modelo de trabajo mediante un sistema esférico
        de coordenadas que reproduce con precisión todos sus detalles geométricos.</p>
    - img: /uploads/cad-cam-list-5.jpg
      number: 5
      title: >
        <h3>Diseño informático</h3>
        <p class="dv-div-text list">Creación y modelado
          3D del producto deseado mediante un software informático que dibuja la infraestructura
          y transfiere los datos a un robot o máquina fresadora.</p>
    - img: /uploads/cad-cam-list-6.jpg
      number: 6
      title: >
        <h3>Fresado mecánico</h3>
        <p class="dv-div-text list">La restauración
          creada en la computadora se talla de forma automática a partir de un bloque
          cerámico de Disilicato de Litio <em>(LS<sub>2</sub>)</em> u Óxido de Zirconio
          <em>(ZrO<sub>2</sub>)</em>.</p>
    - img: /uploads/cad-cam-list-7.jpg
      number: 7
      title: >
        <h3>Prueba de cofias cerámicas</h3>
        <p class="dv-div-text list">Verificación
          del contorno, integridad estructural, extensión, sellado, adaptación, estabilidad
          y asentamiento del esqueleto cerámico generado por el sistema.</p>
    - img: /uploads/cad-cam-list-8.jpg
      number: 8
      title: >
        <h3>Estratificación de porcelana</h3>
        <p class="dv-div-text list">Acabado
          artístico manual para caracterizar la rehabilitación, añadir tinciones, lograr
          policromatismo y translucidez, afinar el color y reproducir detalles anatómicos.</p>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title">TECNOLOGÍA CAD-CAM</h1>
    <p>Tecnología computarizada que permite el escaneo en 3D, digitalización y transferencia de datos a un software que diseña y confecciona el núcleo de cualquier tipo de restauración totalcerámica a través de la activación y control de un sistema robótico de microfresado.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-cad-cam.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¡La Solución Más Estética y Natural para Su Nueva Sonrisa!</h1>
    <br>
    <h2 >Sin los indeseables bordes negros que resaltan a nivel de encía en
    muchas restauraciones fijas de núcleo metálico.</h2>
  # anex-links
anexes:
  enforce: false
  display: false
  items:
    - img: /uploads/sections-3d-radiography.jpg
      content: >
        <;
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: false
          to: ""
          placeholder: <span> LOAD FILE</span>
articleBlock:
  direction: reverse
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Ventajas del CAD-CAM en prótesis fija
      </h2>
      <ul>

      <li> <i class="icon-check circle"></i>Alta estética dental debido a que es una tecnología que utiliza sistemas cerámicos de última generación. </li>

      <li><i class="icon-check circle"></i>Tratamiento más conservador que favorece la preservación del tejido dental sano.</li>

      <li><i class="icon-check
      circle"></i>Coronas y puentes fijos 100% libres de metal.</li>

      <li><i class="icon-check circle"></i>Mayor biocompatibilidad y resistencia a la placa dental.</li>

      <li><i class="icon-check circle"></i>Precisión en la adaptación a los pilares, con un sellado marginal periférico exacto que garantiza su desempeño a largo plazo.</li>

      <li><i class="icon-check circle"></i>Longevidad muy similar a las prótesis tradicionales de metal-porcelana.</li>

      <li><i class="icon-check
      circle"></i>Rapidez de elaboración, en virtud a que es un sistema robotizado que simplifica el proceso de laboratorio.</li></ul>
    image:
      display: false
      size: 120px
      src: /uploads/icon-implants-logo.jpg
    link:
      display: false
      placeholder: algo
      to: /
  img: /uploads/aside-cad-cam.jpeg
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Rehabilitación Oral</h5>
      to: "/especialidades/protesis/\n"
      img: "/uploads/procedures-rehabilitation.jpg"
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
      img: /img/procedures-diagnostic.jpg
    - title: >
        <h5> Diseño digital de sonrisa </h5>
      to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
      img: /img/procedures-design.jpg
    - title: >
        <h5> Carillas de porcelana </h5>
      to: /especialidades/estetica-dental/carillas-de-porcelana/
      img: /img/procedures-veneers.jpg
    - title: >
        <h5> Brackets Estéticos </h5>
      to: /especialidades/ortodoncia/aparatos-esteticos/
      img: /img/procedures-brackets.jpg
    - title: >
        <h5> Prótesis sobre implantes </h5>
      to: /especialidades/implantes-dentales/protesis-sobre-implantes/
      img: /img/procedures-dental-implants.jpg
---

<div class="container">

![Hopper The Rabbit](/img/gallery-blocks-main-cad-cam.jpg)

<div class="row">
<div class="item np">

## ¿Cuándo Disilicato?

Al ser más translúcido, nos permite lograr un mejor mimetismo con los
dientes naturales y es nuestro material de elección para confeccionar incrustaciones,
carillas y coronas anteriores sobre sustrato claro.

</div>

<div class="item np">

## ¿Cuándo Zirconio?

Por su mayor opacidad y resistencia a la fractura, lo indicamos para
coronas posteriores y sobre sustrato oscuro, puentes cerámicos anteriores y
posteriores y rehabilitación de implantes dentales.

</div>
</div>
</div>
