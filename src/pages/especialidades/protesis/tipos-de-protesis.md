---
templateKey: annex-page
language: es
redirects: >
  /en/specialties/prosthodontics/types-of-dental-prosthesis
title: Tipos de prótesis dental
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-prosthesis-types.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 33%
  content:
    position: center
    body: >
      <i></i>

# Gallery Section
listGallery:
  display: true
  position: top
  type: List
  content: >

  blocks:
    - img: /uploads/implant-types-Implant-supported-prosthesis.jpg
      number: 1
      title: >

        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-implant-supported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Prótesis implantosoportada</h2>
        </div>

        <p class="dv-srv-pr">Es la que se confecciona sobre implantes dentales
        y es, en la gran mayoría de los casos, el tipo de prótesis ideal. Los implantes
        nos permiten elaborar restauraciones fijas e individualizadas sin tener que
        tallar, desgastar y comprometer otros dientes sanos. Además, es la única alternativa
        fija en los casos de brechas edéntulas sin pilar posterior, poca cantidad o
        ausencia total de dientes remanentes.</p>
            </div>
    - img: /uploads/implant-types-prosthetic-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-types-prosthetic-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Prótesis dentasoportada</h2>
        </div>
        <p class="dv-srv-pr">Es también conocida como prótesis fija convencional
        y va cementada sobre los dientes contiguos al espacio vacío. Indudablemente
        era la mejor opción antes de la aparición de los implantes dentales. Para su
        elaboración, es necesario tallar muñones y desgastar los dientes que le servirán
        de base <em>(pilares)</em> a cada extremo, aún cuando estén intactos y completamente
        sanos.</p>
          </div>
    - img: /uploads/implant-types-dentomucosupported-prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-dentomucosupported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Prótesis dentomucosoportada</h2>
        </div>
        <p class="dv-srv-pr">Es una dentadura parcial removible de acrílico <em>(material
        plástico)</em>, metal-acrílico o nylon termoplástico. Puede ser insertada y
        removida por el propio paciente. Descansa sobre la mucosa bucal y se retiene
        mediante un sistema de elementos extracoronales o ganchos que rodean a algunas
        de las piezas dentarias aún presentes. Aunque por sus limitaciones estéticas
        y funcionales dista mucho del prototipo protésico ideal, sigue teniendo vigencia
        como dentadura provisional, e incluso definitiva; en casos de contraindicación
        quirúrgica o limitación económica.</p>
            </div>
    - img: /uploads/implant-types-mucosupported prosthesis.jpg
      number: 1
      title: >
        <div class="content-wrapper">
        <div class="title-icon">
          <i class="icon-mucosupported-prosthesis icon-title"></i>
          <h2 class="dv-div-title" style="width:80%">Prótesis mucosoportada</h2>
        </div>
        <p class="dv-srv-pr">Es la popular "plancha" y aún se usa cuando ya no
        queda ningún diente en la boca. Cuenta con muy poca retención y estabilidad,
        aunque algunas personas se acostumbran y llegan a tolerarla. Actualmente existe
        la posibilidad de anclarla a 2, 3 o 4 implantes para transformarla en una sobredentadura
        implantoasistida mucho más cómoda y funcional, claro está, siempre y cuando
        las condiciones óseas y sistémicas del paciente lo permitan.</p>
                </div>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="bebas title"> Tipos de prótesis dental</h1>
     <p>Existe más de una solución protésica
     para un mismo caso clínico, cada una con ventajas y desventajas. Será nuestra
     labor en consulta exponerlas con detalle y orientarle durante el proceso de selección.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-prosthesis-types.jpg
  content: >
    <i></i>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>Contamos con Sistemas Cerámicos de Última Generación...</h1>
    <br>
    <br>
    <h2 >En DENTAL VIP garantizamos la más cuidadosa selección de materiales
    dentales para su Rehabilitación Oral.</h2>

# anex-links
anexes:
  enforce: true
  display: false
  items:
    - img: /uploads/sections-dds-mock-up.jpg
      content: >
        <h2>Mock-up digital</h2>
      footer:
      icon:
        display: false
        img: /uploads/sections-facilities.jpg
      link:
        display: true
        to: /especialidades/estetica-dental/carillas-de-porcelana/
      placeholder: <span>Más Información</span>
customBlocks:
  display: false
  type: row
  blocks:
    - header:
        display: true
        content: >
          <h2></h2>
      body:
        display: true
        images:
          - src: /uploads/gallery-blocks-ceramic-brackets.jpg
      footer:
        display: true
        content: >
          <p>=</p>
articleBlock:
  direction: normal
  content:
    body: >
      <h2 class="dv-div-title dv-pb-40">Un enfoque verdaderamente
      biológico y conservador</h2>
      <p class="dv-div-text
      dv-pb-15">Creemos que la rutina
      de desgastar o sobrecargar dientes sanos con la única intención de reponer otros
      ausentes, debería ser ya cosa del pasado. Comprometer la salud, integridad y longevidad
      de sus pilares, tal y como ocurre con las diversas variantes de la prótesis convencional,
      era el precio obligado a pagar antes del descubrimiento y desarrollo de la oseointegración.</p><p
      class="dv-div-text text-right">Estamos convencidos de que, en condiciones favorables,
      los implantes son la solución ideal y biológica por excelencia en el campo de
      la Rehabilitación Oral. Además de reponer dientes, conservan el hueso maxilar
      y evitan la aparición de nuevas patologías en la dentición natural remanente</p>
    image:
      display: false
      size: 100px
      src: /uploads/icons-abutment-es.jpg
    link:
      display: true
      placeholder: Más Información
      to: >
        /especialidades/implantes-dentales/
  img: /uploads/aside-prosthesis-types.jpg
# Procedures Section
procedures:
  title: >
    <h1 class="title"> Otros Procedimientos</h1>
  procedures:
    - title: >
        <h5> Tecnología cad-cam </h5>
      to: /especialidades/protesis/tecnologia-cad-cam/
      img: /uploads/procedures-cad-cam.jpg
    - title: >
        <h5> Diagnóstico y Planificación 3D </h5>
      to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d/
      img: /uploads/procedures-diagnostic.jpg
    - title: >
        <h5> Carillas de porcelana </h5>
      to: /especialidades/estetica-dental/carillas-de-porcelana/
      img: /uploads/procedures-veneers.jpg
    - title: >
        <h5> Prótesis sobre implantes </h5>
      to: /especialidades/implantes-dentales/protesis-sobre-implantes/
      img: /uploads/procedures-dental-implants.jpg
    - title: >
        <h5> Diseño digital de sonrisa </h5>
      to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
      img: /uploads/procedures-design.jpg
    - title: >
        <h5> Brackets Estéticos </h5>
      to: /especialidades/ortodoncia/aparatos-esteticos/
      img: /uploads/procedures-brackets.jpg
---

<div class="row container">
<div class="item full">

![Hopper The Rabbit](/img/gallery-blocks-aesthetic.jpg)

## ¡Estética y función!

<strong>Dos grandes objetivos.</strong> Lograr restauraciones que luzcan
bellas, radiantes y naturales, que armonicen con su rostro, labios y encías,
que resalten su sonrisa, que sean cómodas para hablar, comer y masticar y que
permanezcan incólumes al paso del tiempo.

</div>
<div class="item full">

![Hopper The Rabbit](/img/gallery-blocks-security.jpg)

## ¡Seguridad y confianza!

<strong>Dos grandes beneficios.</strong> Todo ser humano necesita sentirse
bien y seguro de sí mismo. Una dentadura sana y atractiva le permitirá sonreír
sin ningún tipo de complejo, aumentará su autoestima y hará que disfrute de
una vida plena, sin límites y llena de satisfacciones.

</div>
</div>
