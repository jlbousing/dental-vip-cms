---
templateKey: specialties-page
language: es
redirects: /en/specialties/prosthodontics/
title: Prótesis
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-prosthodontics.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 44%
  content:
    position: center
    body: >
      <h1 class="bebas">Prótesis</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-prosthodontics"></i></h1>
    <p class="thin"><em>Las consecuencias psicológicas asociadas a la Ausencia de Dientes generan
    mucha inseguridad en las personas y cambios conductuales capaces de limitar, e
    incluso destruir; sus relaciones sociales, afectivas y laborales.</em></p>

# Description Section
article:
  content: >
    <p>Es bien conocido que la integridad de las arcadas dentarias es factor
    clave para mantener el equilibrio y correcta función de todo el SISTEMA ESTOMATOGNÁTICO
    conformado por dientes, maxilares, músculos masticatorios y articulaciones temporomandibulares.
    <strong>Si se pierden uno o varios dientes y no son reemplazados a la brevedad,
    las piezas vecinas a los espacios vacíos se inclinan y desplazan hacia ellos alterando
    la oclusión y función masticatoria.  </strong>Además, pueden empezar a aparecer
    nuevos espacios entre los dientes anteriores <em>(diastemas)</em> que comprometen
    seriamente la estética dental.</p><p> <strong>La Prostodoncia es sin duda una
    de las Especialidades con más campo de acción dentro de la Odontología actual.
    </strong> Puede abarcar desde la reconstrucción de un solo diente parcialmente
    destruido hasta la REHABILITACIÓN COMPLETA bimaxilar en pacientes totalmente edéntulos.</p><p>Dentro
    del arsenal protésico a nuestra disposición encontramos las conocidas y populares
    dentaduras removibles, las prótesis fijas convencionales y las  <strong> sofisticadas
    prótesis implantosoportadas. </strong> Sin discusión alguna, estas últimas son
    en la actualidad, en todos sus tipos y versiones; las de primera elección. Aunque
    la función ha sido tradicionalmente el objetivo primordial del Prostodoncista,
    hoy en día la estética tiene igual relevancia y las tendencias nos han llevado
    a confeccionar  <strong>aparatos totalmente estéticos y libres de metal </strong>
    como las prótesis flexibles de nylon termoplástico por inyección tipo Valplast<sup>®</sup>
    y las  <strong>impresionantes coronas y dentaduras de Zirconio o de Disilicato
    de Litio  </strong>elaboradas con TECNOLOGÍA COMPUTARIZADA CAD-CAM.</p><p>Dicho
    lo anterior, no es difícil comprender  <strong>la importancia que tiene la participación
    de un Especialista, </strong> de un Prostodoncista que garantice el éxito estético
    y funcional de su tratamiento restaurador, que verdaderamente esté en capacidad
    de diseñar y fabricar dispositivos protésicos que le permitan hablar, comer y
    sonreír con comodidad y sin ningún tipo de limitación.</p>
  img: /uploads/aside-prosthodontics-correct.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Nuestra misión consiste en difundir los beneficios de la salud oral y promover
    el Poder Emocional de una Bella Sonrisa, de modo que todos los pacientes puedan
    disfrutar al máximo de la Odontología, del éxito y de la vida en general.
      </em>
    </p>
  footer:
    author: >
      <strong>Dra. Filomena Montemurro Tafuri</strong>
    details: >
      <strong>
        Prostodoncista
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-prosthodontics.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-prosthodontics.jpg
  mobilePosition: '{"mobilePosition320":"-687px","mobilePosition360":"-835px","mobilePosition375":"-1089px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿Qué es una prótesis dental?</h4>
      content: >-
        <p>Es un dispositivo anatómico que simula o contiene dientes artificiales
        que sustituyen a las coronas de los dientes naturales muy destruidos,
        ausentes o perdidos. Según su extensión pueden ser individuales, parciales
        o totales; y según su soporte y retención, fijas o removibles.</p>
    - title: >-
        <h4>¿Por qué es tan importante la salud de las encías para cualquier
        dispositivo protésico?</h4>
      content: >-
        <p>La durabilidad de cualquier tipo de prótesis va a depender de su
        integridad estructural y de la salud de sus pilares. Indistintamente de
        que sean soportadas por dientes naturales, muñones o implantes dentales,
        se encuentran todos anclados y retenidos por los tejidos periodontales de
        soporte; por el tejido óseo y encía principalmente. Si no hay buena
        higiene, podrían infectarse, inflamarse, reabsorberse el hueso alveolar,
        aflojarse y caerse los pilares; para finalmente, perderse prematuramente
        la prótesis dental.</p>
    - title: <h4>¿Cuánto tiempo toma confeccionar una prótesis dental?</h4>
      content: >-
        <p>Depende mucho del caso y tipo de rehabilitación. Los lapsos pueden
        variar desde 2 o 3 días para las provisionales hasta los 6 o 7 meses para
        las que se fabrican sobre implantes, claro está, tomando en consideración
        el período de oseointegración. Por norma general y en condiciones
        normales, 3 semanas es el lapso de tiempo promedio necesario para hacer
        pruebas y culminar la gran mayoría de nuestras restauraciones definitivas;
        sean fijas o removibles.</p>
    - title: <h4>¿Cómo es una prótesis removible convencional?</h4>
      content: >-
        <p>Son dispositivos de “quita y pon” que pueden ser insertados y retirados
        por el propio paciente, se apoyan sobre la mucosa bucal, y si son
        parciales; se retienen con ganchos que rodean a algunos dientes naturales.
        Suelen ser elaboradas con aleaciones metálicas de cromo-cobalto, resina
        acrílica termopolimerizable y nylon termoplástico por inyección. Por sus
        grandes limitaciones estéticas y funcionales solo las indicamos como
        estructuras provisionales o de transición en rehabilitaciones extensas, o
        definitivas; solo en aquellos casos donde de verdad no aplique otra
        alternativa.</p>
    - title: <h4>¿Qué cuidados hay que tener con una dentadura de este tipo?</h4>
      content: >-
        <p>Controles profesionales periódicos e higiene bucal escrupulosa para
        evitar enfermedades periodontales y caries en los dientes pilares,
        particularmente en las zonas donde se apoyan los ganchos. Es además
        necesario limpiar la prótesis después de cada comida, empleando para ello
        un cepillo de cerdas suaves y pasta de dientes. Recomendamos siempre
        retirar la dentadura para dormir, de modo que la mucosa bucal de soporte
        tenga oportunidad de revascularización y regeneración, fenómenos
        biológicos esenciales para prevenir la aparición y recurrencia de lesiones
        inflamatorias, degenerativas o infecciosas.</p>
    - title: <h4>¿Cuándo se debe reemplazar una dentadura “quita y pon”?</h4>
      content: >-
        <p>Cuando se fracture alguno de sus componentes estructurales, cuando se
        pierda algún pilar, cuando sea notable la reabsorción del hueso basal que
        la soporta o cuando sus ajustes periódicos necesarios no sean capaces de
        garantizar su estabilidad y retención. Debemos entender que las dentaduras
        removibles tradicionales son muy pobres desde el punto de vista
        biomecánico, generan siempre fuerzas indeseables sobre sus dientes pilares
        y causan reabsorción de sus tejidos de soporte; circunstancias todas que
        limitan considerablemente su longevidad o vida útil.</p>
    - title: <h4>¿Cómo es una prótesis fija dentosoportada?</h4>
      content: >-
        <p>La prótesis fija clásica va cementada a los dientes naturales
        remanentes, previamente desgastados y convertidos en muñones. Usualmente
        constan de un núcleo metálico recubierto de porcelana, aunque en la
        actualidad es posible confeccionarlas de pura cerámica o sobre un núcleo
        de color blanco a base de Zirconio. Generalmente se indican, por su mayor
        naturalidad y translucidez, las estructuras totalcerámicas para los
        dientes anteriores y, por su mayor resistencia, las de núcleo metálico
        para los posteriores.</p>
    - title: <h4>¿Qué es un muñón?</h4>
      content: >-
        <p>Es un diente natural que ha sido tallado y desgastado para servir de
        pilar a una corona o prótesis fija convencional. Cuando la estructura
        dental remanente sea escasa o se encuentre debilitada, será necesario
        confeccionar un “muñón artificial” que consta de dos porciones, un perno
        que va alojado en el interior del conducto radicular <em>(previamente
        endodonciado y desobturado)</em> y una porción coronal que reemplaza a la
        dentina perdida y refuerza a la existente. Los muñones artificiales pueden
        ser elaborados con materiales a base de fibra de vidrio o mediante la
        fundición y colado de aleaciones metálicas de alta resistencia.</p>
    - title: <h4>¿Y el tallado de muñones no daña los dientes?</h4>
      content: >-
        <p>Depende del caso. Si los dientes están ya muy cariados o fracturados,
        por el contrario, el tallado y posterior reconstrucción del muñón los
        reforzará, permitirá su restauración estética y garantizará su permanencia
        en boca. Diferente sería si tuviéramos que tallar dientes completamente
        sanos con la única finalidad de reponer otros ausentes mediante prótesis
        fija convencional. Hoy en día la principal ventaja de los implantes
        dentales es que nos permiten conservar intactos los dientes vecinos a los
        espacios edéntulos.</p>
    - title: >-
        <h4>¿Es siempre necesario realizar una endodoncia antes de tallar un
        muñón?</h4>
      content: >-
        <p>No siempre, pero sí en la gran mayoría de los casos. El fresado
        mecánico aplicado a un diente vital para desgastarlo y convertirlo en
        muñón constituye una gran agresión física y genera tanta fricción y calor
        que usualmente, y al corto o mediano plazo, causa inflamación
        irreversible, degeneración y muerte de su tejido pulpar; acompañada
        siempre de sensibilidad extrema y dolor. Tal condición pondría en riesgo a
        las restauraciones fijas definitivas, ya que habría entonces que
        desprenderlas o perforarlas para endodonciar sus pilares. La endodoncia
        preventiva por razones protésicas es en la actualidad un criterio clínico
        de amplia aceptación a nivel mundial.</p>
    - title: <h4>¿Cuántos tipos de porcelana se utilizan en prostodoncia fija?</h4>
      content: >-
        <p>Químicamente, las porcelanas o cerámicas dentales se pueden agrupar en
        tres grandes familias: feldespáticas, aluminosas y zirconiosas. Las
        feldespáticas de matriz vítrea <em>(a base de feldespato y cuarzo)</em>
        son las más estéticas pero más frágiles de todas, por lo que se utilizan
        principalmente para el recubrimiento de estructuras metálicas o cerámicas
        de base. Las aluminosas <em>(con alto contenido de óxido de aluminio)</em>
        son muy resistentes pero muy opacas y poco estéticas, por lo que en la
        actualidad se reservan únicamente para la confección de cofias y
        estructuras internas, siendo necesario recubrirlas con porcelanas de menor
        cantidad de alúmina para lograr un buen mimetismo con el diente natural.
        Las zirconiosas son las más novedosas y están compuestas por óxido de
        zirconio <em>(ZrO<sub>2</sub>)</em> altamente sinterizado, lo que las hace
        sumamente resistentes a la flexión y fractura, y por ende, los materiales
        idóneos para elaborar prótesis cerámicas en zonas de alto compromiso
        mecánico. Sin embargo, y al igual que las aluminosas de alta resistencia,
        estas cerámicas son muy opacas <em>(no tienen fase vítrea)</em>, y por
        ello se emplean únicamente para fabricar el núcleo de la restauración, es
        decir; deben también recubrirse con porcelanas convencionales para lograr
        un buen resultado estético.</p>
    - title: <h4>¿Son las prótesis fijas para toda la vida?</h4>
      content: >-
        <p>Influyen tantos, pero tantos factores en la longevidad de una
        rehabilitación fija que ningún profesional está en capacidad de predecir,
        a ciencia cierta, su duración en años. Consideramos que en condiciones
        favorables, entre 15 y 20 años es en promedio el tiempo de vida útil para
        la gran mayoría de los casos. Una vez cumplido el ciclo, la rehabilitación
        podrá ser idealmente sustituida por implantes dentales, o en su defecto,
        replicada; siempre y cuando los pilares ofrezcan condiciones favorables
        para ello.</p>
    - title: <h4>¿Qué cuidados hay que tener con una prótesis fija?</h4>
      content: >-
        <p>Higiene bucal escrupulosa, sentido común para evitar morder
        inapropiadamente objetos contundentes, uso permanente de una férula
        nocturna de protección y controles profesionales periódicos. Es
        fundamental, aparte del cepillado normal, el uso constante del cepillo
        interdental, hilo dental especial, enjuague e irrigador bucal. Un
        Waterpik<sup>®</sup> es el mejor complemento para la higiene oral de
        personas portadoras de implantes dentales, prótesis fija y ortodoncia.
        Siempre recomendamos a nuestros pacientes un control de rutina cada 6 o 12
        meses para dinamizar la férula y evaluar su condición periodontal, función
        oclusal, estabilidad, integridad y sellado marginal de las
        restauraciones.</p>
    - title: <h4>¿Cómo es el hilo dental especial para prótesis?</h4>
      content: >-
        <p>Consta generalmente de 3 porciones. Un primer tramo rígido para poder
        insertarlo directamente por debajo de la prótesis, un segundo tramo
        esponjoso para limpiar alrededor de la restauración, entre pilares y entre
        los espacios interdentales; y un tercer tramo sin cera para remover la
        placa del surco gingival.</p>
    - title: <h4>¿Cómo es una prótesis implantoasistida o implantosoportada?</h4>
      content: >-
        <p>Es la que va retenida exclusivamente por implantes dentales. Existen
        varios tipos y básicamente se elaboran con los mismos materiales que las
        prótesis tradicionales. Estructuralmente son diseñadas bajo un sistema de
        macho y hembra, en el que los implantes alojan en su interior a los
        abutments o pilares protésicos, y sobre estos; se ajustan, cementan o
        atornillan los dientes artificiales.</p>
    - title: <h4>¿Por qué son mejores las prótesis sobre implantes dentales?</h4>
      content: >-
        <p>En esencia porque son estructuras totalmente independientes,
        autosuficientes y que más se aproximan al prototipo protésico ideal. No se
        apoyan ni retienen en los dientes naturales del paciente, no les
        comprometen, no les transmiten fuerzas nocivas ni sobrecargas funcionales,
        y en consecuencia; sustituyen los dientes perdidos sin ningún efecto
        negativo sobre los presentes, condición imposible de lograr con ningún
        tipo de prótesis convencional. Además, es la única alternativa fija para
        aquellas personas que hayan perdido todos o la mayor parte de sus
        dientes.</p>
    - title: <h4>¿Cuántos tipos de prótesis sobre implantes hay?</h4>
      content: >-
        <p>A groso modo, 3 tipos: fijas de metal-porcelana o totalcerámica,
        híbridas de metal-acrílico o metal-porcelana y sobredentaduras removibles.
        En la sección de IMPLANTES DENTALES podrá encontrar una descripción muy
        bien detallada de cada una de ellas y sus variantes.</p>
    - title: >-
        <h4>¿Qué es mejor, trabajar sobre una raíz restaurable o colocar de una
        vez un implante dental?</h4>
      content: >-
        <p>Siempre que sea posible debemos conservar los dientes naturales. Aunque
        en la actualidad los implantes dentales representan una alternativa
        restauradora de gran calidad, jamás superarán las propiedades biológicas y
        expectativas funcionales de una pieza natural bien rehabilitada. Cuando un
        solo diente haya sufrido fractura o gran destrucción por caries de su
        corona clínica, pero conserve su raíz buena integridad, longitud favorable
        y adecuado soporte periodontal; será siempre preferible, y sin discusión
        alguna, su tratamiento y restauración con endodoncia, muñón artificial y
        corona de porcelana.</p>
    - title: >-
        <h4>Pero, ¿cómo saber si un diente natural puede ser exitosamente
        restaurado?</h4>
      content: >-
        <p>Estudiando el caso minuciosamente mediante examen intraoral, evaluación
        radiográfica y mucho criterio clínico. Todo dependerá de las condiciones
        periapicales que presente, de la cantidad de tejido dentario remanente, de
        su estado periodontal, de sus requerimientos estéticos, de su morfología
        radicular, de su localización en la arcada dental, de las cargas oclusales
        a que será expuesto y del hecho de si funcionará como corona individual o
        como pilar de puente fijo.</p>
    - title: >-
        <h4>¿Pueden las prótesis convencionales ser reemplazadas por implantes
        dentales?</h4>
      content: >-
        <p>¡Por supuesto que sí!, y es de hecho lo más recomendable para proteger
        los dientes naturales que permanezcan en boca, pero claro está; siempre y
        cuando el estado de salud general del paciente lo permita, contemos con
        suficiente hueso maxilar para la implantación pretendida y esté la persona
        dispuesta a hacer un nuevo esfuerzo y una nueva inversión en tiempo y
        dinero.</p>

cases:
  title: >
    <h1>Prótesis - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-prosthodontics-es-01.jpg
      - /uploads/clinic-cases-prosthodontics-es-02.jpg
      - /uploads/clinic-cases-prosthodontics-es-03.jpg
      - /uploads/clinic-cases-prosthodontics-es-04.jpg
      - /uploads/clinic-cases-prosthodontics-es-05.jpg
      - /uploads/clinic-cases-prosthodontics-es-06.jpg
      - /uploads/clinic-cases-prosthodontics-es-07.jpg
      - /uploads/clinic-cases-prosthodontics-es-08.jpg
      - /uploads/clinic-cases-prosthodontics-es-09.jpg
      - /uploads/clinic-cases-prosthodontics-es-10.jpg
      - /uploads/clinic-cases-prosthodontics-es-11.jpg
      - /uploads/clinic-cases-prosthodontics-es-12.jpg
      - /uploads/clinic-cases-prosthodontics-es-13.jpg
      - /uploads/clinic-cases-prosthodontics-es-14.jpg
      - /uploads/clinic-cases-prosthodontics-es-15.jpg
      - /uploads/clinic-cases-prosthodontics-es-16.jpg
      - /uploads/clinic-cases-prosthodontics-es-17.jpg
      - /uploads/clinic-cases-prosthodontics-es-18.jpg
      - /uploads/clinic-cases-prosthodontics-es-19.jpg
      - /uploads/clinic-cases-prosthodontics-es-20.jpg
      - /uploads/clinic-cases-prosthodontics-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Desgaste Dental Severo </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reemplazo de Prótesis Parcial Fija</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sustitución de Dentaduras Removibles</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Rehabilitación Oral Avanzada </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Dental Extreme Makeover </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Prótesis Fija Dentosoportada </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Diseño Cerámico con Tecnología CAD-CAM </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Ferulización por Enfermedad Periodontal </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Retratamiento Protésico </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Valplast<sup>®</sup></h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Agenesia de Incisivos Laterales Superiores </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>1ra Fase Restauradora en un Caso Complejo </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Coronas jacket de Porcelana </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Bruxismo Extremo </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reconstrucción Estética y Funcional</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sistema Cerámico IPS E-MAX<sup>®</sup> CAD </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>All-on-four </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Rehabilitación Mixta </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Coronas Sobre Implantes Dentales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Corona de Disilicato en Incisivo Central </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-prosthodontics-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Dentadura Total Mucosoportada Superior</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">ME DESCUIDÉ MUCHOS AÑOS Y MIS DIENTES ERAN UN VERDADERO DESASTRE. NECESITÉ
    EXTRACCIONES, ALGUNAS ENDODONCIAS, IMPLANTES Y VARIAS CORONAS DE ZIRCONIO. AUNQUE
    EL TRATAMIENTO DURÓ CASI 6 MESES, LOS ESPECIALISTAS PRÁCTICAMENTE REHICIERON MI
    BOCA".</p>
  images:
    portrait: /uploads/quotes-prosthodontics-portrait.jpg
    landscape: /uploads/quotes-prosthodontics.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-prosthodontics-ceramic.jpg
      content: >
        <h1 class="bebas">All Ceramic... All You Need!</h1>
        <p>Un sistema que nos permite seleccionar el
        material de cerámica sin metal más apropiado para cada situación, en función
        de la indicación inicial y requerimientos de resistencia. Disilicato de Litio
        para restauraciones individuales y Óxido de Zirconio para estructuras extensas.</p>
         <br/>
      footer:
        icon:
          display: true
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: false
          to: ""
          placeholder: <span>mas</span>
    - img: /uploads/sections-prosthodontics-prosthesis-types.jpg
      content: >
        <h1 class="bebas">Tipos de Prótesis Dental</h1>
        <p>Parciales y totales, fijas y removibles,convencionales e implantoasistidas, acrílicas y de porcelana. Múltiples son las alternativas para sustituir los dientes que lamentablemente se hayan perdido,o que sea imposible conservar por más tiempo en boca.</p>
        <br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: true
          to: /especialidades/protesis/tipos-de-protesis/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-prosthodontics-cad-cam.png
      content: >
        <h1 class="bebas">Tecnología CAD-CAM</h1>
        <p>Es la tecnología más innovadora disponible
            en prótesis fija y supone un gran adelanto con relación a la Odontología convencional.
            La empleamos para fabricar incrustaciones de porcelana, coronas y puentes, prótesis
            sobre implantes y en otros tratamientos restauradores indirectos.</p>
        <br/>
      footer:
        icon:
          display: false
          img: /uploads/sections-prosthodontics-icon-ceramic.jpg
        link:
          display: true
          to: /especialidades/protesis/tecnologia-cad-cam/
          placeholder: <span>Más Información</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
