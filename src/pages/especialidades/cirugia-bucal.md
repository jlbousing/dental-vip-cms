---
templateKey: specialties-page
language: es
redirects: /en/specialties/oral-surgery/
title: Cirugía Bucal
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-oral-surgery.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 38%
  content:
    position: center
    body: >
      <h1 class="bebas">Cirugía Bucal</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-oral-surgery"></i></h1>
    <p class="thin"><em>Evite dolor, complicaciones y malos ratos. Un verdadero Cirujano estará
    en capacidad de ejecutar una intervención limpia, segura, rápida, precisa y sin
    apenas secuelas postoperatorias.</em></p>

# Description Section
article:
  content: >
    <p><p>A pesar de que la boca representa una región pequeña y limitada del cuerpo
    humano, se pueden presentar en ella <b>gran cantidad de entidades patológicas
    congénitas y/o adquiridas que requieren tratamiento quirúrgico electivo.</b> Entre
    las más comunes de las que ambulatoriamente se tratan en nuestra clínica podemos
    mencionar la extracción de los terceros molares incluidos <i>(cordales)</i>, extracciones
    dentarias simples, complejas y múltiples; colocación de IMPLANTES DENTALES, frenilectomías,
    apicectomías y cirugías periapicales, extirpación y biopsia de lesiones orales
    menores y quistes dentígeros; entre otras.</p><p><b>Usualmente todos estos procedimientos
    son efectuados bajo anestesia local;</b> sin embargo, eventualmente se realizan
    en conjunto con un Médico Anestesiólogo bajo SEDACIÓN CONSCIENTE, sobre todo en
    pacientes excesivamente nerviosos o con algún compromiso de tipo físico o emocional.
    <b>Su objetivo es lograr que las personas entren en un estado de tranquilidad
    y relajación profunda mientras dure el acto clínico.</b></p><p><b>Aunque la
    finalidad de la intervención puede variar, las técnicas empleadas son siempre
    muy similares;</b> e implican, previa anestesia, la incisión de la encía, su desprendimiento
    en mayor o menor grado, la extirpación de los tejidos patológicos o excedentes,
    la colocación de implantes o injertos biocompatibles y, por último; el cierre
    y sutura de la herida. Será siempre imprescindible, para el éxito final del procedimiento,
    que el operador conozca en detalle la función específica y aplicación clínica
    del numeroso instrumental quirúrgico de DIÉRESIS, EXCÉRESIS Y SINÉRESIS.</p><p><b>La
    Cirugía Bucal es ciencia, arte y habilidad,</b> y aunque está regida por los mismos
    principios de la cirugía general; tiene sus propias peculiaridades que emanan
    de la zona anatómica a tratar. No en vano, <b>es la Especialidad más antigua de
    la Odontología.</b></p>
  img: /uploads/aside-oral-surgery.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
     Por norma general, las extracciones de las cordales ectópicas, incluidas
    o semierupcionadas son más fáciles de realizar en pacientes menores de 25 años;
    antes de que sus raíces se hayan desarrollado por completo.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Cirujano Bucal
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-oral-surgery.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-oral-surgery.jpg
  mobilePosition: '{"mobilePosition320":"-376px","mobilePosition360":"-376px","mobilePosition375":"-587px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: <h4>¿Por qué elegir a un Especialista en Cirugía Bucal?</h4>
      content: >-
        <p>Porque es un amplio conocedor de la anatomía y patología oral, porque
        ejerce gran dominio sobre los principios médico-quirúrgicos contemporáneos
        y porque posee gran habilidad y destreza en el manejo de las técnicas
        clínicas más innovadoras de la Especialidad; cualidades todas que le
        confieren el nivel de experto en la materia.</p>
    - title: <h4>¿Son muy dolorosos los actos quirúrgicos realizados en la boca?</h4>
      content: >-
        <p>La evolución que la Odontología ha experimentado en las últimas décadas
        nos permite afirmar que, en condiciones normales, la molestia asociada a
        cualquier acto quirúrgico es verdaderamente pequeña. Aunque todos los
        procedimientos de Cirugía Oral son esencia invasivos, la realidad es que
        el postoperatorio es perfectamente controlable. La aplicación de una
        técnica anestésica efectiva, de un acto quirúrgico controlado y la
        adopción de un protocolo postoperatorio eficaz; reducirán al mínimo el
        riesgo y la posibilidad de dolor, infección e inflamación de la zona
        afectada.</p>
    - title: >-
        <h4>¿Es necesario someterse a exámenes de sangre antes de una
        intervención?</h4>
      content: >-
        <p>Aunque sean extremadamente raras las complicaciones quirúrgicas en
        pacientes jóvenes y aparentemente sanos, nunca estará de más contar con un
        perfil hematológico preoperatorio que evalúe la capacidad de coagulación
        sanguínea y cicatrización tisular. De modo que un análisis de sangre será
        rutinario como requisito previo a la intervención.</p>
    - title: <h4>¿Debe el paciente acudir en ayunas?</h4>
      content: >-
        <p>¡Por el contrario! Si el procedimiento va a ser realizado bajo
        anestesia local, debemos siempre evitarlo. El ayuno es únicamente
        necesario en casos de anestesia general.</p>
    - title: <h4>¿Cuáles otros cuidados preoperatorios son recomendables?</h4>
      content: >-
        <p>Generalmente sugerimos utilizar ropa fresca y ligera durante el
        procedimiento, cepillar bien los dientes, encías y mucosas antes de la
        intervención; y contar con un acompañante adulto que le pueda prestar el
        apoyo necesario.</p>
    - title: "<h4>Para una Cirugía Oral, ¿me podrían suministrar anestesia general?</h4>"
      content: >-
        <p>Solo en los casos de Cirugía Mayor como la Ortognática y Maxilofacial.
        Por su gran efectividad, seguridad y ausencia casi absoluta de efectos
        secundarios; la anestesia local troncular o infiltrativa es la de elección
        para las intervenciones de Cirugía Oral menor en medios ambulatorios. La
        sedación consciente con derivados de la benzodiazepina es la mejor opción
        para pacientes especiales o excesivamente aprensivos; sin embargo,
        requiere de la participación de un Médico Anestesiólogo.</p>
    - title: <h4>¿Qué es una extracción atraumática y cuál es su importancia?</h4>
      content: >-
        <p>Los métodos quirúrgicos atraumáticos para la extracción de dientes
        tienen como objetivo preservar intacto el hueso que rodea a la raíz
        dental. Esta filosofía de trabajo se fundamenta en un conjunto de
        principios muy bien definidos y en un instrumental especialmente diseñado
        para preservar el alvéolo y su cresta ósea, favoreciendo así la ulterior
        colocación de implantes dentales. En implantología oral el hueso es oro, y
        si hay hueso, tenemos que cuidarlo.</p>
    - title: >-
        <h4>Al extraer un diente, ¿se puede colocar un implante de forma
        inmediata?</h4>
      content: >-
        <p>A veces sí, a veces no. Los implantes inmediatos son insertados en el
        mismo acto quirúrgico en el que se realizan las extracciones de los
        dientes a reemplazar. Aunque son muy deseables y ofrecen grandes ventajas,
        no pueden ser colocados en presencia de lesiones apicales extensas y/o
        patología periodontal activa.</p>
    - title: <h4>¿Es siempre necesario extraer las cordales o muelas del juicio?</h4>
      content: >-
        <p>¡Definitivamente no! Deben extraerse cuando no sean capaces de
        erupcionar por falta de espacio y permanezcan entonces incluidas, cuando
        generen dolor o molestias crónicas, cuando erupcionen en mala posición,
        cuando interfieran con la oclusión o mordida o cuando padezcan lesiones
        cariosas de considerable extensión.</p>
    - title: <h4>¿Qué es un diente incluido?</h4>
      content: >-
        <p> La inclusión dentaria es una alteración del desarrollo en la que un
        determinado diente, llegada la época normal de su erupción, permanece en
        el interior de los tejidos de la cavidad oral <em>(hueso alveolar y mucosa
        bucal)</em>. Los dientes que padecen con mayor frecuencia esta situación
        son los terceros molares <em>(muelas del juicio o cordales)</em>;
        principalmente los del maxilar inferior. El diagnóstico de la inclusión de
        un diente solo puede ser realizado recurriendo a exámenes radiológicos,
        usualmente una radiografía panorámica dental-sinusal.</p>
    - title: <h4>¿Deben ser siempre extraídos?</h4>
      content: >-
        <p>Sí en la gran mayoría de los casos; sin embargo, no existe una regla
        general para tomar la decisión. Los diferentes criterios deben ser
        ponderados en cada situación clínica particular, sopesando muy bien los
        riesgos y beneficios de la intervención.</p>
    - title: <h4>¿En qué consiste una frenilectomía o frenectomía?</h4>
      content: >-
        <p>Consiste en una pequeña operación en la cual se reposiciona el frenillo
        lingual o labial para que no interfiera con la correcta posición de los
        dientes y/o movilidad de la lengua. Las inserciones labiales muy bajas o
        muy fibrosas generan presión y espacios antiestéticos entre los incisivos
        centrales <em>(diastemas)</em>. Las inserciones linguales anómalas generan
        serias alteraciones de fonética y dicción.</p>
    - title: <h4>¿Son comunes los quistes y tumores de la cavidad oral?</h4>
      content: >-
        <p>Afortunadamente, son patologías de muy baja prevalencia. Sin embargo,
        una vez detectada su existencia, se hace imperativo estudiar el caso y
        determinar la conducta terapéutica a implementar.</p>
    - title: <h4>¿Qué es una biopsia y para qué se realiza?</h4>
      content: >-
        <p>Es un estudio microscópico de un trozo de tejido o una porción de
        líquido orgánico que se extrae de un ser vivo. Algunas biopsias involucran
        la remoción de una cantidad pequeña de tejido con una aguja, mientras que
        otras abarcan la remoción quirúrgica de la lesión entera. Usualmente, una
        biopsia se realiza para el despistaje del cáncer oral o para el
        diagnóstico de úlceras, erosiones y ampollas que no muestren evidencia de
        curación espontánea en un lapso de entre 5 y 10 días, nódulos de
        crecimiento rápido, lesiones negras, lesiones blancas, lesiones rojas con
        sospecha de eritroplasia, lesiones sin diagnóstico definido, y en algunos
        casos; para el estudio de enfermedades infecciosas, micóticas y
        bacterianas.</p>
    - title: <h4>¿En qué consiste una apicectomía?</h4>
      content: >-
        <p>La apicectomía es un procedimiento quirúrgico cuyo objetivo es eliminar
        el ápice <em>(extremo)</em> de la raíz de una pieza dental para solventar
        un proceso infeccioso localizado de origen endodóntico. Este tipo de
        cirugía se realiza únicamente si han fracasado otros tratamientos más
        conservadores como la endodoncia convencional, si es imposible acceder al
        ápice de la raíz, si existen falsos conductos en el diente afectado, o
        cuando dentro de él; se haya fracturado algún instrumento de uso
        intraradicular.</p>
    - title: <h4>¿Cuándo están indicados los injertos óseos?</h4>
      content: >-
        <p>Usualmente en casos de enfermedad periodontal avanzada y en los casos
        en que la cantidad y calidad del hueso receptor no sean favorables para la
        colocación de implantes dentales. Aunque existen varios tipos de injertos,
        generalmente se ejecuta una técnica mixta. Se suele colocar hueso autólogo
        tomado del mismo paciente <em>(autoinjerto)</em> y hueso liofilizado de
        origen animal <em>(xenoinjerto)</em>.</p>
    - title: >-
        <h4>¿Es normal la inflamación después de un procedimiento de Cirugía
        Oral?</h4>
      content: >-
        <p>Es natural experimentar cierta hinchazón después de cualquier cirugía.
        Un corte quirúrgico en la piel o mucosa, también llamado incisión, es una
        forma de agresión al organismo. La respuesta natural del cuerpo a una
        lesión de este tipo es el proceso inflamatorio, que como sabemos; cursa
        con aumento de volumen. Sin embargo, si la inflamación llegase a ser
        exagerada, contacte de inmediato con su Especialista.</p>
    - title: >-
        <h4>¿Es necesario tomar antibióticos, analgésicos y
        antiinflamatorios?</h4>
      content: >-
        <p>Depende de la situación. Los procedimientos simples rara vez requieren
        medicación postoperatoria; sin embargo, las intervenciones más complejas
        sí. Generalmente prescribimos un antibiótico de amplio espectro y un
        analgésico antiinflamatorio no esteroideo <em>(AINE)</em>.</p>
    - title: "<h4>En caso de hemorragia, ¿cuál es la conducta a seguir?</h4>"
      content: >-
        <p>Es normal sufrir un ligero sangrado durante las primeras 24 horas. Si
        ese sangrado fuese más intenso <em>(hemorragia)</em>, se deberá entonces
        doblar una o más gasas, colocarlas sobre la herida y comprimirlas
        mordiendo suavemente con los dientes antagonistas hasta que pare la
        sangre. Si fuese necesario colocar más gasas, nunca retire las primeras;
        aplique otras nuevas sobre estas. Coloque hielo y evite acostarse. Si el
        problema persistiese, contacte de inmediato con su Especialista.</p>
    - title: <h4>¿Por cuánto tiempo deben permanecer las suturas en boca?</h4>
      content: >-
        <p>Todo depende del caso y extensión del procedimiento. Por norma general,
        es suficiente con un período de entre 7 y 10 días; sin embargo, podría
        extenderse por más tiempo. En la actualidad es muy común el uso de suturas
        reabsorbibles que, por ser biodegradables; no requieren de una segunda
        intervención clínica para su remoción.</p>

cases:
  title: >
    <h1>Cirugía Bucal - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-oral-surgery-es-01.jpg
      - /uploads/clinic-cases-oral-surgery-es-02.jpg
      - /uploads/clinic-cases-oral-surgery-es-03.jpg
      - /uploads/clinic-cases-oral-surgery-es-04.jpg
      - /uploads/clinic-cases-oral-surgery-es-05.jpg
      - /uploads/clinic-cases-oral-surgery-es-06.jpg
      - /uploads/clinic-cases-oral-surgery-es-07.jpg
      - /uploads/clinic-cases-oral-surgery-es-08.jpg
      - /uploads/clinic-cases-oral-surgery-es-09.jpg
      - /uploads/clinic-cases-oral-surgery-es-10.jpg
      - /uploads/clinic-cases-oral-surgery-es-11.jpg
      - /uploads/clinic-cases-oral-surgery-es-12.jpg
      - /uploads/clinic-cases-oral-surgery-es-13.jpg
      - /uploads/clinic-cases-oral-surgery-es-14.jpg
      - /uploads/clinic-cases-oral-surgery-es-15.jpg
      - /uploads/clinic-cases-oral-surgery-es-16.jpg
      - /uploads/clinic-cases-oral-surgery-es-17.jpg
      - /uploads/clinic-cases-oral-surgery-es-18.jpg
      - /uploads/clinic-cases-oral-surgery-es-19.jpg
      - /uploads/clinic-cases-oral-surgery-es-20.jpg
      - /uploads/clinic-cases-oral-surgery-es-21.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Exposición de implantes Dentales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracción de Cordal Inferior </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Exodoncia, Injerto de Hueso e Implante Inmediato </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracciones Múltiples </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Frenilectomía </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Regeneración ósea con hueso autólogo </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Exposición de Canino Incluido </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sinus Lift </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Biopsia Excisional </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Apicectomía </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Implantología con Plasma Rico en Fibrina (PRF) </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Eliminación de Torus Mandibulares </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Elevación de Seno Maxilar e Implantes Dentales </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Regeneración Tisular Guiada </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Reconstrucción de Tabla Ósea Vestibular </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracción Simple de Restos Radiculares </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Resección Quirúrgica de Lesión Fibromatosa </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pilar de Cicatrización </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-19-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Corticotomía para Expansión de Reborde Alveolar </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-20-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Odontectomía de Cordal Superior Incluida </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-oral-surgery-es-21-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Extracción de un Primer Molar Superior </h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">FUI SOMETIDO A VARIAS EXTRACCIONES Y DOS INJERTOS DE HUESO COMO PREPARACIÓN
    PREVIA A LA COLOCACIÓN DE IMPLANTES DENTALES, Y DE VERDAD, NO ME PUDO IR MEJOR
    CON EL DR. GARABÁN. ES MUY SUTIL EN SU TRABAJO Y SE PERCIBE DE INMEDIATO SU GRAN
    DESTREZA Y EXPERIENCIA PROFESIONAL".</p>
  images:
    portrait: /uploads/quotes-oral-surgery-portrait.jpg
    landscape: /uploads/quotes-oral-surgery.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
