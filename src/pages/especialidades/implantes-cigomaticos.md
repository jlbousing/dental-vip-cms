---
templateKey: specialties-page
language: es
redirects: /en/specialties/zygomatic-implants/
title: Implantes Cigomáticos
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-zygomatic-implants.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">Implantes Cigomáticos</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-zygomatic-implants"></i></h1>
    <p class="thin"><em>Sin duda, la mejor alternativa para aquellos pacientes con Gran Pérdida
    de Hueso en sus maxilares y en los que ya no es posible colocar implantes dentales
    convencionales. “Una solución óptima a un problema complejo”.</em></p>

# Description Section
article:
  content: >
    <p>  Cuando se pierden uno o varios dientes, y no son reemplazados a la
    brevedad,  se inicia de inmediato un lento pero progresivo proceso de reabsorción
    a nivel  del hueso maxilar que va mermando en ancho y alto la cantidad de estructura
     ósea disponible, y necesaria, para la colocación de implantes dentales. “Lo
     que no se usa se atrofia”, y es precisamente lo que le sucede al REBORDE  MAXILAR
    EDÉNTULO, naturalmente, a consecuencia de la falta de estimulación por  ausencia
    de cargas funcionales. De modo que  <strong>    si transcurre mucho tiempo
    entre la ablasión dental y la intención de    rehabilitación, es muy probable
    que para entonces ya no exista el suficiente    material óseo capaz de garantizar
    un buen anclaje primario y una adecuada    oseointegración de los dispositivos
    convencionales  de titanio</strong>, que una vez implantados, deberían funcionar
    como sustitutos  artificiales de las raíces dentarias y sólidos soportes para
    la fijación de  estructuras protésicas.</p> <p>  Afortunadamente en muchos
    casos, cuando el defecto del reborde es leve y bien  localizado, es posible
    recurrir sin inconvenientes al emplazamiento de  INJERTOS autólogos o sintéticos
    como paso previo o simultáneo a la colocación  de implantes dentales. Sin embargo,
    si la pérdida ósea es muy grande y  generalizada, estos procedimientos suelen
    volverse muy incómodos, complejos,  extensos e impredecibles; ya que implican
    la necesidad de generar una gran  herida corporal a nivel de pelvis o cabeza
    para recolectar tacos de hueso de  cresta ilíaca o calota, altas posibilidades
    de rechazo por la crueldad propia  de la técnica quirúrgica y un período de
    cicatrización tisular que podría  abarcar hasta 5 o 6 meses de espera, antes
    de poder continuar con el  tratamiento.</p> <p>  Ante tales eventualidades,
     <strong>es muchas veces preferible olvidarse del maxilar y de los injertos,
    y fijar    los implantes a otros huesos de la región orofacial</strong>, siendo
    por su compacta estructura y cercanía anatómica los huesos MALAR O  CIGOMÁTICO
    <em>(que forma el pómulo)</em> y Esfenoides, los de preferencia. De modo que
     <strong>los Implantes Cigomáticos no son más que dispositivos dentales especiales,
       mucho más largos que los convencionales</strong>  <em>(entre 30 y 55 mm.
    de longitud) </em>y que nos permiten elaborar sin  inconvenientes dentaduras
    fijas aún en los casos de atrofia maxilar severa o  avanzada; y es por ello
    que, en conjunto con los Implantes Pterigoideos  <em>(muy similares a los Cigomáticos
    pero que se anclan a nivel de la sutura    pterigomaxilar)</em>, se les conoce
    también en el argot popular como  <strong>“Implantes Dentales para Pacientes
    con Poco Hueso”.</strong> Y es que  en definitiva, la relativa sencillez de
    su implantación  <em>(en relación a la colocación de injertos múltiples)</em>,
    la ausencia de  morbilidad de una zona donante y un periodo de curación mucho
    más corto y  favorable para el paciente, sugieren a estos IMPLANTES LARGOS como
    una  alternativa más simple y segura para casos extremos, en comparación con
    otras  técnicas de regeneración ósea e implantología oral.</p>
  img: /uploads/aside-zygomatic-implants.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      El tratamiento con Implantes Cigomáticos es un procedimiento que implica
    una intervención de Cirugía Maxilofacial, en block quirúrgico de ambiente hospitalario,
    anestesia general y todos los recursos de soporte vital que, indiscutiblemente,
    amerita el caso.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
         Cirujano Maxilofacial
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-zygomatic-implants.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-zygomatic-implants.jpg
  mobilePosition: '{"mobilePosition320":"-171px","mobilePosition360":"-171px","mobilePosition375":"-171px"}'
  content: >
    <i></i>
accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: >-
        <h4>¿En qué consiste la fase de estudio y planificación del
        tratamiento?</h4>
      content: >-
        <p>En realidad es muy amplia y abarca muchas consideraciones, pero una de
        las más relevantes es la valoración del estado de la estructura ósea y
        dental del paciente a través de una tomografía volumétrica en 3D. Esta
        prueba proporciona al Especialista valiosa información y le permiten, a
        priori, constatar que la persona realmente no puede ser tratada con
        implantes dentales convencionales.</p>
    - title: >-
        <h4>¿Qué pacientes pueden someterse a esta técnica de implantología
        oral?</h4>
      content: >-
        <p>En principio, todos aquellos que hayan perdido dientes y mucho volumen
        óseo en sus estructuras maxilares, y que por supuesto; aspiren a una
        rehabilitación de mucha más calidad que la que le pueda ofrecer cualquier
        opción con dentaduras removibles.</p>
    - title: <h4>¿Cuáles son las causas de la pérdida del hueso?</h4>
      content: >-
        <p>Son múltiples, pero entre las más comunes encontramos la edad, la
        enfermedad periodontal crónica, infecciones de origen odontogénico,
        accidentes y traumatismos faciales, extracción traumática de dientes y
        restos radiculares, uso prolongado de dentaduras removibles, quistes e
        infecciones respiratorias o de senos paranasales, resección de procesos
        tumorales, y por supuesto; la reabsorción fisiológica a consecuencia del
        desinterés o imposibilidad de reemplazar en tiempo razonable las piezas
        dentales perdidas.</p>
    - title: <h4>¿Existe algún límite de edad para optar al procedimiento?</h4>
      content: >-
        <p>Ninguna, siempre y cuando las expectativas de vida de la persona
        justifiquen la inversión de tiempo y dinero. De hecho, gran parte de
        nuestros pacientes sobrepasan los 65 años de edad.</p>
    - title: <h4>¿Qué porcentaje de éxito tiene en la actualidad?</h4>
      content: >-
        <p>¡Extraordinario!, superior al de los implantes convencionales y muy
        cercano al 100 % de los casos, con una tasa de supervivencia media del 95%
        a los 10 años.</p>
    - title: <h4>¿Es un tratamiento doloroso?</h4>
      content: >-
        <p>En lo absoluto, ya que casi siempre se efectúa bajo anestesia general.
        Además, el postoperatorio suele ser muy llevadero, con pocas molestias y
        permite que el paciente se reincorpore en muy corto plazo a su vida
        cotidiana. Aun así, la prescripción de un protocolo analgésico
        completamente individualizado será de rutina y garantizará un control
        efectivo del dolor postquirúrgico en los casos más rebeldes. </p>
    - title: <h4>¿Podría realizarse solo con sedación y anestesia local?</h4>
      content: >-
        <p>Ocasionalmente, y solo en condiciones quirúrgicas muy favorables. Sin
        embargo, para su tranquilidad y la nuestra, en DENTAL VIP jamás
        desestimamos la delicadeza del procedimiento y siempre preferimos la
        anestesia general en block quirúrgico y ambiente hospitalario para este
        tipo de intervenciones.</p>
    - title: <h4>¿De qué material están fabricados estos implantes?</h4>
      content: >-
        <p>De aleaciones de titanio para alto rendimiento. El grado de pureza del
        titanio viene determinado por el porcentaje máximo de oxígeno en el que se
        produce, siendo el de grado 1 el de mayor pureza, ya que su contenido en
        oxígeno y hierro es muy bajo. En total y al día de hoy, existen unos 40
        grados para denominar las diferentes aleaciones de titanio, que abarcan
        desde el titanio puro hasta combinaciones con vanadio, paladio, rutenio,
        aluminio, estaño y molibdeno. Sin embargo, la aleación de titanio grado 5
        representa a la fecha más del 50% del uso total de titanio a nivel mundial
        y se considera un material altamente biocompatible y con cualidades
        biomecánicas superiores al titanio puro.</p>
    - title: <h4>¿Cuáles son sus verdaderas ventajas?</h4>
      content: >-
        <p>En comparación con las tradicionales técnicas de implantología oral,
        que contemplan heroicas reconstrucciones alveolares con injertos autólogos
        múltiples, los <strong>Implantes Largos</strong> ofrecen las siguientes
        ventajas:</p> <ol>  <li>Evitan el alto riesgo de rechazo asociado a los
        injertos maxilares. </li>  <li>Reducen considerablemente el tiempo total
        de tratamiento. </li>  <li>Implican un procedimiento mucho más
        conservador, seguro y predecible. </li>  <li>Mayores probabilidades de
        éxito, ya que el hueso malar es muy compacto y prácticamente inmune a la
        reabsorción ósea <em>(no así los injertos)</em>. </li>  <li>Menor
        morbilidad, ya que no es necesario intervenir en regiones anatómicas
        extraorales para recolectar hueso.  </li>  <li>Alta posibilidad de carga
        inmediata <em>(prótesis fija el mismo día)</em>.  </li> 
        <li>Postoperatorio más corto y favorable para el paciente.  </li> 
        <li>Mejor pronóstico a corto, mediano y largo plazo.  </li>  </ol>
        <p>Además, el protocolo de Implantes Cigomáticos garantiza la función de
        barrera, estabilidad biomecánica y correcta distribución de las fuerzas
        masticatorias a través de los pilares maxilomalares; evitando así las
        sobrecargas y el fracaso ocasional por fatiga. </p>
    - title: <h4>¿Cuáles riesgos y contraindicaciones existen?</h4>
      content: >-
        <p>La mayoría de los estudios citan a la sinusitis y comunicación
        bucosinusal como las complicaciones postoperatorias que aparecen con mayor
        frecuencia, con una incidencia de 4,93 por cada 100 implantes colocados.
        Sin embargo, se considera una prevalencia tan baja que corrobora aún más
        la alta tasa de éxito que se consigue con estos implantes. En relación a
        las contraindicaciones, tenemos que, sin tomar en consideración cualquier
        condición o enfermedad sistémica que pudiera impedir un acto quirúrgico,
        son muy pocas, y generalmente todas asociadas a patologías o infecciones
        preexistentes dentro del seno o antro maxilar. De modo que en algunos
        casos, la interconsulta y tratamiento previo con un Otorrinolaringólogo
        pudiera ser imprescindible. </p>
    - title: <h4>¿Cómo es y cuánto tiempo dura la cirugía?</h4>
      content: >-
        <p>Las 4 técnicas más destacadas son la de Brånemark, la de Stella y
        Warner <em>(Sinus Slot Technique)</em>, la técnica paramaxilar o
        extrasinusal y la técnica ZAGA <em>(Zygoma Anatomy-Guided Approach)</em>
        del Dr. Carlos Aparicio. En la técnica clásica o de Brånemark, se colocan
        los Implantes Cigomáticos maniobrando a través del seno maxilar, hasta
        alcanzar el arco cigomático. En la de Stella y Warner, se logra una
        visualización más directa de la base del hueso malar, gracias a la
        apertura de una amplia ventana en la pared lateral del antro paranasal. En
        la extrasinusal, los implantes se colocan por fuera del hueso maxilar, sin
        involucrar al seno; y en la metodología ZAGA, se contempla cualquier
        combinación posible de las 3 técnicas anteriores, con la finalidad de
        ejecutar una cirugía más individualizada y adaptada a la anatomía
        particular de cada persona.</p> <p>Lógicamente, en función de la
        complejidad del caso, número de dispositivos a implantar, tipo de abordaje
        seleccionado, y por supuesto, la destreza del Cirujano; entre 30 y 120
        minutos suele oscilar el período de tiempo necesario para anestesiar,
        hacer incisiones, diseccionar tejidos, estipular medidas y aproximaciones,
        seleccionar los implantes, fresar y perforar, atornillar, limpiar y
        suturar la herida.</p>
    - title: <h4>¿Puede cualquier Especialista realizar este tipo de operación?</h4>
      content: >-
        <p>Este procedimiento, aunque no es exclusivo de nuestra clínica, no es
        tan común en la práctica habitual, debido a que muy pocos Odontólogos
        están verdaderamente capacitados para brindar estos tratamientos. La
        habilidad quirúrgica pertinente para la colocación de Implantes
        Cigomáticos requiere de un alto nivel de experiencia, impecable dominio de
        la anatomía de cabeza y cuello y un entrenamiento quirúrgico muy avanzado.
        De modo que solo un Cirujano Maxilofacial, realmente experimentado, será
        el profesional indicado.</p>
    - title: <h4>¿Me podría quedar alguna marca o cicatriz en la cara?</h4>
      content: >-
        <p>¡Jamás!, ya que el procedimiento se ejecuta en su totalidad por vía
        intraoral.</p>
    - title: <h4>¿Es luego necesario permanecer hospitalizado?</h4>
      content: >-
        <p>Muy rara vez, y solo en caso de cualquier imprevisto o complicación
        quirúrgica que requiera de cuidados especiales.</p>
    - title: >-
        <h4>¿Cómo es el postoperatorio y cuánto tiempo tarda el paciente en
        recuperarse?</h4>
      content: >-
        <p>El postoperatorio no suele revestir mayores complicaciones, en tanto
        que el paciente cumpla escrupulosamente con las indicaciones
        suministradas. Las pautas a seguir pueden variar según cada intervención,
        el tipo de abordaje utilizado y las condiciones particulares de cada caso;
        pero la medicación <em>(antibióticos, analgésicos y
        antiinflamatorios)</em>, aplicación local de frío, el reposo y la higiene
        oral son fundamentales para conseguir una recuperación lo más rápida y
        satisfactoria posible. Usualmente, entre 2 y 5 días es el tiempo que
        tardan la mayoría de las personas en reiniciar sus actividades
        habituales.</p>
    - title: <h4>¿Son estos implantes más costosos que los implantes normales?</h4>
      content: >-
        <p>¡Indudablemente!, tomando en consideración que son hasta 4 veces más
        largos que los convencionales y se necesita mucho más titanio para su
        fabricación, requieren quirófano y anestesia general, instrumental y
        componentes quirúrgicos de diseño avanzado, una intervención de cirugía
        más larga e invasiva y la participación de un experto Cirujano
        Maxilofacial.</p>
    - title: <h4>¿Qué tipo de prótesis se puede después confeccionar?</h4>
      content: >-
        <p>Usualmente una prótesis híbrida de metal-acrílico o
        metal-porcelana.</p>
    - title: >-
        <h4>¿Solo se utilizan para fijar dentaduras completas, es decir, cuando
        faltan todos los dientes?</h4>
      content: >-
        <p>Tradicionalmente así era, ya que fue un procedimiento diseñado a tal
        fin, con 4 implantes en total, 2 a cada lado del maxilar. Sin embargo, en
        la actualidad se aplican modificaciones de la técnica original que
        permiten ubicar el Implante Cigomático o Pterigoideo en una posición
        adecuada y compatible con una rehabilitación en base a prótesis parcial
        fija <em>(coronas y puentes cerámicos)</em>. De hecho, hoy en día son
        múltiples las posibilidades que ofrecen estos implantes, desde la
        colocación de un único dispositivo asimétrico, hasta innumerables
        combinaciones con implantes convencionales, lógicamente para dar solución
        a la gran mayoría de los casos, y en algunos, hasta para reducir
        costes.</p>
    - title: >-
        <h4>¿Es posible colocar una prótesis fija el mismo día de la intervención,
        aunque sea una provisional?</h4>
      content: >-
        <p>Muchas veces sí, ya que el anclaje multicortical y el probado diseño
        del ápice de estos implantes proporcionan generalmente una gran
        estabilidad primaria. No obstante, es solo después de la cirugía que se
        decidirá si se instala una prótesis fija o una convencional con alivios en
        las zonas donde emerjan las plataformas intraorales. Luego, una vez
        transcurrido el período de oseointegración, al cabo de 4, 5 o 6 meses, es
        que se realiza la rehabilitación protésica definitiva.</p>
    - title: >-
        <h4>Culminado el tratamiento, ¿qué debo hacer para conservar mis implantes
        toda la vida?</h4>
      content: >-
        <p>Básicamente conservarse en buen estado de salud general, practicar muy
        buena higiene oral y acudir regularmente al Odontólogo. En estos casos, es
        muy recomendable realizar un seguimiento vitalicio <em>(clínico y
        radiográfico)</em> del paciente, para poder detectar a tiempo cualquier
        proceso infeccioso incipiente o desajuste de los componentes protésicos
        que pudieran generar males mayores. Además, usualmente recomendamos
        remover la prótesis cada 6 meses para inspeccionar y limpiar los implantes
        y aditamentos protésicos, el tejido blando que les rodea y la base o cara
        interna de la estructura protésica. </p>

cases:
  title: >
    <h1>Implantes Cigomáticos - Casos Clínicos</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-zygomatic-implants-es-01.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-02.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-03.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-04.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-05.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-06.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-07.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-08.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-09.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-10.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-11.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-12.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-13.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-14.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-15.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-16.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-17.jpg
      - /uploads/clinic-cases-zygomatic-implants-es-18.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Cigomático unilateral </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Carga inmediata </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Implantación mixta y función inmediata </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Control postquirúrgico y prótesis definitiva</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Resolución estética en reborde atrófico</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Técnica zaga</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Aspecto quirúrgico y postquirúrgico</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Técnica clásica de Brånemark</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Prótesis híbrida metal-acrílico</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Reimplantación</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Dentadura total implantosoportada </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Restitución de la plenitud facial </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>All-on-6 </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Técnica paramaxilar o extrasinusal</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Antes y después </h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Implantación con prf</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Inicio fase restauradora</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-zygomatic-implants-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: <h3>Híbrida metal-porcelana</h3>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">ESTA TÉCNICA ES UNA VERDADERA BENDICIÓN, Y MÁS AÚN LUEGO DE UNA EXPERIENCIA
    COMO LA MÍA, EN LA QUE POR DESCONOCIMIENTO O DESIDIA, UN ODONTÓLOGO DE MI PAÍS
    ME CONDENÓ A USAR POR AÑOS UNAS DENTADURAS REMOVIBLES QUE NO SE SUJETABAN POR
    NADA DEL MUNDO".</p>
  images:
    portrait: /uploads/quotes-zygomatic-implants-portrait.jpg
    landscape: /uploads/quotes-zygomatic-implants-landscape.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-3d-diagnosis.png
      content: >
        <h1 class="bebas">DIAGNÓSTICO Y PLANIFICACIÓN 3D</h1>
        <p>Nunca planificamos la cirugía con una simple
          placa radiográfica, ya que consideramos imprescindibles el uso de la Tomografía
          Axial Computarizada <i>(TAC)</i> y un programa informático que permita analizar
          estructuras, interpretar datos y determinar las proyecciones quirúrgicas más
          favorables.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/implantes-dentales/diagnostico-y-planificacion-3d
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-sedation.png
      content: >
        <h1 class="bebas">SEDACIÓN Y ANESTESIA GENERAL</h1>
        <p>Siempre bajo la responsabilidad de un Especialista
          en Anestesiología, reanimación y terapia del dolor, con habilidad de titular
          las drogas que se administren y experticia en el manejo de la vía aérea, monitoreo
          de signos vitales y aplicación de técnicas de resucitación.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /especialidades/sedacion-y-anestesia-general/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-prosthesis.png
      content: >
        <h1 class="bebas">PRÓTESIS SOBRE IMPLANTES</h1>
        <p>Al igual que con los implantes dentales convencionales,
        son variadas las opciones disponibles a la hora de rehabilitar Implantes Cigomáticos.
        Aunque el diseño y confección de dentaduras híbridas metal-acrílico suele ser
        lo más habitual, las estructuras de porcelana aplican con total propiedad.</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: especialidades/implantes-dentales/protesis-sobre-implantes/
          placeholder: <span> Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
