---
templateKey: specialties-page
language: es
redirects: /en/specialties/sedation-and-general-anesthesia/
title: Sedación y Anestesia General
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-sedation-and-general-anesthesia.jpg
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">Sedación y Anestesia General</h1>

# Heading Section
heading:
  display: true
  content: >
    <h1 class="title"><i class="icon icon-sedation-and-general-anesthesia"></i></h1>
    <p class="thin"><em>Sus principales objetivos son los de Garantizar el Bienestar y Confort
    del Paciente, suprimiendo por completo el dolor, el malestar físico y el estrés
    psicológico asociado a cualquier acto médico o quirúrgico.</em></p>

# Description Section
article:
  content: >
    <p>
      A pesar de todos los avances que ha experimentado la Odontología en los
      últimos años,
      <strong>
        el miedo y la ansiedad provocados por el tratamiento dental continúan siendo
        dos aspectos muy comunes en los pacientes de todo el mundo.
      </strong>
      Tanto así, que a través de estudios estadísticos recientes se ha podido
      demostrar que hasta un 25% de los adultos evitan o postergan concurrir a la
      consulta del Odontólogo por temor a sufrir dolor, constituyendo la
      circunstancia una verdadera barrera psicológica que impide, en ocasiones;
      recibir la atención necesaria y preservar la salud oral. Y aunque las
      estrategias de manejo del comportamiento son útiles en la gran mayoría de los
      casos, no son siempre efectivas, sobre todo al enfrentar a
      <strong>
        individuos excesivamente nerviosos, aprensivos o que presenten
        discapacidades que les impidan cooperar.
      </strong>
      Y es precisamente en esas vicisitudes, en las que la interacción entre el
      profesional y el doliente falla, y en las que las técnicas de
      acondicionamiento habituales parecen no funcionar, en las que se debe
      considerar la posibilidad de aplicar recursos clínicos alternativos para
      MANEJAR LA ANSIEDAD y el estrés preoperatorio.
    </p>
    <p>
      <strong>Actualmente, la anestesia local por infiltración continúa siendo sin
        discusión alguna, el método más sencillo, frecuente y efectivo para
        dispensar y asegurar un tratamiento dental libre de dolor</strong>; sin embargo, es también incuestionable el hecho de que es un procedimiento
      que per se puede causar incomodidad y rechazo, debido a la necesidad de
      aplicar una o varias inyecciones dentro de la boca.
    </p>
    <p>
      Y es definitivamente en esas situaciones, en las que ni siquiera es posible
      tolerar una punción, en las que debemos recurrir a otras técnicas anestésicas
      como
      <strong> la Sedación Consciente, que representa una opción farmacológica válida, pertinente y muy eficaz para el control del miedo, el estrés y el dolor</strong>; viabilizando el tratamiento odontológico y evitando las típicas confrontaciones con el paciente pediátrico o adulto hipersensible. Además, una EXPERIENCIA ATRAUMÁTICA, prácticamente garantizará el retorno voluntario de estas personas a sus controles periódicos de rutina durante la infancia y la edad adulta.
    </p>
  img: /uploads/aside-sedation-and-general-anesthesia.jpg

# Quote component structure
quote:
  body: >
    <p>
      <em>
      Tanto la Sedación Consciente (SC) como la Anestesia General (AG) son procedimientos
    seguros y confiables, claro está, siempre y cuando se practiquen en condiciones
    ideales de infraestructura y dotación clínica, y por supuesto; bajo la responsabilidad
    de un Equipo Médico Altamente Especializado.
      </em>
    </p>
  footer:
    author: >
      <strong>Dr. Castor José Garabán Povea</strong>
    details: >
      <strong>
        Anestesiólogo
        <br>
        DENTAL VIP, <wbr/>Especialidades Odontológicas s.c.
      </strong>

plainparallax: /uploads/parallax-sedation-and-general-anesthesia.jpg
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-sedation-and-general-anesthesia.jpg
  mobilePosition: '{"mobilePosition320":"-166px","mobilePosition360":"-193px","mobilePosition375":"-309px"}'
  content: >
    <i></i>

accordionList:
  display: true
  title: >
    Preguntas Frecuentes
  items:
    - title: "<h4>Entre la anestesia local y general, ¿cuál es preferible?</h4>"
      content: >-
        <p>Por su gran efectividad, seguridad y ausencia casi absoluta de efectos
        secundarios, la anestesia local troncular o infiltrativa es la de elección
        para las intervenciones de rutina en Odontología y de cirugía oral menor
        en medios ambulatorios; mientras que la sedación consciente inhalatoria o
        con narcóticos intravenosos es la mejor opción para pacientes especiales o
        excesivamente aprensivos. La anestesia general solo es necesaria en los
        casos de cirugía mayor como la ortognática y maxilofacial.</p>
    - title: <h4>¿En qué consiste la Anestesia General?</h4>
      content: >-
        <p>La AG se puede definir como la intoxicación inducida, controlada y
        reversible del sistema nervioso central que produce inconsciencia, amnesia
        temporal, analgesia, pérdida de la sensibilidad, relajación muscular y
        supresión de los reflejos autónomos y sensoriales. Las drogas que se
        suministran cuentan con propiedades hipnóticas y pueden aplicarse de
        distintas maneras, pero generalmente suelen utilizarse las vías
        inhalatoria y endovenosa. </p>
    - title: <h4>¿Qué es la Sedación Consciente?</h4>
      content: >-
        <p>Es una técnica anestésica en la que se administra a los pacientes la
        combinación de uno o varios medicamentos que provocan una leve depresión
        del sistema nervioso central, sin pérdida de la consciencia, pero con
        alteración de la misma. Tiene efecto sobre el dolor, ya que al disminuir
        la ansiedad se eleva el umbral doloroso, facilita la administración del
        anestésico local sin que la persona se entere y, a la dosis correcta;
        produce también amnesia, de forma que el paciente tenga poco o nada que
        recordar del procedimiento.</p>
    - title: <h4>¿Cuál es la diferencia entre ambas y cuál es mejor?</h4>
      content: >-
        <p>Básicamente que en la sedación se preservan los reflejos, el control de
        la respiración y la capacidad de respuesta a los estímulos táctiles y
        verbales. En la anestesia general, el paciente está dormido en un sueño
        profundo, y se requieren frecuentemente la protección de la vía aérea y la
        ventilación asistida. Además, la función cardiovascular suele verse
        también alterada, por lo que se hace pertinente la constante
        monitorización de los signos vitales. Ninguna es mejor que otra, y
        simplemente cada una tiene sus indicaciones. En Odontología, la anestesia
        general para los procedimientos más extensos, complejos e invasivos, como
        la cirugía maxilofacial o colocación de implantes cigomáticos por ejemplo;
        y la sedación, para la gran mayoría de las intervenciones orales en
        circunstancias particulares.<p>
    - title: <h4>¿Cuáles fármacos suelen utilizarse para sedar a un paciente?</h4>
      content: >-
        <p>Oxido Nitroso combinado con Oxígeno
        <em>(N<sub>2</sub>O:O<sub>2</sub>)</em> por vía inhalatoria, y por la
        endovenosa las Benzodiazepinas  como el Diazepam y el Midazolam, el
        Propofol, Tiopental Sódico, Fentanilo, Ketamina, Etomidato y/o cualquier
        combinación de ellos. Además, el anestésico local de preferencia,
        usualmente los pertenecientes al grupo amida; como la Lidocaína o
        Mepivacaína.</p>
    - title: <h4>¿Es la sedación un procedimiento 100% seguro?</h4>
      content: >-
        <p>La realización de cualquier acto médico puede tener siempre efectos
        adversos o indeseables, y la SC no es la excepción. Dentro de los posibles
        riesgos podemos encontrar las reacciones alérgicas, aspiración de
        secreciones a nivel pulmonar, hipoxia, hipoventilación, obstrucción de la
        vía aérea por cuerpos extraños y reacciones anómalas del sistema nervioso
        autónomo. Sin embargo, las complicaciones mencionadas rara vez se
        presentan, y en tal caso; la presencia de un Anestesiólogo garantizará una
        rápida y segura resolución de las mismas.</p>
    - title: <h4>¿Existen contraindicaciones a esta técnica de anestesia?</h4>
      content: >-
        <p>Realmente pocas, entre las que encontramos: historia de
        hipersensibilidad previa al procedimiento, insuficiencia respiratoria,
        insuficiencia hepática grave, embarazo, lactancia, alcoholismo, uso de
        estupefacientes, enfermedades psicóticas, oclusiones intestinales, algunos
        casos de glaucoma y/o cualquier otra condición sistémica que contraindique
        el uso de narcóticos.</p>
    - title: >-
        <h4>Si soy muy nervioso y solo me voy a arreglar una muela, ¿pueden
        dormirme completo?</h4>
      content: >-
        <p> Por supuesto que sí, sin embargo, habría que valorar muy bien si el
        coste y complejidad del tratamiento le compensarían verdaderamente en un
        caso tan sencillo como ese. Lo correcto es que antes de considerar la
        sedación, se hace pertinente agotar todos los medios persuasivos y
        relativos al condicionamiento de la conducta, los cuales muchas veces
        logran minimizar los niveles de ansiedad y modificar radicalmente la
        disposición hacia el tratamiento.</p>
    - title: >-
        <h4>¿En qué casos es entonces recomendable una sedación o una anestesia
        más profunda?</h4>
      content: >
        <p>Verdaderamente no existe un claro consenso sobre las indicaciones para
        la utilización de estas técnicas en Odontología, sin embargo, dependen del
        análisis objetivo y subjetivo de múltiples factores asociados con el
        paciente, el profesional y el tratamiento. Dentro de las indicaciones más
        comunes encontramos: </p> <ol>
          <li>Niños o adultos con experiencias previas médico-odontológicas traumatizantes, y en los que no es posible lograr una comunicación positiva ni la cooperación necesaria para el tratamiento.</li>
          <li>Pacientes alérgicos a los anestésicos locales.</li>
          <li>Niños y adultos con discrasias sanguíneas, ya que la anestesia infiltrativa o troncular podría provocar hemorragias en los espacios látero-faríngeos.</li>
          <li>Personas con retraso mental, trastornos psicomotores, genéticos o musculoesqueléticos; que impidan el tratamiento convencional en estado de consciencia.</li>
          <li>Pacientes con cardiopatías congénitas en los que se vaya a practicar un tratamiento extenso o que contemple la remoción de procesos sépticos dentarios, restauraciones múltiples o de cirugía maxilofacial.</li>
          <li>Pacientes médicamente comprometidos y que su condición general requiera alivio de la ansiedad para prevenir riesgos mayores. </li>
          <li>Situaciones en las que determinemos que la anestesia local no logrará el efecto deseado por el tamaño, ubicación de la lesión y/o duración del procedimiento; como por ejemplo, la colocación de implantes múltiples en ambos maxilares.</li>
          <li>Pacientes odontofóbicos o con verdadero pánico al Odontólogo.</li>
        </ol>
    - title: <h4>¿Pueden producir estas intervenciones algún efecto secundario?</h4>
      content: >-
        <p>Es muy difícil, ya que generalmente los fármacos se emplean con dosis
        muy bien controladas que se metabolizan por completo en poco tiempo,
        permitiendo que el paciente despierte con total normalidad, como si de una
        larga siesta se tratase. No obstante, para dar el alta; la persona debe
        estar consciente y orientada, hemodinámica y respiratoriamente estable y
        sin necesidad de ayuda para la marcha.</p>
    - title: <h4>¿Es necesario hacer algún tipo de estudio o evaluación previa?</h4>
      content: >-
        <p>La valoración preanestésica <em>(VPA)</em> es un protocolo de estudio
        que permite la evaluación del estado físico y de riesgo del paciente, para
        luego establecer un plan anestésico de acuerdo con sus condiciones
        particulares y reducir así la posibilidad de complicaciones. La VPA es
        obligatoria, ha demostrado su importancia y trascendencia en el campo de
        la Anestesiología y es un elemento principal de seguridad en la atención
        médica. Estudios recientes han demostrado que la falta de valoración del
        estado de los pacientes anestésicos influye en más del 70% de los
        accidentes intraoperatorios y fueron la causa de algunos fallecimientos
        ocurridos. La consulta preoperatoria debe tener lugar varios días antes de
        la intervención programada. El lapso previsto debe permitir la realización
        de las pruebas complementarias e interconsultas externas pertinentes,
        sesiones de terapia respiratoria en caso de ser requeridas, la abstinencia
        de tabaco y/o alcohol, e incluso; la administración de algún aporte
        nutricional específico.</p>
    - title: <h4>¿Puede aplicarse la SC libremente en el consultorio dental?</h4>
      content: >-
        <p>¡A NUESTRO CRITERIO JAMÁS!, a menos que el centro cuente con
        instalaciones, equipos y materiales que garanticen un apropiado cuidado
        del paciente, y que incluyan al menos: un ambiente de quirófano anexo al
        salón dental, aparatos y equipos de anestesia, vías aéreas artificiales y
        tubos endotraqueales de todos los diámetros, catéteres intravenosos,
        válvulas y bolsas de asistencia respiratoria, máscaras laríngeas de todos
        los tamaños, cánulas orofaríngeas y nasofaríngeas de variadas dimensiones,
        cilindros de gas medicinal, sistema avanzado de monitorización,
        electrocardiógrafo, laringoscopio y video laringoscopio, fibrobroncoscopio
        para intubaciones difíciles, estimulador de nervios periféricos, sistemas
        de suministro de oxígeno, sistema de purificación de gases, sistema de
        aspiración, equipos de reanimación cardiopulmonar <em>(RCP)</em>, sala de
        recuperación y planta eléctrica de emergencia con autonomía mínima de 3
        horas continuas. Además, en los casos de AG, es imprescindible que la sala
        de operaciones se encuentre integrada a una clínica privada u hospital de
        envergadura, que cuente con terapia intensiva, un equipo médico
        multidisciplinar permanente y el personal auxiliar capacitado para atender
        cualquier posible eventualidad.</p>  <p>Con sinceridad, no conocemos en
        nuestra ciudad capital ninguna clínica dental que cumpla al 100% con estas
        demandas y que esté verdaderamente en capacidad de proporcionar un
        ambiente seguro y eficaz para los tratamientos con sedación, y menos aún;
        con anestesia general. En DENTAL VIP jamás ponemos en riesgo la vida de
        nuestros pacientes y siempre preferimos intervenir estos casos en espacios
        físicos ajenos a nuestra infraestructura habitual.</p>
    - title: <h4>¿Qué medidas de seguridad o de precaución debo tomar?</h4>
      content: >-
        <p>Básicamente el ayuno, para evitar la regurgitación y aspiración
        pulmonar del contenido gástrico tras la inducción de la anestesia, durante
        el transcurrir del procedimiento o en el postoperatorio inmediato. Los
        protocolos actuales de ayuno preoperatorio coinciden en la duración del
        lapso de tiempo durante el cual no debe ingerirse sustancia alguna, e
        indican; 2 horas para los líquidos claros y 6 para los alimentos más
        sólidos. Entiéndase por líquidos claros solo el agua, zumo de frutas sin
        pulpa, bebidas carbonatadas, té claro y café negro.</p>
    - title: <h4>¿Debe estar siempre presente un Médico Anestesiólogo?</h4>
      content: >-
        <p>¡POR SUPUESTO! La responsabilidad de un paciente bajo sedación debe
        estar siempre a cargo de un Médico Especialista en Anestesiología,
        reanimación y terapia del dolor, con experiencia en técnicas
        infiltrativas, habilidad de titular las drogas que se administren y
        experticia en el manejo de la vía aérea, monitoreo de las constantes
        vitales y aplicación de técnicas de resucitación. ¡La presencia de un
        Anestesiólogo puede significar la diferencia entre la vida y la muerte,
        así de claro! </p>
    - title: <h4>¿Deben entubarme y colocarme un respirador artificial?</h4>
      content: >-
        <p>Solo en los casos de anestesia general. Sin embargo, al aplicar una SC,
        se deben tener siempre a mano todos los recursos profesionales y de
        soporte vital que permitan hacer frente a cualquier eventualidad y
        salvaguardar la vida del paciente, y entre los cuales; los tubos
        endotraqueales y dispositivos de ventilación mecánica son
        indispensables.</p>
    - title: <h4>¿Son muy elevados los costos de este tipo de anestesia?</h4>
      content: >-
        <p>Indudablemente suman a la cuenta y elevan el importe final del
        tratamiento. La ocupación y uso de una infraestructura verdaderamente
        diseñada y equipada a tal fin, la intervención de un Médico Anestesiólogo
        con su personal auxiliar y el traslado de los equipos y materiales
        dentales necesarios para cumplir el objetivo terapéutico, son variables
        que tendrán siempre un considerable impacto económico.</p>
    - title: "<h4>Si me van a sedar, ¿puedo ir solo o debo ir acompañado?</h4>"
      content: >-
        <p>Es necesario ir siempre acompañado, ya que es muy útil y reconfortante
        recibir apoyo físico y emocional luego del procedimiento, y además;
        imprescindible delegar la responsabilidad del traslado al lugar de
        residencia. Luego de una sedación, son frecuentes los sentimientos de
        torpeza, confusión y desorientación. </p>
    - title: <h4>¿Cuánto tiempo tardaré en despertarme luego de la intervención?</h4>
      content: >-
        <p>La sedación consciente es una técnica que permite el rápido retorno del
        paciente a su estado de normalidad, lo que hace posible darlo de alta sin
        mayores demoras luego de terminar el procedimiento. Sin embargo, es común
        experimentar cierto grado de somnolencia y cansancio, razón por la cual
        recomendamos esperar, al menos; una hora antes de abandonar las
        instalaciones. </p>
    - title: <h4>¿Qué cuidados postoperatorios debo tener?</h4>
      content: >-
        <p>Básicamente posponer por 24 horas cualquier actividad que requiera de
        coordinación mental, balance o equilibrio; tales como: conducir, operar
        maquinarias, hacer cálculos complejos o cualquier otra función que demande
        precisión psicomotriz.</p>
    - title: "<h4>Luego, ¿es necesario guardar algún tipo de reposo?</h4>"
      content: >-
        <p>Ninguno adicional al que amerite el postoperatorio anestésico de rutina
        <em>(24 horas)</em> y el tratamiento dental dispensado.</p>

cases:
  title: >
    <h1>Sedación Consciente - Galería</h1>
  display: true
  lightbox:
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMAGENES
    items:
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-01.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-02.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-03.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-04.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-05.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-06.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-07.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-08.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-09.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-10.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-11.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-12.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-13.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-14.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-15.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-16.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-17.jpg
      - /uploads/clinic-cases-sedation-and-general-anesthesia-es-18.jpg
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-01-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-02-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-03-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-04-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #4</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-05-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #5</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-06-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #6</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-07-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #7</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-08-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #8</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-09-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #9</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-10-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #10</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-11-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #11</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-12-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #12</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-13-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #13</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-14-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #14</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-15-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #15</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-16-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #16</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-17-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #17</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /img/clinic-cases-sedation-and-general-anesthesia-es-18-thumb.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>SC GALERÍA #18</h3>

# Quote Section
testimonial:
  display: true
  color: "#fff"
  content: >
    <p class="content">COMO EN MUCHAS PERSONAS DE MI GENERACIÓN, EL MIEDO A LOS ODONTÓLOGOS SE
    PRODUJO POR UNA PÉSIMA EXPERIENCIA EN LA NIÑEZ, PERO GRACIAS A LA SEDACIÓN; PERDÍ
    EL PÁNICO Y ME HE PODIDO COLOCAR VARIOS IMPLANTES DENTALES".</p>
  images:
    portrait: /uploads/quotes-sedation-and-general-anesthesia-portrait.jpg
    landscape: /uploads/quotes-sedation-and-general-anesthesia-landscape.jpg
form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-specialties.png

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>
# anex-links
anexes:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h1 class="bebas">En la clínica...</h1>
        <p>La opción estética por excelencia para mejorar el aspecto general de la dentadura o corregir defectos existentes con restauraciones conservadoras que recrean el aspecto natural de los dientes y proporcionan una resistencia comparable al esmalte dental.</p>
      footer:
        icon:
          display: true
          img: /uploads/sections-facilities.jpg
        link:
          display: true
          to: /
          placeholder: <span>mas</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Dele a su Salud el Valor que se Merece!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: >
        /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: >
        /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: >
        /profesionales/
      img: /uploads/procedures-professionals.png
---
