---
templateKey: foreign-patients-page
language: es
redirects: /en/foreign-patients/
title: Pacientes del exterior
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-foreign-patients.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: false
  portraitPosition: center
  content:
    position: center
    body: >
      <h1 class="bebas">PACIENTES DEL EXTERIOR</h1>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-professionals.jpg

# Heading Section
altHeading:
  title: >
    <p>En DENTAL VIP somos consecuentes desde hace años con todos los
      pacientes que nos visitan desde cualquier parte de Venezuela y el
      mundo. Nuestro equipo humano entiende, valora, agradece y honra el
      gran esfuerzo de todas aquellas personas que recorren grandes
      distancias en busca de Atención Odontológica Especializada
      ofreciéndoles un protocolo especial de atención
      <em>
        (ampliamente conocido en USA como "Dental Extreme Makeover")
      </em>
      que contempla dos fases:
    </p>
  columns:
    - head: >
        <div class="dv-title-circle text-center">Fase 1</div>
      body: >
        <p>Contacto, ejecución y recepción de los estudios dentales básicos y evaluaciones diagnósticas pertinentes.
            <br />
            <br />
        Estudio del caso, DIAGNÓSTICO, planificación y remisión de una propuesta concreta de tratamiento; incluyendo por supuesto, un estimado de honorarios profesionales o presupuesto.
        </p>
    - head: >
        <div class="dv-title-circle text-center">Fase 2</div>
      body: >
        <p>Desplazamiento, recepción y alojamiento.
            <br />
            <br />
        TRATAMIENTO ODONTOLÓGICO INTENSIVO Y MULTIDISCIPLINARIO para poder cubrir en tiempo récord <em>(1 o 2 semanas)</em> todos los requerimientos bucales previamente establecidos.
        </p>

heading:
  display: true
  content: >
    <h1 class="title">Su Salud: Un Buen Motivo para Viajar</h1>
    <p>La Globalización ha hecho del Turismo
    Dental una maravillosa herramienta de acceso a la Odontología de Primer Nivel.
    Por su reconocida trayectoria en el sector y razonable estructura de costes, Venezuela
    se consolida sin duda como uno de los mejores destinos a nivel mundial.</p>

    <ul class="maps">
    <li>
        <span><i class="icon-north-america"></i></span>
        <p class="dv-content">Ahorre más del</p> <p class="dv-content-number">70%</p>
        <p class="dv-content">Si reside en</p> <p class="dv-content-country">Estados
        Unidos O Canadá</p>
    </li>
    <li>
        <span><i class="icon-europe"></i></span>
        <p class="dv-content">Ahorre más del</p> <p class="dv-content-number">50%</p>
        <p class="dv-content">Si reside en</p> <p class="dv-content-country">España,
        Italia o Portugal</p>
    </li>
    <li>
        <span><i class="icon-south-america"></i></span>
        <p class="dv-content">Ahorre más del</p> <p class="dv-content-number">50%</p>
        <p class="dv-content">Si reside en</p> <p class="dv-content-country">Centro,
        Sudamérica o Islas del Caribe</p>
    </li>
    </ul>

slogan:
  img: /uploads/slogan-teeth.jpg
  content: >
    <h1>¿Cuenta Ya con un Presupuesto Definitivo?</h1>
    <br>
    <h2 >Remítanoslo vía e-mail para homologarlo y someterlo a su consideración. ¡Con seguridad le sorprenderemos!</h2>

hostSection:
  bg: /uploads/host-bg.jpg
  title: <h1 style="text-align:center;">Traslados y Alojamiento</h1>
  body:
    <p style="word-break:break-word;">Nuestra privilegiada ubicación nos permite contar además con dos excelentes
    infraestructuras hoteleras situadas a menos de 50 metros de la clínica. CHACAO
    SUITES y SHELTER SUITES disponen de cómodas habitaciones, estacionamiento, restaurantes
    y demás servicios que facilitarán y harán agradable, en caso de ser necesaria,
    su breve estadía en la ciudad de Caracas. Si es su deseo, nuestro personal administrativo
    está en capacidad de brindarle apoyo en todo lo referente a boleteria aérea, traslados
    aeropuerto-hotel-aeropuerto y trámites de alojamiento.</p>
  columns:
    - title: <p>Chacao Suites</p>
      link: http://www.hotelchacaosuites.com/
      img: /img/host-chacao-suites.jpg
    - title: <p>Shelter Suites</p>
      link: https://www.hotel-shelter.com/reserva/
      img: /img/host-shelter-suites.jpg

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
    - title: <h5>Implantes</h5>
      to: /la-clinica/implantes-dentales/
      img: /uploads/procedures-implants.png
    - title: <h5>Prótesis</h5>
      to: /especialidades/protesis/
      img: /uploads/procedures-prosthesis.jpg
    - title: <h5>Estética dental</h5>
      to: /especialidades/estetica-dental/
      img: /uploads/procedures-aesthetic-dentistry.png
---
