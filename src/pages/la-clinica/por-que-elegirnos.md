---
templateKey: clinic-page
language: es
redirects: /en/the-clinic/why-choose-us/
title: Por Qué Elegirnos
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
  
# Hero Section 
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-why-choose-us.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: 70% 50%
  content:
    position: center
    body: >
      <h1>Por Qué Elegirnos</h1>


# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/hero-home.jpg
      content: <h2><span><i class="icon-instagram"></i></span> Alguna Vaina</h2>
          <p>gfiaej'osigja'eosijg'eijers'gjv</p>

# Heading Section
heading:
  display: true
  content:
    <h1 class="title">10 Razones que Marcarán la Diferencia</h1>
    <p>Tomar decisiones es muchas veces complicado, pero como seres racionales que somos, siempre nos decantaremos por aquella alternativa, que a priori; maximice nuestro bienestar personal.</p>

# List Section
list:
  display: true
  items:
    - content: >
        <h3>Reconocido Equipo de Odontólogos Especialistas</h3>
        <p> Profesionales Universitarios de Alto Nivel con Estudios de Postgrado en las diferentes ramas de la Odontología Clínica. Todas las Especialidades en el Mismo Lugar.</p>
    - content: >
        <h3> Modernas y Cómodas Instalaciones</h3>
        <p>  Ambiente Tranquilo, Relajado y de Máximo Confort con Servicio de Internet (zona Wi-Fi). Consultorios Privados y Totalmente Independientes.</p>
    - content: >
        <h3> La Mejor Tecnología</h3>
        <p>  Contamos con los Equipos de Última Generación a Nivel Mundial. Siempre estamos a la Vanguardia en Innovación Odontológica.</p>
    - content: >
        <h3> Experiencia y Ética Profesional</h3>
        <p>  Odontólogos de Amplia Trayectoria e Incuestionable Vocación. Un Equipo Humano Verdaderamente Comprometido con lo que Hace.</p>
    - content: >
        <h3> Atención Totalmente Personalizada</h3>
        <p>  No somos un centro de atención masiva, y por ende, no delegamos funciones. Usted Será Siempre Atendido por Su Especialista de Confianza.</p>
    - content: >
        <h3> Organización y Mínimo Lapso de Espera</h3>
        <p>  Valoramos y respetamos su tiempo, por eso, Trabajamos Bajo un Sistema de Previa Cita que nos permite Optimizar Nuestro Servicio.</p>
    - content: >
        <h3> Excelente Ubicación</h3>
        <p>  Estamos en el Municipio Chacao, en el "centro del este" de la Ciudad Capital, dentro de un Complejo Urbanístico Empresarial de gran actividad económica y comercial.</p>
    - content: >
        <h3>Seguridad y Fácil Acceso</h3>
        <p>Numerosa Vigilancia Privada, más de 2.000 Puestos de Estacionamiento a su disposición y entrada peatonal desde el Sistema Subterráneo Metro de Caracas.</p>
    - content: >
        <h3> Precios Justos y Verdaderamente Competitivos</h3>
        <p>  Con Seguridad los Mejores en Odontología de Alto Standing.</p>
    - content: >
        <h3> Financiamiento y Facilidades de Pago </h3>
        <p>  Flexibilidad en el Cobro de Honorarios Profesionales, Planes Especiales de Financiamiento y Punto de Venta Comercial para el pago con Tarjetas de Débito y Crédito.</p>


# Quote Section
testimonial:
  display: false
  color: red
  content: >
    <p>THE RESPECT FOR THE LIFE AND INTEGRITY OF THE HUMAN PERSON, THE PROMOTION AND PRESERVATION OF HEALTH, AS A COMPONENT OF DEVELOPMENT AND SOCIAL WELFARE, AND ITS EFFECTIVE PROJECTION TO THE COMMUNITY; CONSTITUTE IN ALL CIRCUMSTANCES THE PRINCIPAL DUTY OF THE DENTIST".</p>
  images:
    portrait: /uploads/lightbox-smiles-01.jpg
    landscape: /uploads/lightbox-smiles-01.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-why-choose-us.jpg
  content: >
    <h1>¿Vive Fuera de Venezuela?</h1>
    <p class="dv-subtitle text-center text-white">
      ¡También somos una opción!
    </p>
    <p class="text-left dv-subtitle">
      En la actualidad nuestro país se ha convertido en un importante destino de
      Turismo Dental y ya son muchos los pacientes foráneos que nos visitan para
      recibir una Atención Sanitaria de Primer Nivel.
    </p>
    
    <p class="text-left dv-subtitle">
      Nuestra reconocida Calidad Asistencial y la posibilidad de Ahorrar Grandes
      Sumas de Dinero en tratamientos bucodentales de complejidad son dos ventajas
      competitivas difíciles de ignorar.
    </p>
    
    <p class="text-left dv-subtitle">
      Para su comodidad contamos con una privilegiada ubicación y dos excelentes
      infraestructuras hoteleras situadas a menos de 50 metros de la clínica. CHACAO
      SUITES y SHELTER SUITES disponen de lindas y confortables habitaciones,
      estacionamiento, restaurantes y demás servicios que facilitarán y harán
      agradable su breve estadía en la ciudad de Caracas.
    </p>
    <br>
    <h1>¿Pensando en viajar y visitarnos?</h1>
    <h3>Conozca Más Acerca de Nuestro Protocolo Clínico de Actuación.</h3>
    <br>
    <a class="link"  href="/pacientes-del-exterior/">Más Información</a>

# Gallery Section
gallery:
  display: false
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
  items:
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>

# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
  - title: <h5>Implantes</h5>
    to: >
      /especialidades/implantes-dentales/
    img: /uploads/procedures-implants.png
  - title: <h5>Prótesis</h5>
    to: >
      /especialidades/protesis/
    img: /uploads/procedures-prosthesis.jpg
  - title: <h5>Estética dental</h5>
    to: >
      /especialidades/estetica-dental/
    img: /uploads/procedures-aesthetic-dentistry.png
---
