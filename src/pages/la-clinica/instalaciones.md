---
templateKey: clinic-page
language: es
redirects: /en/the-clinic/facilities/
title: Instalaciones
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
  
# Hero Section 
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-facilities.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Instalaciones</h1>


# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2><span><i class="icon-instagram"></i></span> En la clínica...</h2>

# Heading Section
heading:
  display: true
  content:
    <h1 class="title">Un Espacio Diseñado para Su Tranquilidad</h1>
    <p>Unas instalaciones únicas, un equipamiento tecnológico excepcional y un ambiente idóneo para que nuestros pacientes disfruten de una Odontología diferente con el máximo nivel de confort, higiene y seguridad; en busca siempre del mejor resultado posible.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>


# Quote Section
testimonial:
  display: false
  color: '#ededed'
  content: >
    <p class="content">EL RESPETO A LA VIDA Y A LA INTEGRIDAD DE LA PERSONA HUMANA, EL FOMENTO Y LA PRESERVACIÓN DE LA SALUD, COMO COMPONENTE DEL DESARROLLO Y BIENESTAR SOCIAL, Y SU PROYECCIÓN EFECTIVA A LA COMUNIDAD; CONSTITUYEN EN TODAS LAS CIRCUNSTANCIAS EL DEBER PRIMORDIAL DEL ODONTÓLOGO".</p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait.jpg
    landscape: /uploads/quotes-phillosophy.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Síganos</h1>
    <h2>En nuestro blog y redes sociales</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">Noticias, Artículos, Consejos de Actualidad y Mucho Más...</h2>

# Gallery Section
gallery:
  display: true
  type: gridGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-facilities-01.png
      - /uploads/lightbox-facilities-02.png
      - /uploads/lightbox-facilities-03.png
      - /uploads/lightbox-facilities-04.png
      - /uploads/lightbox-facilities-05.png
      - /uploads/lightbox-facilities-06.png
      - /uploads/lightbox-facilities-07.png
      - /uploads/lightbox-facilities-08.png
      - /uploads/lightbox-facilities-09.png
      - /uploads/lightbox-facilities-10.png
      - /uploads/lightbox-facilities-11.png
      - /uploads/lightbox-facilities-12.png
      - /uploads/lightbox-facilities-13.png
      - /uploads/lightbox-facilities-14.png
      - /uploads/lightbox-facilities-15.png
      - /uploads/lightbox-facilities-16.png
      - /uploads/lightbox-facilities-17.png
      - /uploads/lightbox-facilities-18.png
      - /uploads/lightbox-facilities-19.png
      - /uploads/lightbox-facilities-20.png
      - /uploads/lightbox-facilities-21.png
  items:
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-01.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>MULTICENTRO EMPRESARIAL DEL ESTE</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-02.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>ACCESO PRINCIPAL</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-03.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Recepción</h3>        
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-04.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sala de Espera</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-05.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sala de Espera</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-06.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Sala de Espera</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-07.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Servicio Externo</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-08.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pasillo Central</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-09.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-10.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Área de entrevista</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-11.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 1</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-12.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Unidad Clínica</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-13.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-14.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Esterilización</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-15.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 2</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-16.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>radiología</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-17.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-18.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Alta Succión</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-19.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Gabinete 3</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-20.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Iluminación Led</h3>
    - link:
        display: false
        to: /pacientes-del-exterior/
      image: /uploads/lightbox-facilities-thumb-21.jpg
      action: true
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Laboratorio</h3>
# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
  - title: <h5>Implantes</h5>
    to: >
      /especialidades/implantes-dentales/
    img: /uploads/procedures-implants.png
  - title: <h5>Ortodoncia</h5>
    to: >
      /especialidades/ortodoncia/
    img: /uploads/procedures-orthodontics.png
  - title: <h5>Estética dental</h5>
    to: >
      /especialidades/estetica-dental/
    img: /uploads/procedures-aesthetic-dentistry.png
---
