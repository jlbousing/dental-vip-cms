---
templateKey: payment-options-page
language: es
redirects: /en/the-clinic/guarantees
title: Garantías
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-guarantees.png
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Garantías</h1>

banner: >
  <div class="banner">
    <aside>
      <p>
        <span style="margin-bottom: 25px;display: block;">¿Está en la Búsqueda de Tratamientos Dentales de Alta Gama? </span>
        VISÍTENOS <span style="color: #333;">¡PODEMOS AYUDARLE!</span>
      </p>
    </aside>
    <aside>
      <span class="travel">
        <i class="icon-travel"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
      </span>
    </aside>
  </div>

boxes:
  display: false
  boxes:
    - title: <h5>UNITED MEDICAL CREDIT</h5>
      to: /la-clinica/implantes-dentales/
      img: /uploads/procedures-united-medical-credit.png
    - title: <h5>DENEFITS</h5>
      to: /especialidades/ortodoncia/
      img: /uploads/procedures-denefits.png
    - title: <h5>MY MEDICAL FUNDING</h5>
      to: /especialidades/estetica-dental/
      img: /uploads/procedures-my-medical-founding.png
---

<h1>Nuestra Política de Garantías</h1>

<div class="row">
  <div>
    <p>
      A menudo, muchos pacientes pasan por alto el tema de las garantías a la
      hora de decidir qué clínica dental elegir y, a nuestro criterio; es sin
      duda alguna uno de los factores más importantes que diferencian a un
      centro de otro.
    </p>
    <p>
      DENTAL VIP puede ofrecerle la mejor garantía de cualquier Clínica Dental
      en Caracas porque confiamos en el alto nivel de nuestro trabajo, en la
      calidad de los materiales que utilizamos y en la precisión de los técnicos
      dentales que laboran en nuestro laboratorio protésico.
    </p>
    <p>
      Si uno de nuestros tratamientos garantizados falla estructuralmente,
      nuestros Odontólogos lo reemplazarán sin ningún tipo de costo adicional,
      por supuesto; bajo los términos y condiciones que se detallan más
      adelante. Esto le brindará a Usted la tranquilidad de saber que si algo
      sucede, ¡estaremos aquí para resolverlo!
    </p>
    <p></p>
  </div>
  <div class="icon">
    <i class="icon-schedule"> </i>
  </div>
</div>

<div class="message" style="margin-top:3.2rem !important">
  <p class="big">
    En el caso de retratamiento en un paciente foráneo, cubriremos los gastos de
    alojamiento, pero no los de traslado o pasaje aéreo.
  </p>
</div>
<p>
  A continuación se muestra un esquema general de los tiempos de garantía para
  los diferentes tipos de soluciones dentales que ofrecemos:
</p>
<br />
<br />
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 17%;"></span>
  </div>
  <div class="title">REPARACIONES</div>
  <div class="time">6 meses</div>
</div>
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 34%;"></span>
  </div>
  <div class="title">OBTURACIONES</div>
  <div class="time">1 año</div>
</div>
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 68%;"></span>
  </div>
  <div class="title">DENTADURAS REMOVIBLES</div>
  <div class="time">2 años</div>
</div>
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 68%;"></span>
  </div>
  <div class="title">CARILLAS DE PORCELANA</div>
  <div class="time">2 años</div>
</div>
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 68%;"></span>
  </div>
  <div class="title">PRÓTESIS FIJA</div>
  <div class="time">2 años</div>
</div>
<div class="percentaje">
  <div class="progress-bar">
    <span class="progress-bar-fill" style="width: 100%;"></span>
  </div>
  <div class="title">IMPLANTES DENTALES <i>(ZIMMER BIOMET)</i></div>
  <div class="time">3 años</div>
</div>
<br />
<p>
  El principal requisito para hacer cumplir el derecho de garantía en casos de
  implantes dentales, prótesis fija y carillas de porcelana es el uso constante
  de la férula nocturna suministrada al final del tratamiento. A tal fin, deberá
  ser presentada para su evaluación y para verificar, en función de su grado de
  desgaste natural; el uso correcto de la misma. Ninguna garantía será válida si
  no se está en posesión del dispositivo, si no ajusta correctamente debido a la
  falta de uso o si se detecta una férula distinta a la elaborada por nuestros
  Especialistas.
</p>
<p>
  Otra exigencia para que la garantía sea válida es asistir a un chequeo anual y
  limpieza dental profesional. Si vive en el exterior, se podrá realizar los
  controles regulares en su país de origen, pero para mantener la garantía;
  deberá enviarnos un correo electrónico con la factura del tratamiento, un set
  completo de fotografías intraorales y una radiografía panorámica para ser
  archivados en su historial y poder estar al tanto de su condición clínica. La
  atemporalidad en la ejecución de este requerimiento o en la recepción de
  dichos registros cancelará automáticamente cualquier responsabilidad en
  términos de garantía.
</p>
<p>
  Las circunstancias que pueden poner en peligro la longevidad de su tratamiento
  son principalmente las enfermedades crónicas, malos hábitos y deficiencias de
  higiene oral. En estos casos, dos chequeos anuales serán obligatorios para que
  la garantía sea válida; y uno de ellos deberá hacerse en nuestra clínica. En
  el caso de algunos factores de riesgo específicos no podremos ofrecer ningún
  tipo de garantía, pero siempre se lo informaremos de antemano.
</p>
<div class="message red">
  <h1>Factores de riesgo excluyentes:</h1>
  <ul style="font-weight: 300;">
    <li>Patologías inmunológicas o degenerativas.</li>
    <li>Accidentes y traumatismos severos.</li>
    <li>
      Enfermedades sistémicas que tengan un efecto perjudicial sobre el
      organismo como la diabetes no controlada, epilepsia, osteoporosis y el
      síndrome de inmunodeficiencia adquirida <i>(SIDA)</i>; entre otras.
    </li>
    <li>Radio y quimioterapia.</li>
    <li>Tabaquismo severo.</li>
  </ul>
</div>

<p>
  La garantía se invalida si el paciente o un tercero modifica, sin nuestro
  expreso consentimiento, la prótesis o el trabajo dental realizado. De igual
  forma, no están cubiertas las piezas plásticas y acrílicas, soluciones
  temporales, extensiones y revestimientos; así como ninguna futura endodoncia o
  tratamiento de la inflamación pulpar.
</p>
<p>
  Ocasionalmente, los dientes sufren trauma durante la preparación de coronas y
  puentes, y que a posteriori; ameritan tratamiento de conducto radicular. Como
  tal circunstancia es imprevisible en la gran mayoría de los casos, DENTAL VIP
  no puede hacerse responsable de ninguna patología endodóntica que no sea
  evidente al momento de la consulta y diagnóstico inicial.
</p>
<p>
  Escapan a la cobertura de esta Política de Garantías cualquier gasto o importe
  realizado en otra clínica dental, como por ejemplo; el cementado final de una
  prótesis fija definitiva.
</p>
<p>
  Aunque de presentarse cualquier complicación o imprevisto buscaremos siempre
  la conciliación como procedimiento de preferencia para acordar y resolver
  cualquier disputa o situación clínica, nos reservamos el derecho de someter el
  tratamiento dispensado a la evaluación de peritos profesionales
  independientes; por supuesto, solo en aquellos casos de duda o desacuerdo en
  términos de garantías.
</p>
<br />
<br />
<h2 class="left section-title">
  <b>¿Qué incluye la garantía? </b>
</h2>
<p>
  Aunque nos encantaría poder garantizar nuestros tratamientos para toda la
  vida, es verdaderamente imposible y algo utópico; ya que la duración en el
  tiempo de cualquier rehabilitación dental va a depender de una gran cantidad
  de factores totalmente ajenos a nuestro quehacer: la genética y predisposición
  innata a ciertos estados patológicos, los hábitos de alimentación, el estado
  de salud general presente y futuro, la aparición, desarrollo y evolución de
  enfermedades sistémicas, óseas, hormonales, degenerativas, neoplásicas o
  mentales; entre otras, la osteoporosis e ingesta asociada de bifosfonatos en
  mujeres, el consumo de drogas y otros fármacos, la exposición a radiaciones,
  el padecimiento de golpes y traumatismos faciales, el desarrollo de bruxismo y
  otros hábitos parafuncionales, el verdadero conocimiento, dominio, destreza y
  ejecución de las diferentes técnicas de higiene oral; la frecuencia del
  cepillado, el uso constante del cepillo interdental e hilo dental, el uso
  regular de la férula de descarga oclusal y la asistencia a los controles
  periódicos programados; son todas condiciones y actividades solo inherentes al
  paciente, y por ende, bajo su única y absoluta responsabilidad.
</p>
<p>
  De modo que las garantías dentales solo pueden cubrir los gastos
  <i>(parciales o totales)</i> de volver a realizar un tratamiento cuando es
  necesario repetirlo, dentro de un lapso de tiempo razonable, por factores
  inherentes al profesional y/o a los biomateriales utilizados que evidencien
  directamente fallas de diagnóstico, planificación, ejecución y calidad de la
  atención suministrada.
</p>
<p>
  DENTAL VIP ofrece una Política de Garantías muy sincera y amigable para el
  paciente. En caso de tener que hacerla efectiva y residir fuera de Venezuela,
  organizaremos su viaje en las condiciones habituales y cubriremos lo
  siguiente:
</p>
<ul class="check-list">
  <li><i class="icon-check circle"></i> Reserva del boleto aéreo.</li>
  <li><i class="icon-check circle"></i> Reserva de hotel.</li>
  <li>
    <i class="icon-check circle"></i> Traslados desde y hacia el aeropuerto.
  </li>
  <li>
    <i class="icon-check circle"></i> Alojamiento hasta por un máximo de 7
    noches.
  </li>
  <li>
    <i class="icon-check circle"></i> Retratamiento dental libre de costos.
  </li>
</ul>
<h2 class="left section-title">
  <b>¿Cómo se efectúa el procedimiento? </b>
</h2>
<p>
  En el caso de cualquier imprevisto, deberá acudir inmediatamente a consulta si
  vive en la ciudad; o si no, enviar fotos y una breve descripción del problema
  a través de nuestros formularios de contacto. Si es necesario regresar a
  Caracas, examinaremos su historial clínico y las imágenes suministradas para
  tratar de determinar por qué ocurrió la falla, planificar el retratamiento y
  hacer efectiva; si aplica, la Política de Garantías ya descrita.
</p>
<br>