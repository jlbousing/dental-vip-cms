---
templateKey: clinic-page
language: es
redirects: /en/the-clinic/payment-options/
title: Financiamiento
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-financing.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Financiamiento</h1>

# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2></h2>

# Heading Section
heading:
  display: true
  content: >
    <h1 style="
    margin-bottom: 1.6rem;" class="title">DENTAL FLEX: El Mejor Plan de Financiamiento para su Salud Oral</h1>
    <p class="small" style="
    line-height: 1.2;
    "><b style="color: red">*</b> Oferta sujeta a la previa aprobación de nuestra entidad financiera aliada tras el estudio de la documentación requerida y firma del contrato.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>

# Quote Section
testimonial:
  display: false
  color: "#ededed"
  content: >
    <p class="content">EL RESPETO A LA VIDA Y A LA INTEGRIDAD DE LA PERSONA HUMANA, EL FOMENTO Y LA PRESERVACIÓN DE LA SALUD, COMO COMPONENTE DEL DESARROLLO Y BIENESTAR SOCIAL, Y SU PROYECCIÓN EFECTIVA A LA COMUNIDAD; CONSTITUYEN EN TODAS LAS CIRCUNSTANCIAS EL DEBER PRIMORDIAL DEL ODONTÓLOGO".</p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait.jpg
    landscape: /uploads/quotes-phillosophy.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-financing.jpg
  content: >
    <h1>Recaudos y Documentos a Consignar</h1>
    <ul class="cols-2">
      <li>
        <i class="icon-check circle"></i
        ><span
          >Presupuesto en original emitido por DENTAL VIP, Especialidades
          Odontológicas s.c.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Planilla de solicitud de crédito debidamente cumplimentada en todos
          sus campos y firmada por el solicitante y su cónyuge&nbsp;<em
            >(en caso de estar casado).</em
          ></span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Fotocopia de la cédula de identidad vigente del solicitante y su
          cónyuge.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Fotocopia del registro único de información fiscal&nbsp;<em
            >(RIF)</em
          >
          vigente del solicitante y su cónyuge.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Constancia de trabajo en original o certificación de ingresos del
          solicitante y su cónyuge, con fecha de emisión no mayor a
          tres&nbsp;<em>(3)</em>&nbsp;meses.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Balance personal o mancomunado en original firmado por un contador
          público colegiado, con fecha de emisión no mayor a
          seis&nbsp;<em>(6)</em>&nbsp;meses.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Últimos tres<em>&nbsp;(3)</em>&nbsp;estados de cuentas corrientes,
          tarjetas de crédito y libretas de ahorro que el solicitante posea,
          sellados y firmados por el banco emisor.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span
          >Referencias bancarias, con fecha de emisión no mayor a dos
          <em>(2)</em> meses.</span
        >
      </li>
      <li>
        <i class="icon-check circle"></i
        ><span>Última declaración de ISLR.</span>
      </li>
    </ul>
    <br>
    <h3>Le garantizamos un trámite simple, rápido y accesible para que Usted solo
        tenga que preocuparse de cosas más importantes.</h3>
    <h1 class="mt-1">¿Alguna Duda?</h1>
    <br>
    <a class="link"  href="/contacto/">Más Información</a>

# Gallery Section
gallery:
  display: false
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
  items:
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>

# financing section
financing:
  display: true
  banner: /uploads/banner-financing.png
  content: >
    <p>
    Conscientes de la implicación económica que conlleva la elección de un Centro
    Odontológico de Primer Nivel, que cuente con Especialistas altamente
    capacitados y que fundamente su quehacer en los mejores materiales y
    tecnologías disponibles, hemos desarrollado DENTAL FLEX, un instrumento
    financiero destinado a promover la accesibilidad a este tipo de servicio. Con
    él, podrá obtener un crédito de hasta el 100% del importe total de su
    tratamiento, sin inicial y a plazos de 24, 36 o 48 meses
    <span style="color: red;">*</span>.Nuestra alianza estratégica con las
    principales entidades bancarias del país permite que los trámites fluyan sin
    inconvenientes ni demoras administrativas.
    </p>
    <p>
    El proceso es muy sencillo. Una vez que le hayamos evaluado clínica y
    radiográficamente, que contemos con un presupuesto definitivo y que haya Usted
    aportado la documentación requerida, nuestro personal de apoyo formalizará la
    solicitud, y en corto plazo, dará respuesta a su requerimiento.
    </p>
    <p>
    El proceso es muy sencillo. Una vez que le hayamos evaluado clínica y
    radiográficamente, que contemos con un presupuesto definitivo y que haya Usted
    aportado la documentación requerida, nuestro personal de apoyo formalizará la
    solicitud, y en corto plazo, dará respuesta a su requerimiento.
    </p>

  modal:
    display: true
    interval: 20000
    content: >
      <h1 class="dv-modal-title">¡Ofrecemos disculpas!</h1>
      <p class="text-center">A causa del acelerado fenómeno de hiperinflación actual, este producto ha sido temporalmente suspendido.</p><p class="text-center">Por los momentos, todos nuestros tratamientos deben ser cancelados al contado.</p>
      <hr>
    placeholder: OK

  calculator:
    warning: >
      <p style="text-align:center;line-height: 1.2" class="advise small"><b style="color:red">*</b> Introduzca el monto sin puntos, comas o decimales.</p>
    placeholders:
      amount: >
        <p>Monto a financiar *</p>
      time: >
        <p>Plazo</p>
      rate: >
        <p>Tasa</p>
      calculate: >
        <span>Calcular</span>
      currency: >
        <span>Bs.S</span>
      result: >
        <span>Cuota Mensual:</span>
    advise: >
      <p  style="line-height: 1.2" class="advise small">El resultado obtenido es referencial y de carácter informativo, y en consecuencia, no podrá asumirse que el Banco esté obligado a otorgar los créditos que se le soliciten bajo las mismas condiciones arriba indicadas. Aunque la información que se utiliza en este simulador se actualiza de forma continua, la tasa de interés empleada para el cálculo podría sufrir algún tipo de variación.</p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
    - title: <h5>Implantes</h5>
      to: >
        /especialidades/implantes-dentales/
      img: /uploads/procedures-implants.png
    - title: <h5>Ortodoncia</h5>
      to: >
        /especialidades/ortodoncia/
      img: /uploads/procedures-orthodontics.png
    - title: <h5>Estética dental</h5>
      to: >
        /especialidades/estetica-dental/
      img: /uploads/procedures-aesthetic-dentistry.png
---
