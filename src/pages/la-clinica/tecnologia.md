---
templateKey: clinic-page
language: es
redirects: /en/the-clinic/technology/
title: Tecnología
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
  
# Hero Section 
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-technology.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: center
  content:
    position: center
    body: >
      <h1>Tecnología</h1>


# Paragraph
paragraph:
  display: false
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2><span><i class="icon-instagram"></i></span> En la clínica...</h2>

# Heading Section
heading:
  display: true
  content:
    <h1 class="title">Un Equipamiento Clínico de Última Generación</h1>
    <p>En DENTAL VIP contamos con los equipos más modernos y la más avanzada tecnología dental para ofrecerle a Usted y a toda su familia tratamientos Odontológicos de vanguardia y comprobada calidad.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>


# Quote Section
testimonial:
  display: false
  color: '#ededed'
  content: >
    <p class="content">EL RESPETO A LA VIDA Y A LA INTEGRIDAD DE LA PERSONA HUMANA, EL FOMENTO Y LA PRESERVACIÓN DE LA SALUD, COMO COMPONENTE DEL DESARROLLO Y BIENESTAR SOCIAL, Y SU PROYECCIÓN EFECTIVA A LA COMUNIDAD; CONSTITUYEN EN TODAS LAS CIRCUNSTANCIAS EL DEBER PRIMORDIAL DEL ODONTÓLOGO".</p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait.jpg
    landscape: /uploads/quotes-phillosophy.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Síganos</h1>
    <h2>En nuestro blog y redes sociales</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">Noticias, Artículos, Consejos de Actualidad y Mucho Más...</h2>

# Gallery Section
gallery:
  display: true
  type: staticGallery
  carousel:
    display: false
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-facilities-01.png
  items:
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-01.jpg
    placeholder: none
    body: >
        <h3> Láser Dental</h3>
        <p>  El láser de diodo EPIC™ es un dispositivo quirúrgico y terapéutico de
        vanguardia, concebido para realizar una amplia variedad de procedimientos en tejidos
        blandos, blanqueamiento dental y alivio temporal del dolor.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-02.jpg
    placeholder: none
    body: >
        <h3> Lámpara LED de Alta Intensidad</h3>
        <p class="dv-text-feat">  La ZOOM Advanced Power es una lámpara de luz
        fría para blanqueamiento dental que emite luz UV en longitudes de onda cercanas
        al azul <em>(entre 365-500 nm)</em> y que aporta energía para acelerar la difusión
        y oxidación de las soluciones blanqueadoras dentro de la estructura dental.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-03.jpg
    placeholder: none
    body: >
        <h3> SISTEMA CAD-CAM</h3>
        <p>  Tecnología computarizada que permite el escaneo en 3D, digitalización
        y transferencia de datos a un software que diseña y confecciona cualquier tipo
        de restauración cerámica fija a través de la activación y control de un sistema
        robótico de microfresado.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-04.jpg
    placeholder: none
    body: >
        <h3> RADIOVISIÓGRAFO KODAK RVG 5100</h3>
        <p>  Radiología digital computarizada que minimiza la generación de rayos
        X y proporciona imágenes instantáneas de alta definición con más de 20 pares de
        líneas visibles por milímetro, ampliando así las capacidades técnicas de diagnóstico
        y evaluación clínica.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-05.jpg
    placeholder: none
    body: >
        <h3> CÁMARA INTRAORAL</h3>
        <p>  Captura y almacena imágenes digitales de alta resolución que permiten
        apreciar con detalle la condición oral inicial del paciente, diseñar la sonrisa,
        comparar y evaluar los resultados finales del tratamiento odontoestético.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-06.jpg
    placeholder: none
    body: >
        <h3> SILLONES ERGONÓMICOS Y BIARTICULADOS</h3>
        <p>  Unidades dentales de última generación con respaldares anatómicos amplios
        y envolventes que brindan gran confort al paciente, reducen el nivel de estrés
        y evitan tensiones o molestias posturales durante el tratamiento.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-07.jpg
    placeholder: none
    body: >
        <h3> LÁMPARA DE FOTOCURADO</h3>
        <p>  Dispositivo inalámbrico de luz halógena utilizado para polimerizar los
        materiales plásticos fotosensibles en los procesos de restauración dental y odontología
        operatoria.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-08.jpg
    placeholder: none
    body: >
        <h3> NSK CLEAN HEAD SYSTEM®</h3>
        <p>  Equipamiento rotatorio de alta velocidad especialmente fabricado para
        prevenir la entrada y acumulación de fluidos orales y demás contaminantes, garantizando
        la asepsia en todo tipo de procedimiento dental.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-09.jpg
    placeholder: none
    body: >
        <h3> LOCALIZADOR DE ÁPICES</h3>
        <p>  Instrumento electrónico que basándose en la frecuencia, resistencia
        e impedancia eléctrica es capaz de localizar la mayor constricción apical de la
        raíz y determinar la longitud de trabajo dentro del conducto radicular.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-10.jpg
    placeholder: none
    body: >
        <h3> ULTRASONIDO BIOSONIC® S1</h3>
        <p class="dv-text-feat">  Potente scaler ultrasónico piezoeléctrico que
        facilita la eliminación de cálculo dental, obturaciones, coronas defectuosas y
        la irrigación e instrumentación mecánica de los conductos radiculares en la terapia
        endodóntica.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-11.jpg
    placeholder: none
    body: >
        <h3> VACUUM DENTAL</h3>
        <p class="dv-text-feat">  Máquina de vacío para el termoformado de placas
        miorelajantes, férulas de blanqueamiento, férulas para el bruxismo, protectores
        bucales, placas base, cubetas individuales, retenedores de ortodoncia, guías quirúrgicas
        y coronas provisionales.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-12.jpg
    placeholder: none
    body: >
        <h3> ODONTOLOGÍA DIGITAL</h3>
        <p class="dv-text-feat">  Cámara digital extraoral, software de diseño
        de sonrisa y monitores LED para optimizar la comunicación con el paciente y el
        trabajo en equipo.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-13.jpg
    placeholder: none
    body: >
        <h3> UNIDAD QUIRÚRGICA</h3>
        <p class="dv-text-feat">  Motor eléctrico para cirugía e implantología
        oral diseñado bajo un concepto de manejo intuitivo, con función de mecanizado
        y control automático del torque que facilitan la correcta conformación del lecho
        quirúrgico, inserción y atornillado de los implantes dentales.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-14.jpg
    placeholder: none
    body: >
        <h3> SANDBLASTER</h3>
        <p class="dv-text-feat">  Tecnología de arenado y microabrasión para la
        preparación interna de metales y restauraciones cerámicas, ajuste y asentamiento
        de coronas, optimización de contactos proximales y remoción de óxidos y cementos
        residuales.</p>
    action: false
  - link:
      display: false
      to: /
    image: /uploads/gallery-technology-15.jpg
    placeholder: none
    body: >
        <h3> PROGRAMAT® P310</h3>
        <p class="dv-text-feat">  Moderno horno de cocción para cerámica dental
        equipado con variados programas de sinterizado y tecnología de mufla QTK2 que
        garantizan una distribución homogénea del calor, óptima coalescencia atómica y
        prolongación del ciclo de vida de los diversos elementos cerámicos.</p>
    action: false

# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
  - title: <h5>Implantes</h5>
    to: >
      /especialidades/implantes-dentales/
    img: /uploads/procedures-implants.png
  - title: <h5>Prótesis</h5>
    to: >
      /especialidades/protesis/
    img: /uploads/procedures-prosthesis.jpg
  - title: <h5>Estética dental</h5>
    to: >
      /especialidades/estetica-dental/
    img: /uploads/procedures-aesthetic-dentistry.png
---
