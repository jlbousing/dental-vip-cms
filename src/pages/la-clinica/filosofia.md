---
templateKey: clinic-page
language: es
redirects: /en/the-clinic/philosophy/
title: Filosofía
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag

# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-philosophy.jpg
    isParallax: true
  anim:
    display: true
    type: bottom
  height: full
  indicator: true
  portraitPosition: 83%
  content:
    position: center
    body: >
      <h1>Filosofía</h1>

# Paragraph
paragraph:
  display: true
  items:
    - img: /uploads/sections-facilities.jpg
      content: >
        <h2><span><i class="icon-clinic-chair"></i></span> En la clínica...</h2>
        <p class="light-font">La clínica DENTAL VIP ha sido concebida y diseñada para que el paciente se sienta inmerso en un ambiente acogedor y disfrute de <strong>un clima relajado, tranquilo y de máximo confort,</strong> desde el mismo momento en que es recibido y a lo largo de toda su visita. <strong>Contamos con unas modernas y cómodas instalaciones, la mejor tecnología de vanguardia, los equipos más avanzados y un reconocido grupo de Odontólogos Especialistas de alto nivel.</strong> Esto nos permite ofrecer un nuevo concepto en Odontología: <em>moderna, integral y especializada</em>.</p>
        <p class="light-font">Una privilegiada ubicación con fácil acceso, la disponibilidad de estacionamiento estructural en el propio inmueble y la presencia permanente de un numeroso personal de seguridad privada conforman también parte esencial de nuestra concepción de servicio, de nuestra intención por hacer de su experiencia global algo positivo y de nuestro gran empeño por <strong>lograr que su calendario de visitas sea lo más cómodo, expedito y seguro posible.</strong></p>
    - img: /uploads/sections-treatment.jpg
      content: >
        <h2><span><i class="icon-teeth-implants"></i></span> En el tratamiento...</h2>
        <p class="light-font">Como nuestra principal preocupación es su salud, en DENTAL VIP <strong>trabajamos con los materiales e instrumentos de mayor calidad.</strong> Garantizamos ética en los servicios y óptimos resultados en la resolución de casos clínicos de alta complejidad. Para ello, nos valemos de <strong>competencia profesional, compromiso, flexibilidad y capacidad de innovación.</strong> Además, somos un equipo multidisciplinario que se mantiene en constante evolución, entrenándonos día a día en las nuevas técnicas y procedimientos que nos permitan mejorar aún más los resultados estéticos y funcionales de todos nuestros tratamientos.</p>
        <p class="light-font">En nuestro quehacer siempre nos aseguramos de proporcionar <strong>un servicio Médico-Odontológico completamente personalizado y ajustado a sus necesidades.</strong> Nuestro coordinador clínico hará que los Especialistas trabajen en equipo, poniendo a su disposición <strong>experiencia, conocimiento científico y los más recientes avances en el campo de la salud oral.</strong></p>
    - img: /uploads/sections-humanity.jpg
      content: >
        <h2><span><i class="icon-handshake"></i></span> En lo humano...</h2>
        <p class="light-font">Nos hemos fijado como norte lograr que los pacientes reciban la mejor atención, por ello ofrecemos siempre <strong>un trato personal, amable, sincero y muy profesional por parte de todo el equipo humano que labora en la institución.</strong> Transparencia, honestidad, tolerancia y equidad en la colaboración constituyen nuestra base para el éxito, la armonía y la satisfacción en el trabajo. Aunque los resultados del tratamiento sean siempre su principal motivación, intentaremos ir más allá para superar expectativas, lograr su entera aprobación y <strong>consolidarnos como centro de referencia para amigos y familiares cercanos.</strong></p>
        <p class="light-font">Nuestra Filosofía bien se refleja en nuestro gran esfuerzo por <strong>conseguir una buena comunicación entre el Odontólogo y el Paciente.</strong> Para nosotros es fundamental que Usted logre comprender y razonar cuál es su problema dental, cuáles fueron sus causas y cuáles son sus consecuencias y alternativas terapéuticas, para que juntos logremos darle solución y podamos además prevenir su recurrencia. Claro debe quedar que <strong>la prevención es la piedra angular de toda estrategia en salud.</strong></p>
    - img: /uploads/sections-economics.jpg
      content: >
        <h2><span><i class="icon-money"></i></span> En lo económico...</h2>
        <p class="light-font">Muchos pacientes acuden a un seguro dental, a una franquicia o a una clínica popular por el precio que ofrecen, pero generalmente la atención es muy básica, masiva, poco especializada y ofrecida por odontólogos itinerantes que apenas se inician en la profesión. <strong>Cuando de atención privada se trate, desconfíe siempre de las consultas gratis, ofertas 2x1 y honorarios excesivamente bajos, ya que suelen encubrir un gran deterioro de la calidad asistencial</strong> que puede poner en riesgo su salud y hacerle presa fácil de la mala praxis profesional. Trabajar muy rápido, atender muchos pacientes, delegar funciones y escatimar al máximo en formación académica, infraestructura, tecnología, bioseguridad y gastos de material clínico; es filosofía común en aquellos que ofertan odontología barata. </p>
        <p class="light-font">DENTAL VIP ofrece <strong>dedicación, personalización, excelencia y exclusividad,</strong> combinando lo mejor de la Odontología Integral Especializada con la tecnología más actual y relevante, todo ello <strong>a precios justos y verdaderamente favorables,</strong> con seguridad por debajo de los de nuestra competencia directa. </p>

# Heading Section
heading:
  display: true
  content:
    <h1 class="title">Nuestra Base para el Trabajo y Éxito Profesional</h1>
    <p>futuro como un gran reto para todos y lo afrontamos con una actitud altamente positiva
    hacia las personas y el país.  Consideramos nuestra situación actual y sus matices
    como una gran oportunidad y lección de vida.</p>

# List Section
list:
  display: false
  items:
    - content: >
        <h4></h4>
        <p></p>

# Quote Section
testimonial:
  display: true
  color: "#ededed"
  content: >
    <p class="content">EL RESPETO A LA VIDA Y A LA INTEGRIDAD DE LA PERSONA HUMANA, EL FOMENTO Y LA PRESERVACIÓN DE LA SALUD, COMO COMPONENTE DEL DESARROLLO Y BIENESTAR SOCIAL, Y SU PROYECCIÓN EFECTIVA A LA COMUNIDAD; CONSTITUYEN EN TODAS LAS CIRCUNSTANCIAS EL DEBER PRIMORDIAL DEL ODONTÓLOGO".</p>
  images:
    portrait: /uploads/quotes-phillosophy-portrait.jpg
    landscape: /uploads/quotes-phillosophy.jpg

# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Síganos</h1>
    <h2>En nuestro blog y redes sociales</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">Noticias, Artículos, Consejos de Actualidad y Mucho Más...</h2>

# Gallery Section
gallery:
  display: false
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
  items:
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>

# financing section
financing:
  display: false
  banner: /uploads/banner-financing.png
  content: >
    <p></P>
  modal:
    display: false
    interval: 10
    content: >
      <hr>
    placeholder: OK

  calculator:
    warning: <p></p>
    placeholders:
      amount: >
        <p></p>
      time: >
        <p></p>
      rate: >
        <p></p>
      calculate: >
        <p></p>
      currency: >
        <p></p>
      result: >
        <p></p>
    advise: >
      <p></p>

# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Una Especialidad para Cada Tratamiento!</h1>
  procedures:
    - title: <h5>Implantes</h5>
      to: >
        /especialidades/implantes-dentales/
      img: /uploads/procedures-implants.png
    - title: <h5>Ortodoncia</h5>
      to: >
        /especialidades/ortodoncia/
      img: /uploads/procedures-orthodontics.png
    - title: <h5>Estética dental</h5>
      to: >
        /especialidades/estetica-dental/
      img: /uploads/procedures-aesthetic-dentistry.png
---
