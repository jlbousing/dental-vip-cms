---
templateKey: disclaimer-page
language: es
title: Descargo de Responsabilidad
description: >
  una breve descripcion de seo
redirects: /en/disclaimer/
published: true
keywords:
  - default
---

# DESCARGO DE RESPONSABILIDAD

- Todo el contenido de este sitio web tiene solo fines informativos y no debe considerarse como una indicación de tratamiento dental. La necesidad y viabilidad de un plan de tratamiento solo pueden ser determinadas en una consulta formal, después de realizar un diagnóstico clínico y radiológico del paciente.

- La información contenida en esta página web no debe considerarse jamás como de criterio único o exclusivo. DENTAL VIP, Especialidades Odontológicas s.c. o sus agentes, directores, profesionales o cualquier otra parte relacionada; niega toda responsabilidad con respecto a cualquier acción, decisión, tratamiento, daño y/o consecuencia negativa asociada a la información contenida en este portal.

- Este sitio y toda la información que contiene está diseñado para documentarle acerca de los tratamientos dentales más frecuentes y con mayor tasa de éxito. Sin embargo, en caso de duda o desacuerdo, DENTAL VIP, Especialidades Odontológicas s.c. le recomienda buscar siempre una segunda opinión.

- Aunque este sitio ofrece enlaces a otros sitios web, DENTAL VIP, Especialidades Odontológicas s.c. no se hace responsable de sus contenidos ni de la seguridad electrónica de los mismos.

- Aclaramos que todos los procedimientos dentales implican por naturaleza ciertos riesgos para los dientes, maxilares y estructuras faciales cercanas; pudiendo también afectar la salud general y el estilo de vida. Además, que no están exentos de complicaciones que pudieran variar el curso de los tratamientos inicialmente contemplados.

- Dejamos en claro que las imágenes y fotografías que ve en este sitio, son referencia de los resultados de algunos de nuestros tratamientos, sin embargo, de ninguna forma están garantizados y pueden variar en virtud del caso clínico y las condiciones propias del paciente.

- Le informamos que los procedimientos descritos en este sitio pueden no ser adecuados para todos los pacientes. Una consulta que incluya historia clínica, examen físico y radiografías orales; es el único medio para determinar un plan de tratamiento personalizado.

- Este sitio contiene testimonios de usuarios de nuestros productos y/o servicios que reflejan experiencias y opiniones de la vida real, sin embargo, esas experiencias son personales para ellos en particular y pueden no ser necesariamente representativas de todos los pacientes que atendemos. No afirmamos, y no se debe asumir, que todos los usuarios de nuestra clínica tendrán las mismas experiencias.

- Antes de ejecutar cualquier tratamiento en DENTAL VIP, Especialidades Odontológicas s.c., se entregará al paciente un diagnóstico completo y un plan de tratamiento individualizado que describa los costos y detalles de cada procedimiento. De igual forma, se le proporcionará un consentimiento informado por escrito que exprese claramente los riesgos y posibles complicaciones de cada intervención, el cual deberá ser leído y aprobado con la firma del paciente.
