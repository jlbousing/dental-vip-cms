---
templateKey: advise-page
description: >
  some seo
language: es
title: Uso DE COOKIES
redirects: /en/use-of-cookies/
published: true
keywords:
  - default
---

# Uso DE COOKIES

## Política de cookies

La página web [https://www.dentalvipcaracas.com/](/) titularidad de DENTAL VIP, Especialidades Odontológicas s.c. utiliza cookies.

Una cookie es un fragmento de texto que un servidor web puede almacenar en el disco duro de un usuario. Las cookies no son programas, solo datos con un nombre, de modo que no pueden causar daño a los dispositivos informáticos.

Las cookies están pensadas para ofrecer una experiencia de navegación mucho más personalizada a los internautas. Estas permiten, entre otras cosas, que un sitio web almacene información en el equipo de un usuario y luego la recupere, que la página le cargue mucho más rápido a los visitantes recurrentes y que les ofrezca información que pueda ser de su verdadero interés; en función del uso que realicen y del contenido de la misma.

Con frecuencia, las páginas web utilizan cookies de terceros con el objetivo de crear estadísticas y perfiles publicitarios de los usuarios. Esta actividad es totalmente legal, siempre y cuando cuente con nuestro consentimiento.

En caso de no querer recibir cookies, por favor configure su navegador de internet para que las borre del disco duro de su dispositivo, las bloquee o le avise en caso de instalación de las mismas. Para continuar sin cambios en la configuración de las cookies, simplemente continúe navegando en la página web.

## Consentimiento

Las cookies que utilizamos no almacenan dato personal alguno, ni ningún tipo de información que pueda identificarle, salvo que quiera registrarse de forma voluntaria con el fin de utilizar los servicios que ponemos a su disposición o de recibir información sobre promociones y contenidos de su interés.

Al navegar y continuar en nuestra web, nos indica que está consintiendo el uso de las cookies antes enunciadas, y en las condiciones contenidas en la presente Política de Cookies. En caso de no estar de acuerdo, envíe un correo electrónico a [info@dentalvipcaracas.com](mailto:info@dentalvipcaracas.com)

## Cómo bloquear o eliminar las cookies instaladas

Puede Usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones de su navegador. Puede encontrar información sobre cómo hacerlo, en relación con los navegadores más comunes, en los links que se incluyen a continuación:

Explorer: [https://support.microsoft.comes-es/kb/278835](https://support.microsoft.com/es-es/kb/278835)

Chrome: [http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647](http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647)

Firefox: [http://support.mozilla.org/es/kb/Borrar%20cookies](http://support.mozilla.org/es/kb/Borrar%20cookies)

Safari: [http://support.apple.com/kb/ph5042](http://support.apple.com/kb/ph5042)

## Modificaciones

La página web [https://www.dentalvipcaracas.com/](/) titularidad de DENTAL VIP, Especialidades Odontológicas s.c. puede modificar esta Política de Cookies en función de los requerimientos legales, o bien, con la finalidad de adaptarla a las exigencias dictadas por cualquiera de las diferentes instituciones públicas del estado.

Por esta razón, aconsejamos a los usuarios que la visiten periódicamente. Cuando se produzcan cambios significativos en esta Política de Cookies, se comunicarán de inmediato, bien mediante la web o a través de los correos electrónicos registrados.
