---

templateKey: thank-you-page
language: es
redirects: /en/thank-you
title: Thank you
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
# Parallax Section
parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-follow-us.jpg
  content: >
    <h1 class="big">Síganos</h1>
    <h2>En nuestro blog y redes sociales</h2>
    <div class="social-links">
        <div class="social-item">
            <a href="/blog/" target="_blank" rel="noopener noreferrer"
            ><i alt="blog" class="icon-blog"></i
            ></a>

            <h3>Blog</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.instagram.com/dental_vip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="instagram" class="icon-instagram"></i
            ></a>

            <h3>Instagram</h3>
        </div>
        <div class="social-item">
            <a
            href="https://www.facebook.com/dentalvip/"
            target="_blank"
            rel="noopener noreferrer"
            ><i alt="facebook" class="icon-facebook"></i
            ></a>

            <h3>Facebook</h3>
        </div>
        </div>
    <h2 class="nmb">Noticias, Artículos, Consejos de Actualidad y Mucho Más...</h2>
# Brand Section
brand:
  logo: /uploads/logo.svg
  title: >
    <h4><b style="color:#333">¡SU MENSAJE HA SIDO ENVIADO CON ÉXITO!.<b></h4>
  main: >
    <p>Responderemos tan pronto inicie nuestra próxima jornada de trabajo.</p>
    <p>¡Mil gracias por su confianza!</p>
  partners:
    - image: /uploads/partners-idd.jpg
      alt: Institute of Digital Dentistry
    - image: /uploads/partners-qdc.jpg
      alt: Quality Dental Center
    - image: /uploads/partners-iti.jpg
      alt: International Team for Implantology
  footer: >
    <h1>Últimos Posts</h1>

# Gallery Section
gallery:
  type: singleGallery
  carousel:
    display: true
    placeholder: GIRE EL DISPOSITIVO PARA AMPLIAR LAS IMÁGENES
    items:
      - /uploads/lightbox-smiles-01.jpg
      - /uploads/lightbox-smiles-02.jpg
      - /uploads/lightbox-smiles-03.jpg
      - /uploads/lightbox-smiles-04.jpg
      - /uploads/lightbox-smiles-05.jpg
      - /uploads/lightbox-smiles-06.jpg
      - /uploads/lightbox-smiles-07.jpg
      - /uploads/lightbox-smiles-08.jpg
      - /uploads/lightbox-smiles-09.jpg
      - /uploads/lightbox-smiles-10.jpg
      - /uploads/lightbox-smiles-11.jpg
      - /uploads/lightbox-smiles-12.jpg
      - /uploads/lightbox-smiles-13.jpg
      - /uploads/lightbox-smiles-14.jpg
      - /uploads/lightbox-smiles-15.jpg
      - /uploads/lightbox-smiles-16.jpg
      - /uploads/lightbox-smiles-17.jpg
      - /uploads/lightbox-smiles-18.jpg
      - /uploads/lightbox-smiles-19.jpg
      - /uploads/lightbox-smiles-20.jpg
      - /uploads/lightbox-smiles-21.jpg
      - /uploads/lightbox-smiles-22.jpg
      - /uploads/lightbox-smiles-23.jpg
      - /uploads/lightbox-smiles-24.jpg
  items:
    - link:
        display: true
        to: /la-clinica/por-que-elegirnos/
      image: /img/gallery-why.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Por Qué Elegirnos</h3>
        <p class="dv-text-feat"> Nuestra trayectoria es su mejor garantía</p> 
        <p class="dv-text-feat-100">
          Conozca las 10 razones que nos distinguen de la competencia y
          conforman nuestra propuesta de valor.
        </p>
    - link:
        display: true
        to: /la-clinica/instalaciones/
      image: /img/gallery-facilities.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>La Clínica en Imágenes</h3>
        <p class="dv-text-feat">
          Un ambiente relajado, tranquilo y de máximo confort...
        </p>
    - link:
        display: true
        to: /la-clinica/tecnologia/
      image: /img/gallery-technology.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Dotación y Tecnología</h3>
        <p class="dv-text-feat">
          ¡A la vanguardia en equipos y procesos digitales!
        </p>
    - link:
        display: true
        to: /profesionales/
      image: /img/gallery-professionals.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Staff profesional</h3>
        <p class="dv-text-feat">¡Su boca en manos de expertos!</p>
    - link:
        display: false
        to: /
      image: /img/gallery-smiles.png
      action: true
      placeholder: >
        <span> Ver galeria </span>
      body: >
        <h3>Galería de Sonrisas</h3>
         <p class="d-none d-lg-block dv-text-feat">
          ¡Pasión por la belleza... Devoción por la naturalidad!
        </p> <p class="dv-text-feat-100">
          Una pequeña muestra de lo que podemos hacer por Usted: Odontología
          moderna, integral y especializada.
        </p>
    - link:
        display: true
        to: /pacientes-del-exterior/
      image: /img/gallery-foreigns.png
      action: false
      placeholder: >
        <span> Ver más </span>
      body: >
        <h3>Pacientes del exterior</h3>
        <p class="dv-text-feat">Un protocolo especial de atención</p> 
        <p class="dv-text-feat-100">
          Somos consecuentes con todos los pacientes que nos visitan desde
          cualquier parte de Venezuela y el mundo.
        </p>
---