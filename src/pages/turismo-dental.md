---
templateKey: dental-tourism
language: es
redirects: /en/dental-tourism/
title: Turismo dental
description: seo description
keywords:
  - default keyowrd
published: true
tags:
  - default tag
# Hero Section
hero:
  background:
    scaleOnReveal: false
    img: /uploads/hero-dental-tourism.png
    isParallax: false
  anim:
    display: false
    type: bottom
  height: half
  indicator: false
  portraitPosition: 83%
  content:
    position: center
    body: >
      <h1>Ahorre Hasta un 70% en<br>Tratamientos Dentales
        de Alta Gama</h1>
prices:
  footer:
    image: /uploads/icons-OANDA.png
    title:
      <p>Precios expresados en dólares Americanos <em>(USD)</em>.<br>Conviértalos
      de inmediato a su moneda con:<p>
    to: https://www1.oanda.com/lang/es/currency/converter/
  rows:
    - title: Implantes Dentales
      icon: icon-dental-implants
      rows:
        - procedure: >
            <p>Tomografía Digital 3D</p>
          price: $ 60
          currency: USD
        - procedure: >
            <p> Sedación Consciente <em>(opcional)</em></p>
          price: $ 500
          currency: USD
        - procedure: >
            <p> Implante Dental Unitario</p>
          price: $ 700
          currency: USD
        - procedure: >
            <p> Implantes Dentales Múltiples</p>
          price: $ 650
          currency: USD c/u
        - procedure: >
            <p> Sinus Lift</p>
          price: $ 850
          currency: USD
        - procedure: >
            <p> Injerto de Hueso Artificial</p>
          price: $ 200
          currency: USD por cc
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/implantes-dentales/
    - title: Implantes Cigomáticos
      icon: icon-zygomatic-implants
      rows:
        - procedure: >
            <p> Anestesia General</p>
          price: $ 650
          currency: USD
        - procedure: >
            <p> Gastos de Quirófano</p>
          price: $ 800
          currency: USD
        - procedure: >
            <p> Implante Cigomático Unilateral</p>
          price: $ 2,800
          currency: USD
        - procedure: >
            <p> Implantes Cigomáticos  Bilaterales</p>
          price: $ 2,500
          currency: USD c/u
        - procedure: >
            <p> Quad Zygoma</p>
          price: $ 9,800
          currency: USD
        - procedure: >
            <p> Dentadura Provisional</p>
          price: $ 580
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/implantes-cigomaticos/
    - title: Prótesis sobre Implantes
      icon: ICON-IMPLANT SUP
      rows:
        - procedure: >
            <p> Corona Metal-Porcelana</p>
          price: $ 480
          currency: USD
        - procedure: >
            <p> Corona de Zirconio CAD/CAM</p>
          price: $ 570
          currency: USD
        - procedure: >
            <p> Puente Fijo sobre Implantes</p>
          price: $ 480
          currency: USD por unidad
        - procedure: >
            <p> Sobredentadura Removible</p>
          price: $ 1,350
          currency: USD
        - procedure: >
            <p> All on Four</p>
          price: $ 2,600
          currency: USD
        - procedure: >
            <p> Prótesis Híbrida</p>
          price: $ 2,600
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/implantes-dentales/protesis-sobre-implantes/
    - title: Estética Dental
      icon: icon-aestetic-denti
      rows:
        - procedure: >
            <p> Diseño Digital de Sonrisa</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p> Gingivoplastia</p>
          price: $ 120
          currency: USD por cuadrante
        - procedure: >
            <p> Recontorneado Dental</p>
          price: $ 50
          currency: USD por arcada
        - procedure: >
            <p> Blanqueamiento Dental LED</p>
          price: $ 180
          currency: USD
        - procedure: >
            <p> Carilla de Resina Compuesta</p>
          price: $ 100
          currency: USD
        - procedure: >
            <p> Carilla de Porcelana IPS e.max</p>
          price: $ 490
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/estetica-dental/
    - title: Endodoncia
      icon: icon-endodontics
      rows:
        - procedure: >
            <p> Tratamiento de Conducto</p>
          price: $ 190
          currency: USD
        - procedure: >
            <p> Retratamiento Endodóntico</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p> Desobturación para Perno</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p> Perno-Muñón Artificial</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p> Apicectomía o Resección  Radicular</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p> Obturación a Retro con MTA</p>
          price: $ 100
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/endodoncia/
    - title: Cirugía Bucal
      icon: icon-oral-sur
      rows:
        - procedure: >
            <p> Extracción Simple</p>
          price: $ 45
          currency: USD
        - procedure: >
            <p> Extracción Quirúrgica</p>
          price: $ 80
          currency: USD
        - procedure: >
            <p> Cirugía de Cordales</p>
          price: $ 150
          currency: USD
        - procedure: >
            <p> Frenectomía Labial o Lingual</p>
          price: $ 100
          currency: USD
        - procedure: >
            <p> Alveoloplastia</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p> Remoción y Biopsia de Lesiones Orales</p>
          price: $ 180
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/cirugia-bucal/
    - title: Prótesis Convencional
      icon: icon-prosthodontics
      rows:
        - procedure: >
            <p> Coronas y Puentes Metal-Porcelana</p>
          price: $ 430
          currency: USD por unidad
        - procedure: >
            <p> Coronas y Puentes IPS e.max</p>
          price: $ 480
          currency: USD por unidad
        - procedure: >
            <p> Coronas y Puentes de Zirconio CAD/CAM</p>
          price: $ 550
          currency: USD por unidad
        - procedure: >
            <p> Dentadura Parcial Removible</p>
          price: $ 450
          currency: USD
        - procedure: >
            <p> Dentadura Total</p>
          price: $ 550
          currency: USD
        - procedure: >
            <p> Férula Oclusal</p>
          price: $ 180
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/protesis/
    - title: Odontología General
      icon: icon-general-denti
      rows:
        - procedure: >
            <p> Limpieza Dental</p>
          price: $ 40
          currency: USD
        - procedure: >
            <p> Limpieza Profunda y Profilaxis</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p> Resina Fotocurada de 1 Superficie</p>
          price: $ 50
          currency: USD
        - procedure: >
            <p> Resina Fotocurada de 2 Superficies</p>
          price: $ 70
          currency: USD
        - procedure: >
            <p> Resina Fotocurada de 3 Superficies</p>
          price: $ 90
          currency: USD
        - procedure: >
            <p> Incrustaciones Cerámicas</p>
          price: $ 420
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/odontologia-general/
    - title: Periodoncia
      icon: icon-periodontics
      rows:
        - procedure: >
            <p> Raspado y Alisado Radicular</p>
          price: $ 120
          currency: USD por cuadrante
        - procedure: >
            <p> Curetaje Abierto</p>
          price: $ 180
          currency: USD por cuadrante
        - procedure: >
            <p> Cirugía Mucogingival</p>
          price: $ 250
          currency: USD
        - procedure: >
            <p> Injerto de Encía</p>
          price: $ 180
          currency: USD por diente
        - procedure: >
            <p> Gingivectomía</p>
          price: $ 120
          currency: USD por cuadrante
        - procedure: >
            <p> Regeneración Tisular Guiada</p>
          price: $ 350
          currency: USD por defecto
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/periodoncia/
    - title: Ortodoncia
      icon: icon-orthodontics
      rows:
        - procedure: >
            <p> Brackets Metálicos</p>
          price: $ 500
          currency: USD (Superiores e Inferiores)
        - procedure: >
            <p> Brackets Cerámicos</p>
          price: $ 1,000
          currency: USD (Superiores e Inferiores)
        - procedure: >
            <p> Brackets de Zafiro</p>
          price: $ 1,500
          currency: USD (Superiores e Inferiores)
        - procedure: >
            <p> Retenedor Fijo</p>
          price: $ 180
          currency: USD
        - procedure: >
            <p> Retenedor de Hawley o Essix</p>
          price: $ 150
          currency: USD
        - procedure: >
            <p> Aparatos de Ortopedia Dentofacial</p>
          price: $ 1,800
          currency: USD
      link:
        image: /uploads/icon-oral-surgery.jpg
        title: MÁS INFORMACIÓN
        to: /especialidades/ortodoncia/
routes:
  title: <h1> Rutas Más Frecuentes</h1>
  image: travel
  icons:
    clock: clock
    currency: currency
  footer: >-
    Al planificar su viaje, necesitaremos verificar la disponibilidad de vuelos
    sin escalas y las tarifas vigentes.
  departures:
    - from: "Desde: BOGOTÁ"
      flag: co
      time: "1h : 46m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: BRIDGETOWN"
      flag: bb
      time: "1h : 33m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: GEORGETOWN"
      flag: gy
      time: "1h : 48m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: KINGSTOWN"
      flag: vc
      time: "1h : 21m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: MADRID"
      flag: es
      time: "9h : 12m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: MIAMI"
      flag: us
      time: "3h : 14m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: PANAMÁ"
      flag: pa
      time: "2h : 17m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: PUERTO ESPAÑA"
      flag: tt
      time: "1h : 14m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: TORONTO"
      flag: ca
      time: "5h : 18m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa
    - from: "Desde: WILLEMSTAD"
      flag: cw
      time: "0h : 51m"
      cost: |
        875.00 USD
      visa: No se Requiere Visa

blocksDescription:
  sections:
    left:
      content: >
        <div class="title big">
        <h1>Turismo Dental en Venezuela</h1>
        </div>
        <hr />
        <div class="content">
          <h2>¡Una Atractiva Realidad!</h2>
          <p>
            Coma, sonría y disfrute como antes. No comprometa más su calidad
            de vida y aproveche los grandes beneficios que ofrece la
            globalización en el sector de la salud. Tome en serio nuestra
            propuesta de servicio y supere de una vez por todas cualquier
            barrera que le impida lucir unos dientes blancos, sanos y
            hermosos.
          </p>
          
        </div>
      image: /uploads/aside-dental-tourism-map.png
    right:
      - content: >
          <span class="icon">
            <i class="icon-accesibility"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Accesibilidad</h2>
          </div>
          <div class="content">
            <p>
              Gran posibilidad de materializar tratamientos dentales extensos, estéticos o
              de alta complejidad; a menudo inalcanzables para la gran mayoría de las
              personas en muchos países de Europa, Norte, Centro y Sudamérica e Islas del
              Caribe.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-inmediacy"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Inmediatez</h2>
          </div>
          <div class="content">
            <p>
              Su cita al instante, sin demoras ni listas de espera. Atención exclusiva,
              intensiva y multidisciplinaria para cubrir en tiempo récord todos los
              requisitos orales previamente establecidos en las fases de diagnóstico y
              planificación terapéutica.
            </p>
          </div>
      - content: >
          <span class="icon">
            <i class="icon-quality"></i>
          </span>
          <hr />
          <div class="title">
            <h2>Máxima Calidad</h2>
          </div>
          <div class="content">
            <p>
              A su disposición la experiencia del mejor equipo local de Odontólogos
              Especialistas, las más modernas y cómodas instalaciones y la última
              tecnología a nivel mundial. Un servicio ampliamente reconocido y en
              concordancia con los más altos estándares internacionales en materia de
              salud oral.
            </p>
          </div>

gallerySteps:
  title: >
    <h1> Hacemos la Odontología Simple y Asequible </h1>
  steps:
    - title: >
        <h3> Contacto y Envío de Estudios Dentales Básicos</h3>
      image: /uploads/steps-1-dental-tourism.png
    - title: >
        <h3> Plan de Tratamiento y Presupuesto</h3>
      image: /uploads/steps-2-dental-tourism.png
    - title: >
        <h3> Viaje, Recepción y Alojamiento</h3>
      image: /uploads/steps-3-dental-tourism.png
    - title: >
        <h3>  Estudios Diagnósticos Complementarios</h3>
      image: /uploads/steps-4-dental-tourism.png
    - title: >
        <h3> Tratamiento Dental Intensivo</h3>
      image: /uploads/steps-5-dental-tourism.png
    - title: >
        <h3> Nueva Sonrisa y Regreso a Casa</h3>
      image: /uploads/steps-6-dental-tourism.png

# Heading Section
heading:
  display: true
  content:
    <h1 class="title">¡Visítenos en Caracas y Presuma de una Sonrisa VIP!</h1>
    <p>Ya son miles los pacientes que han
    encontrado en Venezuela la mejor solución para sus necesidades de salud oral,
    gracias a nuestra razonable estructura de costos y fácil acceso a servicios dentales
    de primer nivel.</p>

parallax:
  display: true
  portraitPosition: center
  img: /uploads/parallax-dental-tourism.png
  content: >
    <h1 class="bold">¡Dental Extreme Makeover en Solo 1 o 2 Semanas!</h1>
    <h2  class="bold"> Un tratamiento verdaderamente intensivo y multidisciplinario.</h2>

form:
  title: >
    <h1>¡Consúltenos Ahora Mismo!</h1>
  background: /uploads/parallax-form-annexed-pages.jpg

# anex-links
anexes:
  display: true
  items:
    - img: /uploads/sections-dental-tourism-calculator.png
      content: >
        <h1>¿Cuenta ya con un presupuesto definitivo?</h1>
        <p>¿Ha visitado a varios Odontólogos y aún no logra encontrar una opción de tratamiento que verdaderamente esté dentro de sus posibilidades? ¿Quiere sacar más provecho a su dinero?</p>
        <p >Envíenos su presupuesto para mejorarlo y someterlo a su consideración. ¡Con seguridad le sorprenderemos!</p><br>
      footer:
        icon:
          display: false
          img: /uploads/sections-icons-aesthetic-dentistry.jpg
        link:
          display: true
          to: mailto:info@dentalvipcaracas.com?Subject=Tengo%20ya%20un%20presupuesto
          placeholder: <span> CARGAR ARCHIVO</span>
    - img: /uploads/sections-dental-tourism-hotel.png
      content: >
        <h1>Hoteles seguros y confortables</h1>
        <p> 
            Nuestra privilegiada ubicación nos permite contar con dos excelentes infraestructuras hoteleras situadas a menos de 50 metros de la clínica. CHACAO SUITES y SHELTER SUITES ofrecen lindas y confortables habitaciones, restaurantes, seguridad privada y demás servicios que facilitarán y harán agradable su breve estadía en la ciudad de Caracas. </p>
        <p class="dv-srv-pl text-left dv-npr">COSTO ESTIMADO POR DÍA: 50.00 USD <em>(comidas no incluidas)</em>.</p> <br>
        <div class="link-box"><a href="http://www.hotelchacaosuites.com/index.php?idioma=en" class="link">CHACAO SUITES</a> &nbsp;&nbsp;&nbsp; <a class="link">SHELTER SUITES</a></div>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: /especialidades/estetica-dental/diseno-digital-de-sonrisa/
          placeholder: <span>Más Información</span>
    - img: /uploads/sections-dental-tourism-24.png
      content: >
        <h1>Asistencia de viaje y soporte permanente</h1>
        <p class="dv-srv-pl text-right dv-npr">Una atención personalizada las 24 horas del día y el acompañamiento permanente de un guía bilingüe, si así lo desea, son parte integral de nuestra concepción de servicio, de nuestro gran esfuerzo por hacer que su experiencia sea lo más cómoda, fructífera y placentera posible.</p>
        <p class="dv-srv-pl text-right dv-npr">¡Intentaremos hacerle sentir como en casa!</p>
      footer:
        icon:
          display: false
          img: /uploads/sections-facilities.jpg
        link:
          display: false
          to: /especialidades/estetica-dental/carillas-de-porcelana/
          placeholder: <span>Más Información</span>
# Procedures Section
procedures:
  title: >
    <h1 class="title">¡Estamos a Solo Un Paso de Distancia!</h1>
  procedures:
    - title: >
        <h5>Instalaciones</h5>
      to: /la-clinica/instalaciones/
      img: /uploads/procedures-facilities.jpg
    - title: >
        <h5>Tecnología</h5>
      to: /la-clinica/tecnologia/
      img: /uploads/procedures-technology.jpg
    - title: >
        <h5>Profesionales</h5>
      to: /profesionales/
      img: /uploads/procedures-professionals.png
---
